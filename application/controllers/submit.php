<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		// if($this->session->userdata("oq_utype") != 2)
 		// 	redirect(base_url()."index.php/home/logout");
	}

	public function generate_result($qzid, $uid)
	{
		// if($uid != $this->session->userdata("oq_uid"))
		// 	redirect(base_url()."index.php/home/logout");
		// $html["quiz_d"] = $this->sp_model->read("get_quiz_data", array($qzid, 0), 0);

		if($this->session->userdata("oq_utype") != 2)
			$html["student"] = $this->db->query("SELECT *
											FROM `oq_user`
											WHERE `oq_userid` = ".$uid)->result_array();
		$html["quiz_d"] = $this->db->query("SELECT *
											FROM `qz_quiz`
											WHERE `qz_qid` = ".$qzid)->result_array();
		if( count($html["quiz_d"]) == 0)
			redirect(base_url()."index.php/home/logout");
		// $data = $this->sp_model->read("cheat_sheet", array($uid, $qzid), 0);

		$data = $this->db->query("SELECT qs_question.qz_qid AS 'QUIZ NUMBER', 
								as_asheet.oq_userid AS 'USER ID', 
								as_asheet.qs_qsid AS 'QUESTION ID', 
								qs_question.qs_question AS 'QUESTION',
								as_asheet.as_answer AS 'ANSWER',
								qs_question.qs_ans AS 'CORRECT ANSWER', 
								qs_question.qs_choice1 AS 'A', 
								qs_question.qs_choice2 AS 'B',
								qs_question.qs_choice3 AS 'C',
								qs_question.qs_choice4 AS 'D',
								as_asheet.as_status AS 'STATUS',
								as_asheet.as_date AS 'DATE',  
								sc_category.sc_sname AS 'CATEGORY',
								as_asheet.qz_qid
								FROM qs_question 
								INNER JOIN sc_category
								ON sc_category.sc_sid = qs_question.sc_sid
								LEFT JOIN as_asheet ON qs_question.qs_qsid = as_asheet.qs_qsid
								AND as_asheet.oq_userid = ".$uid."
								HAVING qs_question.qz_qid = ".$qzid."
								ORDER BY qs_question.qs_qsid ASC")->result_array();
		$html["html"] = "<table class='table table-striped' width='100%' align='center' >
		<tr>
		<th>Question</th>
		<th>Your Answer</th>
		<th>Correct Answer</th>
		<th>Category</th>
		</tr>";
		$cat = array(); 
		$score = 0;
		$over = 0;
		foreach ($data as $key => $value) 
		{
			$ans = ($value["ANSWER"] == null) ? 'No answer' : $value["ANSWER"];

			$exist = false;
			foreach ($cat as $keyx => $valuex)
				if ( $valuex["Category"][0] == $value["CATEGORY"] )
					$exist = true;

			if (! $exist)
				$cat[] =  array("Category" => array($value["CATEGORY"], 0, 0));

			foreach ($cat as $keyx => $valuex)
				if ( $valuex["Category"][0] == $value["CATEGORY"] )
				{
					$over++;
					if( $ans == $value["CORRECT ANSWER"] )
					{
						$score++;
						$cat[$keyx]["Category"][1]++;
					}
					else
					{
						$cat[$keyx]["Category"][2]++;
					}
				}

			$ans2 = "";
			$ans3 = "";

			switch ($ans) {
				case 'A':
					$ans2 = " - " . strip_tags($value["A"]);
					break;

				case 'B':
					$ans2 = " - " . strip_tags($value["B"]);
					break;

				case 'C':
					$ans2 = " - " . strip_tags($value["C"]);
					break;

				case 'D':
					$ans2 = " - " . strip_tags($value["D"]);
					break;
				
				default:
					$ans2 = "";
					break;
			}

			switch ($value["CORRECT ANSWER"]) {
				case 'A':
					$ans3 = " - " . strip_tags($value["A"]);
					break;

				case 'B':
					$ans3 = " - " . strip_tags($value["B"]);
					break;

				case 'C':
					$ans3 = " - " . strip_tags($value["C"]);
					break;

				case 'D':
					$ans3 = " - " . strip_tags($value["D"]);
					break;
				
				default:
					$ans3 = "";
					break;
			}
					

			$html["html"] .= "<tr>
					<td>" . strip_tags($value["QUESTION"]) . "</td>
					<td>" . $ans . $ans2 . "</td>
					<td>" . $value["CORRECT ANSWER"] . $ans3 . "</td>
					<td>" . $value["CATEGORY"] . "</td>
					</tr>";
		}
		$html["html"] .= "</table>";
		$highest = 0;
		$lowest = 0;
		$categh = "";
		$categl = "";
		foreach ($cat as $key => $value) 
		{
			if ( $highest < $value["Category"][1] )
			{
				$highest = $value["Category"][1];
				$categh = $value["Category"][0];
			}
			if ( $lowest < $value["Category"][2] )
			{
				$lowest = $value["Category"][2];
				$categl = $value["Category"][0];
			}
		}
		$msg = "Your Overall Score: " . $score . "/" . $over . "<br />";
		if($highest > 0)
			$msg .= "You got highest score in " . $categh . " with a score of " . $highest . "<br />";
		if($lowest > 0)
			$msg .= "You got lowest score in " . $categl . " with " . $lowest . " errors.<br />";
		$html["msg"] = $msg;

		$perc = intval( ($score / $over) * 100 );

		$rs_desc = "Score: " . $score . "/" . $over . " Percentage: " . $perc . " Passing Rate: " . $html["quiz_d"][0]["oq_qrate"];

		$html["result"] = "Percentage: " . $perc . "% Passing Rate: " . $html["quiz_d"][0]["oq_qrate"] . "%";

		if($this->input->post("qzid"))
			$this->save_result($rs_desc);
		else
			$this->load->view('result/index', $html);
	}

	public function update_link()
	{
		// $this->sp_model->save("update_link", array($this->input->post("linkid")), 0);
		$this->db->query("UPDATE `link`
						SET
						`status` = '1'
						WHERE `link_id` = ".$this->input->post("linkid"));

		$this->generate_result($this->input->post("qzid"), $this->session->userdata("oq_uid"));
	}

	private function save_result($data)
	{
		// no sp
		$this->db->query("INSERT INTO `rs_results`() VALUES (null, '".$this->session->userdata('oq_uid')."', '".$this->input->post('qzid')."', NOW(), '".$data."')");
	}

}