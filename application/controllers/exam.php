<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_utype") != 2 || $this->session->userdata("oq_exam") == "0")
 			redirect(base_url()."index.php/home/logout");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
	}
 
	public function quiz($qzid, $uid, $qno = 0)
	{
		if($uid != $this->session->userdata("oq_uid"))
			redirect(base_url()."index.php/home/logout");
		// $data =	$this->sp_model->read("check_quiz_link", array($qzid, $uid), 0);
		$data =	$this->db->query("SELECT link_id, COUNT(*) as 'COUNT' 
								FROM link
								WHERE qz_qid = ".$qzid." AND stud_id = ".$uid)->result_array();
		if ( $data[0]["COUNT"] == "1" )
		{
			$this->preparequiz($qzid, $uid, $qno, $data[0]["link_id"]);
		}
		else
		{
			redirect(base_url()."index.php/home/logout");
		}
	}

	private function preparequiz($qzid, $uid, $qno = 0, $link)
	{
		$data["linkid"] = $link;
		// $data["result"] = $this->sp_model->read("get_result", array($uid, $qzid), 0);
		$data["result"] = $this->db->query("SELECT *
											FROM `rs_results`
											WHERE `oq_userid` = ".$uid."
											AND `qz_qid` = ".$qzid)->result_array();
		if(count($data["result"]) == "0")
		{	
			// $data["time"] = $this->sp_model->read("get_time", array($uid, $qzid), 0);
			$data["time"] = $this->db->query("SELECT `tm_tid`, `tm_time` AS 'time'
											FROM `tm_time`
											WHERE `oq_userid` = ".$uid."
											AND `qz_qid` = ".$qzid)->result_array();
			// var_dump($data["time"]);
			if(count($data["time"]) == "0")
			{
				// $data["time"] = $this->sp_model->read("get_time", array(0, $qzid), 0);
				$data["time"] = $this->db->query("SELECT `qz_qtimelimit` AS 'time'
													FROM `qz_quiz`
													WHERE `qz_qid` = ".$qzid)->result_array();
				// $this->sp_model->save("save_time", array($data["time"][0]["time"], $uid, $qzid, 0), 0);
				$this->db->query("INSERT INTO `tm_time`
								VALUES
								(
								null,
								".$data["time"][0]["time"].",
								".$uid.",
								".$qzid."
								)");
				// $data["time"] = $this->sp_model->read("get_time", array($uid, $qzid), 0);
				$data["time"] = $this->db->query("SELECT `tm_tid`, `tm_time` AS 'time'
											FROM `tm_time`
											WHERE `tm_tid` = ".$this->db->insert_id())->result_array();

											// WHERE `oq_userid` = ".$uid."
											// AND `qz_qid` = ".$qzid)->result_array();
			}
			// $data["questions"] = $this->sp_model->read("get_question_data", array($qzid, 0), 0);
			$data["questions"] = $this->db->query("SELECT * FROM `qs_question`
													WHERE `qz_qid` = ".$qzid)->result_array();
			// $data["quiz_d"] = $this->sp_model->read("get_quiz_data", array($qzid, 0), 0);
			$data["quiz_d"] = $this->db->query("SELECT *
												FROM `qz_quiz`
												WHERE `qz_qid` = ".$qzid)->result_array();
			$data["qnumbers"] = array();
			foreach ($data["questions"] as $key => $value) 
				$data["qnumbers"][] = $value["qs_qsid"];

			if($qno != 0)
			{
				if (in_array($qno, $this->session->userdata("qnumbers")))
				{
					$data["qs_no"] = $qno;
				}
				else
				{
					redirect(base_url()."index.php/home/logout");			
				}
			}

			$this->load->view("exam/index", $data);
		}
		else
		{

		}
		
	}

	public function getquestion()
	{
		// $data = $this->sp_model->read("get_question_data", array($this->input->post("qzid"), 0), 0);
		$data = $this->db->query("SELECT * FROM `qs_question`
									WHERE `qz_qid` = ".$this->input->post("qzid"))->result_array();
		foreach ($data as $key => $value) 
		{
			if ( $this->input->post("qid") == $value["qs_qsid"])
			{
				$data = $this->getanswer($value["qs_qsid"]);
				$a = "";$b = "";$c = "";$d = "";$asheetid = "";
				if(count($data)>0)
				{
					if($data[0]["as_answer"] == "A")
						$a = "checked";
					elseif($data[0]["as_answer"] == "B")
						$b = "checked";
					elseif($data[0]["as_answer"] == "C")
						$c = "checked";
					elseif($data[0]["as_answer"] == "D")
						$d = "checked";

					$asheetid = $data[0]["as_aid"];
				}
				echo "<table align='center' width='100%'>
				<tr>
				<td align='left' width='20%'><p class='lead'>Question:</p></td><td colspan='2' width='80%'>" . $value["qs_question"] .
				"</td>
				</tr>
				<tr><td colspan='3'><hr /></td></tr>
				<tr>
				<td align='left' rowspan='4'><p class='lead'>Select from choices:</p></td><td width='10%'><input " . $a . " class='x" . $value["qs_qsid"] . "' type='radio' name='answer' value='A'></td><td align='left'>" . $value["qs_choice1"] . "</td>
				</tr>
				<tr>
				<td><input " . $b . " class='x" . $value["qs_qsid"] . "' type='radio' name='answer' value='B'></td><td align='left'>" . $value["qs_choice2"] . "</td>
				</tr>
				<tr>
				<td><input " . $c . " class='x" . $value["qs_qsid"] . "' type='radio' name='answer' value='C'></td><td align='left'>" . $value["qs_choice3"] . "</td>
				</tr>				
				<tr>
				<td><input " . $d . " class='x" . $value["qs_qsid"] . "' type='radio' name='answer' value='D'></td><td align='left'>" . $value["qs_choice4"] . "</td>
				</tr>
				</table>
				<script>
					$(function(){
						$('.x".$value["qs_qsid"]."').click(function(){
							$.ajax({
		                      type: 'POST',
		                      async: false,
		                      url: '".base_url()."index.php/exam/save_answer',
		                      data: {
		                        qz_qid  : '".$value["qz_qid"]."',
		                        qs_qsid	: '".$value["qs_qsid"]."',
		                        answer  : $(this).val(),
		                        as_id   : '".$asheetid."'
		                      }
		                    });
						});
					});
				</script>";
				return;
			}
		}
	}

	private function getanswer($qsid)
	{
		// return $this->sp_model->read("get_answer", array($qsid, $this->session->userdata("oq_uid")), 0);
		return $this->db->query("SELECT * FROM as_asheet WHERE `qs_qsid` = ".$qsid." AND `oq_userid` = ".$this->session->userdata("oq_uid"))->result_array();
	}

	public function save_answer()
	{
		// $this->sp_model->save("save_answer", array($this->input->post("qs_qsid"), $this->input->post("answer"), $this->session->userdata("oq_uid"),$this->input->post("qz_qid"), $this->input->post("as_id")), 0);
		// if ( count($this->db->query("SELECT * FROM `as_asheet` WHERE `as_aid` = ".$this->input->post("as_id"))->result_array()) > 0 )
		
		$data = $this->db->query("SELECT *  
						FROM `as_asheet`
						WHERE `qs_qsid` = " . $this->input->post("qs_qsid") .
						" AND `qz_qid` = " . $this->input->post("qz_qid") . 
						" AND `oq_userid` = " .$this->session->userdata("oq_uid"))->result_array();

		// if( $this->input->post("as_id") )
		if ( count($data) > 0 )
		{
			$this->db->query("UPDATE `as_asheet`
							SET
							`as_answer` = '".$this->input->post("answer")."',
							`as_date` = NOW()
							WHERE `as_aid` = ".$data[0]["as_aid"]);
							// WHERE `as_aid` = ".$this->input->post("as_id"));
		}
		else
		{
			$this->db->query("INSERT INTO `as_asheet`
								VALUES
								(
								null,
								".$this->input->post("qs_qsid").",
								'".$this->input->post("answer")."',
								'',
								".$this->session->userdata("oq_uid").",
								NOW(),
								".$this->input->post("qz_qid")."
								)");
		}
	}

	public function save_time()
	{
		// $this->sp_model->save("save_time", array($this->input->post("time"), 0, 0, $this->input->post("tid")), 0);
		$this->db->query("UPDATE `tm_time`
							SET `tm_time` = ".$this->input->post("time")."
							WHERE `tm_tid` = ".$this->input->post("tid"));
	}

}

/* End of file exam.php */
/* Location: ./application/controllers/exam.php */