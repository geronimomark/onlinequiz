<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 	}

 	public function edit_profile()
 	{
 		if($this->session->userdata("oq_profile") == "0")
 		{
 			$data["new"] = TRUE;
 		}
 		else
 		{
 			$data["new"] = FALSE;
 			$data["result"] = $this->db->query("SELECT * FROM `profile` WHERE user_id = " . $this->session->userdata("oq_uid"))->result_array();
 			$data["result"] = $data["result"][0];
 		}
 		$this->load->view("profile/edit_profile", $data);
 	}

 	public function save_profile()
 	{
 		if($this->session->userdata("oq_profile") == "0")
 		{
 			$result = $this->db->query("INSERT INTO `profile`
					()
					VALUES
					(
					null,
					" . $this->session->userdata("oq_uid") . ",
					'" . strtoupper($this->input->post("fname")) . "',
					'" . strtoupper($this->input->post("mname")) . "',
					'" . strtoupper($this->input->post("lname")) . "',
					'" . $this->input->post("bday") . "',
					'" . $this->input->post("gender") . "',
					'" . strtoupper($this->input->post("school")) . "',
					'" . strtoupper($this->input->post("course")) . "',
					'" . strtoupper($this->input->post("ylevel")) . "',
					'" . strtoupper($this->input->post("subject")) . "',
					'" . strtoupper($this->input->post("algebra")) . "',
					'" . strtoupper($this->input->post("trigo")) . "',
					'" . strtoupper($this->input->post("geometry")) . "',
					'" . strtoupper($this->input->post("diff")) . "',
					'" . strtoupper($this->input->post("integral")) . "',
					'" . strtoupper($this->input->post("advmath")) . "',
					'" . strtoupper($this->input->post("diffeq")) . "',
					'" . $this->input->post("q1") . "',
					'" . $this->input->post("q2") . "',
					'" . $this->input->post("q3") . "',
					'" . $this->input->post("q4") . "',
					NOW()
					)");
 		}
 		else
 		{
 			$result = $this->db->query("UPDATE `profile`
										SET `fname` = '" . strtoupper($this->input->post("fname")) . "',
										`mname` = '" . strtoupper($this->input->post("mname")) . "',
										`lname` = '" . strtoupper($this->input->post("lname")) . "',
										`bday` = '" . $this->input->post("bday") . "',
										`gender` = '" . $this->input->post("gender") . "',
										`school` = '" . strtoupper($this->input->post("school")) . "',
										`course` = '" . strtoupper($this->input->post("course")) . "',
										`yearlevel` = '" . strtoupper($this->input->post("ylevel")) . "',
										`subject` = '" . strtoupper($this->input->post("subject")) . "',
										`algebra` = '" . strtoupper($this->input->post("algebra")) . "',
										`trigo` = '" . strtoupper($this->input->post("trigo")) . "',
										`geometry` = '" . strtoupper($this->input->post("geometry")) . "',
										`diff` = '" . strtoupper($this->input->post("diff")) . "',
										`integral` = '" . strtoupper($this->input->post("integral")) . "',
										`advmath` = '" . strtoupper($this->input->post("advmath")) . "',
										`diffeq` = '" . strtoupper($this->input->post("diffeq")) . "',
										`q1` = '" . $this->input->post("q1") . "',
										`q2` = '" . $this->input->post("q2") . "',
										`q3` = '" . $this->input->post("q3") . "',
										`q4` = '" . $this->input->post("q4") . "',
										`update_date` = NOW()
										WHERE user_id = " . $this->session->userdata("oq_uid"));
 		}
 		if($result)
 		{
 			$this->db->query("UPDATE `oq_user` SET oq_profile = '1' WHERE `oq_userid` = " . $this->session->userdata("oq_uid"));
 			$this->session->set_userdata("oq_profile", "1");
 			echo "success";
 		}
 		else
 		{
 			echo 0;
 		}
 			
 	}

 	public function logout()
	{
		// $this->sp_model->save("log_data", array($this->session->userdata("oq_uid"), "Logout"), 0);
		$this->db->query("INSERT INTO `lg_logs`
								(`oq_userid`,
								`lg_date`,
								`lg_action`)
								VALUES
								(" . $this->session->userdata("oq_uid") . ",
								NOW(),
								'Logout'
								)");
		$this->session->sess_destroy();
		redirect(base_url() .'index.php/');
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */