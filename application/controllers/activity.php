<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_activity") == "0")
 			redirect(base_url()."index.php/home/logout");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
	}
 
	public function game($page = "game")
	{
		$this->load->view('activity/'.$page);
	}

	public function save_score()
	{
		if($this->input->post())
		{
			$query = $this->db->query("SELECT * FROM `scoreboard`
									WHERE `user_id` = ".$this->session->userdata("oq_uid")." 
									AND category = ".$this->input->post("cat_id"))->result_array();
			$result = FALSE;
			if(isset($query[0]["score"]) && $query[0]["score"] < $this->input->post("score"))
			{
				$result = $this->db->query("UPDATE `scoreboard`
										SET `score` = ".$this->input->post("score").",
										`dateof` = NOW()");
			}
			elseif(count($query) == "0")
			{
				$result = $this->db->query("INSERT INTO `scoreboard` ()
										VALUES(
											null,
											".$this->session->userdata("oq_uid").",
											".$this->input->post("cat_id").",
											'".$this->input->post("cat_name")."',
											".$this->input->post("score").",
											NOW()
										)");
			}
			if($result)
			{
				echo "success";
			}
			else
			{
				echo "failed";
			}
		}
	}

	public function scores()
	{
		if($this->session->userdata("oq_utype") != 1) {
			$where = "WHERE a.user_id = ".$this->session->userdata("oq_uid");
		} else {
			$where = "";
		}
		$data["data"] = $this->db->query("SELECT 
										a.category_name, 
										a.score, 
										a.dateof, 
										CONCAT(b.fname, ' ', b.lname) as `name`,
										b.course,
										b.school 
										FROM `scoreboard` a
										INNER JOIN `profile` b
										ON a.`user_id` = b.`user_id` "
										.$where.
										" ORDER BY a.category, a.score")->result_array();
		$this->load->view("activity/scores", $data);
	}
}

/* End of file activity.php */
/* Location: ./application/controllers/activity.php */