<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
 

 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
 	}

	public function index()
	{
		$this->load->view('home/index');
	}

	public function logout()
	{
		// $this->sp_model->save("log_data", array($this->session->userdata("oq_uid"), "Logout"), 0);
		$this->db->query("INSERT INTO `lg_logs`
								(`oq_userid`,
								`lg_date`,
								`lg_action`)
								VALUES
								(" . $this->session->userdata("oq_uid") . ",
								NOW(),
								'Logout'
								)");
		$this->session->sess_destroy();
		redirect(base_url() .'index.php/');
	}

	public function update_password()
	{
		// no sp
		try 
		{
			$this->db->query("UPDATE `oq_user` SET `oq_password`='".$this->input->post("password")."' WHERE `oq_userid` = '".$this->session->userdata("oq_uid")."'");
			$this->session->set_userdata("oq_password", $this->input->post("password"));
			echo "success";
		} 
		catch (Exception $e) 
		{
			
		}
		
	}	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */