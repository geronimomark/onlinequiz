<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->model("sp_model");
		if($this->session->userdata("oq_name"))
			redirect(base_url().'index.php/');
	}

	public function index()
	{
		$this->load->view('login/index');
	}

	public function log()
	{
		if(! $this->input->post("username") || ! $this->input->post("password"))
		{
			echo 0;
		}
		else
		{
			// $result = $this->sp_model->read("get_userdata", array($this->input->post("username"), $this->input->post("password")), 0);
			$result = $this->db->query("SELECT *
										FROM `oq_user`
										WHERE `oq_username` = '" . $this->input->post("username") ."'
										AND `oq_password` = '" . $this->input->post("password") . "'")->result_array();
			if(count($result) == 0)
			{
				echo 0;
			}
			else
			{
				$data = array(
						"oq_uid"		=> $result[0]["oq_userid"],
						"oq_name"		=> $result[0]["oq_fname"],
						"oq_utype"		=> $result[0]["oq_utype"],
						"oq_password"	=> $result[0]["oq_password"],
						"oq_lname"		=> $result[0]["oq_lname"],
						"oq_exam"		=> $result[0]["oq_exam"],
						"oq_lecture"	=> $result[0]["oq_lecture"],
						"oq_activity"	=> $result[0]["oq_activity"],
						"oq_profile"	=> $result[0]["oq_profile"]
					);
				$this->session->set_userdata($data);
				// $this->sp_model->save("log_data", array($this->session->userdata("oq_uid"), "Login"), 0);
				$this->db->query("INSERT INTO `lg_logs`
								(`oq_userid`,
								`lg_date`,
								`lg_action`)
								VALUES
								(" . $this->session->userdata("oq_uid") . ",
								NOW(),
								'Login'
								)");
				echo 1;
			}
		}
		
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */