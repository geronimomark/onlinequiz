<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
 
	public function index()
	{
		$this->load->view('contact/index');
	}

	public function send_message()
	{
		echo $this->db->query("INSERT INTO `inquiry` VALUES(null, ".$this->input->post('stud_id').", 
			'".$this->input->post('topic')."', '".$this->input->post('msg')."', NOW())");
	}

	public function messages()
	{
		$data["msg"] = $this->db->query("SELECT A.*, CONCAT(B.oq_fname, ' ', B.oq_lname) as `Name` FROM `inquiry` A INNER JOIN `oq_user` B ON A.uid = B.oq_userid ORDER BY `date` DESC LIMIT 20")->result_array();
		$this->load->view('contact/inquiry', $data);
	}
}

/* End of file contact.php */
/* Location: ./application/controllers/contact.php */