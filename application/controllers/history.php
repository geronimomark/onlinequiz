<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class History extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_utype") != 2 || $this->session->userdata("oq_exam") == "0")
 			redirect(base_url()."index.php/home/logout");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
	}
 
	public function index()
	{
		// $data["linked_quiz"] = $this->sp_model->read("get_linked_quiz", array($this->session->userdata("oq_uid")), 0);

		$data["linked_quiz"] = $this->db->query("SELECT qz_quiz.*,  `link`.`status`
								FROM qz_quiz 
								INNER JOIN link 
								ON qz_quiz.qz_qid = link.qz_qid 
								AND stud_id = " . $this->session->userdata("oq_uid"))->result_array();
		$this->load->view('history/index', $data);
	}

}

/* End of file history.php */
/* Location: ./application/controllers/history.php */