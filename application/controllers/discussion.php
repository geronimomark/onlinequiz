<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discussion extends CI_Controller {

 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
 	}
 
	public function index()
	{
		// no sp
		if($this->session->userdata("oq_utype") == 1)
		{
			$data["x"] = $this->db->query("SELECT A.*, B.qz_qname, CONCAT(C.oq_fname, ' ', C.oq_lname) as Name 
											FROM `fr_forum` A 
											INNER JOIN qz_quiz B ON B.qz_qid = A.qz_qid
											INNER JOIN oq_user C ON C.oq_userid = A.uid
											WHERE A.uid = ".$this->session->userdata('oq_uid')."
											ORDER BY A.dt DESC")->result_array();
		}
		else
		{
			$data["x"] = $this->db->query("SELECT A.*, B.qz_qname, CONCAT(C.oq_fname, ' ', C.oq_lname) as Name 
											FROM `fr_forum` A 
											INNER JOIN qz_quiz B ON B.qz_qid = A.qz_qid
											INNER JOIN oq_user C ON C.oq_userid = A.uid
											INNER JOIN link D ON D.qz_qid = A.qz_qid
											WHERE D.stud_id = ".$this->session->userdata('oq_uid')."
											ORDER BY A.dt DESC")->result_array();
		}
		
		$this->load->view('discussion/index', $data);
	}

	public function comments($fid)
	{
		$this->check_user_link($fid);
		$data["x"] = $this->db->query("SELECT A.*, B.qz_qname, CONCAT(C.oq_fname, ' ', C.oq_lname) as Name 
											FROM `fr_forum` A 
											INNER JOIN qz_quiz B ON B.qz_qid = A.qz_qid
											INNER JOIN oq_user C ON C.oq_userid = A.uid
											INNER JOIN link D ON D.qz_qid = A.qz_qid
											WHERE A.fr_fid =".$fid)->result_array();

		$data["y"] = $this->db->query("SELECT A.*, CONCAT(B.oq_fname, ' ', B.oq_lname) as Name 
										FROM `cm_comments` A
										INNER JOIN oq_user B ON B.oq_userid = A.uid
										WHERE `fr_fid` = ".$fid."
										ORDER BY `cm_cid`")->result_array();
		$this->load->view('discussion/comments', $data);
	}

	public function delete_comment($comment, $forum = FALSE) 
	{
		if($this->session->userdata("oq_utype") == 1)
		{
			$this->db->query("DELETE FROM `onlinequiz`.`cm_comments` WHERE `cm_comments`.`cm_cid` = " .$comment);
		}
		if($forum)
			redirect(base_url()."index.php/discussion/comments/".$forum);
		else
			redirect(base_url()."index.php/discussion/index");
	}

	private function check_user_link($fid)
	{
		if($this->session->userdata("oq_utype") == 1)
		{
			$count = $this->db->query("SELECT COUNT(B.qz_qname)
											FROM `fr_forum` A 
											INNER JOIN qz_quiz B ON B.qz_qid = A.qz_qid
											INNER JOIN oq_user C ON C.oq_userid = A.uid
											INNER JOIN link D ON D.qz_qid = A.qz_qid
											WHERE D.teach_id = ".$this->session->userdata('oq_uid')."
											AND A.fr_fid =".$fid)->result_array();
		}
		else
		{
			$count = $this->db->query("SELECT COUNT(B.qz_qname)
											FROM `fr_forum` A 
											INNER JOIN qz_quiz B ON B.qz_qid = A.qz_qid
											INNER JOIN oq_user C ON C.oq_userid = A.uid
											INNER JOIN link D ON D.qz_qid = A.qz_qid
											WHERE D.stud_id = ".$this->session->userdata('oq_uid')."
											AND A.fr_fid =".$fid)->result_array();
		}
		if($count == "0")
			redirect(base_url()."index.php/home/logout");
	}

	public function add_comment()
	{
		// no sp
		$query = $this->db->query("INSERT INTO `cm_comments`(`cm_cid`, `fr_fid`, `cm_comment`, `uid`, `dt`) 
							VALUES (
								null,
								".$this->input->post('fid').",
								'".$this->input->post('comment')."',
								".$this->session->userdata('oq_uid').",
								'".date('Y-m-d H:i:s')."')");
		if ($query)
		{
			$y = $this->db->query("SELECT A.*, CONCAT(B.oq_fname, ' ', B.oq_lname) as Name 
										FROM `cm_comments` A
										INNER JOIN oq_user B ON B.oq_userid = A.uid
										WHERE `fr_fid` = ".$this->input->post('fid')."
										ORDER BY `cm_cid`")->result_array();
			$table = "<table align='center' class='table table-striped' border='0' width='100%' style='table-layout:fixed;'>";
			foreach ($y as $key => $value) 
			{
				$table .= "<tr>
				<td style='word-wrap:break-word;' width='25%'>".$value['Name']."</td>
              	<td style='word-wrap:break-word;' width='15%'>".$value['dt']."</td>
				<td style='word-wrap:break-word;'>".$value['cm_comment']."</td>
				</tr>";
			}
            $table .= "</table>";
			echo "success|".$table;
		}
	}

}

/* End of file discussion.php */
/* Location: ./application/controllers/discussion.php */