<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_utype") != 1)
 			redirect(base_url()."index.php/home/logout");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
 	}
 
	public function users($page = "view_users", $id = 0)
	{
		if($page == "view_users")
		{
			// $data["new_users"] = $this->sp_model->read("get_user_data", array($id), 0);
			if($id > 0)
			{
				$data["new_users"] = $this->db->query("SELECT * 
									FROM oq_user 
									WHERE `oq_userid` = ".$id)->result_array();
			}
			else
			{
				$data["new_users"] = $this->db->query("SELECT * FROM oq_user 
														ORDER BY `oq_userid` DESC;")->result_array();
			}
			$this->load->view('admin/users/' . $page, $data);
		}
		elseif($page == "edit_user")
		{
			// $data["edit_data"] = $this->sp_model->read("get_user_data", array($id), 0);
			if($id > 0)
			{
				$data["edit_data"] = $this->db->query("SELECT * 
									FROM oq_user 
									WHERE `oq_userid` = ".$id)->result_array();
			}
			else
			{
				$data["edit_data"] = $this->db->query("SELECT * FROM oq_user 
														ORDER BY `oq_userid` DESC")->result_array();
			}
			$this->load->view('admin/users/' . $page, $data);
		}
		else
		{
			$this->load->view('admin/users/' . $page);
		}
		
	}

	public function quizzez($page = "view_quiz", $id = 0)
	{
		if($page == "view_quiz")
		{
			// $data["new_quiz"] = $this->sp_model->read("get_quiz_data", array($id, $this->session->userdata("oq_uid")), 0);
			if($id > 0)
			{
				$data["new_quiz"] = $this->db->query("SELECT *
														FROM `qz_quiz`
														WHERE `qz_qid` = ".$id)->result_array();
			}
			else
			{
				$data["new_quiz"] = $this->db->query("SELECT * FROM `qz_quiz`
														WHERE `oq_userid` = ".$this->session->userdata("oq_uid")."
														ORDER BY `qz_qid`
														DESC LIMIT 10")->result_array();
			}
			$this->load->view('admin/quizzez/' . $page, $data);
		}
		elseif($page == "edit_quiz")
		{
			// $data["edit_data"] = $this->sp_model->read("get_quiz_data", array($id, $this->session->userdata("oq_uid")), 0);
			if($id > 0)
			{
				$data["edit_data"] = $this->db->query("SELECT *
														FROM `qz_quiz`
														WHERE `qz_qid` = ".$id)->result_array();
			}
			else
			{
				$data["edit_data"] = $this->db->query("SELECT * FROM `qz_quiz`
														WHERE `oq_userid` = ".$this->session->userdata("oq_uid")."
														ORDER BY `qz_qid`
														DESC LIMIT 10")->result_array();
			}
			$this->load->view('admin/quizzez/' . $page, $data);
		}
		else
		{
			$this->load->view('admin/quizzez/' . $page);
		}
	}

	public function questions($page = "view_question", $qid = 0, $qsid = 0)
	{
		$data["categories"] = $this->getCategories();
		$data["quizid"] = $qid;
		if( $data["quizid"] == 0)
		{
			show_404();
			break;
		}
		if($page == "view_question")
		{ 			
			// $data["new_question"] = $this->sp_model->read("get_question_data", array($qid, $qsid), 0);
			if($qsid > 0)
			{
				$data["new_question"] = $this->db->query("SELECT *
															FROM `qs_question`
															WHERE `qs_qsid` = ".$qsid)->result_array();
			}
			else
			{
				$data["new_question"] = $this->db->query("SELECT * FROM `qs_question`
															WHERE `qz_qid` = ".$qid)->result_array();
			}
			$this->load->view('admin/questions/' . $page, $data);
		}
		elseif($page == "edit_question")
		{
			// $data["edit_question"] = $this->sp_model->read("get_question_data", array($qid, $qsid), 0);
			if($qsid > 0)
			{
				$data["edit_question"] = $this->db->query("SELECT *
															FROM `qs_question`
															WHERE `qs_qsid` = ".$qsid)->result_array();
			}
			else
			{
				$data["edit_question"] = $this->db->query("SELECT * FROM `qs_question`
															WHERE `qz_qid` = ".$qid)->result_array();
			}
			$this->load->view('admin/questions/' . $page, $data);
		}
		else
		{
			$this->load->view('admin/questions/' . $page, $data);
		}
	}

	public function categories($page = "view_category", $id = 0)
	{
		if($page == "view_category")
		{
			// $data["new_category"] = $this->sp_model->read("get_category_data", array($id), 0);
			if($id > 0)
			{
				$data["new_category"] = $this->db->query("SELECT *
															FROM `sc_category`
															WHERE `sc_sid` = ".$id)->result_array();
			}
			else
			{
				$data["new_category"] = $this->db->query("SELECT *
															FROM `sc_category`")->result_array();
			}
			$this->load->view('admin/categories/' . $page, $data);
		}
		elseif($page == "edit_category")
		{
			// $data["edit_category"] = $this->sp_model->read("get_category_data", array($id), 0);
			if($id > 0)
			{
				$data["edit_category"] = $this->db->query("SELECT *
															FROM `sc_category`
															WHERE `sc_sid` = ".$id)->result_array();
			}
			else
			{
				$data["edit_category"] = $this->db->query("SELECT *
															FROM `sc_category`")->result_array();
			}
			$this->load->view('admin/categories/' . $page, $data);
		}
		else
		{
			$this->load->view('admin/categories/' . $page);
		}
	}

	public function sections($page = "view_section", $id = 0)
	{
		$this->load->view('admin/sections/' . $page);
	}

	public function assign($page = "index", $id = 0)
	{
		// $data["edit_data"] = $this->sp_model->read("get_quiz_data", array($id, $this->session->userdata("oq_uid")), 0);
		if($id > 0)
		{
			$data["edit_data"] = $this->db->query("SELECT *
													FROM `qz_quiz`
													WHERE `qz_qid` = ".$id)->result_array();
		}
		else
		{
			$data["edit_data"] = $this->db->query("SELECT * FROM `qz_quiz`
													WHERE `oq_userid` = ".$this->session->userdata("oq_uid")."
													ORDER BY `qz_qid`
													DESC LIMIT 10")->result_array();
		}
		if(count($data["edit_data"]) > 0)
		{
			// $data["new_users"] = $this->sp_model->read("get_user_data", array(0), 0);
			$data["new_users"] = $this->db->query("SELECT * FROM oq_user 
													ORDER BY `oq_userid` DESC")->result_array();
			// $data["linked"] = $this->sp_model->read("get_linked", array($id), 0);	
			$data["linked"] = $this->db->query("SELECT * FROM link
													WHERE `qz_qid` = ".$id)->result_array();
		}
		else
		{
			redirect(base_url()."index.php/home/logout");
		}
		$this->load->view('admin/assign/' . $page, $data);
	}

	public function assign_search()
	{
		$count = 0;
		// $result = $this->sp_model->read("search_user", $this->input->post("search"), 0);
		$x = 0;
		if ( ucwords($this->input->post("search")) == 'ADMIN' )
			$x = 1;
		elseif ( ucwords($this->input->post("search")) == 'STUDENT' )
			$x = 2;
		$result = $this->db->query("SELECT * 
									FROM oq_user 
									WHERE `oq_fname` LIKE '".$this->input->post("search")."%'
									OR `oq_lname` LIKE '".$this->input->post("search")."%'
									OR `oq_utype` = ".$x."
									OR `oq_date` LIKE '".$this->input->post("search")."%'
									ORDER BY `oq_userid` DESC
									LIMIT 10")->result_array();
		$html = "<table border='0' width='100%'><tr><th width='50%'>Name</th><th>Click Checkbox to Assign Student</th></tr>";
		foreach ($result as $key => $value)
		{
			$count++;
			if( $this->session->userdata("oq_uid") != $value['oq_userid'] )
			{
				$html .= "<tr><td>" . $value['oq_fname'] . " " . $value['oq_lname'] . "</td>";
	            $checked = "";
	            // $linked = $this->sp_model->read("get_linked", array($this->input->post("qzid")), 0);
	            $linked = $this->db->query("SELECT * FROM link
											WHERE `qz_qid` = ".$this->input->post("qzid"))->result_array();
	            foreach ($linked as $keyx => $valx)
	            {
	                if( $valx["stud_id"] == $value['oq_userid'] )
    	                $checked = "checked";
                }
                $html .= "<td><input id='" . $value['oq_userid'] . "' type='checkbox' value='" . $value['oq_userid'] . "' " . $checked . "/></td>
				<script>
	              $(function(){
    	            $('#" . $value['oq_userid'] . "').click(function(e){
                  		if( $('#" . $value['oq_userid'] . "').prop('checked') == true ){
                    		$.ajax({
		                      type: 'POST',
        		              url: '" . base_url() . "index.php/admin/create_link',
		                      data: {
    			                qz_qid  : '" . $this->input->post("qzid") . "',
                		        stud_id : '" . $value['oq_userid'] . "'
                  				}
                    		});
                  		}else{
                    		var r = confirm('Are you sure you want to remove this student? This will remove all records of " . $value['oq_fname'] . " " . $value['oq_lname'] . " for this Quiz.');
		                    if (r == true){
        		              $.ajax({
                		        type: 'POST',
		                        url: '" . base_url() . "index.php/admin/remove_link',
		                        data: {
        		                  qz_qid  : '" . $this->input->post("qzid") . "',
                		          stud_id : '" . $value['oq_userid'] . "'
 			                       }
            		          });
                    		}else{
                      			$('#" . $value['oq_userid'] . "').prop('checked', true);
							}
                  		}
	                });
    	          });
        	    </script>          
            	</tr>";
			}
		}
        $html .= "</table>|" . $count;
        echo $html;
	}

	public function check_duplicate()
	{
		if(! $this->input->post("uname"))
		{
			echo 0;
		}
		else
		{
			// $result = $this->sp_model->read("check_duplicate", array($this->input->post("uname"), ""), 0);
			$result = $this->db->query("SELECT COUNT(*) as DUPCOUNT
										FROM `oq_user`
										WHERE `oq_username` = '".$this->input->post("uname")."'")->result_array();
			echo $result[0]["DUPCOUNT"];
		}
	}

	public function check_duplicate_cat()
	{
		if(! $this->input->post("cat"))
		{
			echo 0;
		}
		else
		{
			// $result = $this->sp_model->read("check_duplicate", array("", $this->input->post("cat")), 0);
			$result = $this->db->query("SELECT COUNT(*) as DUPCOUNT
										FROM `sc_category`
										WHERE `sc_sname` = UCASE('".$this->input->post("cat")."')")->result_array();
			echo $result[0]["DUPCOUNT"];
		}
	}

	public function save_user()
	{
		$id = (! $this->input->post("uid")) ? 0 : $this->input->post("uid");
		// $data = array(
		// 		$this->input->post("fname"),
  //               $this->input->post("lname"),
  //               $this->input->post("email"),
  //               $this->input->post("utype"),
  //               $this->input->post("user"),
  //               $this->input->post("pass"),
  //               $id
		// 	);
		// $this->sp_model->save("save_user", $data, 0);
		if($id > 0)
		{
			$this->db->query("UPDATE `oq_user`
								SET
								`oq_fname` = '".$this->input->post("fname")."',
								`oq_lname` = '".$this->input->post("lname")."',
								`oq_email` = '".$this->input->post("email")."',
								`oq_utype` = '".$this->input->post("utype")."',
								`oq_username` = '".$this->input->post("user")."',
								`oq_password` = '".$this->input->post("pass")."',
								`oq_exam` = '".$this->input->post("exam")."',
								`oq_lecture` = '".$this->input->post("lecture")."',
								`oq_activity` = '".$this->input->post("activity")."'
								WHERE `oq_userid` = '".$id."'");
		}
		else
		{
			$this->db->query("INSERT INTO `oq_user`
							VALUES
							(null,
							'".$this->input->post("fname")."',
							'".$this->input->post("lname")."',
							'".$this->input->post("email")."',
							'".$this->input->post("utype")."',
							'".$this->input->post("user")."',
							'".$this->input->post("pass")."',
							NOW(),
							'".$this->input->post("exam")."',
							'".$this->input->post("lecture")."',
							'".$this->input->post("activity")."',
							'0'
							)");
		}
		echo "success";
		


	}

	public function search_user()
	{
		$count = 0;
		$html = "<table border='0' width='100%'>
          <tr>
            <th>Name</th>
            <th>Usertype</th>
            <th colspan='2'>Action</th></tr>";
		// $result = $this->sp_model->read("search_user", $this->input->post("search"), 0);
		$x = 0;
		if ( ucwords($this->input->post("search")) == 'ADMIN' )
			$x = 1;
		elseif ( ucwords($this->input->post("search")) == 'STUDENT' )
			$x = 2;
		$result = $this->db->query("SELECT * 
									FROM oq_user 
									WHERE `oq_fname` LIKE '".$this->input->post("search")."%'
									OR `oq_lname` LIKE '".$this->input->post("search")."%'
									OR `oq_utype` = ".$x."
									OR `oq_date` LIKE '".$this->input->post("search")."%'
									ORDER BY `oq_userid` DESC
									LIMIT 10")->result_array();
		foreach ($result as $key => $value) 
		{
			if ( $this->session->userdata("oq_uid") != $value['oq_userid'] )
			{
				$count++;
				$x = ($value["oq_utype"] == 1) ? 'Admin' : 'Student';
				$html .= "<tr>
	            <td>" . $value["oq_fname"] . " " . $value["oq_lname"] . "</td>
	            <td>" .  $x . "</td>
	            <td><a href='" . base_url() . "index.php/admin/users/edit_user/" . $value['oq_userid'] . "'>Edit</a></td>
	            <td><a href='#'>Delete</a></td></tr>";				
			}
		}
		echo $html . "</table>" . "|" . $count;
	}

	public function delete_user()
	{
		if(! $this->input->post("uid"))
		{
			echo "failed";
		}
		else
		{
			// $this->sp_model->save("del_user", $this->input->post("uid"), 0);
			$this->db->query("DELETE
								FROM `oq_user`
								WHERE `oq_userid` = ".$this->input->post("uid"));
			echo "success";
		}
	}

	public function save_quiz()
	{
		$id = (! $this->input->post("qid")) ? 0 : $this->input->post("qid");
		// $data = array(
		// 		$this->input->post("qname"),
  //               $this->input->post("qdesc"),
  //               $this->input->post("qtime") * 60,
  //               $this->session->userdata("oq_uid"),
  //               $this->input->post("qrate"),
  //               $id
		// 	);
		// $this->sp_model->save("save_quiz", $data, 0);
		if($id > 0)
		{
			$this->db->query("UPDATE `qz_quiz`
							SET
							`qz_qname` = '".$this->input->post("qname")."',
							`qz_qdesc` = '".$this->input->post("qdesc")."',
							`qz_qdate` = NOW(),
							`qz_qtimelimit` = ". ($this->input->post("qtime") * 60) .",
							`oq_qrate` = ".$this->input->post("qrate")."
							WHERE `qz_qid` = ".$id);
		}
		else
		{
			$this->db->query("INSERT INTO `qz_quiz`
							VALUES
							(null,
							'".$this->input->post("qname")."',
							'".$this->input->post("qdesc")."',
							NOW(),
							".($this->input->post("qtime") * 60).",
							".$this->session->userdata("oq_uid").",
							".$this->input->post("qrate").")");

			//no sp

			$this->db->query("INSERT INTO `fr_forum` 
							VALUES 
							(null,
							".$this->db->insert_id().",
							".$this->session->userdata("oq_uid").",
							'".date('Y-m-d H:i:s')."')");

			$this->db->insert_id();
		}
		echo "success";
	}	

	public function delete_quiz()
	{
		if(! $this->input->post("qid"))
		{
			echo "failed";
		}
		else
		{
			// $this->sp_model->save("del_quiz", $this->input->post("qid"), 0);
			$this->db->query("DELETE
							FROM `qz_quiz`
							WHERE `qz_qid` = ".$this->input->post("qid"));
			echo "success";
		}
	}	

	private function getCategories()
	{
		// return $this->sp_model->read("get_category_data", array(0), 0);
		return $this->db->query("SELECT * FROM `sc_category`")->result_array();;
	}

	public function save_question()
	{
		$id = (! $this->input->post("qs_qsid")) ? 0 : $this->input->post("qs_qsid");
		// $data = array(
		// 		$id,
		// 		$this->input->post("qz_qid"),
  //               $this->input->post("qs_question"),
  //               $this->input->post("sc_sid"),
  //               $this->input->post("qs_choice1"),
  //               $this->input->post("qs_choice2"),
  //               $this->input->post("qs_choice3"),
  //               $this->input->post("qs_choice4"),
  //               $this->input->post("qs_ans")
		// 	);
		// $this->sp_model->save("save_question", $data, 0);
		if($id > 0)
		{
			$this->db->query("UPDATE `qs_question`
							SET
							`qs_question` = '".$this->input->post("qs_question")."',
							`sc_sid` = '".$this->input->post("sc_sid")."',
							`qs_choice1` = '".$this->input->post("qs_choice1")."',
							`qs_choice2` = '".$this->input->post("qs_choice2")."',
							`qs_choice3` = '".$this->input->post("qs_choice3")."',
							`qs_choice4` = '".$this->input->post("qs_choice4")."',
							`qs_ans` = '".$this->input->post("qs_ans")."'
							WHERE 
							`qs_qsid` = ".$id);
		}
		else
		{
			$this->db->query("INSERT INTO `qs_question`
								VALUES
								(
									null,
									".$this->input->post("qz_qid").",
									'',
									'".$this->input->post("qs_question")."',
									'".$this->input->post("sc_sid")."',
									'".$this->input->post("qs_choice1")."',
									'".$this->input->post("qs_choice2")."',
									'".$this->input->post("qs_choice3")."',
									'".$this->input->post("qs_choice4")."',
									'".$this->input->post("qs_ans")."'
								)");
		}
		echo "success";
	}

	public function delete_question()
	{
		if(! $this->input->post("qsid"))
		{
			echo "failed";
		}
		else
		{
			// $this->sp_model->save("del_question", $this->input->post("qsid"), 0);
			$this->db->query("DELETE
								FROM `qs_question`
								WHERE `qs_qsid` = ".$this->input->post("qsid"));
			echo "success";
		}
	}	

	public function save_category()
	{
		$id = (! $this->input->post("cid")) ? 0 : $this->input->post("cid");
		// $data = array(
		// 		$this->input->post("cat"),
  //               $id
		// 	);
		// $this->sp_model->save("save_category", $data, 0);
		if($id > 0)
		{
			$this->db->query("UPDATE `sc_category`
							SET `sc_sname` = UCASE('".$this->input->post("cat")."')
							WHERE `sc_sid` = ".$id);
		}
		else
		{
			$this->db->query("INSERT INTO `sc_category`
							VALUES
							(
							null,
							UCASE('".$this->input->post("cat")."')
							)");
		}
		$this->getCategories();
		echo "success";
	}

	public function delete_category()
	{
		if(! $this->input->post("cid"))
		{
			echo "failed";
		}
		else
		{
			// $this->sp_model->save("del_category", $this->input->post("cid"), 0);
			$this->db->query("DELETE
								FROM `sc_category`
								WHERE `sc_sid` = ".$this->input->post("cid"));
			echo "success";
		}
	}

	public function check_link()
	{
		// $data = $this->sp_model->read("check_link", array($this->input->post('cid')), 0);
		$data = $this->db->query("SELECT count(*) as 'COUNT' 
									FROM `qs_question`
									WHERE `sc_sid` = ".$this->input->post('cid'))->result_array();
		echo $data[0]['COUNT'];
	}

	public function create_link()
	{
		// $this->sp_model->save("create_link", array($this->input->post("qz_qid"), $this->input->post("stud_id"), $this->session->userdata("oq_uid")), 0);
		$this->db->query("INSERT INTO `link`
							VALUES
							(null,
							".$this->input->post("qz_qid").",
							".$this->input->post("stud_id").",
							".$this->session->userdata("oq_uid").",
							NOW(),
							'0'
							)");
	}

	public function remove_link()
	{
		// $this->sp_model->save("remove_link", array($this->input->post("qz_qid"), $this->input->post("stud_id"), $this->session->userdata("oq_uid")), 0);
		$this->db->query("DELETE FROM `link` WHERE `qz_qid` = ".$this->input->post("qz_qid")." AND `stud_id` = ".$this->input->post("stud_id")." AND `teach_id` = ".$this->session->userdata("oq_uid"));
	}

	public function logs()
	{
		// no sp
		$data["logs"] = $this->db->query('SELECT A.`lg_lid`, A.`oq_userid`, CONCAT(B.`oq_fname`, " ",B.`oq_lname`) as Name,A.`lg_date`, A.`lg_action` 
								FROM `lg_logs` A
								INNER JOIN `oq_user` B
								ON A.`oq_userid` = B.`oq_userid`
								AND lg_date LIKE ("'.date("Y-m-d").'%")')->result_array();
		$this->load->view('admin/logs', $data);

	}

	public function get_logs()
	{
		// no sp
		$data = $this->db->query('SELECT A.`lg_lid`, A.`oq_userid`, CONCAT(B.`oq_fname`, " ",B.`oq_lname`) as Name,A.`lg_date`, A.`lg_action` 
								FROM `lg_logs` A
								INNER JOIN `oq_user` B
								ON A.`oq_userid` = B.`oq_userid`
								AND lg_date LIKE ("'.$this->input->post("date").'%")')->result_array();
		$html = '<table class="table table-striped" align="center">
          <tr>
            <th>Name</th>
            <th>Timestamp</th>
            <th>Action</th>
          </tr>';
		foreach ($data as $key => $value) 
		{
			$html .= "<tr><td>".$value["Name"]."</td><td>".$value["lg_date"]."</td><td>".$value["lg_action"]."</td><tr>";
		}
		echo $html.= "</table>|" . count($data);
	}

	public function exams()
	{
		// no sp
		$data['exam'] = $this->db->query("SELECT * FROM `qz_quiz` 
				LEFT JOIN 
				(SELECT count( `qz_qid` ) AS participants,qz_qid
				FROM `link`
				GROUP BY qz_qid) as A
				ON qz_quiz.`qz_qid` = A.`qz_qid`
				LEFT JOIN 
				(SELECT count( `qz_qid` ) AS 'Kulang',qz_qid as 'qz_qid2'
				FROM `link`
				WHERE `status` = '0'
				GROUP BY qz_qid2) as B
				ON qz_quiz.`qz_qid` = B.`qz_qid2` WHERE qz_quiz.`oq_userid` = '".$this->session->userdata("oq_uid")."'")->result_array();
		$this->load->view('admin/admin_exam', $data);
	}

	public function admin_summary($quiz)
	{
		// no sp
		$data['results'] = $this->db->query("SELECT A.*, CONCAT(B.`oq_fname`, ' ', B.`oq_lname`) as Name, C.`qz_qname` FROM `rs_results` A 
							INNER JOIN `oq_user` B
							ON A.`oq_userid` = B.`oq_userid`
							INNER JOIN `qz_quiz` C
							ON C.`qz_qid` = A.`qz_qid`
							WHERE A.`qz_qid`= ". $quiz)->result_array();
		$this->load->view('admin/admin_summary', $data);

	}

	public function reports()
	{
		$data["survey"] = json_encode($this->db->query("SELECT 
											CONCAT(b.`fname`, ' ', b.`lname`) as 'NAME',
											`bday` as 'BIRTHDAY',
											`gender` as 'GENDER',
											`school` as 'SCHOOL',
											`course` as 'COURSE',
											`yearlevel` as 'YEAR LEVEL',
											`subject` as 'FAVORITE SUBJECT',
											`algebra` as 'ALGEBRA',
											`trigo` as 'TRIGONOMETRY',
											`geometry` as 'GEOMETRY',
											`diff` as 'DIFF CALC',
											`integral` as 'INTEGRAL CALC',
											`advmath` as 'ADVANCE MATH',
											`diffeq` as 'DIFF EQUATION',
											`q1` as 'Answer 1', 
											`q2` as 'Answer 2', 
											`q3` as 'Answer 3', 
											`q4` as 'Answer 4', 
											`update_date` as 'UPDATE DATE' 
											FROM `profile` b")->result_array());
		$data["scoreboard"] = json_encode($this->db->query("SELECT CONCAT(b.`fname`, ' ', b.`lname`) as 'NAME',
											a.`category_name` as 'CATEGORY',
											a.`score` as 'SCORE',
											a.`dateof` as 'DATE'
											FROM `scoreboard` a 
											INNER JOIN `profile` b 
											ON a.`user_id` = b.`user_id`")->result_array());
		$this->load->view("admin/reports/index", $data);
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */