<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lecture extends CI_Controller {

 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("sp_model");
 		if(! $this->session->userdata("oq_name"))
 			redirect(base_url() . "index.php/login");
 		if($this->session->userdata("oq_lecture") != "1")
 			redirect(base_url() . "index.php/home");
 		if($this->session->userdata("oq_profile") == "0")
 			redirect(base_url() . "index.php/profile/edit_profile");
 	}

 	public function algebra($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("algebra/".$page);
 		$this->load->view("template/footer_algebra");
 		$this->load->view("template/footer_lectures");
 	}

 	public function trigo($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("trigo/".$page);
 		$this->load->view("template/footer_trigo");
 		$this->load->view("template/footer_lectures");
 	}

	public function geometry($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("geometry/".$page);
 		$this->load->view("template/footer_geometry");
 		$this->load->view("template/footer_lectures");
 	}

 	public function diffcalc($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("diffcalc/".$page);
 		$this->load->view("template/footer_diffcalc");
 		$this->load->view("template/footer_lectures");
 	}

 	public function integral($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("integral/".$page);
 		$this->load->view("template/footer_integral");
 		$this->load->view("template/footer_lectures");
 	}

 	public function diffeq($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("diffeq/".$page);
 		$this->load->view("template/footer_diffeq");
 		$this->load->view("template/footer_lectures");
 	}

 	public function advmath($page = "0")
 	{
 		$this->load->view("template/header_lectures");
 		$this->load->view("advmath/".$page);
 		$this->load->view("template/footer_lectures");
 	}

}

/* End of file lecture.php */
/* Location: ./application/controllers/lecture.php */