<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Sp_model extends CI_Model{
        
    public function __construct()
    {
        parent::__construct();	
        $this->load->library('db_query');       
    }
    
    public function save($proc, $param, $out, $query = FALSE)
    {
        return $this->db_query->write_sp($proc, $param, $out, $query);
    }
    
    public function read($proc, $param, $out)
    {
        return $this->db_query->read_sp($proc, $param, $out);
    }
    
}