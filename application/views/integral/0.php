<div class="style1" id="content">
    <h2 align="center" class="style17">integral calculus </h2>
    <h2 align="center">indefinite integral </h2>
    <p>&nbsp;</p>
    <ul>
      <li>Given a function, f(x), an <span class="style22">anti-derivative </span>of f(x) is any function F(x) such that F'(x) = f(x)</li>
      <li>If F(x) is any anti-derivative of f(x) then the most general anti-derivative of f(x) is called an<span class="style22"> indefinite integral</span>. 
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/integral/images/1.3.jpg" width="165" height="20" /></p>
        </blockquote>
        <ul>
          <li>C = any constant</li>
          <li><img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002.gif" />= integral symbol</li>
          <li>f(x) = integrand</li>
          <li>x = integration variable</li>
          <li>c = constant of integration   </li>
        </ul>
      </li>
    </ul>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="17" />Example:</strong></p>
    <p>Evaluate the following indefinite integral <img src="<?php echo base_url();?>assets/integral/images/1.4.jpg" width="100" height="22" /></p>
    <blockquote>
      <p class="style23">Solution:</p>
      <p><img src="<?php echo base_url();?>assets/integral/images/1.5.jpg" width="250" height="32" /></p>
      <p>&nbsp;</p>
    </blockquote>
    <p class="style26"><u>PROPERTIES OF INDEFINITE INTEGRAL</u> </p>
    <p>1.<img src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0000.gif" alt="1" width="12" height="21" />k f(x) dx = k <img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0001.gif" />f(x) dx ; where k is any number.</p>
    <p>2. <img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0002.gif" />-f (x)dx = -<img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0003.gif" />f(x) dx ; where k = -1</p>
    <p>3. <img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0004.gif" />f(x) &plusmn;  g(x) dx = <img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0005.gif" />f(x) dx &plusmn; <img width="12" height="21" src="<?php echo base_url();?>assets/integral/images/Topic 1_clip_image002_0005.gif" />g(x) dx </p>
    <p>&nbsp;</p>
    <p class="style26"><u>COMPUTING INDEFINITE INTEGRALS</u></p>
    <ul>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/integral/images/1.6.jpg" width="171" height="35" /></p>
        <p><img src="<?php echo base_url();?>assets/integral/images/1.7.jpg" width="205" height="30" /></p>
        <p>&nbsp;</p>
      </blockquote>
    </ul>
    <p class="style26"><u>TRIGONOMETRIC FUNCTIONS</u></p>
    <p align="center" class="style24"><img src="<?php echo base_url();?>assets/integral/images/1.8.jpg" width="442" height="95" /></p>
    <p align="center" class="style24">&nbsp;</p>
    <p align="left" class="style26"><u>EXPONENTIAL AND LOGARITHMIC FUNCTIONS</u></p>
    <p align="center" class="style24"><u><img src="<?php echo base_url();?>assets/integral/images/1.9.jpg" width="519" height="60" /></u></p>
    <p align="center" class="style24">&nbsp;</p>
    <p align="left" class="style26"><u>INVERSE TRIGONOMETRIC AND HYPERBOLIC FUNCTIONS</u></p>
    <p align="center" class="style24"><u><img src="<?php echo base_url();?>assets/integral/images/2.1.jpg" width="487" height="131" /></u></p>
    <p align="center" class="style24">&nbsp;</p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" />Examples:</p>
    <p>Evaluate
      each of the following indefinite integrals:</p>
    <p>a. <img src="<?php echo base_url();?>assets/integral/images/2.2.jpg" width="125" height="22" /></p>
    <p>b. <img src="<?php echo base_url();?>assets/integral/images/2.3.jpg" width="95" height="22" /></p>
    <p>c. &nbsp;&nbsp;<img src="<?php echo base_url();?>assets/integral/images/2.4.jpg" width="162" height="30" /></p>
    <p>&nbsp;</p>
    <blockquote>
      <p><span class="style23">Solutions</span></p>
      <p><span class="style23">a. <img src="<?php echo base_url();?>assets/integral/images/2.5.jpg" width="375" height="34" /></span></p>
      <p> <img src="<?php echo base_url();?>assets/integral/images/2.6.jpg" width="255" height="34" /></p>
      <p>&nbsp;</p>
      <p><span class="style23">b. <img src="<?php echo base_url();?>assets/integral/images/2.7.jpg" width="225" height="32" /></span></p>
      <p>&nbsp;</p>
      <p class="style23">c. <img src="<?php echo base_url();?>assets/integral/images/2.9.jpg" width="384" height="40" /></p>
      <blockquote>
        <blockquote>
          <blockquote>
            <blockquote>
              <blockquote>
                <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/3.jpg" width="235" height="55" /></p>
                <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/3.1.jpg" width="200" height="32" /></p>
                <p class="style23">&nbsp;</p>
              </blockquote>
            </blockquote>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p class="style26"><u>INTEGRAL PROPERTIES</u></p>
    <p>1. <img src="<?php echo base_url();?>assets/integral/images/3.2.jpg" width="180" height="28" />    </p>
    <p>2. <img src="<?php echo base_url();?>assets/integral/images/3.3.jpg" width="95" height="28" /></p>
    <p>3. <img src="<?php echo base_url();?>assets/integral/images/3.4.jpg" width="180" height="28" /></p>
    <p>4. <img src="<?php echo base_url();?>assets/integral/images/3.5.jpg" width="328" height="28" /></p>
    <p>5. <img src="<?php echo base_url();?>assets/integral/images/3.6.jpg" width="225" height="28" /></p>
    <p>6. <img src="<?php echo base_url();?>assets/integral/images/3.7.jpg" width="165" height="28" /></p>
    <p>7. <img src="<?php echo base_url();?>assets/integral/images/4.7.jpg" width="110" height="28" /></p>
    <p class="style23"><span class="style25"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" />Examples:</span></p>
    <p class="style23">a. <img src="<?php echo base_url();?>assets/integral/images/3.8.jpg" width="95" height="28" /></p>
    <p class="style23">b. <img src="<?php echo base_url();?>assets/integral/images/3.9.jpg" width="95" height="28" /></p>
    <p class="style23">&nbsp;</p>
    <blockquote>
      <p class="style23">Solutions:</p>
      <p class="style23">a. <img src="<?php echo base_url();?>assets/integral/images/4.jpg" width="200" height="28" /></p>
      <blockquote>
        <blockquote>
          <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/4.1.jpg" width="45" height="32" /></p>
          <p class="style23">&nbsp;</p>
        </blockquote>
      </blockquote>
      <p class="style23">b. <img src="<?php echo base_url();?>assets/integral/images/4.2.jpg" width="255" height="28" /></p>
      <blockquote>
        <blockquote>
          <blockquote>
            <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/4.4.jpg" width="110" height="28" /></p>
            <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/4.5.jpg" width="65" height="32" /></p>
            <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/4.6.jpg" width="50" height="32" /></p>
            <p class="style23">&nbsp;</p>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <ul>
      <li>Alternate notation for the derivative portion: 
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/integral/images/4.8.jpg" width="140" height="34" /></p>
          <p>&nbsp;</p>
        </blockquote>
      </li>
    </ul>
    <p><span class="style25"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" />Examples:</span></p>
    <p>1. Differentiate the given integral <img src="<?php echo base_url();?>assets/integral/images/4.9.jpg" width="110" height="34" /></p>
    <blockquote>
      <p><span class="style23">Solution:</span></p>
      <p><img src="<?php echo base_url();?>assets/integral/images/5.jpg" width="500" height="38" /></p>
      <blockquote>
        <blockquote>
          <blockquote>
            <p><img src="<?php echo base_url();?>assets/integral/images/5.1.jpg" width="260" height="35" /></p>
            <p>&nbsp;</p>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p>2. Evaluate the given integral <img src="<?php echo base_url();?>assets/integral/images/5.2.jpg" width="100" height="36" /></p>
    <blockquote>
      <p><span class="style23">Solution:</span></p>
      <blockquote>
        <p><span class="style23">u = 2 - 8t2 ; du = -16tdt</span></p>
        <p><span class="style23">t = 30 ; u = -70 t = 5</span></p>
        <p><img src="<?php echo base_url();?>assets/integral/images/5.3.jpg" width="75" height="32" /></p>
        <p><span class="style23">u = -198</span></p>
        <p><img src="<?php echo base_url();?>assets/integral/images/5.4.jpg" width="190" height="32" /></p>
        <blockquote>
          <blockquote>
            <p><img src="<?php echo base_url();?>assets/integral/images/5.5.jpg" width="130" height="32" /></p>
          </blockquote>
        </blockquote>
        <blockquote>
          <blockquote>
            <p class="style23"><img src="<?php echo base_url();?>assets/integral/images/5.6.jpg" width="129" height="32" /></p>
          </blockquote>
        </blockquote>
      </blockquote>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </blockquote>
  </div>