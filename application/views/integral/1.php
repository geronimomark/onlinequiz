<div class="style1" id="content">
    <h2 align="center" class="style17">integral calculus </h2>
    <h2 align="center">integration using partial fractions </h2>
    <ul>
      <li>The process of expressing a fraction in terms of simpler fractions called<span class="style28"> partial fractions</span>. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="13" /><strong>Examples:</strong></p>
    <p>1. Determine: <img src="<?php echo base_url();?>assets/integral/images/p1.jpg" width="100" height="32" /></p>
    <blockquote>
      <p class="style29">Solution:</p>
      <blockquote>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.1.jpg" width="200" height="32" /></p>
        <p class="style29">&nbsp;</p>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.2.jpg" width="170" height="32" /></p>
        <p class="style29">&nbsp;</p>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.3.jpg" width="170" height="16" /></p>
        <p class="style29">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style29">2. Determine <img src="<?php echo base_url();?>assets/integral/images/p1.4.jpg" width="90" height="32" /></p>
    <blockquote>
      <p class="style29">Solution:</p>
      <blockquote>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.5.jpg" width="190" height="32" /></p>
        <p class="style29">&nbsp;</p>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.6.jpg" width="180" height="32" /></p>
        <p class="style29">&nbsp;</p>
        <p class="style29"><img src="<?php echo base_url();?>assets/integral/images/p1.7.jpg" width="170" height="32" /></p>
      </blockquote>
    </blockquote>
  </div>