<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/Integral CALCULUS</h2>
    <p align="center"><span class="style27">Integral Formulas</span></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f1.jpg" width="145" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f2.jpg" width="160" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f3.jpg" width="160" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f4.jpg" width="200" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f5.jpg" width="160" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f6.jpg" width="160" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f7.jpg" width="160" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f8.jpg" width="165" height="28" /> </p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f9.jpg" width="190" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f10.jpg" width="200" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f11.jpg" width="130" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f12.jpg" width="165" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f13.jpg" width="130" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f14.jpg" width="190" height="28" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/formula/images/f15.jpg" width="180" height="28" /></p>
    <blockquote>
      <blockquote>
        <p align="center" class="style25">Integral Formulas</p>
      </blockquote>
    </blockquote>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.1.jpg" width="130" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.2.jpg" width="160" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.3.jpg" width="155" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.4.jpg" width="170" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.5.jpg" width="190" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.6.jpg" width="190" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.7.jpg" width="190" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.8.jpg" width="330" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.9.jpg" width="240" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.10.jpg" width="310" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.11.jpg" width="310" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.12.jpg" width="220" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.13.jpg" width="240" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.14.jpg" width="210" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.15.jpg" width="235" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.16.jpg" width="225" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.17.jpg" width="280" height="28" /></p>
    <p align="left" class="style25"><img src="<?php echo base_url();?>assets/formula/images/f1.18.jpg" width="280" height="28" /></p>
  </div>