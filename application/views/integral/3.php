<div class="style1" id="content">
    <h2 align="center" class="style17">integral calculus </h2>
    <h2 align="center">applications of integration </h2>
    <p class="style26"><u>AREA BETWEEN CURVES </u></p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" /><strong>Example:</strong></p>
    <p>Find the area below f(x) = - x<sup>2</sup> + 4x + 3 and above g(x) = -x3 + 7x2 - 10x + 5 over the interval 1&le;x&le;2.</p>
    <blockquote>
      <p class="style27">Solution: </p>
      <blockquote>
        <p><span class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.2.jpg" width="300" height="20" /></span></p>
        <p class="style27">Area between curves as a difference of areas </p>
      </blockquote>
      <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.3.jpg" width="497" height="133" /></p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.4.jpg" width="400" height="25" /></p>
        <blockquote>
          <blockquote>
            <blockquote>
              <blockquote>
                <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.6.jpg" width="160" height="25" /></p>
                <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.7.jpg" width="175" height="25" /></p>
                <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/1.9.jpg" width="40" height="30" /></p>
              </blockquote>
            </blockquote>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style26"><u>DISTANCE, VELOCITY, ACCELERATION </u></p>
    <ul>
      <li>Velocity Function:
        <blockquote>
          <blockquote>
            <p><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.jpg" width="184" height="36" /></p>
          </blockquote>
        </blockquote>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /><strong>Examples:</strong></p>
    <p>1. Suppose an object is acted upon by a constant force F. Find v(t) and s(t). By Newton's law F = ma, so the acceleration is F/m, where m is the mass of the object. </p>
    <blockquote>
      <p class="style27">Solution:</p>
      <p class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.1.jpg" width="399" height="32" /></p>
      <p class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.3.jpg" width="501" height="32" /></p>
      <blockquote>
        <blockquote>
          <blockquote>
            <blockquote>
              <blockquote>
                <blockquote>
                  <p class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.5.jpg" width="200" height="32" /></p>
                  <p class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.6.jpg" width="200" height="32" /></p>
                </blockquote>
              </blockquote>
              <p class="style27">When t<sub>0</sub> = 0 </p>
              <p class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.7.jpg" width="130" height="32" /></p>
            </blockquote>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style27">2. The acceleration of an object given by a(t) = cos(&nbsp;&pi;t), and its velocity at time t = 0 is 1/(2&nbsp;&pi;). Find both the net and the total distance traveled in the first 1.5 seconds.</p>
    <blockquote>
      <p align="left" class="style27">Solution:</p>
      <blockquote>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/2.8.jpg" width="426" height="35" />  The net distance traveled:</p>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/3.jpg" width="249" height="32" /> </p>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/3.1.jpg" width="165" height="35" /></p>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/3.2.jpg" width="65" height="15" /></p>
        <p align="left" class="style27">The total distance:</p>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/3.3.jpg" width="315" height="35" /> </p>
        <p align="left" class="style27"><img src="<?php echo base_url();?>assets/integral/images/Practice Problem/3.4.jpg" width="85" height="17" /></p>
      </blockquote>
    </blockquote>
    <p align="left" class="style27">&nbsp;  </p>
    <p align="left" class="style27">&nbsp; </p>
  </div>