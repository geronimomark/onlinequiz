<div class="style1" id="content">
    <p align="justify" class="style4">INDEFINITE INTEGRAL </p>
    <div align="justify">
      <ul>
        <li>Dawkins, P. (2007). Indefinite Integrals (pp. 4-10). Retrieved from Pauls Online Math Notes: http://tutorial.math.lamar.edu/sitemap.asp</li>
      </ul>
    </div>
    <p align="justify" class="style4">INTEGRATION USING PARTIAL FRACTIONS </p>
    <div align="justify">
      <ul>
        <li>Bird, J . (2007). Integration using partial fractions (p. 426). Engineering Mathematics (4th edition). UK: Newness </li>
      </ul>
    </div>
    <p align="justify" class="style4">AREA PROBLEM </p>
    <div align="justify">
      <ul>
        <li>Dawkins, P. (2007). Area Problems (pp. 12). Retrieved from Pauls Online Math Notes: http://tutorial.math.lamar.edu/sitemap.aspx</li>
      </ul>
    </div>
    <p align="justify" class="style4">APPLICATION OF INTEGRATION </p>
    <div align="justify">
      <ul>
        <li>Dawkins, P. (2007). Applications of Integration (Chapter 3). Retrieved from Pauls Online Math Notes: http://tutorial.math.lamar.edu/sitemap.asp</li>
      </ul>
    </div>
    </div>