<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Contact Us</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <script>
    function initialize()
    {
    var myLatlng = new google.maps.LatLng(14.5833,120.9667);
    var mapProp = {
      center:myLatlng,
      zoom:10,
      mapTypeId:google.maps.MapTypeId.ROADMAP
      };
    var map=new google.maps.Map(document.getElementById("googleMap")
      ,mapProp);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map
    });

    var contentString = "<table width='250'><tr><td width='50%' align='center'><img width='100px' height='100px' src='<?php echo base_url().'assets/images/mai.jpg'?>' /></td><td style='vertical-align:top;'><p style='margin-left: 2px;font-weight:bold;'>Make it EC-E</p><p>CEO: Maila Angeles</p></td></tr></table>";

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });



    }

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {font-family: "Times New Roman"}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "contact";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">
        <!-- <h1>Contact Us</h1> -->
        <!-- <p class="lead">Your contact details</p> -->
        <table width="100%">
          <tr>
              <td width="50%" style="vertical-align:top;">
                <div class="alert alert-success" style="width:auto;">
                <p align="center"><span class="style1">Add us on Facebook </span><a href="https://www.facebook.com/maila.angeles"><img src="<?php echo base_url();?>assets/images/facebook.gif"></a></p>
                </div>
                <p class="style1">&nbsp;&nbsp;Contact Us</p>
                <form class="form-signin" role="form">
                <input id ="topic" type="text" class="form-control" placeholder="Topic" style="margin-bottom:3px;" required>
                <textarea id ="msg" type="text" class="form-control" placeholder="Your message here" style="margin-bottom:3px;" required></textarea>
                <button type="submit" class="btn btn-lg btn-primary style1" id="save">Send Message</button>
                </form>
              </td>
              <td><div id="googleMap" style="width:500px;height:380px;"></div></td>
          </tr>
        </table>
        
      </div>

      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>
      $(function(){
        $("#save").click(function(e){
          if($("#topic").val() != "" && $("#msg").val() != ""){
            e.preventDefault();
            $.ajax({
              type: "POST",
              url: '<?php echo base_url();?>index.php/contact/send_message',
              data: {
                topic  : $("#topic").val(),
                msg  : $("#msg").val(),
                stud_id : "<?php echo $this->session->userdata('oq_uid');?>"
              },
              success: function(msg){
                if(msg == "1"){
                  alert("Message sent!");
                  $("#topic").val("");
                  $("#msg").val("");
                }else{
                  alert("Message not sent. Please try again later.")
                }
              }
            });
          }
        });
      });
    </script>
  </body>
</html>
