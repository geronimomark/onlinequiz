<div class="style1" id="content">
    <h2 align="center" class="style6">differential equation </h2>
    <h2 align="center">ordinary differential equation </h2>
    <ul>
      <li>An <span class="style5">ordinary differential equation (ode) i</span>s an equation containing derivatives, in which the unknown function depends on a single variable. Only derivatives appear in the equation.</li>
      <li>A differential equation containing only ordinary derivatives is therefore called ordinary <span class="style5">differential equation (ode)</span>. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" /><strong>Example:</strong></p>
    <blockquote>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/1.1.jpg" width="55" height="34" /></p>
        <p class="style7">- y is<span class="style5"> dependent variable</span> , depends only on the <span class="style5">independent variabl</span>e x. </p>
      </blockquote>
    </blockquote>
    <ul>
      <li>A differential equation in which the dependent variable depends on two or more independent variables is called a <span class="style5">partial differential equation (pde)</span>. </li>
    </ul>
    <p class="style8"><u>ORDER OF AN ODE</u> </p>
    <ul>
      <ul>
        <li>The order of an ode is given by the order of the highest derivative.</li>
      </ul>
      <p>&nbsp;</p>
      <li>First Order:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.1.jpg" alt="1" width="55" height="34" /></p>
    </blockquote>
    <ul>
      <li>Second Order: </li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.2.jpg" width="135" height="32" /></p>
      <p>&nbsp;</p>
    </blockquote>
    <p class="style8"><u>LINEAR ODE</u></p>
    <ul>
      <ul>
        <li>An ode is linear if it can be put into the following form: </li>
      </ul>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/1.3.jpg" width="265" height="32" /></p>
        <p>&nbsp;	</p>
        <p class="style7">- where a(x), b(x), and f(x) are functions of x only (i.e. , y occurs only in the derivatives and in the so - called 'zero order' term c(x)). </p>
      </blockquote>
      <li>Homogeneous, linear ode is one in which the right hand side, f(x), is zero.</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.4.jpg" width="235" height="32" /></p>
      <p>&nbsp;</p>
    </blockquote>
    <p class="style8"><u>SOLUTION BY SEPARATION OF VARIABLES</u></p>
    <ul>
      <li>If the first order ode can be put into the form:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.5.jpg" width="105" height="32" /></p>
    </blockquote>
    <ul>
      <li>then a separation variable:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.6.jpg" width="105" height="32" /></p>
    </blockquote>
    <ul>
      <li>in principle, both side of this equation can now be integrated to give</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/1.7.jpg" width="168" height="42" /><span class="style7">; where A is constant</span> </p>
    </blockquote>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" /><strong>Example:</strong></p>
    <p>Solve the equation <img src="<?php echo base_url();?>assets/diffeq/images/1.8.jpg" width="120" height="32" /></p>
    <blockquote>
      <p class="style7">Solution:</p>
      <blockquote>
        <p class="style7">Separate the variable </p>
        <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/1.9.jpg" width="110" height="32" /></p>
        <p class="style7">&nbsp;</p>
        <p><span class="style7">Integrate both sides</span></p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.jpg" width="110" height="32" /></p>
        <p>&nbsp;</p>
        <p><span class="style7">Use the partial fractions</span></p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.1.jpg" width="160" height="32" /></p>
        <p>&nbsp;</p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.3.jpg" width="190" height="32" /></p>
        <p>&nbsp;</p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.2.jpg" width="160" height="16" /></p>
        <p>&nbsp;</p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.4.jpg" width="210" height="32" /></p>
        <p>&nbsp;</p>
        <p><img src="<?php echo base_url();?>assets/diffeq/images/2.5.jpg" width="180" height="32" /></p>
      </blockquote>
    </blockquote>
  </div>