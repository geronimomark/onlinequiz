<div class="style1" id="content">
    <h2 align="center" class="style6">differential equation </h2>
    <h2 align="center">ordinary differential equation </h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li>The general solution:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffeq/images/2.9.jpg" width="145" height="16" /></p>
    </blockquote>
    <p><span class="style7">- where y' = v, v is a function of x representing the derivative</span> <span class="style7">, using differentiation y&quot; = v'</span> </p>
    <p class="style7">- therefore: v' + q(x)v = f(x) </p>
    <p class="style7">- After doing such integration, we have:</p>
    <blockquote>
      <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.jpg" width="270" height="22" /> </p>
      <p class="style7">&nbsp;</p>
    </blockquote>
    <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="28" height="16" />Example</p>
     <p>Find the general solution of x2y&quot; + 2xy' = 1 , x <img src="<?php echo base_url();?>assets/diffeq/images/Second Order_clip_image002.gif" alt="1" width="10" height="20" /> 1 </p>
     <blockquote>
       <p class="style7">Solution:</p>
       <blockquote>
         <p class="style7">Using the substitution of y' = v </p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.1.jpg" width="100" height="32" /></p>
         <p class="style7">Using integration </p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.3.jpg" width="180" height="32" /></p>
         <p class="style7">where I = x<sup>2</sup></p>
         <p class="style7">Substitute the value of I</p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.4.jpg" width="100" height="16" />  </p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.5.jpg" width="90" height="32" /></p>
         <p class="style7">Integrating gives</p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.6.jpg" width="90" height="17" /> </p>
         <p class="style7">Where c<sub>1</sub> is constant </p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.8.jpg" width="85" height="32" /></p>
         <p class="style7">But v = y'</p>
         <p class="style7"><img src="<?php echo base_url();?>assets/diffeq/images/3.9.jpg" width="100" height="32" /> </p>
       </blockquote>
    </blockquote>
     <ul>
       <li>The general solution of a 2nd order ode will always contain two arbitrary constant.</li>
       <li>Initial process: y = y<sub>0</sub> at x = x<sub>0</sub> and y' = y<sub>0</sub>' at x = x<sub>0</sub></li>
       <li>Alternative way to write the condition: y(x<sub>0</sub>) = y<sub>0</sub> and y'(x<sub>0</sub>) = y<sub>0</sub>' </li>
     </ul>
     <p>&nbsp;</p>
     <blockquote><blockquote>
       <p class="style7">&nbsp;</p>
       </blockquote>
    </blockquote>
  </div>