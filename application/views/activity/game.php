﻿<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Online Game System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/activity/web/newcss.css">
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script language="javascript" type="text/javascript">
    function help1()
    {
        var answer = parseInt(document.getElementById("ansNum").value);
        var a=0, b=1, c=2, d=3;
        var check=0;

        var cat_id = "";
        var cat_name = "";

        do
        {
            var rand = Math.floor(Math.random() * 4);

            if(rand!=answer && rand==a)
            {
                document.getElementById("a").value="";
                check+=1;
                a=-1;
            }

            else if(rand!=answer && rand==b)
            {
                document.getElementById("b").value="";
                check+=1;
                b=-1;
            }
            else if(rand!=answer && rand==c)
            {
                document.getElementById("c").value="";
                check+=1;
                c=-1;
            }
            else if(rand!=answer && rand==d)
            {
                document.getElementById("d").value="";
                check+=1;
                d=-1;
            }
        }while(check<2);
        document.getElementById("h1").style.visibility="hidden";
    }
    function help2()
    {
        getQ();
        document.getElementById("h2").style.visibility="hidden";
    }
    function help3()
    {
        var answer = document.getElementById("ans").value;
        var rand = Math.floor(Math.random() * parseInt(answer.length-1));
        var display = "One character of the correct answer is: ";
        display += answer.charAt(rand);
        alert(display);
            //document.getElementById("q").value += "\n\n "+"(one correct character " +answer.charAt(rand) + ")";
            document.getElementById("h3").style.visibility="hidden";
        }
        function help4()
        {
            var amt = parseInt(document.getElementById("amount").value)/2;
            document.getElementById("amount").value = amt;
            document.getElementById("h4").style.visibility="hidden";

        }

        function getQ()
        {
            var doneQuest = ""; //list of finished questions
            var questSplit = [1]; //question holder
            var quest = [30]; //database for question
            var picture = [30]; //database for pictures
            var rand = 0, c=0; //used for all randomized objects
            var answerDB = [4];//array database
            var answerSplit = [1];//choice holder
            var answerDisplay = [4];//random displayer


            var questCategory = "";
            questCategory = document.getElementById("category").value;

            if(questCategory=="1")
            {
                //questions
                quest[1] = "In algebra, what refers to the operation of root extraction";
                quest[2] = "What refers to the difference between an approximate value of a quantity and its exact value or true value?";
                quest[3] = "If the roots of an equation are zero, then these roots are classified as what type of solution?";
                quest[4] = "What refer to a sequence of numbers where the succeeding term is greater than preceding term?";
                quest[5] = "Convergent series is a sequence of decreasing numbers or when the succeeding term is _________ preceeding term.";
                quest[6] = "In complex algebra, we use a diagram to represent a complex plane commonly called _____.";
                quest[7] = "What is the sequence of numbers such that successive term differ by a constant?";
                quest[8] = "What is a frequency curve which is composed of a series of rectangles connected with the steps as the base and the frequency as the height?";
                quest[9] = "The graphical representation of a cumulative frequency distribution in a set of statistical data is called ________.";
                quest[10] = "If a = b, then b = a. This illustrate which axiom in algebra";
                quest[11] = "What refers to the smallest natural number for which 2 natural numbers are factors";
                quest[12] = "To compute the value of n factorial, in symbolic form (n!), where n is large number , we use formula called";
                quest[13] = "It is a measure of relationship between two variable.";
                quest[14] = "In character population which is measurable";
                quest[15] = "The quartile deviation is a measure of ";
                quest[16] = "The problem of the region bounded by two concentric circles is called";
                quest[17] = "When two planes intersect with each other, the amount of divergence between the two planes is expressed by measuring the__________.";
                quest[18] = "It is a polyhedron of which two faces are equal polygons in parallel planes, and the other faces are parallelograms.";
                quest[19] = "Each of the faces of a regular hexahedron is a";
                quest[20] = "An annulus is a plane figure, which composed of two concentric circles. The area of the annulus can be calculated by getting the difference between the area of a larger circle. Also, it can be calculated by removing the hole. This method is called";
                quest[21] = "Which of the following quadratic equations will have two real and distinct roots?";
                quest[22] = "Radicals can be added if they have the same radicand and the same";
                quest[23] = "Drawing a card from a deck of cards is called";
                quest[24] = "The set of integers does not satisfy the closure property under the operation of";
                quest[25] = "A number which can be expressed as the quotient of two integers is";
                quest[26] = "All board reviewees are not more than 25 years old. This statement implies that they are";
                quest[27] = "The roots of the equation 6x^2 - 61x + 143 = 0 are";
                quest[28] = "A set of elements that is taken without regard to the order in which the elements are arranged is called a:";
                quest[29] = "If the value of the discriminant of a quadratic equation is 1.25 , then the root of the quadratic are:";
                quest[30] = "Find the value of k that will make x^2 - 28x + k a perfect square trinomial";
                //choices
                answerDB[1] = "Evolution_Resolution_Revolution_Involution";
                answerDB[2] = "Absolute Error_Relative Error_Mistake_Difference";
                answerDB[3] = "Trivial Solution_Conditional Solution_Extraneous Solution_Hypergolic Solution";
                answerDB[4] = "Divergent Sequence_Isometric Sequence_Convergent Sequence_Dissonant Sequence";
                answerDB[5] = "Lessr Than_Equal to_Twice_Greater Than";
                answerDB[6] = "Argand diagram_Maxwell diagram_Venn diagram_Block diagram";
                answerDB[7] = "Arithmetic Progression_Infinite Geometric Progression_Geometric Progression_Harmonic Progression";
                answerDB[8] = "Frequency Distribution_Ogive_Histogram_Bar graph";
                answerDB[9] = "ogive_leptokurtic_histogram_kurtosis";
                answerDB[10] = "Symmetric axiom_Reflexive axiom_Transitive axiom_Replacement  axiom";
                answerDB[11] = "Least Common Multiple_Least Common Divison_Least Common Denominator_Least Common Factor";
                answerDB[12] = "Stirling Approximation_Richardson-Duchman Formula_Diophantine Formula_Matheson’s Formula";
                answerDB[13] = "Correlation_Equation_Function_Relation";
                answerDB[14] = "Frequency_Unit_Distribution_Sample";
                answerDB[15] = "dipersion_division_certainty_central tendency";
                answerDB[16] = "annalus_circular disk_ring_washer";
                answerDB[17] = "dihedral angle_polyhedral angle_reflex angle_plane angle";
                answerDB[18] = "Prism_Tetrahedron_Frustum_Prismatoid";
                answerDB[19] = "square_triangle_rectangle_hexagon";
                answerDB[20] = "Law of extremities_Law of deduction_Law of reduction_Law of composition";
                answerDB[21] = "6x^2 - 61x + 143 = 0_x^2 - 22x + 121 = 0_9x^2 - 6x + 1 = 0_6x^2 - 5x + 4";
                answerDB[22] = "order_power_exponent_coefficient";
                answerDB[23] = "an experiment_a trial_an outcome_an event";
                answerDB[24] = "division_multiplication_subtraction_addition";
                answerDB[25] = "irrational_rational_natural_prime";
                answerDB[26] = "25 years old or less_25 years old or more_at least 25 years old_less than 25 years old";
                answerDB[27] = "real and distinct_real and equal_complex and distinct_complex and unequal";
                answerDB[28] = "combination_permutation_progression_probability";
                answerDB[29] = "real and unequal_real and equal_complex and unequal_imaginary and distinct";
                answerDB[30] = "196_169_144_121";


                picture[1] = "";
                
            }
            else if(questCategory=="2")
            {
                //questions
                quest[1] = "In how many ways can a picture be painted by using two or more of 7 different colors?";
                quest[2] = "What is the probability of getting a number 4 thrice in five tosses of a die?";
                quest[3] = "In how many ways can 8 persons be seated at a round table if a certain 2 are sit next to each other?";
                quest[4] = "If x: y: z = 4: -3:2 and 2x + 4y - 3z = 20, find the value of x.";
                quest[5] = "At a math contest, the judges eliminate 1/3 of the contestants after each half hour. If 81 contestants were present at the start, how many would be left after 2 hours?";
                quest[6] = "Getting an odd number by throwing a die is called";
                quest[7] = "Two prime numbers, which differ by two, are called prime twins. Which of the following pairs of numbers are prime twins?";
                quest[8] = "The probability of A's winning a game against B is called 1/3. What is the probability that A will win at least two off total of 3 games?";
                quest[9] = "If P (n+1, 4) = 2P(n, 4)";
                quest[10] = "What is the probability of getting the number 1 thrice when a die is tossed 5 times?";
                quest[11] = "In how many ways can 7 boys be seated in a row so that 3 boys are always seated together?";
                quest[12] = "What is the probability of obtaining at least 4 tails when a coin is tossed five times?";
                quest[13] = "Mr. Diaz can finish a job in 9 hours. After working for 5 hrs, he decided to take a rest. Mr. Torres helped Mr. Diaz finished the job in 2 hrs and 20 minutes. How long would it take Mr. Torres to do the job alone?";
                quest[14] = "Find 2 numbers whose sum is 12 and the sum of their squares is 74.";
                quest[15] = "Two jeepneys start at the same point but are going in different directions. If jeepney A runs at the rate of 60 km/hr and jeepney B at 50 km/hr and both starts at the same time, when will the two jeepney be 550 km apart?";
                quest[16] = "A man and a boy can dig a trench in 20 days. It would take the boy 9 days longer to dig it alone than it would take the man. How long would it take the boy to dig it alone?";
                quest[17] = "What is the sum of prime numbers between 1 and 15?";
                quest[18] = "The sum of integers that are exactly divisible by 15 between 288 and 887 is";
                quest[19] = "If 16 is 4 more than 3x, find the value of 2x - 5.";
                quest[20] = "Which of the following quadratic trinomial is NOT factorable?";
                quest[21] = "If the radius of the circle is diminished by 20%, then its area is diminished by";
                quest[22] = "Two sisters are 14 and 21 years old respectively. In how many years will the ratio of their ages be 3:4?";
                quest[23] = "Car A runs 30 km/hr less than Car B. Car A covers 250 km in the same time car B travels 400 km. Find the rate of each.";
                quest[24] = "The product of two irrational numbers is  ";
                quest[25] = "The length of a rectangle is increased by 50%. By what percent wouldt the width have to be decreased to maintain the same area?";
                quest[26] = "The altitude of the cone is equal to half the diameter of the sphere circumscribed about it. How many times is the volume of the sphere greater than that of the cones?";
                quest[27] = "The perimeter of a rectangle is 22. If one of the rectangle is doubled and the other tripled, the perimeter would be 32 more than the perimeter of the original rectangle. What are the sides of the rectangle?";
                quest[28] = "If a polygon has 54 diagonals, then it must have  ";
                quest[29] = "A trench is constructed so that its cross section is trapezoid the area of which is 21 sq. ft. if the shorter base is 2 times its height and the longer base is 5 ft. longer than its height. Find the length of the shorter base.";
                quest[30] = "If the acute angles of a right triangle are in the ratio 1:2, then the number of degrees in the small angle is:";			
                //choices
                answerDB[1] = "120_110_128_131";
                answerDB[2] = "0.0322_0.0232_0.3220_0.2330";
                answerDB[3] = "1,440_1,008_4,140_5,040";
                answerDB[4] = "-8_7_-4_6";
                answerDB[5] = "16_18_12_10";
                answerDB[6] = "an event_a trial_an outcome_an experiment";
                answerDB[7] = "(17, 19)_(13, 15)_(7, 9)_(1, 3)";
                answerDB[8] = "7/27_8/27_19/278_15/27";
                answerDB[9] = "7_10_-7_4";
                answerDB[10] = "0.0322_0.0355_0.0233_0.0223";
                answerDB[11] = "720_360_144_270";
                answerDB[12] = "0.1875_0.1785_0.1758_0.1857";
                answerDB[13] = "5 hrs and 15 min_6 hrs and 20 min_4 hrs and 10 min_3 hrs and 5 min";
                answerDB[14] = "7 and 5_6 and 6_4 and 8_3 and 9";
                answerDB[15] = "5 hrs_4 hrs_6 hrs_7 hrs";
                answerDB[16] = "45 days_54 days_35 days_36 days";
                answerDB[17] = "41_38_39_42";
                answerDB[18] = "23,700_23,805_22,815_21,810";
                answerDB[19] = "3_2_4_5";
                answerDB[20] = "6x^2 - 52x - 60_6x^2 - 61x + 143_6x^2 - 7x – 343_6x^2 + 5x – 4";
                answerDB[21] = "36%_52%_46%_25%";
                answerDB[22] = "6_7_8_9";
                answerDB[23] = "50 km/hr and 80 km/hr_60 km/hr and 90 km/hr_70 km/hr and 100 km/hr_800 km/hr and 110 km/hr";
                answerDB[24] = "sometimes irrational_always irrational_never irrational_rational";
                answerDB[25] = "33 1/3_50_66 2/3_150";
                answerDB[26] = "4_½_2_¼";
                answerDB[27] = "6 and 5_8 and 7_10 and 9_12 and 11";
                answerDB[28] = "12 sides_10 sides_11 sides_13 sides";
                answerDB[29] = "6_5_4_3";
                answerDB[30] = "30_20_45_90";
                
            }
            else if(questCategory=="3")
            {
                //questions
                
                quest[1] = "The length of the wire fence around a circular flowerbed is 10p feet. The area of the flowerbed in sq. ft. is";
                quest[2] = "The perimeter of rectangle is 28 m and its diagonal is 10 m. Find the area of the rectangle.";
                quest[3] = "The parabola y= 4 - x^2 opens";
                quest[4] = "The intersection of the medians of the triangle whose vertices are (-6, -8), (3, -5), and (4, -2) are at";
                quest[5] = "A parabola maybe defined as the set of points that are equidistant from a fixed point and a fixed line. The fixed point is called the focus and the fixed line is called the";
                quest[6] = "If the eccentricity of a conic section is greater than one, then it is";
                quest[7] = "The vertex of the parabola y^2 - 4x + 6y + 13 = 0 is at";
                quest[8] = "The equation of a line passing through (2, 2) and (4, 8) is y = _________.";
                quest[9] = "The straight line equation y = x - 5 passes through the points";
                quest[10] = "The equation of the line passing through (0, 4) and parallel to the x-axis is";
                quest[11] = "The area of the triangle whose vertices are (-3, 8), (7, 4), and (5, -1) is";
                quest[12] = "A quadrilateral whose four sides are equal and parallel with no angle equal to a right angle is called ________.";
                quest[13] = "A tree stands vertically on a hillside that makes an angle of 22 deg. with the horizontal. From a point 15 m down the hill from the base of the tree, the angle of elevation of the top of tree is 44 deg. Find the distance from the point to the top of the flagpole.";
                quest[14] = "A flagpole 49 m high is situated at the top of a hill. At a point 183 m down the hill, the angle between the surface of the hill and line to the top of the flagpole is 8 degrees. Find the distance from the point to the top of the flagpole";
                quest[15] = "A 90-degree arc on the earth is equal to how many nautical miles in length?";
                quest[16] = "The plane of a small cirlce on a sphere of radius 25 cm is 7 cm from the center of the sphere. Find the radius of the small circle.";
                quest[17] = "Find the kilometers of the length of an arc of a great circle on the earth, if its length is 12.5 degrees";
                quest[18] = "A spherical triangle has angles A, B, and C, each of which is less than 180 degrees. Which of the following is true?";
                quest[19] = "A spherical triangle, which contains at least one side equal to a right angle, is called";
                quest[20] = "Express in hour, minutes and seconds the time corresponding to 260 deg and 34'.";
                quest[21] = "An icosahedron is a regular polyhedron having";
                quest[22] = "The angle formed by two intersecting planes is called a";
                quest[23] = "Find the area of pentagon whose apothem is 10 cm.";
                quest[24] = "The radii of two sphere are in the ratio 3:4 and the sum of their surface area is 2500 p sq. cm. Find the radius of the smaller sphere.";
                quest[25] = "A face diagonal of a cube is 4 cm. Find the volume of the cube";
                quest[26] = "The volume of a cube is reduced to _________ if all sides are halved.";
                quest[27] = "The sum of angles of a polygon of n sides equals.";
                quest[28] = "Each exterior angle of a polygon of n sides is";
                quest[29] = "The median of a trapezoid equals ______ the sum of the bases";
                quest[30] = "A pyramid has a base whose sides are 10 m, 16 m, and 18 m. If the altitude of the pyramid is 20 m, find the volume of the inscribed cone.";	
				//choices
				answerDB[1] = "25p_5p_50p_100p";
                answerDB[2] = "48 sq. m_38 sq. m_10 sq. m_28 sq. m";
                answerDB[3] = "downward_upward_to the left_to the right";
                answerDB[4] = "(1/3, -5)_(-1/3, 5)_(-1/3, -5)_(0, 0)";
                answerDB[5] = "directrix_tangent line_latus rectum_asymptote";
                answerDB[6] = "a hyperbola_a parabola_a circle_an ellipse";
                answerDB[7] = "(1, -3)_(-3, 1)_(0, 0)_(1, 1)";
                answerDB[8] = "3x – 4_x – 4_3x + 2_x + 2";
                answerDB[9] = "(0, -4)_(2, 2)_(3, 0)_(6, 3)";
                answerDB[10] = "y = 4_4x + y = 0_y = x^2 + 4_4y - 2 = x";
                answerDB[11] = "30 sq. units_60 sq. units_29 sq. units_40 sq. units";
                answerDB[12] = "rhombus_trapezoid_parallelepiped_parallelogram";
                answerDB[13] = "7. 81 m_7. 71 m_7. 61 m_7.51 m";
                answerDB[14] = "223.1 m_232.1 m_322.1 m_233.1 m";
                answerDB[15] = "5,400_5,600_5,200_5,000";
                answerDB[16] = "24_23_22_21";
                answerDB[17] = "1389_1370_1352_1333";
                answerDB[18] = "A + B + C > 180_A + B + C = 360_A + B + C > 360_A + B + C = 180";
                answerDB[19] = "a quadrantal triangle_an isosceles triangle_a right triangle_a polar triangle";
                answerDB[20] = "17 hours, 22 minutes and 16 seconds_17 hours, 23 minutes and 16 seconds_b.	17 hours, 21 minutes and 16 seconds_17 hours, 20 minutes and 16 seconds";
                answerDB[21] = "20 faces_18 faces_16 faces_12 faces";
                answerDB[22] = "dihedral angle_central angle_vertical angle_face angle";
                answerDB[23] = "336.72 sq. cm_373.65 sq. cm_327.36 sq. cm_363.27 sq. cm";
                answerDB[24] = "15 cm_10 cm_20 cm_25 cm";
                answerDB[25] = "22. 63 cm^3_21.64 cm^3_23.62 cm^3_24.64 cm^3";
                answerDB[26] = "1/8_1/16_1/4_1/2";
                answerDB[27] = "(n - 2) * 180 degrees_(n - 2)(180 degrees)/n_360 degrees_180 degrees";
                answerDB[28] = "360/n_180/n_(n - 2)(180)/n_always 72 degrees";
                answerDB[29] = "1/2_1/3_1/4_1/5";
                answerDB[30] = "274.18_1715.36_325.1_376.57";
                

            }
            else if(questCategory=="4")
            {
 //questions
 
 quest[1] = "The tangent and a secant are drawn to a circle from the same external point. If the tangent is 6 inches and the external segment of the secant is 3 inches, the length of the secant is ______ inches";
 quest[2] = "Two secant from a point outside the circles are 24 and 32. If the external segment of the first is 8, the external of the second is:";
 quest[3] = "A trench is constructed so that its cross section is a trapezoid the area of which is 21 square feet. If the shorter base is 2 times its height and the longer base is 5 feet longer than its height, find the height of the trench";
 quest[4] = "If the circular cylindrical tank axis horizontal, diameter 1 meter, and length 2 meters is filled with water up to a depth of 0.75 meter, how much water is the tank?";
 quest[5] = "The diagonal of a rhombus are equal to 30 cm and 40 cm. Find the distance between the plane of the rhombus and a point M if the latter is 20 cm distant from each of its sides.";
 quest[6] = "During the recent olympics on 4-men relay team completed in a 1600 meter relay have the following individual speed, R1 = 26 kph, R2 = 27 kph, R3 = 28 kph, R4 = 29 kph. What is the average speed of the team?";
 quest[7] = "A garden can be cultivated by 8 boys in 5 days. 5 men can do the same work in 6 days. How long would it take for 8 boys and 5 men to finish the job?";
 quest[8] = "At what time between 7 and 8 o'clock are the hands of the clock, at the right angles?";
 quest[9] = "At what time between 7 and 8 o'clock are the hands of the clock at 180 degrees apart?";
 quest[10] = "At what time between 7 and 8 o'clock are the hands of the clock together?";
 quest[11] = "A man left his office for a business appointment one afternoon and noticed his watch at past 2 o'clock. Between two to three hours later, the man returned and noticed that the hands of the clock have exchanged places. What time did he leave and arrive?";
 quest[12] = "Separate 132 into 2 such that the larger divided by the smaller, the quotient is 6 and the remainder is 13. Find the numbers";
 quest[13] = "A number of two digits divided by the sum of the digits, the quotient is 7 and the remainder is 6. If the digits of the number are interchanged the resulting number exceeds three times the sum of the digits by 5. What is the number?";
 quest[14] = "The total tank of the car is filled with 50 liters of alcogas 25% of which is alcohol. How much of the mixture must be drawn off which when replaced by pure alcohol will yield a 50 - 50% alcogas?";
 quest[15] = "An alloy contains 25% silver and 10% copper. How much silver and how much copper must be added to 20 lbs of the alloy to obtain an alloy containing 38% silver and 36% copper?";
 quest[16] = "A motorist is traveling from A to B at a constant rate of 30 kph and return from B to A at constant rate of 20 kph. What is his average velocity?";
 quest[17] = "If A and B are not mutually exclusive events, then the probability of the joint occurrence of A and B or P(A and B) is";
 quest[18] = "When the occurence or no occurence of event A has no effect on the occurence of event B, then A and B are said to be";
 quest[19] = "The probability that the Ginebra basketball team will win the championship is assessed as being 1/3. Find the odds that the team will win.";
 quest[20] = "The odds that a reviewee will not pass the board exam are 1:4. Find the probability that the reviewee will not pass the exam.";
 quest[21] = "An experiment consists of selecting a random of three television tubes form a lot of 5 containing 2 defective. What is the probability of getting exactly two defective tubes?";
 quest[22] = "Out of 1,000 ECE reviewees, the probability that a reviewee picked at random is over 25 years old is 0.25 and the probability that he is less than 20 years old is 0.15. What is the probability that a reviewee picked at random is between 20 and 25 years old?";
 quest[23] = "It is the highest score distribution minus the lowest score in the distribution";
 quest[24] = "It is a measure of central tendency, which depends upon the number of score and not the magnitude of the scores.";
 quest[25] = "If sin x = cos y, then";
 quest[26] = "cos x > 0 and sin x < 0, then x is in quadrant";
 quest[27] = "If an angle is 9 degrees more than twice its supplement then the number of degrees in the angle is";
 quest[28] = "In the triangle, one angle is 3 times as big as the other. If the sum of these angle is 120 degrees. Find the measure of the third angle.";
 quest[29] = "A triangle has sides of 4, 6, and 8 units long. If the shortest side of a similar triangle is 12 unit long, what are the lengths of the other two sides?";
 quest[30] = "Find the value of (1 + i)^12";	
				//choices
				answerDB[1] = "12_15_14_18";
                answerDB[2] = "6_8_10_12";
                answerDB[3] = "3 ft_4 ft_5 ft_6 ft";
                answerDB[4] = "1.2637 cubic meter_ 3.1415 cubic meter_1.51 cubic meter_2 cubic meter";
                answerDB[5] = "16 cm_15 cm_18 cm_17 cm";
                answerDB[6] = "27.45_28.46_26.44_25.44";
                answerDB[7] = "20/11 days_15/11 days_30/11 days_45/11 days";
                answerDB[8] = "7:22_7:18_7:15_7:24";
                answerDB[9]	= "7:05_7:10_7:11_7:30"; 	
                answerDB[10] = "7:30_7:38_7:45_7:20";
                answerDB[11] = "2:26.01 and 5:12.17_2:15 and 5:30_2:20 and 5:32_2:23 and 5:40";
                answerDB[12] = "17 and 115_20 and 112_24 and 108_30 and 102";
                answerDB[13] = "83_72_48_39";
                answerDB[14] = "16.7_15.6_17.8_18";
                answerDB[15] = "14 lbs silver_15 lbs silver_20 lbs silver_30 lbs silver";
                answerDB[16] = "24_30_15_8";
                answerDB[17] = "not equal to zero_equal to zero_equal to unity_infinite";
                answerDB[18] = "independent_mutually exclusive_complementary_dependent";
                answerDB[19] = "1:2_2:1_1:3_3:1";
                answerDB[20] = "0.20_0.80_0.25_0.75";
                answerDB[21] = "0.30_0.25_0.35_0.40";
                answerDB[22] = "0.60_0.55_0.65_0.70";
                answerDB[23] = "range_deviation_variance_interval";
                answerDB[24] = "median_mean_mode_geometric mean";
                answerDB[25] = "x + y = 90 degrees_xy = 90 degrees_x - y = 90 degrees_none of the above";
                answerDB[26] = "IV_III_II_1";
                answerDB[27] = "123_114_57_27";
                answerDB[28] = "60 degrees_90 degrees_100 degrees_110 degrees";
                answerDB[29] = "18 and 24_19 and 23_17 and 25_16 and 26";
                answerDB[30] = "-64_-64i_-36_-36i";
            }
            else if(questCategory=="5")
            {
                quest[1] = "What is the next term in the geometric sequence 16, -4, 1, -1/4,...? ";
                quest[2] = "A manufacturing company processes raw ore. The number of tons of refined material the company can produce during t days using Process A is A(t) = t2 + 2t and using process B is B(t) = 10t. The company has only 7 days to process ore and must choose 1 of the processes. What is the maximum output of refined material, in tons, for this time period?";
                quest[3] = "Listed below are 4 functions, each denoted g(x) and each involving a real number constant c > 1. If f(x) = 2x, which of these 4 functions yields the greatest value for f(g(x)), for all x > 1?";
                quest[4] = "If the function f satisfies the equation f(x + y) = f(x) + f(y) for every pair of real numbers x and y, what are the possible value of f(0)?";
                quest[5] = "The imaginary number i is defined such that i2 = -1. What does i + i2 + i3 + ..... + i23 equal?";
                quest[6] = "In an arithmetic series, the terms of the series are equally spread out. For example in 1 + 5 + 9 + 13 + 17, consecutive terms are 4 apart. If the first term in arithmetic series is 3. the last term is 136, and the sum is 1,390, what are the first 3 terms?";
                quest[7] = "Which of the following equations could represent the graph?";
                quest[8] = "log x + log 20 = 2. Then x";
                quest[9] = "Find the equation of line passing through (2, -4) and parallel to 3x + 2y = 4";
                quest[10] = "Write the formula for the nth term: 3, 9, 27, 81, 243.....";
                quest[11] = "A bag can contains 5 apples and 3 oranges. If you select 4 pieces of fruit without looking, how many ways can you get 4 apples?";
                quest[12] = "How many ways can a president, vice - president, and secretary be chosen from a club with 10 members?";
                quest[13] = "Your company uses the quadratic model y = -4.5x^2 + 150x to represent the average number of new customers who will be signed on (x) weeks after the release of your new service. How many new customers can you expect to gain in week 8?";
                quest[14] = "Identify the given equation as an identity, an inconsistent equation, or a conditional equation. 3(2f + 48) = 6f + 144";
                quest[15] = "Solve the equation [1/3]^x = 18. Round your answer to three decimal places.";
                quest[16] = "A triangular lake-front lot has a perimeter of 1900 feet. One side is 200 feet longer than the shortest side, while the third is 500 feet longer than the shortest side. Find the lengths of all three sides";
                quest[17] = "If $5000 is invested in an account that pay interest compounded continuously, how long will it take to grow to $10,000 at 4.25%?";
                quest[18] = "The perimeter of a rectangle is 48 m. If the width were doubled, and the length were increased by 19 m, the perimeter would be 100 m. What are the length and width of the rectangle?";
                quest[19] = "Find the sum of geometric series; 3 - 9 + 27 - 81 + 243";
                quest[20] = "The number of bacteria growing in an incubation culture increases with time according to B = 8400(3)^x, where x is time in days. Find the number of bacteria when x = 0 and x = 5.";
                quest[21] = "Multiply both sides of an equation by any number results in an equivalent equation";
                quest[22] = "An equation that satisfied for every value of the variable for which both sides are defined is called a(n) ";
                quest[23] = "An equation of the form ax + b = 0 is called a(n) ________ equation or a(n) ________________ equation.";
                quest[24] = "Clarissa and Shawna, working together, ca paint the exterior of a house in 6 days. Clarissa by herself can complete this job in 5 days less than Shawna. How long will it take Clarissa to complete the job by herself?";
                quest[25] = "A search plane has a cruising speed of 250 miles per hour and carries enough fuel for at mosh 5 hours of flying. If there is a wind that averages 30 miles per hour and the direction of the search is with the wind one way and against it the other, how far can the search plane travel before it has to turn back?";
                quest[26] = "A life raft, set drift from a sinking ship 150 miles offshore, travels directly toward a Coast Guard station at the rate of 5 miles per hour. At the time that the raft is set adrift, a rescue helicopter is dispatched from the Coast Guard station. If the helicopter's average speed is 90 miles per hour, how long will it take the helicopter to reach the life raft?";
                quest[27] = "A metra commuter train leaves Union Station in Chicago at 12 noon. Two hours later, an Amtrak train leaves on the same track, traveling at an average speed that is 50 miles per hour faster than the Metra train. At 3 PM the Amtrak train is 10 miles behind the commuter train. How fast is each going?";
                quest[28] = "The total amount barrowed (whether by an individual from a bank in the form of a loan or by a bank from an individual in the form of a savings account) is called the";
                quest[29] = "What rate of interest compoinded annually is required to double an investment in 3 years?";
                quest[30] = "How long does it take for an investment to double in value if it is invested 8% compounded monthly?";
                
                
                answerDB[1] = "1/16_1/8_-1/8_0";
                answerDB[2] = "70_63_10_8";
                answerDB[3] = "g(x) = cx_g(x) = c/x_g(x) = x/c_g(x) = x – c";
                answerDB[4] = "0 only_ 0 and 1 only_Any real positive number_Any real number";
                answerDB[5] = "-1_0_ –i_i";
                answerDB[6] = "3, 10, 17_3, 23, 43_3, 36, 66_3, 12, 24";
                answerDB[7] = "y = 3x^3 - 18x -12_y = x^2 – 4_y = x^3_y = x^2";
                answerDB[8] = "5_-18_80_1/10";
                answerDB[9] = "y = -3/2 x – 1_y = -2/3 x - 8/3_y = 3/2 x – 7_y = -3/2 x + 1";
                answerDB[10] = "an = 3^n_an = 3^(n - 1) + 2_an = 3n_an = 6n – 3";
                answerDB[11] = "5_8_15_10";
                answerDB[12] = "720_30_120_6";
                answerDB[13] = "912 customers_456 customers_312 customers_1164 customers";
                answerDB[14] = "Conditional (All real numbers)_Conditional (1)_Identity_Inconsistent";
                answerDB[15] = "-2.631_2.631_ -0.380_-6.000";
                answerDB[16] = "400 ft, 600 ft, 900 ft_500 ft, 500 ft, 500 ft_100 ft, 200 ft, 300 ft_500 ft, 700 ft, 1000 ft";
                answerDB[17] = "16.3 years_12.8 years_2.9 years_14.1 years";
                answerDB[18] = "width 7, length 17_width 7, length 12_width 12, length 12_width 17, length 7";
                answerDB[19] = "183_-183_-363_363";
                answerDB[20] = "8,400 ; 2,041,200_8,400 ; 226,800_8,400 ; 126,000_25,200 ; 2,041,200";
                answerDB[21] = "False_True_None_None";
                answerDB[22] = "identity_non-linear_substitution_linear";
                answerDB[23] = "linear, first degree_non-linear , first degree_linear, second degree_non-linear, second degree";
                answerDB[24] = "It takes Clarissa 10 days by herself_It takes Clarissa 11 days by herself_It takes Clarissa 12 days by herself_It takes Clarissa 13 days by herself";
                answerDB[25] = "The search plane can go as far as 616 miles_The search plane can go as far as 614 miles_The search plane can go as far as 724 miles_The search plane can go as far as 726 miles";
                answerDB[26] = "The helicopter will reach the life raft in a little less than 1 hr and 35 min_The helicopter will reach the life raft in a little less than 1 hr and 20 min_The helicopter will reach the life raft in a little less than 1 hr and 50 min_The helicopter will reach the life raft in a little less than 1 hr and 15 min";
                answerDB[27] = "The Metra commuter averages 30 mi/hr; The Amtrak averages 80 mi/hr_The Metra commuter averages 30 mi/hr; The Amtrak averages 65 mi/hr_The Metra commuter averages 25 mi/hr; The Amtrak averages 80 mi/hr_The Metra commuter averages 25 mi/hr; The Amtrak averages 65 mi/hr";
                answerDB[28] = "principal_initial_investment_capital";
                answerDB[29] = "25.992 %_26.992 %_24.992 %_23.992 %";
                answerDB[30] = "about 8.69 yeary_about 6.41 year_about 5.12 year_about 9.25 year";
                
                picture[7] = "<?php echo base_url();?>assets/activity/web/Al1.jpg";
            }
            else if(questCategory=="6")
            {
                quest[1] = "If Tanisha has $ 100 to invest at 8% per annum compounded monthly, how long will it be before she has $ 150? If the compounding is continuous how long will it be?";
                quest[2] = "A satelite dish is shaped like paraboloid of revolution. The signals that emanate from a satellite strike the surface of the dish and are reflected to a single point, where the receiver is located. If the dish is 10 feet across at its opening and 4 feet deep at its center, at what position should the receiver be placed?";
                quest[3] = "The reflector of a flashlight in the shape of a paraboloid of revolution. Its diameter is 4 inches and its depth is 1 inch. How far from the vertex should the light bulb be placed so that the rays will be reflected parallel to the axis?";
                quest[4] = "The cable of suspension bridge are in the shape of a parabola, as shown in figure. The towers supporting the cable are 600 feet apart and 80 feet high. If the cables touch the road surface midway between the towers, what is the height of the cable from the road at a point 150 feet from the center of the bridge?";
                quest[5] = "What is the next term in the geometric sequence 16, -4, 1, -1/4";
                quest[6] = "A manufacturing company processes raw ore. The number of tons of refined material the company can produce during t days using Process A is A(t) = t^2 + 2t and using Process B is B(t) = 10t. The company has only 7 days to process ore and must choose 1 of the processes. What is the maximum output of refined material, in tons, for this time period?";
                quest[7] = "For the 2 functions, f(x) and g(x), tables of values are shown below. What is the value of g(f(3))?";
                quest[8] = "If the function f satisfies the equation f(x + y) = f(x) + f(y) for every pair of real numbers x and y, what are the possible values of f(0)?";
                quest[9] = "In arithmetic series, the terms of the series are equally spread out. For example, in 1 + 5 + 9 + 13 + 17, consecutive term are 4 apart. If the first term in an arithmetic series is 3, the last term is 136, and the sum is 1,390, what is the first 3 terms?";
                quest[10] = "Which of the following is equal to a^(bc) for all values of a, b, and c?";
                quest[11] = "If a > 0 and b > 0, which of the following could be an equation of the line graphed in the xy-plane?";
                quest[12] = "Which of the following products is a real number?";
                quest[13] = "Specify the domain of the function f(x) = x/x-7";
                quest[14] = "State whether the given function is linear and constant, linear but not constant, or non linear.";
                quest[15] = "In a chemistry class, 6 liters of a 4% silver iodide solution must be mixed with a 10% solution to get a 6% solution. How many liters of the 10% solution are needed?";
                quest[16] = "A rectangular Persian carpet has a perimeter of 244 inches. The length of the carpet is 26 in. more than the width. What are the dimension of the carpet?";
                quest[17] = "Your company uses the quadratic model y = -4.5x^2 + 150x to represent the average number of new customers who will be signed on (x) weeks after the release of your new service. How many new customers you can expect to gain in week 12?";
                quest[18] = "A rock is thrown vertically upward from the surface of the moon at a velocity of 28 m/sec. The graph shows the height of the rock, in meters, after x seconds.";
                quest[19] = "Solve the logarithmic equation: log (x + 9) = 1 - log x";
                quest[20] = "One method to determine the time since an animal died is to estimate the percentage of carbon -14 remaining in its bones. The percent in P in the decimal form of carbon -14 remaining x years is given by P(x) = e^(0.000121x). Approximate (to the nearest whole year) the age of a fossil if there is 5% of carbon - 14 remaining.";
                quest[21] = "Solve the system of linear equations. x - y = 7 ; 6x - 2y = 52";
                quest[22] = "The distribution of B.A degrees conferred by a local college is listed below, by major. What is the probability that a randomly selected degree is not in Mathematics?";
                quest[23] = "How many committees of 5 people can be selected from 9 men and 7 women if the committee must have 3 men and 2 women";
                quest[24] = "How many automobile license plates can be made involving 3 letters followed by 3 digits, if letters cannot be repeated (used more than once) but digits can be repeated?";
                quest[25] = "log (x+9) = 1- log x";
                quest[26] = "(3.3)^x = 43";
                quest[27] = "Find f(1) when f(x) = x^2 + 2x + 3";
                quest[28] = "Find the percent change if a quantity changes from P1 to P2. Round your answer to nearest tenth if appropriate.";
                
                
                answerDB[1] = "5.09 yr., 5.07 yr_5.34 yr, 5.29 yr_5.12 yr, 5.08 yr_5.54 yr, 5.51 yr";
                answerDB[2] = "1.5625 ft from the base of the dish, along the axis symmetry_1.7213 ft from the base of the dish, along the axis symmetry_2.1231 ft from the base of the dish, along the axis symmetry_2.1235 ft from the base of the dish, along the axis symmetry";
                answerDB[3] = "1 inch from the vertex along the axis of symmetry_1.5 inch from the vertex along the axis of symmetry_2 inch from the vertex along the axis of symmetry_2.5 inch from the vertex along the axis of symmetry";
                answerDB[4] = "20 ft_15 ft_25 ft_30 ft";
                answerDB[5] = "1/16_1/8_0_-1/8";
                answerDB[6] = "70_63_10_8";
                answerDB[7] = "-3_-5_2_7";
                answerDB[8] = "0 only_1 only_Any positive real number_Any real number";
                answerDB[9] = "3, 10, 17_3, 23, 43_3, 43, 83_3, 139, 1,251";
                answerDB[10] = "(a^b)^c_ab_a^(b+c)_ab^c";
                answerDB[11] = "y = ax – b_y = -ax – b_y = ax + b_y = -ax + b";
                answerDB[12] = "(1 + i) (1 + i)_(1 + i) (1 - i)_(-1 - i) (-1 - i)_(-1 + i) (-1 + i)";
                answerDB[13] = "x is not equal to 7_x is not equal to -7_all real numbers_x is greater than to 0";
                answerDB[14] = "Nonlinear_Linear but not constant_Linear, constant_None of the above";
                answerDB[15] = "3 liters_2 liters_6 liters_4 liter";
                answerDB[16] = "Width: 48 in. ; length: 74 in._Width: 109 in. ; length: 135 in._b.	Width: 74 in. ; length: 100 in._Width: 96 in. ; length: 122 in.";
                answerDB[17] = "152 customers_252 customers_576 customers_1746 customers";
                answerDB[18] = "The turning point is at approximately (17.5, 245). This is the point at which the rock reaches its maximum height and starts to fall back towards the surface of the moon._The turning point is at approximately (35, 245). This is the point at which the rock reaches its maximum height and starts to fall back_The turning point is at approximately (35, 0), This is the point at which the rock reaches the surface of the moon again._The turning point is at approximately (17.5, 245). This is the point at which the rocks reaches its maximum velocity and starts to slow down.";
                answerDB[19] = "1_-1_-1, 10_-10, 1";
                answerDB[20] = "24,758_-13,301_10,752_5728";
                answerDB[21] = "(9, 1)_(-9, 0)_(10, 0)_No solution";
                answerDB[22] = "0.768_0.683_0.232_0.303";
                answerDB[23] = "1764_1904_21,168_1,744";
                answerDB[24] = "15,600,000_17,576,000_15,571,400_15,869,020";
                answerDB[25] = "1_-1_-1, 10_-10, 1";
                answerDB[26] = "3.1503_3.2737_3.138_3.1626";
                answerDB[27] = "6_2_0_-4";
                answerDB[28] = "292.3_-74.5_-292.3_74.5";
                
                picture[4] = "<?php echo base_url();?>assets/activity/web/Al2.jpg";
                picture[7] = "<?php echo base_url();?>assets/activity/web/Al3.jpg";
                picture[18] = "<?php echo base_url();?>assets/activity/web/Al4.jpg";
                picture[22] = "<?php echo base_url();?>assets/activity/web/Al5.jpg";
                
            }
            else if(questCategory=="7")
            {
                quest[1] = "What is the distance of the polygon, in meters?";
                quest[2] = "What will be the cost of tiling a room measuring 12 feet by 15 feet if square tiles costing $ 2 each and measuring 12 inches on each side are used?";
                quest[3] = "Find the cost of building a rectangular driveway measuring 12 feet by 30 feet if concrete costs $12.50 per square yard.";
                quest[4] = "A television antenna 12 ft high is to be anchored by three wires each attached to the top of the antenna and to points on the roof 5 ft from the base of the antenna. If wire costs $0.75 per foot, what will be the costs of the wire needed to anchor the antenna?";
                quest[5] = "In the given figure, S represents the shaded area when a triangle is removed from a square. Find S if the side of the square is 10.";
                quest[6] = "What type of triangle is ABC?";
                quest[7] = "If L1 || L2 and L3 || L4, which of the following statement is true?";
                quest[8] = "The figure shows a regular hexagon. Select the formula for computing the total area of the hexagon.";
                quest[9] = "Which of the following pairs of angles are complementary?";
                quest[10] = "Select the geometric figure that posses all of the following characteristics";
                quest[11] = "The perimeter of a square is 20. Find its area.";
                quest[12] = "The base of a rectangle is three times as long as the altitude. The area is 147 square inches. Find the base and the altitude.";
                quest[13] = "The area of a triangle is 72. If one side is 12, what is the altitude to that side?";
                quest[14] = "How long must a tent rope be to reach from the top of a 12 foot pole to a point on the ground which is 16 feet from the foot of the pole?";
                quest[15] = "A boat travels south 24 miles, then east 6 miles, and then north 16 miles. How far is it from its starting point?";
                quest[16] = "The midpoint of a chord 10 inches in length is 12 inches from the center of a circle. Find the length of the diameter.";
                quest[17] = "Two parallel chords of a circle each have length16. The distance between them is 12. Find the radius of the circle.";
                quest[18] = "In the circle, chords AB and CD intersects at E. AE = 18, EB = 8, and CE = 4. Find ED.";
                quest[19] = "Find the measure of an angle of a regular nine - sided polygon.";
                quest[20] = "Into how many triangular regions would a convex polygonal region with 100 sides be separated by drawing all possible diagonals from a single vertex?";
                quest[21] = "If the circumference of a circle is a number between 16 and 24 and the radius is an integer, find the radius.";
                quest[22] = "The area of one circle is 100 times the area of a second. What is the ratio of the diameter of the first to that of the second?";
                quest[23] = "A circular lake is 2 miles in diameter. If you walk at 3 miles per hour, about how many hours will it take to walk around it? (Give the answer to the nearest whole number)";
                quest[24] = "A school room is 22 feet wide, 26 feet long, and 12 feet high. If there should be an allowance of 200 cubic feet of air space for each person in the room, and if there are to be two teachers in the room, how many pupils may there be in a class?";
                quest[25] = "If a right circular cones is inscribed in a hemisphere such that both have the same base, find the ratio of the volume of the cone to the volume of the hemisphere.";
                quest[26] = "Which of the following must be true about the numbers x and y graphed on the number line? I. x + y > 0, II. y - x > 0, III. xy > 0";
                quest[27] = "In the given parallelogram ABCD, AM = MB, BC = squareroot of 2, and DC = 2. What is the area of ABCD?";
                quest[28] = "The base of a rectangular solid is a square with side of length 3 feet. If the height of the rectangular solid is 5 feet, what is the volume of the solid, in cubic feet?";
                quest[29] = "If the edge of a cube is doubled in length, then the volume of the cube is multiplied by a factor of";
                quest[30] = "The given figure consists of semicircle AED and square ABCD. If the length of a side of the square is 12 feet, what is the number of square feet enclosed by the semicircle?";
                
                answerDB[1] = "3.28 m_0.328 m_32.8 m_328 m";
                answerDB[2] = "$360_$3600_$4,320_$180";
                answerDB[3] = "$500_$1500_$400_$4500";
                answerDB[4] = "$29.25_$27_$9.75_$38.25";
                answerDB[5] = "50_40_75_100";
                answerDB[6] = "Isosceles_Right_Equilateral_Scalene";
                answerDB[7] = "z = 140 degrees_s = t_r = 140 degrees_z = 40 degrees";
                answerDB[8] = "3hb_6hb_6 (h + b)_3h + b";
                answerDB[9] = "3 and 5_4 and 1_5 and 2_1 and 2";
                answerDB[10] = "Trapezoid_Rhombus_Rectangle_Parallelogram";
                answerDB[11] = "25_30_15_20";
                answerDB[12] = "altitude = 7 and base = 21_altitude = 5 and base = 25_altitude = 8 and base = 27_altitude = 4 and base = 20";
                answerDB[13] = "12_13_11_10";
                answerDB[14] = "20 feet_19 feet_21 feet_22 feet";
                answerDB[15] = "10 miles_15 miles_13 miles_17 miles";
                answerDB[16] = "26 inches_27 inches_25 inches_24 inches";
                answerDB[17] = "10_11_12_13";
                answerDB[18] = "36_33_29_25";
                answerDB[19] = "140_135_145_150";
                answerDB[20] = "98_97_96_95";
                answerDB[21] = "3_4_2_1";
                answerDB[22] = "10 : 1_1 : 10_20 : 1_1 : 20";
                answerDB[23] = "2_1_3_4";
                answerDB[24] = "32 pupils_31 pupils_33 pupils_34 pupils";
                answerDB[25] = "1/2_1/8_1/4_1/3";
                answerDB[26] = "II only_I only_III only_IV only";
                answerDB[27] = "2_3_4_5";
                answerDB[28] = "45_60_30_15";
                answerDB[29] = "8_4_6_2";
                answerDB[30] = "18 pi_36 pi_12 pi_6 pi";
                
                
                picture[1] = "<?php echo base_url();?>assets/activity/web/Al6.jpg";
                picture[2] = "<?php echo base_url();?>assets/activity/web/Al7.jpg";
                picture[5] = "<?php echo base_url();?>assets/activity/web/Al8.jpg";	
                picture[6] = "<?php echo base_url();?>assets/activity/web/Al9.jpg";
                picture[7] = "<?php echo base_url();?>assets/activity/web/Al10.jpg";
                picture[8] = "<?php echo base_url();?>assets/activity/web/Al11.jpg";
                picture[9] = "<?php echo base_url();?>assets/activity/web/Al12.jpg";
                picture[10] = "<?php echo base_url();?>assets/activity/web/Al13.jpg";
                picture[26] = "<?php echo base_url();?>assets/activity/web/Al14.jpg";
                picture[30] = "<?php echo base_url();?>assets/activity/web/Al15.jpg";	
            }
            else if(questCategory=="8")
            {	
             
                quest[1] = "In the given figure, two rectangular solids meet to form L-shaped solid. What is the volume of the solid?";
                quest[2] = "The area of square ABCD in the figure is 64. What is the area of the shaded triangle AED?";
                quest[3] = "A stack of three cubes of the same size has a volume of 24 cubic inches. What is the length, in inches, of an edge of one of the cubes?";
                quest[4] = "What is the perimeter of the given figure, if all intersecting line segments meet at right angles";
                quest[5] = "In the given figure, John's barn is 200 yards due north of his house and his tractor is 300 yards due east of his house. How many yards must he walk to go directly from his tractor to his barn if he walks in a straight line?";
                quest[6] = "A rectangular garden has a perimeter of 28 yards. The width of the garden is 6 yards less than its length. What is the area of the garden, in square yards?";
                quest[7] = "A regular hexagon is formed from 6 equilateral triangles, as shown in the figure. If each triangle has perimeter 4, then the perimeter of the hexagon is";
                quest[8] = "In square ABCD in figure, the measure of <AEF = 140 degrees. What is the value of x?";
                quest[9] = "In the given figure, CD is parallel to AB. What is the measure of <ACB?";
                quest[10] = "If the distance between the points (x, 11) and (1, -1) is 13, then which of the following could be the value of x";
                quest[11] = "In the given figure, line m is parallel to line n, and line t is transversal crossing both m and n. Which of the following lists has 3 angles that are all equal in measure?";
                quest[12] = "As shown in the figure, Triangle ABC is isosceles with the length of AB equal to the length of AC. The measure of A is 40 degrees and point B, C, and D are collinear. What is the measure of angle ACD?";
                quest[13] = "A person had a rectangular - shaped garden with sides of lengths 16 feet and 9 feet. The garden was changed into a square design with the same area as the original rectangular-shaped garden. How many feet in length are each of the sides of the new square-shaped garden?";
                quest[14] = "A rectangular box with a base 2 inches by 6 inches is 10 inches tall and holds 12 ounces of breakfast cereal. The manufacturer wants to use a new box with a base 3 inches by 5 inches. How many inches tall should the new box be in order to hold exactly the same volume as the original box? (Note: The volume of a rectangular box may be calculated by multiplying the area of the base by the height of the box)";
                quest[15] = "In the given figure, AB and CD are parallel, and lengths are given in units. What is the area, in square units, of trapezoid ABCD?";
                
                answerDB[1] = "56_48_64_120";
                answerDB[2] = "32_28_24_16";
                answerDB[3] = "2_4_3_8";
                answerDB[4] = "12x_10x_8x_6x";
                answerDB[5] = "360.55_43.21_32.43_323.21";
                answerDB[6] = "40_36_48_132";
                answerDB[7] = "8_16_24_32";
                answerDB[8] = "50_45_40_30";
                answerDB[9] = "60_120_35_25";
                answerDB[10] = "6_5_4_2";
                answerDB[11] = "a, b, d_a, c, d_a, c, e_b, c, d";
                answerDB[12] = "110 degrees_140 degrees_80 degrees_70 degrees";
                answerDB[13] = "12_16_9_7";
                answerDB[14] = "8_9_10_11";
                answerDB[15] = "52_36_64_65";
                
                picture[1] = "<?php echo base_url();?>assets/activity/web/Al16.jpg";
                picture[2] = "<?php echo base_url();?>assets/activity/web/Al17.jpg";
                picture[4] = "<?php echo base_url();?>assets/activity/web/Al18.jpg";
                picture[7] = "<?php echo base_url();?>assets/activity/web/Al19.jpg";
                picture[8] = "<?php echo base_url();?>assets/activity/web/A20.jpg";
                picture[9] = "<?php echo base_url();?>assets/activity/web/A21.jpg";
                picture[11] = "<?php echo base_url();?>assets/activity/web/A22.jpg";
                picture[12] = "<?php echo base_url();?>assets/activity/web/A23.jpg";
                
            }
            else if(questCategory=="9")
            {	
             
                quest[1] = "Center at (24, 5), passing through (24, 0)";
                quest[2] = "A pyramid was constructed for a mini golf center which featured the man-made wonders of the world. The pyramid is depicted in the figure. Find the length x of a slant edge of the pyramid.";
                quest[3] = "Two forces of 21 N and 11 N act on object. The angle between the forces is 36 degrees. Find the magnitude of the resultant and the angle that it makes with the larger force.";
                quest[4] = "A pilot wants to fly on a bearing of 63.3 degrees. By flying due east, he finds that a 51- mph wind, blowing from the south, puts him on course. Find the airspeed of the plane.";
                quest[5] = "Two forces of 650 N and 250 N acts on an object. The angle between the forces is 45 degrees. Find the magnitude of the resultant and the angle that it makes with the smaller force.";
                quest[6] = "To find the distance between two small towns, an electronic distance measuring (EDM) instrument is placed on a hill from which both towns are visible. If the distance from the EDM to the towns is 4 miles and the angle between the two lines of sight is 69 degrees, what is the distance between the towns?";
                quest[7] = "a is the radius of any spherical bowling ball, and b is its volume.";
                quest[8] = "From a hot air balloon, the angle between a radio antenna straight below and the base of the library downtown is 57 degrees, as shown on figure. If the distance between the radio antenna and the library is 1.3 miles, how many miles high is the balloon?";
                quest[9] = "The anchoring wire of a telephone pole has snapped and needs to be replaced. The telephone pole is 30 feet tall. The anchor for the wire is 13.8 feet from the bottom of the pole.";
                quest[10] = "What is the smallest positive value for x where y = sin 2x reaches its maximum?";
                quest[11] = "Two triangles have equal bases. The altitude of one triangle is 3 units more than its base and the altitude of the other triangle is 3 units less than its base. Find the altitudes, if the areas of the triangles differ by 21 square units";
                quest[12] = "A ship started sailing S 42 deg. 35 min. W at the rate of 5 kph. After 2 hours, ship B started at the same port going N 46 deg. 20 min. W at the rate of 7 kph. After how many hours will the second ship be exactly north of ship A";
                quest[13] = "An aerolift airplane can fly at an airspeed of 300 mph. If there is a wind blowing towards the cast at a 50mph, what should be the plane's compass heading in order for its course to be 30 degrees? What will be the plane's ground speed if it flies in this course?";
                quest[14] = "A man finds the angle of elevation of the top of a tower to be 30 degrees. He walks 85 m nearer the tower and finds its angle of elevation to be 60 degrees. What is the height of the tower?";
                quest[15] = "A pole cast a shadow 15 m long when the angle of elevation of the sun is 62 degrees. If the pole is leaned 15 degrees from the vertical directly towards the sun, determine the length of the pole.";
                quest[16] = "A wire supporting a pole is fastened to it 20 feet from the ground and to the ground 15 feet from the pole. Determine the length of the wire and the angle it makes with the pole.";
                quest[17] = "The angle of elevation of the top of tower B from the top of tower A is 28 degrees and the angle of elevation of the top of tower A from the base of tower B is 46 degrees. The two towers lie in the same horizontal plane. If the height of tower B is 120 m, find the height of tower A.";
                quest[18] = "Points A and B are 100 m apart and are the same elevation as the foot of a building. The angles of elevation of the top of the building from points A and B are 21 degrees and 32 degrees respectively. How far is A from the building in meters";
                quest[19] = "The captain of a ship views the top of a lighthouse at an angle of 60 degrees with the horizontal at an elevation of 6 meters above sea level. Five minutes later, the same captain of the ship views the top of the same lighthouse at an angle of 30 degrees with the horizontal. Determine the speed of the ship if the lighthouse is known to be 50 meters above sea level";
                quest[20] = "An observer wishes to determine the height of a tower. He takes sights at the top of the tower from A and B, which are 50 feet apart, at the same elevation on a direct line with the tower. The vertical angle at point A is 30 degrees and at point B is 40 degrees. What is the height of the tower?";
                quest[21] = "A PLDT tower and a monument stand on a level plane. The angles of depression of the top and bottom of the monument viewed from the top of the PLDT tower at 13 degrees and 35 degrees respectively. The height of the tower is 50 m. Find the height of the monument";
                quest[22] = "If an equilateral triangle is circumscribed about a circle of radius 10 cm, determine the side of the triangle";
                quest[23] = "The two legs of a triangle are 300 and 150 m each, respectively. The angle opposite the 150 m side is 26 degrees. What is the third side?";
                quest[24] = "The sides of a triangular lot are 130 m, 180 m, and 190 m. The lot is to be divided by a line bisecting the longest side and drawn from the opposite vertex. Find the length of the line.";
                quest[25] = "The sides of a triangle are 195, 157, and 210, respectively. What is the area of the triangle?";
                quest[26] = "The sides of a triangle are 8, 15, and 17 units. If each side is doubled, how many square units will the area of the new triangle be?";
                quest[27] = "If greenwhich mean time (GMT) is 6AM, what is the time at a place located 30 degrees?";
                quest[28] = "If the longitude of Tokyo is 139 degree East and that of Manila is 121 degrees East, what is the time difference between Tokyo and Manila?";
                quest[29] = "One degree on the equator of the earth is equivalent to";
                quest[30] = "A spherical triangle ABC has an angle C = 90 degrees and sides a = 50 degrees, and c = 80 degrees. Find the value of b in degrees.";
                
                answerDB[1] = "(x - 24)^2 + (y - 5)^2 = 25_(x - 5)^2 + (y - 24)^2 = 576_(x - 5)^2 + (y - 24)^2 = 5_(x - 24)^2 + (y - 5)^2 = 576";
                answerDB[2] = "139.70 inches_58.52 inches_208.94 inches_19,516.50 inches";
                answerDB[3] = "31 N, 12 degrees_2 N, 12 degrees_30 N, 17 degrees_32 N, 12 degrees";
                answerDB[4] = "101 mph_51 mph_152 mph_114 mph";
                answerDB[5] = "845 N, 33 degrees_844 N, 17 degrees_846 N, 12 degrees_7 N, 12 degrees";
                answerDB[6] = "3.9 mi_4.3 mi_5.1 mi_5.4 mi";
                answerDB[7] = "both_neither_a is a function of b_b is a function of a";
                answerDB[8] = "1.3/tan 57_1.3 sin 57_1.3/cos 57_1.3/sin 57";
                answerDB[9] = "35 ft_44 ft_21 ft_10 ft";
                answerDB[10] = "pi/4_pi_3pi/2_5pi/2";
                answerDB[11] = "4 and 10_5 and 11_3 and 9_6 and 12";
                answerDB[12] = "4.03_3.68_5.12_4.83";
                answerDB[13] = "21.7 degrees, 321.8 mph_22.3 degrees, 319.2 mph_20.1 degrees, 309.4 mph_19.7 degrees, 307.4 mph";
                answerDB[14] = "73.61 m_73.16 m_73.31 m_76.31 m";
                answerDB[15] = "54.23 m_48.23 m_42.44 m_46.21 m";
                answerDB[16] = "25 ft, 36.87 degrees_25 ft, 53.13 degrees_24 ft, 36.87 degrees_24 ft, 53.13 degrees";
                answerDB[17] = "79.3 m_66.3 m_87.2 m_90.7 m";
                answerDB[18] = "259.28_265.42_271.64_277.29";
                answerDB[19] = "0.169 m/sec_0.155 m/sec_0.265 m/sec_0.210 m/sec";
                answerDB[20] = "92.54 feet_85.60 feet_110.29 feet_143.97 feet";
                answerDB[21] = "33.51 m_32.12 m_30.11 m_29.13 m";
                answerDB[22] = "34.64 cm_64.12 cm_36.44 cm_32.10 cm";
                answerDB[23] = "341.78 m_282.15 m_218.61 m_197.49 m";
                answerDB[24] = "125 m_128 m_130 m_120 m";
                answerDB[25] = "14,586 sq. units_11,260 sq. units_10,250 sq. units_73,250 sq. units";
                answerDB[26] = "240_420_320_200";
                answerDB[27] = "8 A.M._7 A.M._9 A.M._4 A.M.";
                answerDB[28] = "1 hour and 12 minutes_1 hour and 5 minutes_1 hour and 8 minutes_1 hour and 10 minutes";
                answerDB[29] = "4 minutes_1 minute_30 minutes_1 hour";
                answerDB[30] = "74.33_73.22_75.44_76.55";

                
                picture[2] = "<?php echo base_url();?>assets/activity/web/Al24.jpg";
                picture[8] = "<?php echo base_url();?>assets/activity/web/Al25.jpg";
                
            }
            else if(questCategory=="10")
            {	
             
                quest[1] = "Solve for side b of a right spherical triangle ABC whose parts are a = 46 degrees, c = 75 degrees, and C = 90 degrees";
                quest[2] = "Given a right spherical triangle whose given parts are a= 82 degrees, b = 62 degrees, and C = 90 degrees. What is the value of the side opposite the right angle?";
                quest[3] = "Determine the value of the angle B of an isosceles spherical triangle ABC whose given parts are b = c = 54 degrees 28 minutes and a = 92 degrees and 30 minutes";
                quest[4] = "Solve for angle A in the spherical triangle ABC, given a = 106 degrees 25 minutes, c = 42 degrees 16 minutes, and B = 114 degrees 53 minutes";
                quest[5] = "Solve for angle C of the oblique triangle ABC given, a = 80 degrees, c = 115 degrees and A = 72 degrees";
                
                answerDB[1] = "68 degrees_74 degrees_48 degrees_58 degrees";
                answerDB[2] = "86 degrees and 15 minutes_84 degrees and 45 minutes_83 degrees and 30 minutes_85 degrees and 15 minutes";
                answerDB[3] = "41 degrees and 45 minutes_84 degrees and 25 minutes_55 degrees and 45 minutes_89 degrees and 45 minutes";
                answerDB[4] = "97 degrees and 9 minutes_80 degrees and 42 minutes_45 degrees and 54 minutes_72 degrees and 43 minutes";
                answerDB[5] = "119 degrees_95 degrees_85 degrees_61 degrees";
                
            }
            else if(questCategory=="11")
            {	
             
                quest[1] = "Which of the following equations has a graph that is symmetric with respect to the origin?";
                quest[2] = "The roots of the equation f(x) = 0 are 1 and -2. The roots of f(2x) = 0 are";
                quest[3] = "Let f(x) have the inverse function g(x). Then f(g(x)) =";
                quest[4] = "Which of the following reflection of the graph of y = f(x) in the x-axis?";
                quest[5] = "If f(a) = f(b) = 0 and f(x) is continuous on [a, b], then";
                quest[6] = "If differential are used for computation, then the best approximation, in cubic inches, to the increase in volume of a sphere when the radius is increased from 3 to 3.1 in. is";
                quest[7] = "A cube whose edge is x is contracting. When its surface area is changing at a rate which is equal to 6 times the rate of change of its edge, then the length of the edge is";
                quest[8] = "The positive root of x ln x = 1, to two decimal places, using Newton's method is";
                quest[9] = "A particle starting at rest at t = 0 moves along a line so that its acceleration at time t is 12ft/sec/sec. How much distance does the particle cover during the first 3 sec?";
                quest[10] = "A particle moves along a line with acceleration 2 + 6t at time t. When t = 0, its velocity equals 3 and it is at position s = 2. When t = 1, its position s =";
                quest[11] = "A population of a country increases to the existing population. If the population doubles in 20 years, then the factor of proportionality is";
                quest[12] = "If a substance decomposes at a rate proportional to the amount of the substance present, and if the amount decreases from 40 gm to 10 gm in 2 hr, then the constant of proportionality is";
                quest[13] = "If (g'(x))^2 = g(x) for all real x and g(0) = 0, g(4) = 4, then g(1) equals";
                quest[14] = "The acceleration of the particle moving on a straight line is given by a = cos t, and when t = 0 the article is at rest. The distance it covers from t = 0 to t = 2 equals";
                quest[15] = "A sheet of plywood measuring 8 ft by 4 ft is to circumscribed by an elliptical frame. The ellipse of minimal area has semi major axis equal to";
                quest[16] = "If y = x sin x, then dy/dx =";
                quest[17] = "If f(x) = 7x - 3 + ln x, then f'(1) =";
                quest[18] = "A particle moves along the x-axis. The velocity of the particle at time t is 6t - t^2. What is the total distance traveled by the particle from t = 0 to t = 3";
                quest[19] = "Let f be the function defined above. For what value of k is f continuous at x = 2?";
                quest[20] = "A particle moves along the x- axis with its position at time t given by x(t) = (t - a) (t - b), where a and b are constant and a is not equal to b. For which of the following values of t is the partilce at rest?";
                
                
                answerDB[1] = "y = x^3 + 2x_y = x_y = 2x + 1_y = (x - 1)/x";
                answerDB[2] = "1/2 and -1_1 and -2_-1/2 and 1_2 and -4";
                answerDB[3] = "x_1_1/x_f(x) * g(x)";
                answerDB[4] = "y = -f(x)_y = -f(-x)_y = f(-x)_y = |f(x)|";
                answerDB[5] = "f'(x) may be different from zero for all x on [a, b]_f(x) must be identically zero_f'(x) must exist for every x on (a, b)_none of the preceeding is true";
                answerDB[6] = "11.3_11.7_12.1_33.9";
                answerDB[7] = "1/2_1_3/4_2";
                answerDB[8] = "1.7632_1.7577_1.7462 _1.7013";
                answerDB[9] = "54 ft_48 ft_32 ft_16 ft";
                answerDB[10] = "7_6_5_2";
                answerDB[11] = "1/20 ln 2_1/2 ln 20_ln 20_ln 2";
                answerDB[12] = "ln 1/4_-1/4_1/2_- ln 2";
                answerDB[13] = "1/4_1/2_1_2";
                answerDB[14] = "1 - cos 2_sin 2_cos 2_cos 2 – 1";
                answerDB[15] = "16 ft_11.314 ft_8.944 ft_5.657 ft";
                answerDB[16] = "sin x + xcosx_sin x + cos x_sin x - cos x_x(sin x + cosx)";
                answerDB[17] = "8_7_6_5";
                answerDB[18] = "18_9_6_3";
                answerDB[19] = "5_3_2_1";
                answerDB[20] = "t = (a + b) / 2_t = ab_t = a + b_t = 2(a + b)";
                
            }
            
            else if(questCategory=="12")
            {	
             
              quest[1] = "The sum of two positive numbers is 50. What are the numbers if their product is to be the largest possible";
              quest[2] = "A triangle has variable sides x, y, z subject to the constraint such that the perimeter is fixed to 18 cm. What is the maximum possible area for the triangle";
              quest[3] = "A farmer has enough money to build only 1000 meters of fence. What are the dimensions of the field he can enclose the maximum area =";
              quest[4] = "Find the minimum amount of tin sheet that can be made into a closed cylinder having a volume of 108 cu. inches in square inches";
              quest[5] = "A box is to be constructed from a piece of zinc 20 sq. in by cutting equal squares from each corner and turning up the zinc to form the side. What is the volume of the largest box that can be so constructed?";
              quest[6] = "A poster is to contain 300 (cm square) of printed matter with margins of 10 cm at the top and bottom and 5 cm at each side. Find the overall dimension if the total area of the poster is minimum";
              quest[7] = "A normal window is in the shape of rectangle surmounted by a semi-circle. What is the ratio of the width of the rectangle to the total height so that it will yield a window admitting the most light for a given perimeter?";
              quest[8] = "Determine the diameter of a closed cylindrical tank having a volume of 11.3 cu. m to obtain minimum surface area";
              quest[9] = "The cost of fuel in running a locomotive is proportional to the square of the speed and is $25 per hour, regardless of the speed. What is the speed which will make the cost per mile a minimum?";
              quest[10] = "The cost C of a product is a function of the quantity x of the product. C(x) = x^2 - 4000x + 50. Find the quantity for which the cost is minimum";
              quest[11] = "An open top rectangular tank with square bases is to have a volume of 10 cu. m. The materials for its bottom is to cost P 15 per square meter and that for the sides P6 per square meter. Find the most economical dimension for the tank.";
              quest[12] = "What is the maximum profit when the profit-versus-production function is as given below? P is profit and x is unit of production";
              quest[13] = "A boatman is at A which is 4.5 km from the nearest point B on a straight shore BM. He wishes to reach in minimum time a point C situated on the shore 9 km from B. How far from C should he land if he can row at the rate of 6 kph and can walk at the rate of 7.5 kph?";
              quest[14] = "A fencing is limited to 20 ft. length. What is the maximum rectangular area that can be fenced in using two perpendicular corner sides of an existing wall?";
              quest[15] = "The cost per hour of running a motor boat is proportional to the cube of the speed. At what speed will the boat run against a current of 8 km/hr in order to go a given distance most economically?";
              quest[16] = "Given a cone of diameter x and altitude of h. What percent is the volume of the largest cylinder which can be inscribed in the cone to the volume of the cone";
              quest[17] = "At any distance x from the source of light, the intensity of illumination varies directly as the intensity of the source and inversely as the square of x. Suppose that there is a light at A, and another at B, the one at B having an intensity 8 times that of A. The distance AB is 4m. At what point from A on the line AB will the intensity of illumination be least?";
              quest[18] = "A wall h meters high is 2m away from the building. The shortest ladder that can reach the building with one end resting on the ground outside the wall is 6m. How high is the wall in meters";
              quest[19] = "A statue 3m high is standing on a base of 4m high. If an observer's eye is 1.5 m above the ground, how far should he stand from the base in order that the angle subtended by the statue is a maximum";

              
              
              answerDB[1] = "25 & 25_28 & 22_24 & 26_20 & 30";
              answerDB[2] = "15.59 sq cm._18.71 sq cm._17.15 sq cm._14.03 sq cm.";
              answerDB[3] = "25 m x 25 m_15 m x 35 m_20 m x 30 m_22.5 m x 27.5 m";
              answerDB[4] = "125.50_127.50_129.50_123.50";
              answerDB[5] = "592.59 cu in._599.95 cu in._579.50 cu in._622.49 cu in.";
              answerDB[6] = "22.24 cm, 44.5 cm_20.45 cm, 35.6 cm_27.76 cm, 47/8 cm_25.55 cm, 46.7 cm";
              answerDB[7] = "1_1/2_2_2/3";
              answerDB[8] = "2.44_1.64_1.22_2.68";
              answerDB[9] = "50_55_40_45";
              answerDB[10] = "2m x 2m x 2.5m_1.5m x 1.5m x 4.4m_4m x 4m x 0.6m_3m x 3m x 1.1m";
              answerDB[11] = "1/20 ln 2_1/2 ln 20_ln 20_ln 2";
              answerDB[12] = "200,000_285,000_250,000_305,000";
              answerDB[13] = "3.0 km_4.15 km_3.25 km_4.0 km";
              answerDB[14] = "100_120_140_190";
              answerDB[15] = "12 kph_11 kph_13 kph_10 kph";
              answerDB[16] = "44%_46%_56%_65%";
              answerDB[17] = "1.50m_1.33m_2.15m_1.92m";
              answerDB[18] = "50_55_40_45";
              answerDB[19] = "2.24_2.34_2.44_2.14";
              
              picture[12] = "<?php echo base_url();?>assets/activity/web/Al26.jpg";
          }
          else if(questCategory=="13")
          {	
             
              quest[1] = "Determine the order and degrees of the differential equation";
              quest[2] = "Which of the following equations is an exact DE?";
              quest[3] = "Which of the following equations is a variable separable DE?";
              quest[4] = "The equation y^2 = cx is the general solution of:";
              quest[5] = "Solve the differential equation: x(y-1)dx + (x+1)dy =0. If y = 2 when x = 1, determine y when x = 2";
              quest[6] = "If dy = x^2dx; what is the equation of y in terms of x if the curve passes through (1,1)?";
              quest[7] = "Find the equation of the curve at every point of which the tangent line has a slope of 2x";
              quest[8] = "Solve (cos x cos y - cot x) dx - sin x sin y dy = 0";
              quest[9] = "Solve the differential equation dy - xdx = 0, if the curve passes through (1,0)?";
              quest[10] = "What is the solution of the first order differential equation y(k+1) = y(k) + 5";
              quest[11] = "Radium decomposes at a rate proportional to the amount at any instant. ln 100 years, 100 mg of radium decomposes to 96 mg. How many mg will be left after 100 years?";
              quest[12] = "The population of a country doubles in 50 years. How many years will it be five times as much? Assume that the rate of increase is proportional to the number of inhabitants";
              quest[13] = "Radium decomposes at a rate proportional to the amount present. If half of the original amount disappears after 1000 years, what is the percentage lost in 100 years?";
              quest[14] = "An object falls from rest in a medium offering a resistance. The velocity of the object before the object reaches the ground is given by the differential equation dV/dt + V/10 = 32 ft/sec. What is the velocity of the object one second after it falls?";
              quest[15] = "In tank initially holds 100 gallons of salt solution in which 50 lbs of salt has been dissolved. A pipe fills the tank with brine at the rate of 3 gpm, containing 2 lbs of dissolved salt per gallon. Assuming that the mixture is kept uniform by stirring, a drain pipe draws out of the tank the mixture at 2 gpm. Find the amount of salt in the tank at the end of 30 minutes";
              
              answerDB[1] = "Fourth order, first degree_Third order, first degree_First order, fourth degree_First order, third degree";
              answerDB[2] = "2xy dx + (2 + x^2)dy = 0_x^2ydy - ydx = 0_xdy + (3x - 2y)dx = 0_(x^2 + 1)dx -xydy = 0";
              answerDB[3] = "2ydx = (x^2 + 1)dy_y^2dx = (2x -3y)dy_(x + y)dx - 2ydy = 0_(x + x^2y)dy = (2x + xy^2)dx";
              answerDB[4] = "y'= y/2x_y'= x/2y_y'= 2x/y_y'= 2y/x";
              answerDB[5] = "1.55_1.48_1.80_1.63";
              answerDB[6] = "x^3 - 3y + 2 = 0_x^2 - 3y + 3 = 0_x^3 + 3y^2 + 2 = 0_2y + x^3 + 2 = 0";
              answerDB[7] = "y = x^2 + C_x = y^2 + C_y = -x^2 + C_x = -y^2 + C";
              answerDB[8] = "sin x cos y = ln (c sin x)_sin x cos y = ln (c cos x)_sin x cos y = -ln (c sin x)_sin x cos y = -ln (c cos x)";
              answerDB[9] = "x^2 - 2y - 1 = 0_3x^2 + 2y -3 = 0_2y + x^2 - 1 = 0_2x^2 + 2y - 2 =0";
              answerDB[10] = "y(k) = 20 + 5k_y(k) = 4 - 5/k_y(k) = C - k, where C is constant_The solution is non-existent for real values of y";
              answerDB[11] = "92.16_95.32_88.60_90.72";
              answerDB[12] = "116 years_100 years_120 years_98 years";
              answerDB[13] = "6.70%_4.50%_5.36%_4.30%";
              answerDB[14] = "30.45_38.65_40.54_34.12";
              answerDB[15] = "19.53 kg_15.45 kg_12.62 kg_20.62 kg";
              
              picture[1] = "<?php echo base_url();?>assets/activity/web/Al27.jpg";
          }
          else if(questCategory=="14")
          {	
             
              quest[1] = "Determine the inverse laplace transform of l(s) = 200/(s^2 - 50s + 10625)";
              quest[2] = "The inverse laplace transform of s/[ (s square) + (w square)] is";
              quest[3] = "Find the inverse laplace transform of 2s - 18 / s^2 + 9";
              quest[4] = "Determine the inverse laplace transform of 1 / 4s^2 - 8s";
              
              
              answerDB[1] = "l(s) = 2e^(-25t) sin100t_l(s) = 2te^(-25t) sin 100t_l(s) = 2e(-25t) cos 100t_l(s) = 2te^(-25t) cos 100t";
              answerDB[2] = "sin wt_w_(e exponent wt)_cos wt";
              answerDB[3] = "2 cos 3x - 6 sin 3x_2 cos 3x - 6 sin 3x_3 cos 2x - 2 sin 6x_6 cos x - 3 sin 2x";
              answerDB[4] = "1/4 e^t sinht_1/2 e^2t sinht_1/4 e^t cosht_1/2 e^2t cosht";
              
          }
          else if(questCategory=="15")
          {	
             
              quest[1] = "A certain school's enrollment increased 5% this year over last year's enrollment. If the school now has 1,260 students enrolled, how many students were enrolled last year?";
              quest[2] = "In figure below, A, B, C, and D are collinear; AD is 35 units long; AC is 22 units long; and BD is 29 units long. How many units long is BC?";
              quest[3] = "What is the slope of the line determined by the equation 3x + y = 4";
              quest[4] = "In the given figure, AB is parallel to DE and AE intersects BD at C. If the measure <ABC is 40 degrees and the measure of <CED is 60 degrees, what is the measure of <BCE?";
              quest[5] = "Mark bought 3 shirts at a clothing store. Two of the shirt were priced at 2 for $15.00. If the average cost of the 3 shirt was $ 8,000, how much did Mark pay for the third shirt?";
              quest[6] = "In the given figure, points A, B, and C are collinear and AB and BC are each 6 units long. If the area of triangle ACD is 24 square units, how many units long is the altitude BD?";
              quest[7] = "Sam has some quarters, nickels, and dimes. He has 4 more quarters than dimes and 3 more dimes than nickels. If n represents the number of nickels he has, which of the following represents, in cents, the total values of all his coins?";
              quest[8] = "A straight line in the coordinate plane passes through points with (x, y) coordinates (-1, 1) and (2, 3). What are the (x, y) coordinates of the point at which the line passes through the y-axis?";
              quest[9] = "A theater has 25 rows, each with 12 seats. At a certain performance there were, on average, 3 empty seats per row. What was the attendance at that performance";
              quest[10] = "There are 45 people coming to a picnic at which hot dogs will be served, Hotdogs come in packages of 8 that cost $ 2.50 each, and hotdog rolls come in packages of 10 that cost $2.00 each. If enough hotdogs and hotdogs roll will be purchased so that each person can have at least one hotdog in a roll, what is the minimum that can be spent on hotdogs and hotdogs roll?";
              quest[11] = "The given figure shows a right circular cylindrical vessel that is exactly one-quarter full. If 7 liters of liquid are added, the vessel will be exactly three -fifth full. What is the total capacity of the vessel, in liters?";
              quest[12] = "Marchall is making corn bread. His recipe  calls for 3 1/2 cups of cornmeal, but he wants to make only half the amount given in the recipe. How many cups of cornmeal should he use?";
              quest[13] = "Maria worked in a library. She was paid at the rate of $6.00 per hour. If she worked from 10:30 AM to 4:45 PM on Tuesday, how much money did she earn?";
              quest[14] = "How many dollars will x pen cost if 5 such pens cost y dollars";
              quest[15] = "In a music class 30 students, there are 6 more females than males. How many females are in the class?";
              quest[16] = "The graph of which of the following equations is a straight line parallel to the graph of y = 2x?";
              quest[17] = "An equation of the line that contains the origin and the point (1, 2) is";
              quest[18] = "An apartment building contains 12 units consisting of one- and two- bedroom apartments that rent for $ 360 and $ 450 per month, respectively. When all units are rented, the total monthly rental is $ 4,950. What is the number of two bedroom apartment?";
              quest[19] = "If A represent the number of apples purchased at 15 cents each, and B represents the number of bananas purchased at 10 cents each, which of the following represents the total value of the purchase in cents?";
              quest[20] = "32 is 40 percent of what number?";
              quest[21] = "The vertices of triangle are intersections of the lines whose equations are y = 0, x = 3y, and 3x + y =7. This triangle is";
              quest[22] = "The area bounded by the closed curve whose equation is x^2 - 6x + y^2 + 8y = 0 is";
              quest[23] = "Two circle of radii 3 inches and 6 inches have their centers 15 inches apart, Find the length in inches of the common internal tangent.";
              quest[24] = "If 2^x = 8^(y+1) and 9^y = 3^(x-9) then y equals";
              quest[25] = "A point moves so that its distance from the origin is always twice its distance from the poin (3, 0). Its locus is";
              quest[26] = "A cube 4 inches on each side is painted red and cut into 64 1-inch cubes. How many 1-inch cubes are painterd red on two faces only?";
              quest[27] = "What is the approximate magnitude of 8 + 4i";
              quest[28] = "tan A/2 + cot A/2 is equivalent to";
              quest[29] = "From two ships due east of a lighthouse and in line with its foot, the angle of elevation of the top of the lighthouse are x and y, with x > y. The distance between the ships is m. The distance from the lighthouse to the nearer ship is";
              quest[30] = "What is the probability of getting 80% or more of the questions correct on a 10 - questions true - false exam merely by guessing";
              
              
              answerDB[1] = "1, 200_1, 255_1,197_1,020";
              answerDB[2] = "16_13_7_6";
              answerDB[3] = "-3_1/3_3/4_1";
              answerDB[4] = "100 degrees_80 degrees_60 degrees_40 degrees";
              answerDB[5] = "$ 9.00_$ 8.50_$ 7.67_$ 7.00";
              answerDB[6] = "4_2_6_8";
              answerDB[7] = "40n + 205_40n + 130_40n + 7_7n + 130";
              answerDB[8] = "(0, 2/3)_(-2, 0)_(0, 5/3)_(0, 2)";
              answerDB[9] = "225_264_297_300";
              answerDB[10] = "$22.50_$20.50_$25.00_$27.0";
              answerDB[11] = "20_14_21_32";
              answerDB[12] = "1 ¾_5_1 ½_1 ¼";
              answerDB[13] = "$37.50_$34.50_$33.00_$30.00";
              answerDB[14] = "xy/5_5/xy_y/5x_5xy";
              answerDB[15] = "18_24_12_6";
              answerDB[16] = "2x – y = 4_2x + y = 2_2x - 2y = 2_4x - y = 4";
              answerDB[17] = "y = 2x_2y = x_y = x – 1_ y = 2x + 1";
              answerDB[18] = "7_6_5_4";
              answerDB[19] = "15A + 10B_10A + 15B_25 (A + B)_A + B";
              answerDB[20] = "80_800_128_12.8";
              answerDB[21] = "right_acute_equilateral_isosceles";
              answerDB[22] = "25pi_12pi_36pi_48pi";
              answerDB[23] = "12”_14”_10”_8”";
              answerDB[24] = "6_3_9_12";
              answerDB[25] = "a circle_an ellipse_a hyperbola_a straight line";
              answerDB[26] = "24_16_12_8";
              answerDB[27] = "8.94_4.15_12_18.64";
              answerDB[28] = "2 csc A_2 cos A_2 sec A_2 sin A";
              answerDB[29] = "m cos x sin y / sin (x - y)_m sin x cos x / sin (x - y)_cos x sin y / m sin (x + y)_m cot x sin y";
              answerDB[30] = "7/128_7/32_3/16_5/32";
              
              picture[2] = "<?php echo base_url();?>assets/activity/web/Al28.jpg";
              picture[4] = "<?php echo base_url();?>assets/activity/web/Al29.jpg";
              picture[6] = "<?php echo base_url();?>assets/activity/web/Al30.jpg";
              picture[11] = "<?php echo base_url();?>assets/activity/web/Al31.jpg";
              
          }
          else if(questCategory=="16")
          {	
             
              quest[1] = "Find the set of values satisfying the inequality | (10 - x)/3 | < 2";
              quest[2] = "What is the approximate area of parallelogram?";
              quest[3] = "Two planes are perpendicular to each other. The locus of points at distance d from one of these planes and at distance from the other is";
              quest[4] = "The graph of the equation y = -2x + 3 lies in quadrant";
              quest[5] = "A circle of radius .39 is circumscribed about a square. What is the approximate perimeter of the square?";
              quest[6] = "The set of odd integers is closed under";
              quest[7] = "Which of the following properties of zero is the basis for excluding division by zero?";
              quest[8] = "A man usually gets from one corner of a square lot to the opposite corner by walking along two of the sides. Approximately what percent of the distance does he save if he walks along the diagonal?";
              quest[9] = "In given figure, all intersections occur at right angles. What is the approximate perimeter of the polygon?";
              quest[10] = "If 6.12% of 7/13 is .43 percent of x, what is the value of x?";
              quest[11] = "The graph of the equation 4y^2 + x^2 = 25 is";
              quest[12] = "How many digits are in the number (63)^21";
              quest[13] = "If the graphs of x - ay = 10 and 2x - y = 3 are perpendicular lines, a =";
              quest[14] = "If 4m = 5K and 6n = 7K, the ratio of m to n is";
              quest[15] = "In a class of 250 students, 175 take mathematics and 142 take science. How many take both mathematics and science? (All take math and/or science)";
              quest[16] = "If r - s > r + s, then";
              quest[17] = "Given: All seniors are mature students. Which statement expresses a conclusion that logically follows from the given statement?";
              quest[18] = "If 4x - 3 > x + 9, then";
              quest[19] = "A point P is 10 inches from a plane m. The locus of points in space which are 7 inches from P and 5 inches from plane m is";
              quest[20] = "The base of a triangle is 16 inches and its altitude is 10 inches. The area of trapezoid cut off by a line 4 inches from the vertex is";
              quest[21] = "The locus of the centers of all circles of given radius r, in the same plane, passing through a fixed point P, is";
              quest[22] = "A cylindrical tank is 1/2 full. When 6 quarts are added, the tank is 2/3 full. The capacity of the tank, in quarts, is";
              quest[23] = "The diameters of twh wheels are 10 in. and 4 in. The smaller makes 50 more revolutions that the larger in going a certain distance. This distance, in inches is";
              quest[24] = "A boy grew one year from a height of x inches to a height of y inches. The percent of increase was";
              quest[25] = "A boy wishes to cut the largest possible square out of a piece of cardboard in the shape of a right triangle, with legs of 8 inches and 12 inches. The side of the square, in inches is";


              answerDB[1] = "4 < x < 6_-4 > x > -16_4 > x > -16_x < 16";
              answerDB[2] = "14.63_17.25_13.64_11.57";
              answerDB[3] = "4 lines_2 lines_2 points_4 points";
              answerDB[4] = "I, II, and IV only_II and III only_I and III only_I and II only";
              answerDB[5] = "2.21_1.78_1.21_0.78";
              answerDB[6] = "multiplication_division_subtraction_addition";
              answerDB[7] = "K * 0 = 0 for every integer K_0 is its own additive inverse_K + (-K) = 0 for every integer K_K + 0 = k for every integer K";
              answerDB[8] = "29%_27%_31%_33%";
              answerDB[9] = "20.99_20.19_19.38_19.25";
              answerDB[10] = "7.66_8.13_.77_0.077";
              answerDB[11] = "an ellipse_a circle_a hyperbola_a parabola";
              answerDB[12] = "39_38_37_21";
              answerDB[13] = "-2_1_-1_0";
              answerDB[14] = "15:14_14:15_10:21_5:7";
              answerDB[15] = "67_75_33_185";
              answerDB[16] = "s < 0_r > s_r < 0_r > s";
              answerDB[17] = "If Bill is not a mature student, then he is not a senior._If Bill is not a senior, then he is not a mature student._If Bill is a mature students, then he is a senior._All mature students are senior.";
              answerDB[18] = "x > 4_8 > x > 4_x > 3_x > 2";
              answerDB[19] = "a circle_a plane_two circles_a point";
              answerDB[20] = "67.2_134.4_38.6_72";
              answerDB[21] = "a circle_a point_two straight line_a straight line";
              answerDB[22] = "36_40_24_18";
              answerDB[23] = "1750pi_3500pi_1750_3500";
              answerDB[24] = "100 (y - x) / x_y - x / x_100 (x - y) / x_100 (y - x) / y";
              answerDB[25] = "4.8_4.5_5_4";
              
              picture[2] = "<?php echo base_url();?>assets/activity/web/Al32.jpg";
              picture[9] = "<?php echo base_url();?>assets/activity/web/Al33.jpg";
              
              
          }

            //getting questions
            doneQuest = document.getElementById("fquest").value;
            c = parseInt(document.getElementById("count").value);            
            if(c=="0")
            {
                document.getElementById("h1").style.visibility = "visible";
                document.getElementById("h2").style.visibility = "visible";
                document.getElementById("h3").style.visibility = "visible";
                document.getElementById("h4").style.visibility = "visible";
                document.getElementById("questTbl").style.visibility="visible";
                document.getElementById("total").value = quest.length;
            }            
            
            rand = Math.floor(Math.random() * (quest.length-1)) + 1;
            if(doneQuest!="0")
            {
                questSplit = doneQuest.split("_");
                for(var x=0;x<questSplit.length;x++)
                {
                    var ctr = parseInt(questSplit[x]);
                    if(rand == ctr)
                    {
                        rand = Math.floor(Math.random() *  (quest.length-1)) + 1;
                        x=-1;
                    }
                }
            }
            var swapCtr = document.getElementById("swapSwitch").value;
            if(swapCtr=="0")
            {
                document.getElementById("swap").value = rand;
            }
            //counting the finished questions and displaying questions
            c+=1;
            document.getElementById("count").value=c;
            document.getElementById("q").value=quest[rand];
            document.getElementById("pic").src = picture[rand];
            if(doneQuest!="")
            {
                document.getElementById("fquest").value += "_";
            }
            document.getElementById("fquest").value += rand;
            //getting choices and it will appear in random
            answerSplit = answerDB[rand].split("_");
            rand = Math.floor(Math.random() * 4) + 1;
            for(var x=0;x<4;x++)
            {
                for(var y=0;y<4;y++)
                {
                    if(rand==answerDisplay[y])
                    {
                        rand = Math.floor(Math.random() * 4) + 1;
                        y=-1;
                    }
                }
                answerDisplay[x]=rand;
            }
            //displaying choices and amount
            document.getElementById("ans").value = answerSplit[0];
            document.getElementById("a").value=answerSplit[answerDisplay[0]-1];
            document.getElementById("b").value=answerSplit[answerDisplay[1]-1];
            document.getElementById("c").value=answerSplit[answerDisplay[2]-1];
            document.getElementById("d").value=answerSplit[answerDisplay[3]-1];

            if(answerSplit[0]==answerSplit[answerDisplay[0]-1])
               document.getElementById("ansNum").value = "0";
           else if(answerSplit[0]==answerSplit[answerDisplay[1]-1])
            document.getElementById("ansNum").value = "1";
        else if(answerSplit[0]==answerSplit[answerDisplay[2]-1])
            document.getElementById("ansNum").value = "2";
        else if(answerSplit[0]==answerSplit[answerDisplay[3]-1])
            document.getElementById("ansNum").value = "3";
            //amount
            rand = Math.floor((Math.random() * 10) + 1) * 1000;
            document.getElementById("amount").value=rand;

            document.getElementById("draw").value=="1";
            enableChoice();

        }
        function letA()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("a").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }
            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
            
        }
        function letB()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("b").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
        }
        function letC()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("c").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }

            else
            {
                disableChoice();
            }

            
        }
        function letD()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("d").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
        }
        function disableChoice()
        {
            document.getElementById("btn1").disabled = true;
            document.getElementById("btn2").disabled = true;
            document.getElementById("btn3").disabled = true;
            document.getElementById("btn4").disabled = true;
            document.getElementById("helptbl").style.visibility = "hidden";
            var cnt = parseInt(document.getElementById("count").value); 
            var total = parseInt(document.getElementById("total").value);

            if(cnt==total-1)
            {
                //clearing the textboxes after all questions are finished
                document.getElementById("q").value="FINISHED";                
                document.getElementById("h5").style.visibility = "visible";                
                document.getElementById("go").style.visibility = "hidden";
                document.getElementById("helptbl").style.visibility = "hidden";
                document.getElementById("cattbl").style.visibility = "visible";
                document.getElementById("on").value = "1";
                document.getElementById("count").value = "0";
                document.getElementById("fquest").value = "";
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo base_url();?>index.php/activity/save_score',
                    data: {
                        cat_id      : cat_id, 
                        cat_name    : cat_name,
                        score       : document.getElementById("balance").value
                    },
                    success: function(msg){
                        alert(msg);
                    }
                });
                alert("Congratulations! You finished this category!");
                
            }
            
        }
        function gameover()
        {
            document.getElementById("btn1").disabled = true;
            document.getElementById("btn2").disabled = true;
            document.getElementById("btn3").disabled = true;
            document.getElementById("btn4").disabled = true;
            document.getElementById("helptbl").style.visibility = "hidden";
            document.getElementById("on").value = "1";
        }
        function enableChoice()
        {
            document.getElementById("btn1").disabled = false;
            document.getElementById("btn2").disabled = false;
            document.getElementById("btn3").disabled = false;
            document.getElementById("btn4").disabled = false;
            document.getElementById("helptbl").style.visibility = "visible";
        }
        function slot()
        {
            
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/3a.gif";
            },200);
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/2a.gif";
            },400);
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/1a.gif";
            },600);
            

            document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/2a.gif";
            if(document.getElementById("on").value == "0")
            {
                var stop = 0;
                var myTimer = setInterval(function()
                {
                    stop+=1;
                    if(stop<=5)
                    {
                        var ctr = document.getElementById("draw").value;
                        if(ctr=="0")
                            slot2();
                        else if(ctr=="1")
                            slot3();
                    }
                    else
                    {
                        clearInterval(myTimer);
                        getQ();
                    }

                },800);

            }
            document.getElementById("on").value = "1";           
        }
        function slot2()
        {
            var time= 100;
            var rand = [3];
            var disp = [3];
            for(var z=0;z<3;z++)
            {
                disp[z]="";
                rand[z] = Math.floor(Math.random() * 5);
                if(rand[z]==0)
                 disp[z] += "b";

             else if(rand[z]==1)
                 disp[z] += "g";

             else if(rand[z]==2)
                 disp[z] += "l";

             else if(rand[z]==3)
                 disp[z] += "p";

             else if(rand[z]==4)
                 disp[z] += "r";
         }

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "2.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "4.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "6.gif";
        },time);

         time+=200;

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "3.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "3.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "7.gif";
        },time);

         time+=200;

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "4.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "2.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "1.gif";
        },time);

         time+=200;

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "5.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "1.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "2.gif";
        },time);

         time+=200;

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "6.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "7.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "3.gif";
        },time);
         time+=200;
         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "7.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "6.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "4.gif";
        },time);

         time+=200;

         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "1.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "5.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "5.gif";
        },time);
         time+=200;
         setTimeout(function()
         {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "4.gif";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "4.gif";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "4.gif";
        },time);
     }
     function slot3()
     {
        var rand = [3];
        var disp = [3];
        for(var z=0;z<3;z++)
        {
            disp[z]="";
            rand[z] = Math.floor(Math.random() * 9) + 1;
            disp[z] += rand[z];
        }
        var time= 100;

        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a7.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a7.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
        },time);
        time+=100;
        setTimeout(function()
        {
            document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a" +disp[0]+ ".jpg";
            document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a" +disp[1]+ ".jpg";
            document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a" +disp[2]+ ".jpg";
        },time);
    }
    function lot()
    {
     document.getElementById("go").style.visibility = "visible";
     document.getElementById("on").value = "0";
     document.getElementById("questTbl").style.visibility="hidden";           
     document.getElementById("cattbl").style.visibility = "hidden";

     
 }
 function cat1()
 {
     document.getElementById("category").value = "1";
     document.getElementById("cat1").style.visibility = "hidden";
     cat_id = "1";
     lot();
 }
 function cat2()
 {
     document.getElementById("category").value = "2";
     document.getElementById("cat2").style.visibility = "hidden";
     cat_id = "2";
     lot();
 }
 function cat3()
 {
     document.getElementById("category").value = "3";
     document.getElementById("cat3").style.visibility = "hidden";
     cat_id = "3";
     lot();
 }
 function cat4()
 {
     document.getElementById("category").value = "4";
     document.getElementById("cat4").style.visibility = "hidden";
     cat_id = "4";
     lot();
 }
 function cat5()
 {
     document.getElementById("category").value = "5";
     document.getElementById("cat5").style.visibility = "hidden";
     cat_id = "5";
     cat_name = "ALGEBRA 1";
     lot();
 }
 function cat6()
 {
     document.getElementById("category").value = "6";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "6";
     cat_name = "ALGEBRA 2";
     lot();
 }
 function cat7()
 {
     document.getElementById("category").value = "7";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "7";
     cat_name = "GEOMETRY 1";
     lot();
 }
 function cat8()
 {
     document.getElementById("category").value = "8";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "8";
     cat_name = "GEOMETRY 2";
     lot();
 }
 function cat9()
 {
     document.getElementById("category").value = "9";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "9";
     cat_name = "TRIGO 1";
     lot();
 }
 function cat10()
 {
     document.getElementById("category").value = "10";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "10";
     cat_name = "TRIGO 2";
     lot();
 }
 function cat11()
 {
     document.getElementById("category").value = "11";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "11";
     cat_name = "CALCULUS 1";
     lot();
 }
 function cat12()
 {
     document.getElementById("category").value = "12";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "12";
     cat_name = "CALCULUS 2";
     lot();
 }
 function cat13()
 {
     document.getElementById("category").value = "13";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "13";
     cat_name = "DIFF EQUATION";
     lot();
 }
 function cat14()
 {
     document.getElementById("category").value = "14";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "14";
     cat_name = "ADVANCED ALGEBRA";
     lot();
 }
 function cat15()
 {
     document.getElementById("category").value = "15";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "15";
     cat_name = "MIXED SUBJECT 1";
     lot();
 }
 function cat16()
 {
     document.getElementById("category").value = "16";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "16";
     cat_name = "MIXED SUBJECT 2";
     lot();
 }
 function cat17()
 {
     document.getElementById("category").value = "17";
     document.getElementById("cat6").style.visibility = "hidden";
     cat_id = "17";
     lot();
 }
 function skin()
 {
  if(document.getElementById("on").value=="0")
      if(document.getElementById("draw").value=="0")
      {
        document.getElementById("draw").value="1";
        document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
        document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
        document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
    }
    else if(document.getElementById("draw").value=="1")
    {
        document.getElementById("draw").value="0";
        document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/b4.gif";
        document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/g4.gif";
        document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/r4.gif";
    }
}
function nw()
{
  location.reload();
}


</script>
<style type="text/css">
a:hover {color:#FF00FF;}
#header h1
{

    -webkit-animation:myfirst 7s; /* Chrome, Safari, Opera */
    animation:myfirst 7s;
}

/* Chrome, Safari, Opera */
@-webkit-keyframes myfirst
{
    from {background:cadetblue;}
    to {background:lightblue;}
}
#main .b1
{
    background:aliceblue;
    -webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
    transition:width 5s;
}

.b1:hover
{
    width:60px;
}
#main .b2
{
    background:aliceblue;
    -webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
    transition:width 5s;
}

.b2:hover
{
    width:60px;
}
#main .b3
{
    background:aliceblue;
    -webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
    transition:width 5s;
}

.b3:hover
{
    width:60px;
}
#main .b4
{
    background:aliceblue;
    -webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
    transition:width 5s;
}

.b4:hover
{
    width:60px;
}
body {
	background-color: #BCE0A8;
}
.style1 {
	font-size: 16px;
	color: #FF0000;
}
.style4 {
	color: #006666;
	font-style: italic;
	font-weight: bold;
	font-size: 24px;
}
.style7 {color: #0000FF}
.style18 {font-size: 13px; color: #003333; font-weight: bold; font-family: "Arial Narrow"; }
.style19 {
	color: #009933;
	font-weight: bold;
}
.style28 {color: #FF0000}
.style29 {font-size: 14px; font-weight: bold; font-family: "Arial Narrow";}
</style>
</head>
<body>
    <div id="container">

      <div id="header">
          <h1><img src="<?php echo base_url();?>assets/activity/images/head.jpg" width="632" height="141"></h1>
          <p align="left"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/activity/images/links.jpg" alt="1" width="31" height="14" /><span class="style19">Main Page</span></a> </p>
      </div>
      <div id="content">
          <div id="nav">
            <h3 class="style7">&nbsp;</h3>
            <table id="cattbl">
                <tr><th><p><a  class="selected style1">Category</a></p>
                </th>
            </tr>
            <tr>
              <td><a  class="style18" id="cat5" onClick="cat5()">Algebra 1 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat6" onClick="cat6()">Algebra 2 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat7" onClick="cat9()">Trigonometry 1 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat8" onClick="cat10()">Trigonometry 2 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat7()">Geometry 1 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat8()">Geometry 2 </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat11()">Calculus I </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat12()">Calculus II </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat13()">Differential Equation </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat14()">Advance Algebra </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat15()">Mixed Subject I </a></td>
          </tr>
          <tr>
              <td><a  class="style18" id="cat9" onClick="cat16()">Mixed Subject II </a></td>
          </tr>
          <tr>
            <td><div align="left" class="style28"><span class="style29"><a href="<?php echo base_url();?>index.php/activity/game/game2">Past Board Exam</a></span></div></td>
        </tr>
    </table>
    <p>&nbsp;</p>
</div>
<div id="main">
    <h3 class="style4">LOTTERY</h3>
    <form name="home">
        <table  id="questTbl" bgcolor="white" style="visibility: hidden;vertical-align: middle">
           <tr align="center">
            <td>
                <font color="green">BALANCE</font>
                <input type="text"id="balance" name="balance" value="7000" style="color:blue" disabled>
                <font color="red">AMOUNT</font>
                <input type="text"id="amount" name="amount" style="color: darkviolet"disabled>
            </td>
        </tr>
        <tr>
            <td><textarea rows="2.5" cols="40" id="q" name="q" style="color:brown;font-weight:bold;font-size:18" disabled></textarea></td>
        </tr>
        <tr>
            <td align="center">
                <IMG SRC = "<?php echo base_url();?>assets/activity/web/0.jpg" width="120" height="120" id="pic" name="pic"onerror="this.src='<?php echo base_url();?>assets/activity/web/0.jpg'">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="b1" id="btn1" value=" A " onClick="letA()">
                    <input type="text" size="58" id="a" name="a" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="b2" id="btn2" value=" B " onClick="letB()">
                    <input type="text" size="58" id="b" name="b" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="b3" id="btn3" value=" C " onClick="letC()">
                    <input type="text" size="58" id="c" name="c" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="b4" id="btn4" value=" D " onClick="letD()">
                    <input type="text" size="58" id="d" name="d" disabled>
                </td>
            </tr>
            <tr>
                <td><center>
                    <input type="hidden" id="fquest" name="fquest" value="">
                    <input type="hidden" id="count" name="count" value="0">
                    <input type="hidden" id="ans" name="ans">
                    <input type="hidden" id="ansNum" name="ansNum" value="0">
                    <input type="hidden" id="draw" value="0">
                    <input type="hidden" id="on"  value="0">
                    <input type="hidden" id="total" name="total" value="">
                    <input type="hidden" id="category" name="category" value="">
                    <input type="hidden" id="swap" name="swap" value="">
                    <input type="hidden" id="swapSwitch" name="swapSwitch" value="0">
                </center></td>
            </tr>
        </table>
        <br>
        <table border="5" id="go"style="background-color: white;visibility: hidden"><center>
         
            <tr><td><font color="darkseagreen" size="5" style="visibility: visible;"> &nbsp; D &nbsp; <br> &nbsp; R &nbsp; <br> &nbsp; A &nbsp; <br> &nbsp; W &nbsp; </font></td>
                <td><IMG SRC = "<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="1" name="1"></td>
                <td><IMG SRC = "<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="2" name="2" ></td>
                <td><IMG SRC = "<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="3" name="3" ></td>
                <td><a  onCLick="slot()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/1a.gif" id="next" name="next"></a></td>
            </tr></center></table>

        </form>
        <table id="helptbl" name="helptbl" style="visibility: hidden">
            <tr>
                <td><a  id="h1" onClick="help1()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/1.jpg" width="50" height="50"></a></td>
                <td><a  id="h2" onClick="help2()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/2.jpg" width="50" height="50"></a></td>
                <td><a  id="h3" onClick="help3()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/3.jpg" width="50" height="50"></a></td>
                <td><a  id="h4" onClick="help4()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/4.jpg" width="50" height="50"></a></td>
                <td><a  id="h5" onClick="skin()"><IMG SRC = "<?php echo base_url();?>assets/activity/web/5.jpg" width="50" height="50"></a></td>
                <td><a  id="newg" onClick="nw()" style="visibility: visible"><IMG SRC = "<?php echo base_url();?>assets/activity/web/newgame.jpg" width="80" height="50"></a></td>
                <td></td>


            </tr>
        </table>
    </div>
</div>
<div id="footer">	</div>
</div>

</body>
</html>
