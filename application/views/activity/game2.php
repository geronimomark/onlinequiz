﻿<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Online Game System</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/activity/web/newcss.css">
        <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>  
        <script language="javascript" type="text/javascript">
        function help1()
        {
            var answer = parseInt(document.getElementById("ansNum").value);
            var a=0, b=1, c=2, d=3;
            var check=0;

            var cat_id = "";
            var cat_name = "";

            do
            {
                var rand = Math.floor(Math.random() * 4);

                if(rand!=answer && rand==a)
                {
                    document.getElementById("a").value="";
                    check+=1;
                    a=-1;
                }

                else if(rand!=answer && rand==b)
                {
                    document.getElementById("b").value="";
                    check+=1;
                    b=-1;
                }
                else if(rand!=answer && rand==c)
                {
                    document.getElementById("c").value="";
                    check+=1;
                    c=-1;
                }
                else if(rand!=answer && rand==d)
                {
                    document.getElementById("d").value="";
                    check+=1;
                    d=-1;
                }
            }while(check<2);
            document.getElementById("h1").style.visibility="hidden";
        }
        function help2()
        {
            getQ();
            document.getElementById("h2").style.visibility="hidden";
        }
        function help3()
        {
            var answer = document.getElementById("ans").value;
            var rand = Math.floor(Math.random() * parseInt(answer.length-1));
            var display = "One character of the correct answer is: ";
            display += answer.charAt(rand);
            alert(display);
            //document.getElementById("q").value += "\n\n "+"(one correct character " +answer.charAt(rand) + ")";
            document.getElementById("h3").style.visibility="hidden";
        }
        function help4()
        {
            var amt = parseInt(document.getElementById("amount").value)/2;
            document.getElementById("amount").value = amt;
            document.getElementById("h4").style.visibility="hidden";

        }

        function getQ()
        {
            var doneQuest = ""; //list of finished questions
            var questSplit = [1]; //question holder
            var quest = [30]; //database for question
            var picture = [30]; //database for pictures
            var rand = 0, c=0; //used for all randomized objects
            var answerDB = [4];//array database
            var answerSplit = [1];//choice holder
            var answerDisplay = [4];//random displayer


            var questCategory = "";
            questCategory = document.getElementById("category").value;

            if(questCategory=="1")
            {
                //questions
                quest[1] = "In algebra, what refers to the operation of root extraction";
                quest[2] = "What refers to the difference between an approximate value of a quantity and its exact value or true value?";
                quest[3] = "If the roots of an equation are zero, then these roots are classified as what type of solution?";
                quest[4] = "What refer to a sequence of numbers where the succeeding term is greater than preceding term?";
                quest[5] = "Convergent series is a sequence of decreasing numbers or when the succeeding term is _________ preceeding term.";
				quest[6] = "In complex algebra, we use a diagram to represent a complex plane commonly called _____.";
                quest[7] = "What is the sequence of numbers such that successive term differ by a constant?";
                quest[8] = "What is a frequency curve which is composed of a series of rectangles connected with the steps as the base and the frequency as the height?";
                quest[9] = "The graphical representation of a cumulative frequency distribution in a set of statistical data is called ________.";
                quest[10] = "If a = b, then b = a. This illustrate which axiom in algebra";
				quest[11] = "What refers to the smallest natural number for which 2 natural numbers are factors";
                quest[12] = "To compute the value of n factorial, in symbolic form (n!), where n is large number , we use formula called";
                quest[13] = "It is a measure of relationship between two variable.";
                quest[14] = "In character population which is measurable";
                quest[15] = "The quartile deviation is a measure of ";
				quest[16] = "The problem of the region bounded by two concentric circles is called";
                quest[17] = "When two planes intersect with each other, the amount of divergence between the two planes is expressed by measuring the__________.";
                quest[18] = "It is a polyhedron of which two faces are equal polygons in parallel planes, and the other faces are parallelograms.";
                quest[19] = "Each of the faces of a regular hexahedron is a";
                quest[20] = "An annulus is a plane figure, which composed of two concentric circles. The area of the annulus can be calculated by getting the difference between the area of a larger circle. Also, it can be calculated by removing the hole. This method is called";
				quest[21] = "Which of the following quadratic equations will have two real and distinct roots?";
                quest[22] = "Radicals can be added if they have the same radicand and the same";
                quest[23] = "Drawing a card from a deck of cards is called";
                quest[24] = "The set of integers does not satisfy the closure property under the operation of";
                quest[25] = "A number which can be expressed as the quotient of two integers is";
				quest[26] = "All board reviewees are not more than 25 years old. This statement implies that they are";
                quest[27] = "The roots of the equation 6x^2 - 61x + 143 = 0 are";
                quest[28] = "A set of elements that is taken without regard to the order in which the elements are arranged is called a:";
                quest[29] = "If the value of the discriminant of a quadratic equation is 1.25 , then the root of the quadratic are:";
                quest[30] = "Find the value of k that will make x^2 - 28x + k a perfect square trinomial";
                //choices
				answerDB[1] = "Evolution_Resolution_Revolution_Involution";
                answerDB[2] = "Absolute Error_Relative Error_Mistake_Difference";
                answerDB[3] = "Trivial Solution_Conditional Solution_Extraneous Solution_Hypergolic Solution";
                answerDB[4] = "Divergent Sequence_Isometric Sequence_Convergent Sequence_Dissonant Sequence";
                answerDB[5] = "Lessr Than_Equal to_Twice_Greater Than";
				answerDB[6] = "Argand diagram_Maxwell diagram_Venn diagram_Block diagram";
                answerDB[7] = "Arithmetic Progression_Infinite Geometric Progression_Geometric Progression_Harmonic Progression";
                answerDB[8] = "Frequency Distribution_Ogive_Histogram_Bar graph";
                answerDB[9] = "ogive_leptokurtic_histogram_kurtosis";
                answerDB[10] = "Symmetric axiom_Reflexive axiom_Transitive axiom_Replacement  axiom";
				answerDB[11] = "Least Common Multiple_Least Common Divison_Least Common Denominator_Least Common Factor";
                answerDB[12] = "Stirling Approximation_Richardson-Duchman Formula_Diophantine Formula_Matheson’s Formula";
                answerDB[13] = "Correlation_Equation_Function_Relation";
                answerDB[14] = "Frequency_Unit_Distribution_Sample";
                answerDB[15] = "dipersion_division_certainty_central tendency";
				answerDB[16] = "annalus_circular disk_ring_washer";
                answerDB[17] = "dihedral angle_polyhedral angle_reflex angle_plane angle";
                answerDB[18] = "Prism_Tetrahedron_Frustum_Prismatoid";
                answerDB[19] = "square_triangle_rectangle_hexagon";
                answerDB[20] = "Law of extremities_Law of deduction_Law of reduction_Law of composition";
				answerDB[21] = "6x^2 - 61x + 143 = 0_x^2 - 22x + 121 = 0_9x^2 - 6x + 1 = 0_6x^2 - 5x + 4";
                answerDB[22] = "order_power_exponent_coefficient";
                answerDB[23] = "an experiment_a trial_an outcome_an event";
                answerDB[24] = "division_multiplication_subtraction_addition";
                answerDB[25] = "irrational_rational_natural_prime";
				answerDB[26] = "25 years old or less_25 years old or more_at least 25 years old_less than 25 years old";
                answerDB[27] = "real and distinct_real and equal_complex and distinct_complex and unequal";
                answerDB[28] = "combination_permutation_progression_probability";
                answerDB[29] = "real and unequal_real and equal_complex and unequal_imaginary and distinct";
                answerDB[30] = "196_169_144_121";


                picture[1] = "";
                
            }
            else if(questCategory=="2")
            {
                //questions
                quest[1] = "In how many ways can a picture be painted by using two or more of 7 different colors?";
                quest[2] = "What is the probability of getting a number 4 thrice in five tosses of a die?";
                quest[3] = "In how many ways can 8 persons be seated at a round table if a certain 2 are sit next to each other?";
                quest[4] = "If x: y: z = 4: -3:2 and 2x + 4y - 3z = 20, find the value of x.";
                quest[5] = "At a math contest, the judges eliminate 1/3 of the contestants after each half hour. If 81 contestants were present at the start, how many would be left after 2 hours?";
				quest[6] = "Getting an odd number by throwing a die is called";
				quest[7] = "Two prime numbers, which differ by two, are called prime twins. Which of the following pairs of numbers are prime twins?";
                quest[8] = "The probability of A's winning a game against B is called 1/3. What is the probability that A will win at least two off total of 3 games?";
                quest[9] = "If P (n+1, 4) = 2P(n, 4)";
                quest[10] = "What is the probability of getting the number 1 thrice when a die is tossed 5 times?";
                quest[11] = "In how many ways can 7 boys be seated in a row so that 3 boys are always seated together?";
				quest[12] = "What is the probability of obtaining at least 4 tails when a coin is tossed five times?";
             	quest[13] = "Mr. Diaz can finish a job in 9 hours. After working for 5 hrs, he decided to take a rest. Mr. Torres helped Mr. Diaz finished the job in 2 hrs and 20 minutes. How long would it take Mr. Torres to do the job alone?";
				quest[14] = "Find 2 numbers whose sum is 12 and the sum of their squares is 74.";
				quest[15] = "Two jeepneys start at the same point but are going in different directions. If jeepney A runs at the rate of 60 km/hr and jeepney B at 50 km/hr and both starts at the same time, when will the two jeepney be 550 km apart?";
                quest[16] = "A man and a boy can dig a trench in 20 days. It would take the boy 9 days longer to dig it alone than it would take the man. How long would it take the boy to dig it alone?";
                quest[17] = "What is the sum of prime numbers between 1 and 15?";
                quest[18] = "The sum of integers that are exactly divisible by 15 between 288 and 887 is";
                quest[19] = "If 16 is 4 more than 3x, find the value of 2x - 5.";
				quest[20] = "Which of the following quadratic trinomial is NOT factorable?";
				quest[21] = "If the radius of the circle is diminished by 20%, then its area is diminished by";
				quest[22] = "Two sisters are 14 and 21 years old respectively. In how many years will the ratio of their ages be 3:4?";
             	quest[23] = "Car A runs 30 km/hr less than Car B. Car A covers 250 km in the same time car B travels 400 km. Find the rate of each.";
				quest[24] = "The product of two irrational numbers is  ";
				quest[25] = "The length of a rectangle is increased by 50%. By what percent wouldt the width have to be decreased to maintain the same area?";
                quest[26] = "The altitude of the cone is equal to half the diameter of the sphere circumscribed about it. How many times is the volume of the sphere greater than that of the cones?";
                quest[27] = "The perimeter of a rectangle is 22. If one of the rectangle is doubled and the other tripled, the perimeter would be 32 more than the perimeter of the original rectangle. What are the sides of the rectangle?";
                quest[28] = "If a polygon has 54 diagonals, then it must have  ";
                quest[29] = "A trench is constructed so that its cross section is trapezoid the area of which is 21 sq. ft. if the shorter base is 2 times its height and the longer base is 5 ft. longer than its height. Find the length of the shorter base.";
				quest[30] = "If the acute angles of a right triangle are in the ratio 1:2, then the number of degrees in the small angle is:";			
                //choices
                answerDB[1] = "120_110_128_131";
                answerDB[2] = "0.0322_0.0232_0.3220_0.2330";
                answerDB[3] = "1,440_1,008_4,140_5,040";
                answerDB[4] = "-8_7_-4_6";
                answerDB[5] = "16_18_12_10";
				answerDB[6] = "an event_a trial_an outcome_an experiment";
				answerDB[7] = "(17, 19)_(13, 15)_(7, 9)_(1, 3)";
                answerDB[8] = "7/27_8/27_19/278_15/27";
                answerDB[9] = "7_10_-7_4";
                answerDB[10] = "0.0322_0.0355_0.0233_0.0223";
                answerDB[11] = "720_360_144_270";
				answerDB[12] = "0.1875_0.1785_0.1758_0.1857";
				answerDB[13] = "5 hrs and 15 min_6 hrs and 20 min_4 hrs and 10 min_3 hrs and 5 min";
				answerDB[14] = "7 and 5_6 and 6_4 and 8_3 and 9";
				answerDB[15] = "5 hrs_4 hrs_6 hrs_7 hrs";
                answerDB[16] = "45 days_54 days_35 days_36 days";
                answerDB[17] = "41_38_39_42";
                answerDB[18] = "23,700_23,805_22,815_21,810";
                answerDB[19] = "3_2_4_5";
				answerDB[20] = "6x^2 - 52x - 60_6x^2 - 61x + 143_6x^2 - 7x – 343_6x^2 + 5x – 4";
				answerDB[21] = "36%_52%_46%_25%";
				answerDB[22] = "6_7_8_9";
				answerDB[23] = "50 km/hr and 80 km/hr_60 km/hr and 90 km/hr_70 km/hr and 100 km/hr_800 km/hr and 110 km/hr";
				answerDB[24] = "sometimes irrational_always irrational_never irrational_rational";
				answerDB[25] = "33 1/3_50_66 2/3_150";
                answerDB[26] = "4_½_2_¼";
                answerDB[27] = "6 and 5_8 and 7_10 and 9_12 and 11";
                answerDB[28] = "12 sides_10 sides_11 sides_13 sides";
                answerDB[29] = "6_5_4_3";
				answerDB[30] = "30_20_45_90";
            
            }
            else if(questCategory=="3")
			 {
                //questions
			
			    quest[1] = "The length of the wire fence around a circular flowerbed is 10p feet. The area of the flowerbed in sq. ft. is";
                quest[2] = "The perimeter of rectangle is 28 m and its diagonal is 10 m. Find the area of the rectangle.";
                quest[3] = "The parabola y= 4 - x^2 opens";
                quest[4] = "The intersection of the medians of the triangle whose vertices are (-6, -8), (3, -5), and (4, -2) are at";
                quest[5] = "A parabola maybe defined as the set of points that are equidistant from a fixed point and a fixed line. The fixed point is called the focus and the fixed line is called the";
				quest[6] = "If the eccentricity of a conic section is greater than one, then it is";
				quest[7] = "The vertex of the parabola y^2 - 4x + 6y + 13 = 0 is at";
                quest[8] = "The equation of a line passing through (2, 2) and (4, 8) is y = _________.";
                quest[9] = "The straight line equation y = x - 5 passes through the points";
                quest[10] = "The equation of the line passing through (0, 4) and parallel to the x-axis is";
                quest[11] = "The area of the triangle whose vertices are (-3, 8), (7, 4), and (5, -1) is";
				quest[12] = "A quadrilateral whose four sides are equal and parallel with no angle equal to a right angle is called ________.";
             	quest[13] = "A tree stands vertically on a hillside that makes an angle of 22 deg. with the horizontal. From a point 15 m down the hill from the base of the tree, the angle of elevation of the top of tree is 44 deg. Find the distance from the point to the top of the flagpole.";
				quest[14] = "A flagpole 49 m high is situated at the top of a hill. At a point 183 m down the hill, the angle between the surface of the hill and line to the top of the flagpole is 8 degrees. Find the distance from the point to the top of the flagpole";
				quest[15] = "A 90-degree arc on the earth is equal to how many nautical miles in length?";
                quest[16] = "The plane of a small cirlce on a sphere of radius 25 cm is 7 cm from the center of the sphere. Find the radius of the small circle.";
                quest[17] = "Find the kilometers of the length of an arc of a great circle on the earth, if its length is 12.5 degrees";
                quest[18] = "A spherical triangle has angles A, B, and C, each of which is less than 180 degrees. Which of the following is true?";
                quest[19] = "A spherical triangle, which contains at least one side equal to a right angle, is called";
				quest[20] = "Express in hour, minutes and seconds the time corresponding to 260 deg and 34'.";
				quest[21] = "An icosahedron is a regular polyhedron having";
				quest[22] = "The angle formed by two intersecting planes is called a";
             	quest[23] = "Find the area of pentagon whose apothem is 10 cm.";
				quest[24] = "The radii of two sphere are in the ratio 3:4 and the sum of their surface area is 2500 p sq. cm. Find the radius of the smaller sphere.";
				quest[25] = "A face diagonal of a cube is 4 cm. Find the volume of the cube";
                quest[26] = "The volume of a cube is reduced to _________ if all sides are halved.";
                quest[27] = "The sum of angles of a polygon of n sides equals.";
                quest[28] = "Each exterior angle of a polygon of n sides is";
                quest[29] = "The median of a trapezoid equals ______ the sum of the bases";
				quest[30] = "A pyramid has a base whose sides are 10 m, 16 m, and 18 m. If the altitude of the pyramid is 20 m, find the volume of the inscribed cone.";	
				//choices
				answerDB[1] = "25p_5p_50p_100p";
                answerDB[2] = "48 sq. m_38 sq. m_10 sq. m_28 sq. m";
                answerDB[3] = "downward_upward_to the left_to the right";
                answerDB[4] = "(1/3, -5)_(-1/3, 5)_(-1/3, -5)_(0, 0)";
                answerDB[5] = "directrix_tangent line_latus rectum_asymptote";
				answerDB[6] = "a hyperbola_a parabola_a circle_an ellipse";
				answerDB[7] = "(1, -3)_(-3, 1)_(0, 0)_(1, 1)";
                answerDB[8] = "3x – 4_x – 4_3x + 2_x + 2";
                answerDB[9] = "(0, -4)_(2, 2)_(3, 0)_(6, 3)";
                answerDB[10] = "y = 4_4x + y = 0_y = x^2 + 4_4y - 2 = x";
                answerDB[11] = "30 sq. units_60 sq. units_29 sq. units_40 sq. units";
				answerDB[12] = "rhombus_trapezoid_parallelepiped_parallelogram";
				answerDB[13] = "7. 81 m_7. 71 m_7. 61 m_7.51 m";
				answerDB[14] = "223.1 m_232.1 m_322.1 m_233.1 m";
				answerDB[15] = "5,400_5,600_5,200_5,000";
                answerDB[16] = "24_23_22_21";
                answerDB[17] = "1389_1370_1352_1333";
                answerDB[18] = "A + B + C > 180_A + B + C = 360_A + B + C > 360_A + B + C = 180";
                answerDB[19] = "a quadrantal triangle_an isosceles triangle_a right triangle_a polar triangle";
				answerDB[20] = "17 hours, 22 minutes and 16 seconds_17 hours, 23 minutes and 16 seconds_b.	17 hours, 21 minutes and 16 seconds_17 hours, 20 minutes and 16 seconds";
				answerDB[21] = "20 faces_18 faces_16 faces_12 faces";
				answerDB[22] = "dihedral angle_central angle_vertical angle_face angle";
				answerDB[23] = "336.72 sq. cm_373.65 sq. cm_327.36 sq. cm_363.27 sq. cm";
				answerDB[24] = "15 cm_10 cm_20 cm_25 cm";
				answerDB[25] = "22. 63 cm^3_21.64 cm^3_23.62 cm^3_24.64 cm^3";
                answerDB[26] = "1/8_1/16_1/4_1/2";
                answerDB[27] = "(n - 2) * 180 degrees_(n - 2)(180 degrees)/n_360 degrees_180 degrees";
                answerDB[28] = "360/n_180/n_(n - 2)(180)/n_always 72 degrees";
                answerDB[29] = "1/2_1/3_1/4_1/5";
				answerDB[30] = "274.18_1715.36_325.1_376.57";
				

            }
            else if(questCategory=="4")
            {
 //questions
			
			    quest[1] = "The tangent and a secant are drawn to a circle from the same external point. If the tangent is 6 inches and the external segment of the secant is 3 inches, the length of the secant is ______ inches";
                quest[2] = "Two secant from a point outside the circles are 24 and 32. If the external segment of the first is 8, the external of the second is:";
                quest[3] = "A trench is constructed so that its cross section is a trapezoid the area of which is 21 square feet. If the shorter base is 2 times its height and the longer base is 5 feet longer than its height, find the height of the trench";
                quest[4] = "If the circular cylindrical tank axis horizontal, diameter 1 meter, and length 2 meters is filled with water up to a depth of 0.75 meter, how much water is the tank?";
                quest[5] = "The diagonal of a rhombus are equal to 30 cm and 40 cm. Find the distance between the plane of the rhombus and a point M if the latter is 20 cm distant from each of its sides.";
				quest[6] = "During the recent olympics on 4-men relay team completed in a 1600 meter relay have the following individual speed, R1 = 26 kph, R2 = 27 kph, R3 = 28 kph, R4 = 29 kph. What is the average speed of the team?";
				quest[7] = "A garden can be cultivated by 8 boys in 5 days. 5 men can do the same work in 6 days. How long would it take for 8 boys and 5 men to finish the job?";
                quest[8] = "At what time between 7 and 8 o'clock are the hands of the clock, at the right angles?";
                quest[9] = "At what time between 7 and 8 o'clock are the hands of the clock at 180 degrees apart?";
                quest[10] = "At what time between 7 and 8 o'clock are the hands of the clock together?";
                quest[11] = "A man left his office for a business appointment one afternoon and noticed his watch at past 2 o'clock. Between two to three hours later, the man returned and noticed that the hands of the clock have exchanged places. What time did he leave and arrive?";
				quest[12] = "Separate 132 into 2 such that the larger divided by the smaller, the quotient is 6 and the remainder is 13. Find the numbers";
             	quest[13] = "A number of two digits divided by the sum of the digits, the quotient is 7 and the remainder is 6. If the digits of the number are interchanged the resulting number exceeds three times the sum of the digits by 5. What is the number?";
				quest[14] = "The total tank of the car is filled with 50 liters of alcogas 25% of which is alcohol. How much of the mixture must be drawn off which when replaced by pure alcohol will yield a 50 - 50% alcogas?";
				quest[15] = "An alloy contains 25% silver and 10% copper. How much silver and how much copper must be added to 20 lbs of the alloy to obtain an alloy containing 38% silver and 36% copper?";
                quest[16] = "A motorist is traveling from A to B at a constant rate of 30 kph and return from B to A at constant rate of 20 kph. What is his average velocity?";
                quest[17] = "If A and B are not mutually exclusive events, then the probability of the joint occurrence of A and B or P(A and B) is";
                quest[18] = "When the occurence or no occurence of event A has no effect on the occurence of event B, then A and B are said to be";
                quest[19] = "The probability that the Ginebra basketball team will win the championship is assessed as being 1/3. Find the odds that the team will win.";
				quest[20] = "The odds that a reviewee will not pass the board exam are 1:4. Find the probability that the reviewee will not pass the exam.";
				quest[21] = "An experiment consists of selecting a random of three television tubes form a lot of 5 containing 2 defective. What is the probability of getting exactly two defective tubes?";
				quest[22] = "Out of 1,000 ECE reviewees, the probability that a reviewee picked at random is over 25 years old is 0.25 and the probability that he is less than 20 years old is 0.15. What is the probability that a reviewee picked at random is between 20 and 25 years old?";
             	quest[23] = "It is the highest score distribution minus the lowest score in the distribution";
				quest[24] = "It is a measure of central tendency, which depends upon the number of score and not the magnitude of the scores.";
				quest[25] = "If sin x = cos y, then";
                quest[26] = "cos x > 0 and sin x < 0, then x is in quadrant";
                quest[27] = "If an angle is 9 degrees more than twice its supplement then the number of degrees in the angle is";
                quest[28] = "In the triangle, one angle is 3 times as big as the other. If the sum of these angle is 120 degrees. Find the measure of the third angle.";
                quest[29] = "A triangle has sides of 4, 6, and 8 units long. If the shortest side of a similar triangle is 12 unit long, what are the lengths of the other two sides?";
				quest[30] = "Find the value of (1 + i)^12";	
				//choices
				answerDB[1] = "12_15_14_18";
                answerDB[2] = "6_8_10_12";
                answerDB[3] = "3 ft_4 ft_5 ft_6 ft";
                answerDB[4] = "1.2637 cubic meter_ 3.1415 cubic meter_1.51 cubic meter_2 cubic meter";
                answerDB[5] = "16 cm_15 cm_18 cm_17 cm";
				answerDB[6] = "27.45_28.46_26.44_25.44";
				answerDB[7] = "20/11 days_15/11 days_30/11 days_45/11 days";
                answerDB[8] = "7:22_7:18_7:15_7:24";
		        answerDB[9]	= "7:05_7:10_7:11_7:30"; 	
                answerDB[10] = "7:30_7:38_7:45_7:20";
                answerDB[11] = "2:26.01 and 5:12.17_2:15 and 5:30_2:20 and 5:32_2:23 and 5:40";
                answerDB[12] = "17 and 115_20 and 112_24 and 108_30 and 102";
				answerDB[13] = "83_72_48_39";
				answerDB[14] = "16.7_15.6_17.8_18";
				answerDB[15] = "14 lbs silver_15 lbs silver_20 lbs silver_30 lbs silver";
				answerDB[16] = "24_30_15_8";
                answerDB[17] = "not equal to zero_equal to zero_equal to unity_infinite";
                answerDB[18] = "independent_mutually exclusive_complementary_dependent";
                answerDB[19] = "1:2_2:1_1:3_3:1";
                answerDB[20] = "0.20_0.80_0.25_0.75";
				answerDB[21] = "0.30_0.25_0.35_0.40";
				answerDB[22] = "0.60_0.55_0.65_0.70";
				answerDB[23] = "range_deviation_variance_interval";
				answerDB[24] = "median_mean_mode_geometric mean";
				answerDB[25] = "x + y = 90 degrees_xy = 90 degrees_x - y = 90 degrees_none of the above";
				answerDB[26] = "IV_III_II_1";
                answerDB[27] = "123_114_57_27";
                answerDB[28] = "60 degrees_90 degrees_100 degrees_110 degrees";
                answerDB[29] = "18 and 24_19 and 23_17 and 25_16 and 26";
                answerDB[30] = "-64_-64i_-36_-36i";	
  		
		}
		else if(questCategory=="5")
            {
 //questions
   				quest[1] = "Mary is 24 years old. Mary is twice as old as Ann was when Mary was as old as Ann is now. How old is Ann now?";
                quest[2] = "The sum of Kim’s and Kevin’s ages is 18. In 3 years, Kim will be twice as old as Kevin. What are their ages now?";
                quest[3] = "Robert is 15 years older than his brother Stan. However “y” years ago, Robert was twice as old as Stan. If Stan is now “b” years old and b>y, find the value of (b – y)";
                quest[4] = "JJ is three times as old as Jan – Jan. Three years ago, JJ was four times as old as Jan-Jan. The sum of their age is";
                quest[5] = "A girl is one-third as old as her brother and 8 years younger than her sister. The sum of their age is 38 years. How old is the girl?";
				quest[6] = "Paula is now 18 years old and his colleague Monica is 14 years old. How many years ago was Paula twice as old as Monica?";
				quest[7] = "A father tells his son, “I was your age now when you were born.” If the father is now 38 years old, how old was his son 2 years ago?";
                quest[8] = "Six years ago, Nilda was five times as old as Riza. In five years, Nilda will be three times as old as Riza. What is the present age of Riza?";
                quest[9] = "At present, the sum of the parents’ ages is twice the sum of the children’s ages. Five years ago, the sum of the parents’ ages was 4 times of the children’s ages. Fifty years hence, the sum of the parents ‘ages will be equal to the sum of the children’s ages. How many children are there? ";
                quest[10] = "Debbie is now twice as old as Jerry. Four years ago, Debbie was three times as old as Jerry then, how old is Debbie?";
                quest[11] = "A pump can pump out water from a tank in 11 hours. Another pump can pump out water from the same tank in 20 hours. How long will it take both pumps to pump out the water in the tank?";
				quest[12] = "A tank can be filled in 9 hours by one pipe, 12 hours by a second pipe and can be drained when full by a third pipe in 15 hours. How long will it take to fill an empty tank with all pipes in operation?";
             	quest[13] = "Pedro can paint a house in 9 hours while Stewart can paint the same house in 16 hours. They work together for 4 hours, Stewart left and Glenn finished the job alone. How many more days did it take Glenn to finish the job?";
				quest[14] = "It takes Butch twice as long as it takes Dan to do a certain piece of work. Working together they can do the work in 6 days. How long would it take Dan to do it alone?";
				quest[15] = "A and B can do a piece of work in 42 days, B and C in 31 days and C and A in 20 days. In how many days can all of them do the work together?";
                quest[16] = "It takes Myline twice as long as Jeana to do a certain piece of work. Working together, they can finish the work in 6 hours. How long would it take Jeana to do it alone?";
                quest[17] = "Mike, Loui, and Joy can mow the lawn in 4, 6, and 7 hours respectively. What fraction of the yard can they mow in 1 hour if they work together?";
                quest[18] = "A farmer can plow the field in 8 days. After working for 3 days, his son joins him and together they plow the field alone?";
                quest[19] = "Crew no. 1 can finish installation of an antenna tower in 200 man-hour while Crew no. 2 can finish the same job in 300 man-hour. How long will it take both crews to finish the same job, working together?";
				quest[20] = "A goldsmith has two alloys of gold, the first being 70% pure and the second being 60% pure. How many ounces of the 60% pure gold must be used to make 100 ounces of an alloy which will be 66% gold?";
				quest[21] = "If a two digit number has x for its unit’s digit and y for it’s ten digit, represent the number";
             	quest[22] = "One number is 5 less than the other. If their sum is 135, what are the numbers?";
				quest[23] = "Ten less than four times a certain numbers is 14. Determine the number.";
				quest[24] = "The sum of two numbers is 21 and one number is twice the other. Find the numbers.";
                quest[25] = "If eight is added to the product of nine and the numerical number, the sum is seventy –one. Find the unknown number.";
                quest[26] = "Find the fraction such that if 2 is subtracted from its terms its becomes ¼, but if 4 is added to its terms it becomes ½.";
                quest[27] = "The product of 1/4 and 1/5 of a number is 500. What is the number?";
                quest[28] = "The denominator of a certain fraction is three more than twice the numerator. If 7 is added to both terms of the fraction, the resulting fraction is 3/5. Find the original fraction.";
				quest[29] = "Find the product of the two numbers such that twice the first added t the second equals 19 and three times the first is 21 more than the second.";
				quest[30] = "The ten’s digit of a number is 3 less than the units’ digit. If the number is divided by the sum of the digits, the quotient is 4 and the remainder is 3. What is the original number?";
				quest[31] = "The second of the four numbers is 3 less than the first, the third is 4 more than the first and the fourth is two more than the third. Find the fourth number if their sum is 35.";
             	quest[32] = "A jogger starts a course at a steady rate of 8 kph. Five minutes later, a second jogger starts the same course at 10 kph. How long will it take the second jogger to catch the first?";
				quest[33] = "A man rows downstream at the rate of 5 mph and upstream at the rate of 2 mph. How far downstream should he go if he is to return in 7/4 hours after leaving?";
				quest[34] = "An airplane flying with the wind, took 2 hours to travel 1000 km and 2.5 hours in flying back. What was the wind velocity in kph.";
                quest[35] = "A boat travels downstream in 2/3 of the time as it goes going upstream. If the velocity of the river’s current is 8 kph, determine the velocity of the boat in still water.";
                quest[36] = "Two planes leave Manila for a southern city, a distance of 900 km. Plane A travels at a ground speed of 90 kph faster than the plane B. Plane A arrives in their destination 2 hours and 15 minutes ahead of plane B. What is the ground speed of plane A?";
                quest[37] = "On a certain trip, Edgar drive 231 km in exactly the same time as Erwin drive 308 km. If Erwin’s rates exceeded that of Edgar by 13 kph, determine the rate of Erwin.";
               	quest[38] = "In how many minutes after 2 o’clock will the hands of the clock extends in opposite directions for the first time?";
                quest[39] = "In how many minutes after 7 o’clock will the hands be directly opposite each other for the first time?";
                quest[40] = "What time after 3 o’clock will the hands of the clock be together for the first time?";
				quest[41] = "From the time 6:15 PM to the time 7:45 PM of the same day, the minute hand of a standard clock describe an arch of";
				quest[42] = "A storage battery discharges at a rate which is proportional to the charge. If the charge is reduced by 50% of its original value at the end of 2 days, how long will it take to reduce the charge to 25% of its original charge?";
				quest[43] = "The resistance of a wire varies directly with its length and inversely with its area. If a certain piece of wire 10 m long and 0.10 cm in diameter has a resistance of 100 ohms, what will its resistance be if it is uniformly stretched so that its length becomes 12 m?";
             	quest[44] = "The electric power which a transmission line can transmit is proportional to the product of its design voltage and current capacity, and inversely to the transmission distance. A 115 –kilovolt line rated at 100 amperes can transmit 150 megawatts over 150 km. How much power, in megawatts can a 230 kilovolt line rated at 150 amperes transmit over 100 km?";
				quest[45] = "The time required for an elevator to lift a weight varies directly with the weight and the distance through which it is to be lifted and inversely as the power of the motor. If it takes 30 seconds for a 10 hp motor to lift 100 lbs through 50 feet, what size of motor is required to lift 800 lbs in 40 seconds through 40 feet?";
				quest[46] = "The selling price of a TV set is double that of its cost. If the TV set was sold to a customer at profit of 25% of the cost, how much discount was given to the customer?";
                quest[47] = "A group of EE examinees decided to hire mathematics tutor from Excel Review Center and planned to contribute equal amount for tutor fee. If there were 10 more examinees, each would have paid P 2 less. However, if there were 5 less examinees, each would have paid P 2 more. How many examinees there in group?";
                quest[48] = "Jojo brought a second hand betamax VCR and then sold it to Rudy at a profit of 40%. Rudy then sold the VCR to Noel at a profit of 20%. If Noel paid P 2,856 more than it cost to Jojo, how much did Jojo paid for the unit?";
				 quest[49] = "In a certain community of 1,200 people, 60% are literate. Of the males, 50% are literate and of the female 70% are literate. What is the female population?";
                quest[50] = "A merchant has three times on sale, namely a radio for P50, a clock for P30 and a flashlight for P1. At the end of the day, he sold a total of P100 of the three items and has taken exactly P1,000 on the total sales. How many radios did he sale?";
				//choices
				answerDB[1] = "18_16_12_15";
                answerDB[2] = "5, 13_4, 14_7, 11_6, 12";
                answerDB[3] = "15_16_17_18";
                answerDB[4] = "36_24_28_20";
                answerDB[5] = "6_4_5_7";
				answerDB[6] = "10_8_7_5";
				answerDB[7] = "17_15_19_21";
                answerDB[8] = "17_16_15_14";
		        answerDB[9]	= "5_3_4_6"; 	
                answerDB[10] = "16_14_18_24";
                answerDB[11] = "7 hours_6 hours_7 ½ hours_6 ½ hours";
                answerDB[12] = "7 hours and 50 minutes_7 hours and 12 minutes_7 hours and 32 minutes_7 hours and 42 minutes";
				answerDB[13] = "2.75 hours_2.50 hours_2.25 hours_3.00 hours";
				answerDB[14] = "9 days_10 days_11 days_12 days";
				answerDB[15] = "19_17_21_15";
				answerDB[16] = "9 hours_18 hours_12 hours_14 hours";
                answerDB[17] = "47/84_45/84_84/47_39/60";
                answerDB[18] = "12_10_11_13";
                answerDB[19] = "120 man-hour_100 man-hour_140 man-hour_160 man-hour";
                answerDB[20] = "40_35_45_38";
				answerDB[21] = "10y + x_10x + y_Yx_Xy";
				answerDB[22] = "70, 65_85, 50_80, 55_75, 60";
				answerDB[23] = "6_7_8_9";
				answerDB[24] = "7, 14_6, 15_8, 13_9, 12";
				answerDB[25] = "7_5_6_8";
				answerDB[26] = "5/14_3/5_5/12_6/13";
                answerDB[27] = "100_50_75_125";
                answerDB[28] = "5/13_8/5_13/5_3/5";
                answerDB[29] = "24_32_18_20";
                answerDB[30] = "47_36_58_69";	
 				answerDB[31] = "13_10_11_12";
				answerDB[32] = "20 min_21 min_22 min_18 min";
				answerDB[33] = "2.5 miles_3.3 miles_3.1 miles_2.7 miles";
				answerDB[34] = "50_60_70_40";
				answerDB[35] = "40 kph_50 kph_30 kph_60 kph";
				answerDB[36] = "240 kph_205 kph_315 kph_287 kph";
                answerDB[37] = "52 kph_48 kph_44 kph_39 kph";
				 answerDB[27] = "d.	43.6 minutes_a.	42.4 minutes_b.	42.8 minutes_c.	43.2 minutes";
                answerDB[28] = "c.	5.46 minutes_a.	5.22 minutes_b.	5.33 minutes_d.	5.54 minutes";
                answerDB[29] = "d.	3:16.36_a.	3:02.30_b.	3:17.37_c.	3:14.32";
                answerDB[30] = "d.	540 degrees_c.	180 degrees_b.	90 degrees_a.	60 degrees";	
 				answerDB[31] = "a.	3_b.	4_c.	5_d.	6";
				answerDB[32] = "c.	144_a.	80_b.	90_d.	120";
				answerDB[33] = "c.	675_a.	785_3.1 miles_2.7 miles";
				answerDB[34] = "50_60_70_40";
				answerDB[35] = "40 kph_50 kph_30 kph_60 kph";
				answerDB[36] = "240 kph_205 kph_315 kph_287 kph";
                answerDB[37] = "52 kph_48 kph_44 kph_39 kph";
				
				}
           
		    //getting questions
            doneQuest = document.getElementById("fquest").value;
            c = parseInt(document.getElementById("count").value);            
            if(c=="0")
            {
                document.getElementById("h1").style.visibility = "visible";
                document.getElementById("h2").style.visibility = "visible";
                document.getElementById("h3").style.visibility = "visible";
                document.getElementById("h4").style.visibility = "visible";
                document.getElementById("questTbl").style.visibility="visible";
                document.getElementById("total").value = quest.length;
            }            
        
            rand = Math.floor(Math.random() * (quest.length-1)) + 1;
            if(doneQuest!="0")
            {
                questSplit = doneQuest.split("_");
                for(var x=0;x<questSplit.length;x++)
                {
                    var ctr = parseInt(questSplit[x]);
                    if(rand == ctr)
                    {
                        rand = Math.floor(Math.random() *  (quest.length-1)) + 1;
                        x=-1;
                    }
                }
            }
            var swapCtr = document.getElementById("swapSwitch").value;
            if(swapCtr=="0")
            {
                document.getElementById("swap").value = rand;
            }
            //counting the finished questions and displaying questions
            c+=1;
            document.getElementById("count").value=c;
            document.getElementById("q").value=quest[rand];
            document.getElementById("pic").src = picture[rand];
            if(doneQuest!="")
            {
                document.getElementById("fquest").value += "_";
            }
            document.getElementById("fquest").value += rand;
            //getting choices and it will appear in random
            answerSplit = answerDB[rand].split("_");
            rand = Math.floor(Math.random() * 4) + 1;
            for(var x=0;x<4;x++)
            {
                for(var y=0;y<4;y++)
                {
                    if(rand==answerDisplay[y])
                    {
                        rand = Math.floor(Math.random() * 4) + 1;
                        y=-1;
                    }
                }
                answerDisplay[x]=rand;
            }
            //displaying choices and amount
            document.getElementById("ans").value = answerSplit[0];
            document.getElementById("a").value=answerSplit[answerDisplay[0]-1];
            document.getElementById("b").value=answerSplit[answerDisplay[1]-1];
            document.getElementById("c").value=answerSplit[answerDisplay[2]-1];
            document.getElementById("d").value=answerSplit[answerDisplay[3]-1];

            if(answerSplit[0]==answerSplit[answerDisplay[0]-1])
                 document.getElementById("ansNum").value = "0";
            else if(answerSplit[0]==answerSplit[answerDisplay[1]-1])
                document.getElementById("ansNum").value = "1";
            else if(answerSplit[0]==answerSplit[answerDisplay[2]-1])
                document.getElementById("ansNum").value = "2";
            else if(answerSplit[0]==answerSplit[answerDisplay[3]-1])
                document.getElementById("ansNum").value = "3";
            //amount
            rand = Math.floor((Math.random() * 10) + 1) * 1000;
            document.getElementById("amount").value=rand;

            document.getElementById("draw").value=="1";
            enableChoice();

        }
        function letA()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("a").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }
            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
            
        }
        function letB()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("b").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
        }
        function letC()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("c").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }

            else
            {
                disableChoice();
            }

            
        }
        function letD()
        {
            var answer = document.getElementById("ans").value;
            var choice = document.getElementById("d").value;
            var amt = document.getElementById("amount").value;
            var bal = document.getElementById("balance").value;
            var intBal = 0;
            if(choice==answer)
            {
                alert("Your answer is correct!");
                intBal = parseInt(bal) + parseInt(amt);
            }
            else
            {
                alert("Your answer is wrong!");
                intBal = parseInt(bal) - parseInt(amt);
            }

            document.getElementById("balance").value=intBal;
            document.getElementById("on").value = "0";

            if(intBal<=0)
            {
                document.getElementById("q").value="GAME OVER!";
                document.getElementById("next").disabled = true;
                gameover();
            }
            else
            {
                disableChoice();
            }
            
        }
        function disableChoice()
        {
            document.getElementById("btn1").disabled = true;
            document.getElementById("btn2").disabled = true;
            document.getElementById("btn3").disabled = true;
            document.getElementById("btn4").disabled = true;
            document.getElementById("helptbl").style.visibility = "hidden";
            var cnt = parseInt(document.getElementById("count").value); 
            var total = parseInt(document.getElementById("total").value);

            if(cnt==total-1)
            {
                //clearing the textboxes after all questions are finished
                document.getElementById("q").value="FINISHED";                
                document.getElementById("h5").style.visibility = "visible";                
                document.getElementById("go").style.visibility = "hidden";
                document.getElementById("helptbl").style.visibility = "hidden";
                document.getElementById("cattbl").style.visibility = "visible";
                document.getElementById("on").value = "1";
                document.getElementById("count").value = "0";
                document.getElementById("fquest").value = "";
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo base_url();?>index.php/activity/save_score',
                    data: {
                        cat_id      : cat_id, 
                        cat_name    : cat_name,
                        score       : document.getElementById("balance").value
                    },
                    success: function(msg){
                        alert(msg);
                    }
                });
                alert("Congratulations! You finished this category!");
                
            }
            
        }
        function gameover()
        {
            document.getElementById("btn1").disabled = true;
            document.getElementById("btn2").disabled = true;
            document.getElementById("btn3").disabled = true;
            document.getElementById("btn4").disabled = true;
            document.getElementById("helptbl").style.visibility = "hidden";
            document.getElementById("on").value = "1";
        }
        function enableChoice()
        {
            document.getElementById("btn1").disabled = false;
            document.getElementById("btn2").disabled = false;
            document.getElementById("btn3").disabled = false;
            document.getElementById("btn4").disabled = false;
            document.getElementById("helptbl").style.visibility = "visible";
        }
        function slot()
        {
            
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/3a.gif";
            },200);
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/2a.gif";
            },400);
            setTimeout(function()
            {
                document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/1a.gif";
            },600);
            

            document.getElementById("next").src = "<?php echo base_url();?>assets/activity/web/2a.gif";
            if(document.getElementById("on").value == "0")
            {
            var stop = 0;
            var myTimer = setInterval(function()
            {
                stop+=1;
                if(stop<=5)
                {
                    var ctr = document.getElementById("draw").value;
                    if(ctr=="0")
                        slot2();
                    else if(ctr=="1")
                        slot3();
                }
                else
                {
                    clearInterval(myTimer);
                    getQ();
                }

            },800);

            }
            document.getElementById("on").value = "1";           
     }
        function slot2()
        {
            var time= 100;
            var rand = [3];
            var disp = [3];
            for(var z=0;z<3;z++)
            {
                disp[z]="";
                rand[z] = Math.floor(Math.random() * 5);
                if(rand[z]==0)
                   disp[z] += "b";

                else if(rand[z]==1)
                   disp[z] += "g";

                else if(rand[z]==2)
                   disp[z] += "l";

                else if(rand[z]==3)
                   disp[z] += "p";

                else if(rand[z]==4)
                   disp[z] += "r";
            }

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "2.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "4.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "6.gif";
                },time);

                time+=200;

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "3.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "3.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "7.gif";
                },time);

                time+=200;

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "4.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "2.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "1.gif";
                },time);

                time+=200;

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "5.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "1.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "2.gif";
                },time);

                time+=200;

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "6.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "7.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "3.gif";
                },time);
                time+=200;
                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "7.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "6.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "4.gif";
                },time);

                time+=200;

                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "1.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "5.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "5.gif";
                },time);
                time+=200;
                setTimeout(function()
                {
                    document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/" + disp[0] + "4.gif";
                    document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/" + disp[1] + "4.gif";
                    document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/" + disp[2] + "4.gif";
                },time);
      }
      function slot3()
        {
            var rand = [3];
            var disp = [3];
            for(var z=0;z<3;z++)
            {
                disp[z]="";
                rand[z] = Math.floor(Math.random() * 9) + 1;
                disp[z] += rand[z];
            }
            var time= 100;

            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a7.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a5.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a4.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a6.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a7.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a3.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a8.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a1.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a9.jpg";
            },time);
            time+=100;
            setTimeout(function()
            {
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a" +disp[0]+ ".jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a" +disp[1]+ ".jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a" +disp[2]+ ".jpg";
            },time);
      }
       function lot()
       {
           document.getElementById("go").style.visibility = "visible";
           document.getElementById("on").value = "0";
           document.getElementById("questTbl").style.visibility="hidden";           
           document.getElementById("cattbl").style.visibility = "hidden";

           
       }
       function cat1()
       {
           document.getElementById("category").value = "1";
           document.getElementById("cat1").style.visibility = "hidden";
           cat_id = "1";
           cat_name = "PAST BOARD EXAM 1";
           lot();
       }
       function cat2()
       {
           document.getElementById("category").value = "2";
           document.getElementById("cat2").style.visibility = "hidden";
           cat_id = "2";
           cat_name = "PAST BOARD EXAM 2";
           lot();
       }
       function cat3()
       {
           document.getElementById("category").value = "3";
           document.getElementById("cat3").style.visibility = "hidden";
           cat_id = "3";
           cat_name = "PAST BOARD EXAM 3";
           lot();
       }
       function cat4()
       {
           document.getElementById("category").value = "4";
           document.getElementById("cat4").style.visibility = "hidden";
           cat_id = "4";
           cat_name = "PAST BOARD EXAM 4";
           lot();
       }
       function cat5()
       {
           document.getElementById("category").value = "5";
           document.getElementById("cat5").style.visibility = "hidden";
           lot();
       }
        
      function skin()
      {
          if(document.getElementById("on").value=="0")
          if(document.getElementById("draw").value=="0")
          {
                document.getElementById("draw").value="1";
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/a2.jpg";
          }
          else if(document.getElementById("draw").value=="1")
          {
                document.getElementById("draw").value="0";
                document.getElementById("1").src = "<?php echo base_url();?>assets/activity/web/b4.gif";
                document.getElementById("2").src = "<?php echo base_url();?>assets/activity/web/g4.gif";
                document.getElementById("3").src = "<?php echo base_url();?>assets/activity/web/r4.gif";
          }
      }
      function nw()
      {
          location.reload();
      }
      

    </script>
        <style type="text/css">
        a:hover {color:#FF00FF;}
        #header h1
{

-webkit-animation:myfirst 7s; /* Chrome, Safari, Opera */
animation:myfirst 7s;
}

/* Chrome, Safari, Opera */
@-webkit-keyframes myfirst
{
from {background:cadetblue;}
to {background:lightblue;}
}
#main .b1
{
background:aliceblue;
-webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
transition:width 5s;
}

.b1:hover
{
width:60px;
}
#main .b2
{
background:aliceblue;
-webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
transition:width 5s;
}

.b2:hover
{
width:60px;
}
#main .b3
{
background:aliceblue;
-webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
transition:width 5s;
}

.b3:hover
{
width:60px;
}
#main .b4
{
background:aliceblue;
-webkit-transition:width 5s; /* For Safari 3.1 to 6.0 */
transition:width 5s;
}

.b4:hover
{
width:60px;
}
        body {
	background-color: #BCE0A8;
}
.style1 {
	font-size: 16px;
	color: #FF0000;
}
        .style4 {
	color: #006666;
	font-style: italic;
	font-weight: bold;
	font-size: 24px;
}
        .style7 {color: #0000FF}
        .style8 {
	font-size: 13px;
	font-family: "Arial Narrow";
}
.style9 {font-size: 13px; color: #003333; font-weight: bold; }
.style18 {font-size: 13px; color: #003333; font-weight: bold; font-family: "Arial Narrow"; }
        .style19 {
	color: #009933;
	font-weight: bold;
}
        </style>
    </head>
    <body>
    <div id="container">

      <div id="header">
          <h1><img src="<?php echo base_url();?>assets/activity/images/head.jpg" width="632" height="141"></h1>
          <p align="left"><a href="../../../../onlinequiz/index.php"><img src="<?php echo base_url();?>assets/activity/images/links.jpg" alt="1" width="31" height="14" /><span class="style19">Main Page</span></a> </p>
      </div>
            <div id="content">
              <div id="nav">
                    <h3 class="style7">&nbsp;</h3>
          <table id="cattbl">
            <tr><th><p><a href="#" class="selected style1">Category</a></p>
                          </th>
                        </tr>
                        <tr>
                          <td class="style4"><a href="#" class="style18" id="cat1" onClick="cat1()"><strong>Past Board Exam 1 </strong></a></td>
            </tr>
                        <tr>
                          <td class="style4"><a href="#" class="style18" id="cat2" onClick="cat2()">Past Board Exam 2 </a></td>
            </tr>
                        <tr>
                          <td class="style4"><a href="#" class="style18" id="cat3" onClick="cat3()">Past Board Exam 3 </a></td>
            </tr>
                        <tr>
                          <td class="style4"><a href="#" class="style18" id="cat4" onClick="cat4()">Past Board Exam 4 </a></td>
            </tr>
 		 				<!-- <tr>
                          <td class="style4"><a href="#" class="style18" id="cat5" onClick="cat5()">Past Board Exam for Algebra </a></td>
                    </tr> -->
                        <tr>
                          <td class="style4"><a href="<?php echo base_url();?>index.php/activity/game" class="style18">Previous << </a></td>
                    </tr>
                       
                    </table>
              </div>
                <div id="main">
                    <h3 class="style4">LOTTERY</h3>
                        <form name="home">
                                        <table  id="questTbl" bgcolor="white" style="visibility: hidden;vertical-align: middle">
                                             <tr align="center">
                                                <td>
                                                    <font color="green">BALANCE</font>
                                                    <input type="text"id="balance" name="balance" value="7000" style="color:blue" disabled>
                                                     <font color="red">AMOUNT</font>
                                                    <input type="text"id="amount" name="amount" style="color: darkviolet"disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><textarea rows="2.5" cols="40" id="q" name="q" style="color:brown;font-weight:bold;font-size:18" disabled></textarea></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <IMG SRC="<?php echo base_url();?>assets/activity/web/0.jpg" width="120" height="120" id="pic" name="pic"onerror="this.src='<?php echo base_url();?>assets/activity/web/0.jpg'">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" class="b1" id="btn1" value=" A " onClick="letA()">
                                                    <input type="text" size="58" id="a" name="a" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" class="b2" id="btn2" value=" B " onClick="letB()">
                                                    <input type="text" size="58" id="b" name="b" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" class="b3" id="btn3" value=" C " onClick="letC()">
                                                    <input type="text" size="58" id="c" name="c" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" class="b4" id="btn4" value=" D " onClick="letD()">
                                                    <input type="text" size="58" id="d" name="d" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><center>
                                                    <input type="hidden" id="fquest" name="fquest" value="">
                                                    <input type="hidden" id="count" name="count" value="0">
                                                    <input type="hidden" id="ans" name="ans">
                                                    <input type="hidden" id="ansNum" name="ansNum" value="0">
                                                    <input type="hidden" id="draw" value="0">
                                                    <input type="hidden" id="on"  value="0">
                                                    <input type="hidden" id="total" name="total" value="">
                                                    <input type="hidden" id="category" name="category" value="">
                                                    <input type="hidden" id="swap" name="swap" value="">
                                                    <input type="hidden" id="swapSwitch" name="swapSwitch" value="0">
                                                </center></td>
                                            </tr>
                                        </table>
                                        <br>
                                        <table border="5" id="go"style="background-color: white;visibility: hidden"><center>
                                       
                                        <tr><td><font color="darkseagreen" size="5" style="visibility: visible;"> &nbsp; D &nbsp; <br> &nbsp; R &nbsp; <br> &nbsp; A &nbsp; <br> &nbsp; W &nbsp; </font></td>
                                        <td><IMG SRC="<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="1" name="1"></td>
                                        <td><IMG SRC="<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="2" name="2" ></td>
                                        <td><IMG SRC="<?php echo base_url();?>assets/activity/web/default.gif" width="90" height="120" id="3" name="3" ></td>
                                        <td><a href="#" onCLick="slot()"><IMG SRC="<?php echo base_url();?>assets/activity/web/1a.gif" id="next" name="next"></a></td>
                                        </tr></center></table>

                        </form>
                                        <table id="helptbl" name="helptbl" style="visibility: hidden">
                                <tr>
                                        <td><a href="#" id="h1" onClick="help1()"><IMG SRC="<?php echo base_url();?>assets/activity/web/1.jpg" width="50" height="50"></a></td>
                                        <td><a href="#" id="h2" onClick="help2()"><IMG SRC="<?php echo base_url();?>assets/activity/web/2.jpg" width="50" height="50"></a></td>
                                        <td><a href="#" id="h3" onClick="help3()"><IMG SRC="<?php echo base_url();?>assets/activity/web/3.jpg" width="50" height="50"></a></td>
                                        <td><a href="#" id="h4" onClick="help4()"><IMG SRC="<?php echo base_url();?>assets/activity/web/4.jpg" width="50" height="50"></a></td>
                                        <td><a href="#" id="h5" onClick="skin()"><IMG SRC="<?php echo base_url();?>assets/activity/web/5.jpg" width="50" height="50"></a></td>
                                        <td><a href="#" id="newg" onClick="nw()" style="visibility: visible"><IMG SRC="<?php echo base_url();?>assets/activity/web/newgame.jpg" width="80" height="50"></a></td>
                                        <td></td>


                                </tr>
                            </table>
                </div>
            </div>
        <div id="footer">	</div>
        </div>
        
    </body>
</html>
