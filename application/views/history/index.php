<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - History</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-size: x-large;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman"}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "history";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">
      <div class="jumbotron">
      <style>.form-signin{max-width: 800px;}</style>
      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">My Exams</p>
        <!--         <input type="text" class="form-control" placeholder="E.g. Quiz Name, Quiz Description, 2014-01-01" required autofocus>
        <button id="save" class="btn btn-lg btn-primary" type="submit">Search</button> -->
        <hr />
        <table border='0' width='100%'>
          <tr>
            <th><span class="style2">Quiz Name</span></th>
            <th><span class="style2">Time Limit</span></th>
            <th><span class="style2">Action</span></th>
          </tr>
          <?php foreach ($linked_quiz as $key => $value) :?>
          <tr>
            <td><?php echo $value["qz_qname"] . " - " . $value["qz_qdesc"];?></td>
            <td><?php echo $value["qz_qtimelimit"] / 60 . " mins";?></td>
            <?php if($value["status"] != '1'): ?>
            <td><a href="<?php echo base_url(). 'index.php/exam/quiz/' . $value['qz_qid'] . '/' . $this->session->userdata('oq_uid');?>" class="style2">Start Quiz</a></td>
            <?php else:?>
            <td><a href="<?php echo base_url(). 'index.php/submit/generate_result/' . $value['qz_qid'] . '/' . $this->session->userdata('oq_uid');?>" class="style2">View Result</a></td>
            <?php endif;?>
          </tr>
          <?php endforeach;?>
        </table>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
  </body>
</html>
