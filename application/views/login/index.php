<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/signin.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/ece.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-weight: bold;
	color: #009933;
	font-size: xx-large;
}
.style4 {
	font-size: 12px;
	font-style: italic;
}
body {
	background-color: #FFFFFF;
}
.style10 {
	color: #FF0000;
	font-weight: bold;
	font-size: large;
}
-->
    </style>
    <script type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
    </script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

  <body>
<p align="center" class="style1"><img alt="1" src="<?php echo base_url();?>assets/images/head.jpg"></p>
<div class="container">

      <form class="form-signin" action="#">
        <div id="error" class="alert alert-danger"></div>
        <p>
          <input id="username" autocomplete="off" type="text" class="form-control" placeholder="Username" required autofocus>
          <input id="password" type="password" class="form-control" placeholder="Password" required>
        </p>
        <button id="login" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

      <form name="form2">
        <!-- <p>________________________________________________________________________________________________________________________</p> -->
        <hr />
        <!-- <p><a href="../../../../MakeitEcE/greener/Algebra/Variables, Equations, and Inequalities.html" target="_self" class="style5">View Lectures </a></p> -->
        <p align="center" class="style4">&quot;This website does not allow self registrations. If you need to access the assessment exam, please <a href="mailto:makeitece@gmail.com">e-mail</a> us (makeitece@gmail.com) or like us on <a href="https://www.facebook.com/pages/Make-it-Ec-E/694021190679094?skip_nax_wizard=true&ref_type=logout_gear">facebook</a><a href="www.facebook.com" target="_self"></a>.&quot;
          <!-- FOOTER -->
</p>
        <p align="center" class="style4">&nbsp;</p>
        <footer>
          <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
          <p>&copy; 2014 Online Quiz </p>
          <p>Contact Number: 0916-2685891</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        </footer>
    </form>
      <form name="form1" method="post" action="">
        <label></label>
        <p align="center"><span class="style1"><img alt="1" src="<?php echo base_url();?>assets/images/guidelines.jpg"></span></p>
        <p align="center">&nbsp;</p>
  </form>
      <p align="center" class="style10">Student/Teachers Guidelines for  Using the MiE</p>
      <p>&nbsp;</p>
      <p align="justify"> <strong>Brief Background of MiE: </strong></p>
      <p align="justify">Make  it EC-E (MiE) is a learning management system (LMS) designed to supplement the  mathematics review of candidate engineers for National Licensure Board  examination in the Philippines. The content of the LMS is based on an  ABET-certified curriculum. Its design is made simple as possible and it  features gamification of learning activity. The game of choice is lottery using  virtual play money. The randomness of lottery is to be beaten by the degree of  knowledge acquired. The system collects student performance data in a  well-defined database structure that facilitates test question reconstruction.</p>
      <p align="justify"> <em>Brief  Background for Lecture Notes (LN): </em></p>
      <p align="justify">The LN is predefined by content of  ABET-certified ECE subjects. The content of the LN are based on the math courses  of ECE curriculum at TIP which include trigonometry, geometry, calculus,  differential equation, advance mathematics and algebra. </p>
      <p align="justify"><em>Brief Background for Learning Activities  (LA): </em></p>
      <p align="justify">LA  are pre-defined by the lottery game software. Java script and HTML were used.  The player could earn or be penalized depending on the response to a set of  questions. In every event of challenge to answer a question, the result of a  response was immediately reported. Hence, the feedback mechanism is real-time.</p>
      <p align="justify"><br>
        <em>How  to Play the Lottery?</em> </p>
      <p align="justify"><br>
        The  students need to first select the subject in order to play the lottery. Each  subject is composed of multiple choice questions. Every question that appears  on the screen is randomly selected by the system. Once the subject is selected,  the student can play the lottery by clicking the “click here” button. An  initial capital of P 5000 (USD=110) is allotted to the students once the game  is played for the first time. For every question, there is a corresponding  amount of “cash” that is either added to (for correct answers) or deducted (for  wrong answers) from the capital. Bankruptcy will automatically terminate the  game. The student can use the following options only once during the game:  “50-50” risk: the monetary value of the question is reduced by 50%, switch questions:  another question is displayed, and  hint button: a clue will be provided to  help answer the question. The keys for these functions are seen in the lower  panel of the screen. In order to win the game, the student has to  complete all the questions without getting bankrupt. The game is usually played  within an hour per subject.</p>
      <p align="justify"><br>
        <em>Brief Background for Assessment Exam (AE): </em> </p>
      <p align="justify"><br>
        The  assessment exam (AE) provided platform for the teacher to delete, construct or  modify questions. Depending on the instructor, the students are given time  limit to complete the set of questions. All answers to the questions are in  multiple choices. Exam data are collected and analyzed. The results determine  the nature of new set of questions that address the current weak performance of  the examinees.</p>
      <p align="justify"><strong>Other  Components of MiE: </strong></p>
      <p align="justify">Forum  and Login/Logout History</p>
  </div> 
    <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
  
    <div></div>
    <script>

    $(function(){

      $("#error").hide();

      $("#login").click(function(e){
        if($("#username").val() == "" || $("#password").val() == ""){

        }else{
          e.preventDefault();         
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/login/log",
            data: {username:$("#username").val(), password:$("#password").val()},
            success: function(msg){
              if(msg == "1"){
                window.location.replace('<?php echo base_url();?>index.php/');                
              }else{
                $("#error").html("Invalid Username / Password").show();
              }
            }
          });
        }
      });

    });

      </script>
</body>
</html>
