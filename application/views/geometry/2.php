<div class="style1" id="content">
    <h2 align="center" class="style6">geometry</h2>
    <h2 align="center">parabola</h2>
    <ul>
      <li>
        <div align="justify">A parabola is the set of all points in a plane which are equidistant from a fixed point and a fixed line of the plane. The fixed point is called the <span class="style5">focus</span> and the fixed line the <span class="style5">directrix. </span></div>
      </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/Parabola.jpg" width="224" height="188" /></p>
    <ul><li>General Equation:</li>
    </ul>
    <p align="center">Ax<sup>2</sup> + Bxy + Cy<sup>2</sup> + Dx + Ey + F = 0 </p>
    <ul>
      <li>Distance between two points:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/parabola distance formula.jpg" width="205" height="23" /></p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/parabola 1.1.jpg" width="389" height="174" /></p>
    <ul>
      <li>Standard equation of parabola with vertex at the origin, axis x and focus at (a, 0)</li>
    </ul>
    <p align="center">y<sup>2</sup> = 4ax </p>
    <ul>
      <li>Standard equation of parabola with vertex at the origin, axis y and focus at (0, a)</li>
    </ul>
    <p align="center">x<sup>2</sup> = 4ay </p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/parabola 1.2.jpg" width="394" height="151" /></p>
    <ul>
      <li>Standard Equation of Parabola with vertex (0, 0)</li>
    </ul>
    <ol>
      <li>y<sup>2</sup> = 4ax</li>
    </ol>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/geometry/images/x1.1.jpg" width="314" height="163" /></p>
      <p class="style7">Vertex: (0, 0)</p>
      <p class="style7">Focus: (a, 0)</p>
      <p class="style7">Directrix: x = -a</p>
      <p class="style7">Symmetric with respect to x-axis  </p>
    </blockquote>
    <p align="left" class="style7">2. x<sup>2</sup> = 4ay </p>
    <blockquote>
      <p align="left" class="style7"><img src="<?php echo base_url();?>assets/geometry/images/x1.2.jpg" width="305" height="168" /></p>
      <p align="left" class="style7">Vertex: (0, 0)</p>
      <p align="left" class="style7">Focus: (0, a)</p>
      <p align="left" class="style7">Directrix: y = -a</p>
      <p align="left" class="style7">Symmetric with respect to the y-axis  </p>
    </blockquote>
    <p align="left" class="style7"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" /><strong>Example:</strong></p>
    <p align="left" class="style7">a. Find the equation of a parabola having the origin as its vertex, the y axis as its axis, and (-10, -5) on its graph</p>
    <p align="left" class="style7">b. Find the coordinates of its focus and the equation of its directrix.</p>
    <blockquote>
      <p align="left" class="style7">Solution:  </p>
      <blockquote>
        <p align="left" class="style7">a. Use x<sup>2</sup> = 4ay. </p>
        <p align="left" class="style7">(-10)<sup>2</sup> = 4a(-5) </p>
        <p align="left" class="style7">100 = -20a</p>
        <p align="left" class="style7">a = -5  </p>
        <p align="left" class="style7">Equation of the Parabola:</p>
        <p align="left" class="style7">x<sup>2</sup> = 4(-5)y</p>
        <p align="left" class="style7">= -20y   </p>
        <p align="left" class="style7">b. Focus: x<sup>2</sup> - 20y</p>
        <p align="left" class="style7">F (0, a) = F(0, -5)</p>
      </blockquote>
    </blockquote>
    <p align="left" class="style7">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p align="left" class="style7">Directrix: y = -a</p>
        <p align="left" class="style7">= - (-5)</p>
        <p align="left" class="style7">= 5     </p>
      </blockquote>
    </blockquote>
    <p><span class="style7"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /><span class="style8">Exercise:</span></span></p>
    <p>a. Find the equation of a parabola having the origin as its vertex, the x-axis as its axis, and (-4, 8) on its graph.</p>
    <p>b. Find the coordinates of its focus and the equation of its directrix.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p class="style10">Figures Citation:</p>
    <p class="style10">Conic Sections; Parabola (Section 11-1). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</p>
  </div>