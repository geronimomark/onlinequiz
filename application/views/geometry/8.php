<div class="style1" id="content">
    <h2 align="center" class="style16">GEOMETRY</h2>
    <h2 align="center" class="style28">Formulas for area and volume </h2>
    <ul>
      <li class="style28">Rectangle
        
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/f1.1.jpg" width="247" height="111" /></p>
        </blockquote>
      </li>
      <li class="style28">Triangle
        
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/f1.2.jpg" width="218" height="96" /></p>
        </blockquote>
      </li>
      <li class="style28">Circle
        
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/f1.4.jpg" width="256" height="128" /></p>
        </blockquote>
      </li>
      <li class="style28">Right Circular Cylinder
        
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/f1.5.jpg" width="329" height="169" /></p>
        </blockquote>
      </li>
      <li class="style28">Rectangular Prism</li>
    </ul>
    <p class="style28">&nbsp;</p>
    <p align="center" class="style28"><img src="<?php echo base_url();?>assets/geometry/images/f1.7.jpg" width="394" height="120" /></p>
    <ul>
      <li class="style28">Pythagorean Theorem
        
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/f1.6.jpg" width="233" height="147" /></p>
        </blockquote>
        <p>&nbsp;</p>
        <blockquote>
          <p>&nbsp;</p>
        </blockquote>
      </li>
      <blockquote>
        <p class="style28">&nbsp;</p>
      </blockquote>
    </ul>
    <blockquote>
      <blockquote class="style18"><blockquote>&nbsp;</blockquote>
      </blockquote>
    </blockquote>
    <p align="right" class="style25"><a href="<?php echo base_url();?>index.php/lecture/geometry/7">&lt;&lt;Previous</a></p>
    <p class="style17">&nbsp;</p>
    <p class="style29">Reference</p>
    <p class="style29">Anonymous, (2002). Geometry reference formulas (p. 7). Entry Level Mathematics Problem, Book Young. Retrieved from California State University: www.calstate.edu/SAS/ELMProblemBook01_02.pdf&lrm;</p>
    <p align="center">&nbsp;</p>
  </div>