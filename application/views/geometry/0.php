  <div class="style1" id="content">
    <h2 align="center" class="style16">GEOMETRY</h2>
    <h2 align="center">CARTESIAN COORdinate system and straight line </h2>
    <ul>
      <li><span class="style17">Directed Line Segments</span> - when a line segment is measured in a definite sense or direction. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/directed line segments.jpg" width="236" height="66" /></p>
    <ul>
      <li class="style19">Distance formula if the coordinates taken in forward order:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/distance coordinate.jpg" width="186" height="25" /></p>
    <ul>
      <li><strong>Distance formula if the coordinates taken in reverse order:</strong></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/distance coordinate 1.jpg" width="186" height="23" /></p>
    <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="23" height="19" /><strong>Example:</strong> The vertices of the base of an isosceles triangle are at (1, 2) and (4, 1). Find the ordinate of the third vertex if its abscissa is 6.</p>
    <blockquote>
      <p align="left" class="style18">Solution:	 </p>
      <blockquote>
        <p align="left" class="style18">	Let P (6, y) be the required point</p>
      </blockquote>
      <p align="center" class="style18"><img src="<?php echo base_url();?>assets/geometry/images/distance ex 1.1.jpg" width="261" height="145" /></p>
      <blockquote>
        <p align="left" class="style18">AP = BP (Two sides of an isosceles triangle are equal)</p>
        <p align="left" class="style18">By the distance formula:</p>
        <p align="left" class="style18"><img src="<?php echo base_url();?>assets/geometry/images/distance ex 1.2.jpg" width="272" height="25" />  </p>
        <p align="left" class="style18">25 + 4 - 4y + y<sup>2</sup> = 4 + 1 + 2y + y<sup>2</sup></p>
        <p align="left" class="style18">-6y = -24</p>
        <p align="left" class="style18">y = 4</p>
        <p align="left" class="style18">The third vertex is at (6, 4)</p>
      </blockquote>
    </blockquote>
    <p class="style20"><u>DIVISION OF A LINE SEGMENT</u> </p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/division of line segment 1.2.jpg" width="134" height="35" /> <img src="<?php echo base_url();?>assets/geometry/images/division of line segment 1.1.jpg" width="134" height="35" /></p>
    <ul>
      <ul>
        <li>where:
          <blockquote>
            <p>	<img src="<?php echo base_url();?>assets/geometry/images/div of line 1.3.jpg" width="74" height="35" /> </p>
          </blockquote>
        </li>
      </ul>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="23" height="19" /> <strong>Example:</strong></p>
    <p align="justify">The line segment joining (-5, -3) and (3, 4) is to be divided into five equal parts. Find the point of division closest to (-5, -3) </p>
    <blockquote>
      <p align="justify"><span class="style18">Solution:</span></p>
      <blockquote>
        <p align="justify"><span class="style18">Let r<sub>1</sub> = 1 ; r<sub>2</sub> = 4 ; x<sub>1</sub> = -5 ; x<sub>2</sub> = 3; y<sub>1</sub> = -3 ; y<sub>2</sub> = 4 </span></p>
        <p align="justify">&nbsp;</p>
        <p align="justify"><img src="<?php echo base_url();?>assets/geometry/images/div of line 1.4.jpg" width="300" height="38" /></p>
        <p align="justify">&nbsp;</p>
        <p align="justify"><img src="<?php echo base_url();?>assets/geometry/images/div of line 1.5.jpg" width="300" height="38" /></p>
        <p align="justify">&nbsp;</p>
        <p align="justify"><span class="style18">The point of division is at (-17/5 , -8/5) </span></p>
      </blockquote>
    </blockquote>
    <p class="style21">&nbsp;</p>
    <p class="style20"><u>MIDPOINT OF A LINE SEGMENT </u></p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/midpoint.jpg" width="110" height="35" /> </p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/midpoint 2.jpg" width="110" height="35" /></p>
    <p align="left" class="style20"><u>SLOPE OF A LINE</u> </p>
    <p>If P1 (x1, y1) and P2 (x2, y2) are points on a nonvertical line, then the slope m of the line is defined by:</p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/slope.jpg" width="110" height="35" /> </p>
    <p align="left" class="style20"><u>PARALLEL AND PERPENDICULAR LINES</u> </p>
    <ul>
      <li>Two nonvertical lines with slopes m<sub>1</sub> and m<sub>2</sub> are parallel if and only if they have the same slope, <span class="style17"> m<sub>1 = m</sub>2</span> </li>
      <li>Two nonvertical lines with slopes m<sub>1</sub> and m<sub>2</sub> are perpendicular if and only if the product of their slopes is -1, that is<span class="style17"> m<sub>1</sub>m<sub>2</sub> = -1 </span></li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="23" height="19" /><strong>Example:</strong> Use slopes to show that the points A (1,3), B (3, 7), and C (7, 5) are vertices of a right triangle.</p>
    <blockquote>
      <p><span class="style18">Solution:</span></p>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/geometry/images/slope ex 1.1.jpg" width="98" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/geometry/images/slope ex 1.2.jpg" alt="1" width="98" height="32" /></p>
        <p align="left">&nbsp;</p>
        <p align="left"><span class="style18">Since m1m2 = -1, the line through A and B is perpendicular to the line through B and C; thus, ABC is right triangle.</span></p>
        <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/slope ex 1.3.jpg" width="182" height="138" /></p>
      </blockquote>
    </blockquote>
    <p align="left">&nbsp;</p>
    <p align="left" class="style20"><u>LINES PARALLEL TO THE COORDINATE AXES </u></p>
    <ul>
      <li>The vertical line through (a, 0) and the horizontal line through (0, b) are represented, respectively by the equations<span class="style17"> x = a</span> and<span class="style17"> y = b</span>. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="23" height="19" /><strong>Example:</strong> The graph of x = -5 is the vertical line through (-5, 0), and the graph of y = 7 is the horizontal line through (0,7) . </p>
    <blockquote>
      <blockquote>
        <p align="left" class="style21"><img src="<?php echo base_url();?>assets/geometry/images/lines parallel.jpg" width="396" height="163" /></p>
      </blockquote>
    </blockquote>
    <p align="left"><span class="style20"><u>POINT AND SLOPE</u></span></p>
    <ul>
      <li><span class="style17">Point-slope form </span>of the line is passing through P1 (x1, y1) and having slope m is given by the equation y - y<sub>1</sub> = m (x - x<sub>1</sub>) </li>
      <li><span class="style17">Slope intercept form</span> is the  line with y - intercept b and slope m is given by the equation y = mx + b </li>
    </ul>
  </div>