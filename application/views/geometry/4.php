<div class="style1" id="content">
    <h2 align="center" class="style6">geometry</h2>
    <h2 align="center">hyperbola</h2>
    <ul>
      <li><span class="style5">Hyperbola</span> is the locus of a point so moving in a plane that the difference of its distance from two fixed points is always constant and equal to a given line. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.1.jpg" width="218" height="156" /></p>
    <ul>
      <li>Standard Equation:
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.3.jpg" width="213" height="165" /></p>
        </blockquote>
      </li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.2.jpg" width="91" height="32" /></p>
    </blockquote>
    <ul>
      <ul>
        <li>x -intercepts: +/- a (vertices)</li>
        <li>y-intercepts: none</li>
        <li>Foci: F'(-c , 0), F (c, 0)</li>
        <li>c<sup>2</sup> = a<sup>2</sup> + b<sup>2</sup></li>
        <li>Transverse axis length: 2a</li>
        <li>Conjugate axis length: 2b
          <blockquote>
            <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.4.jpg" width="91" height="32" /></p>
          </blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.5.jpg" width="213" height="165" /></p>
        </li>
        <li>x intercepts: none        </li>
        <li>y interceptsL +/- a (vertices)</li>
        <li>Foci: F'(0 , -c), F (0, c)</li>
        <li>c<sup>2</sup> = a<sup>2</sup> + b<sup>2</sup></li>
        <li>Transverse axis length: 2a</li>
        <li>Conjugate axis length: 2b          </li>
      </ul>
    </ul>
    <ul>
      <li>Standard form for horizontal axis:
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.6.jpg" width="127" height="32" /></p>
        </blockquote>
      </li>
      <li>Standard form for vertical axis:
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/geometry/images/hyperbola 1.8.jpg" width="111" height="32" /></p>
        </blockquote>
      </li>
    </ul>
    <p class="style10">Figures Citation:</p>
    <p class="style10">Hyperbola  (pp. 551-553). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</p>
  </div>