<div class="style1" id="content">
    <h2 align="center" class="style16">GEOMETRY</h2>
    <h2 align="center">Formulas</h2>
    <p align="left"><span class="style22">1. RECTANGULAR CARTESSIAN COORDINATES:</span></p>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/F1.jpg" width="314" height="278" /></p>
    <blockquote>
      <p class="style22">1.1 DISTANCE BETWEEN TWO POINTS:</p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/geometry/images/formula 1.jpg" width="172" height="22" /></p>
      </blockquote>
      <p class="style22">1.2 MIDPOINT:      </p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/geometry/images/formula 2.jpg" width="145" height="32" /> </p>
      </blockquote>
      <p class="style22">1.3SLOPE OF A LINE: </p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/geometry/images/formula 3.jpg" width="140" height="32" /> </p>
      </blockquote>
      <p><span class="style22">1.4 CONDITION FOR PARALLEL LINES: </span><span class="style18">m<sub>1</sub> = m<sub>2</sub></span> </p>
      <p><span class="style22">1.5 CONDITION FOR PERPENDICULAR LINES: </span><span class="style18">m<sub>1</sub>m<sub>2</sub> = -1 </span></p>
      <p class="style22">1.6 ANGLE BETWEEN TWO LINES: </p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/geometry/images/formula 4.jpg" width="129" height="32" /></p>
      </blockquote>
      <p class="style22">1.7 DISTANCE BETWEEN A POINT AND A LINE: </p>
      <blockquote>
        <p class="style22"><img src="<?php echo base_url();?>assets/geometry/images/formula 5.jpg" width="141" height="32" /></p>
      </blockquote>
    </blockquote>
    <p class="style22">&nbsp;</p>
    <p class="style22">2. STRAIGHT LINES:</p>
    <p align="center" class="style22"><img src="<?php echo base_url();?>assets/geometry/images/S1.jpg" width="342" height="305" /></p>
    <blockquote>
      <p align="left" class="style22">2.1 GENERAL EQUATION OF A LINE:</p>
      <blockquote>
        <p align="left" class="style23">Ax + Ay + C = 0 </p>
        <p class="style17"><img src="<?php echo base_url();?>assets/geometry/images/S2.jpg" width="229" height="256" /></p>
      </blockquote>
      <p class="style17"><span class="style22">2.2 STANDARD EQUATION OF A LINE:</span>      </p>
    </blockquote>
    <blockquote>
      <blockquote>
        <p align="left" class="style18">a. Point- Slope Form:&nbsp; y - y1 = m(x-x1)</p>
        <p align="left" class="style18">b. Slope-Intercept Form: y = mx + b</p>
        <p align="left" class="style18">c. Two-Intercept Form: </p>
        <blockquote>
          <p align="left" class="style18"><img src="<?php echo base_url();?>assets/geometry/images/S3.jpg" width="72" height="32" /></p>
        </blockquote>
        <p align="left" class="style18">d. Normal Form: </p>
        <blockquote>
          <p align="left" class="style18"><img src="<?php echo base_url();?>assets/geometry/images/s4.jpg" width="115" height="17" /></p>
          <p align="left" class="style18"><img src="<?php echo base_url();?>assets/geometry/images/s5.jpg" width="173" height="82" /></p>
        </blockquote>
        <p align="left" class="style18">e. Two-Point Form </p>
        <blockquote>
          <p class="style17"><img src="<?php echo base_url();?>assets/geometry/images/s7.jpg" width="163" height="36" /></p>
        </blockquote>
        <p class="style18">f. Distance from the point P<sub>3</sub>(x<sub>3</sub>, y<sub>3</sub>) to the line </p>
        <blockquote>
          <p align="justify" class="style18"> where: d is positive if P3 and 0 are on opposite sides of the line d is negative if P3 and 0 are on the same side of the line K has a sign opposite to the sign of C </p>
          <p class="style18"><img src="<?php echo base_url();?>assets/geometry/images/s8.jpg" width="124" height="34" /></p>
          <p align="left" class="style18">where: d is positive if P3 and 0 are on opposite sides of the line. </p>
          <p align="left" class="style18">d is negative if P3 and 0 are on the same side of the line. </p>
          <p align="left" class="style18">K has a sign opposite to the sign of C. </p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style22">3. CIRCLE:</p>
    <blockquote>
      <blockquote>
        <blockquote>
          <p class="style18"><img src="<?php echo base_url();?>assets/geometry/images/C1.jpg" width="278" height="243" /></p>
        </blockquote>
      </blockquote>
      <p class="style24">3.1 GENERAL EQUATION OF CIRCLE </p>
      <blockquote class="style18">
        <p>Ax2 + Cy2 + Dx + Ey + F = 0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (A = C) or&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
      
        <p>&nbsp;x2 + y2 + Dx + Ey + F = 0 (when A = C = 1) &nbsp; </p>
      </blockquote>
      <p class="style24">3.2 STANDARD FORM </p>
      <p class="style18"> 3.2.1 Rectangular Form</p>
      <blockquote class="style18">
        <p class="style18">&nbsp;&nbsp; <span class="style18">a.  Center C(h, k)</span></p>
        <blockquote class="style18">
          <p> Radius, r = a</p>
        </blockquote>
        <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;(x - h)2 + (y - k)2 = a2 </p>
        <p class="style18">&nbsp;&nbsp; b. Center C(0, 0)</p>
        <blockquote class="style18">
          <p align="justify">Radius, r = a ; x<sup>2</sup> + y<sup>2</sup> = 2ax</p>
        </blockquote>
        <p class="style18">&nbsp; c. Center C(0, a)</p>
        <blockquote>
          <p class="style18">Radius, r = a </p>
          <p class="style18">x<sup>2</sup> + y<sup>2</sup> = 2ay</p>
        </blockquote>
      </blockquote>
      <p class="style18">3.2.2 Polar </p>
    
      <blockquote class="style18">
        <p>a. Center&nbsp; <img src="<?php echo base_url();?>assets/geometry/images/Formulas_polar center.jpg" height="25" width="60" /></p>
        <blockquote>
          <p class="style18">&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Radius, r = a</p>
          <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/geometry/images/Formulas_circle standard form.jpg" alt="1" height="30" width="200" /></p>
          <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp; b. Center (0, 0)</p>
          <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; Radius, r = a</p>
          <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp;c. Center C(a, 0)</p>
          <p class="style18">&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Radius, r = a</p>
          <p class="style18">&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/geometry/images/X1.jpg" width="85" height="16" /></p>
          <p class="style18">&nbsp;&nbsp;&nbsp; d. Center C(0, a)</p>
          <p class="style18">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Radius, r = a</p>
          <blockquote>
            <p class="style18"><img src="<?php echo base_url();?>assets/geometry/images/X2.jpg" width="86" height="15" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <blockquote>
      <blockquote class="style18">
        <blockquote>
          <p>&nbsp;</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="right" class="style25"><a href="<?php echo base_url();?>index.php/lecture/geometry/8">Next<span class="style26">&gt;&gt;</span></a></p>
    <p class="style17">References:</p>
    <p class="style18">Fuller, 1992. Analytic geometry basic formulas. Analytic Geometry, 7<sup>th</sup> Edition. USA: Adison-Wesley Publishing Company </p>
    <p align="center">&nbsp;</p>
  </div>