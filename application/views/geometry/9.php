<div class="style1" id="content">
    <h2 align="center" class="style6">geometry</h2>
    <h2 align="center">referenceS</h2>
    <p align="justify" class="style4">CARTESIAN COORDINATE SYSTEM AND STRAIGHT LINE</p>
    <div align="justify">
      <ul>
        <li>Coordinate, Planes, Lines, and Linear Functions. Retrieved from. Pre Algebra (A Transition to Algebra). Glenncoe/Mc Grawhill </li>
      </ul>
    </div>
    <p align="justify"><span class="style4">CIRCLES</span></p>
    <ul>
      <li>Fuller, 1974. Circles (pp. 54 - 57). Analytic Geometry(Fourth Edition). Massachussets: Addison-Wesley Company. </li>
    </ul>
    <p align="justify"><span class="style4">PARABOLA</span></p>
    <div align="justify">
      <ul>
        <li><span class="style1">Conic Sections; Parabola (Section 11-1). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</span></li>
      </ul>
    </div>
    <p align="justify"><span class="style4">ELLIPSE</span></p>
    <div align="justify">
      <ul>
        <li>Fuller, 1992. Ellipse (pp. 125 - 126). Analytic Geometry (7th Ed.). USA: Adison - Wesley Publishing Company.</li>
      </ul>
      <ul>
        <li>Conic Sections; Parabola (Section 11-1). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</li>
      </ul>
    </div>
    <p align="justify" class="style4">HYPERBOLA</p>
    <div align="justify">
      <ul>
        <li><span class="style10">Hyperbola  (pp. 551-553). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</span></li>
      </ul>
      <ul>
        <li>Sanborn &amp; Shewell (1892). The Hyperbola, p. 141. Analytic Geometry for Colleges, Universities, and Technical Schools. New York: C.J. Peters &amp; Son. </li>
      </ul>
      <p class="style4">MENSURATION</p>
    </div>
    <ul><li class="style11"><span class="style1" align="justify">Mensuration (pp. 228 - 242). Retrieved from Department State Education Research and Training: www.dsert.kar.nic.in/.../English-Class%20X-Maths-Chapter09.pdf&lrm;</span></li>
    </ul>
    <p>&nbsp;</p>
  </div>