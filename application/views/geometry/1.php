<div class="style1" id="content">
    <h2 align="center" class="style6">geometry</h2>
    <h2 align="center">CIRCLE</h2>
    <ul>
      <li>
        <div align="justify">A<span class="style5"> circle</span> is a set of all points in a plane that are at the same distance from a fixed point called <span class="style5">center</span>. </div>
      </li>
      <li>
        <div align="justify">The<span class="style5"> circumference</span> of the circle is the distance around the circle. It contains 360<sup>o</sup>.</div>
      </li>
      <li>
        <div align="justify">A<span class="style5"> radius </span>of a circle is a line segment joining the center to a point on the circle. </div>
      </li>
      <li>A<span class="style5"> central angle</span> is an angle formed by two radii.</li>
      <li>An <span class="style5">arc</span> is a continuous part of a circle. A<span class="style5"> semicircle</span> is an arc measuring one-half the circumference of a circle.</li>
      <li>A <span class="style5">minor arc</span> is an arc that is less than a semicircle. A<span class="style5"> major arc</span> is an arc that is greater than a semi circle. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/Circle 1.1.jpg" width="196" height="171" /><img src="<?php echo base_url();?>assets/geometry/images/Circle 1.2.jpg" width="196" height="171" /></p>
    <ul>
      <li>A <span class="style5">diameter </span>of a circle is a chord through the center.</li>
      <li>A <span class="style5">secant </span>of a circle is a line that intersects the circle at two points.</li>
      <li>A<span class="style5"> tangent</span> of a circle is a line that touches the circle at one and only one point no matter how far produced.  </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/Circle 1.3.jpg" width="181" height="215" /></p>
    <ul>
      <li>
        <div align="justify">An<span class="style5"> inscribed polygon </span>is a polygon all of whose sides are chord of a circle.</div>
      </li>
      <li>
        <div align="justify">A<span class="style5"> circumscribed circle </span>is circle passing through each vertex of a polygon.  </div>
      </li>
      <li><span class="style5">Concentric circles </span>are circle that have the same center. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/circle 1.4.jpg" width="221" height="225" /></p>
    <ul>
      <ul>
        <li>Finding the radius:</li>
      </ul>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/circle radius.jpg" width="153" height="18" /></p>
    <ul>
      <ul>
        <li>If the center of a circle is at the origin (h = 0, k = 0) and the radius is r, its equation is:</li>
      </ul>
      <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/circle radius 1.2.jpg" width="85" height="18" /></p>
      <ul>
        <li>General form:</li>
      </ul>
      <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/circle general form.jpg" width="158" height="18" /></p>
      <ul>
        <li>Point circle: this equation us satusfued only by the point (h, k), it depeds on the constant D, E, and F. By completing the squares, tge equation may be presented as:</li>
      </ul>
      <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/circle 1.5.jpg" width="253" height="32" /></p>
    </ul>
    <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="14" /><strong>Example</strong>:</p>
    <p align="left">Find the equation of the circle which passes through the points P(1 , -2), Q(5, 4), and R(10, 5)</p>
    <blockquote>
      <p align="left"><span class="style7">Solution:</span></p>
      <blockquote>
        <p align="left" class="style7">The equation of the circle can be expressed in the form</p>
        <p align="left" class="style7">x<sup>2</sup> + y<sup>2</sup> + Dx + Ey + F = 0  </p>
        <p align="left" class="style7">Substitute x and y:</p>
        <p align="left" class="style7">1 + 4 + D - 2E + F = 0</p>
        <p align="left" class="style7">25 + 16 + 5D + 4E + F = 0</p>
        <p align="left" class="style7">100 + 25 + 10D + 5E + F = 0</p>
        <p align="left" class="style7">The solution of these equations is D = -18, E = 6, and F = 25.</p>
        <p align="left" class="style7">x<sup>2</sup> + y<sup>2</sup> - 18x + 6y + 25    </p>
        <p align="left" class="style7">Perpendicular bisectors of PQ and QR: 2x + 3y = 9 and 5x + y = 42 </p>
        <p align="left" class="style7">Using center radius form: (x - 9)<sup>2</sup> + (y + 3)<sup>2</sup> = 65 </p>
        <p align="center" class="style7"><img src="<?php echo base_url();?>assets/geometry/images/circle ex.jpg" width="222" height="201" /></p>
      </blockquote>
    </blockquote>
    <p align="left" class="style7"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt=" 1" width="31" height="14" /> <span class="style8">Exercise:</span></p>
    <p align="left" class="style7">A circle is tangent to the line 2x - y + 1 = 0 at the point (2, 5), and the center is on the line x + y = 9. Find the equation of the circle. </p>
    <p align="left" class="style7">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p align="center" class="style7">&nbsp;</p>
      </blockquote>
    </blockquote>
  </div>