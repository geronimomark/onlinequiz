<div class="style1" id="content">
    <h2 align="center" class="style6">geometry</h2>
    <h2 align="center">Ellipse</h2>
    <ul>
      <li>An<span class="style5"> ellipse</span> is a sum of all points P in a plane such that the sum of the distances of P from two fixed points in the plane is constant. </li>
      <li>Each of the fixed points, F' and F, is called a <span class="style5">focus</span>, and together they are called <span class="style5">foci</span>. </li>
      <li>Each end of the major axis, V' and V is called a<span class="style5"> vertex</span>.</li>
      <li>The midpoint of the line segment F' F is called the <span class="style5">center</span> of the ellipse.</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.1.jpg" width="226" height="156" /></p>
    <p class="style11"><u>STANDARD EQUATIONS AND THEIR GRAPHS</u> </p>
    <ul>
      <li>Ellipse with foci on x - axis</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.2.jpg" width="269" height="185" /></p>
    <ul>
      <ul>
        <li>If b &gt; 0 </li>
      </ul>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.3.jpg" width="110" height="32" /></p>
      </blockquote>
      <ul>
        <li>If a &gt; b</li>
      </ul>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.4.jpg" width="110" height="32" /></p>
      </blockquote>
      <p>&nbsp;</p>
      <ul>
        <li>Major axis length = 2a</li>
        <li>Minor axis length = 2b</li>
      </ul>
      <p>&nbsp;</p>
      <li>Standard Equation of an Ellipse with Center at (0, 0) </li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.3.jpg" width="110" height="32" /></p>
    </blockquote>
    <p><span class="style12">x intercept:</span> <img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.6.jpg" width="25" height="17" /> <span class="style12">y intercept:</span> <img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.7.jpg" width="25" height="17" /></p>
    <p>Foci: F' (-c, 0), F (c, 0) </p>
    <p>c<sup>2</sup> = a<sup>2</sup> - b<sup>2</sup> </p>
    <p>Major axis length = 2a</p>
    <p>Minor axis length = 2b</p>
    <blockquote>
      <p>&nbsp;</p>
      <p><img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.4.jpg" width="110" height="32" /></p>
    </blockquote>
    <p>x intercept: <img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.7.jpg" alt="1" width="25" height="17" /> y intercept: <img src="<?php echo base_url();?>assets/geometry/images/ellipse 1.6.jpg" alt="1" width="25" height="17" /></p>
    <p>Foci: F'(0, -c), F(0, c)</p>
    <p>c<sup>2</sup> = a<sup>2</sup> - b<sup>2</sup></p>
    <p>Major axis = 2a</p>
    <p>Minor axis = 2b </p>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="17" /><strong>Examples:</strong></p>
    <p>1. Sketch the graph of the equation 9x2 + 16y2 = 144, find the coordinates of the foci, and find the length of the major and minor axis.</p>
    <blockquote>
      <p class="style12">Solution:</p>
      <blockquote>
        <p class="style12">9x<sup>2</sup> + 16y<sup>2</sup> = 144</p>
        <p class="style12">9x<sup>2</sup>/144 + 16y<sup>2</sup>/144 = 144/144</p>
        <p class="style12">x<sup>2</sup>/16 + y<sup>2</sup>/9 = 1</p>
        <p class="style12">a<sup>2</sup> = 16 and b<sup>2</sup> = 9 </p>
        <p class="style12">x intercepts = +/- 4 ; y intercepts = +/- 3</p>
        <p class="style12"><img src="<?php echo base_url();?>assets/geometry/images/ellipse 2.2.jpg" alt="1" width="260" height="164" /></p>
      </blockquote>
    </blockquote>
    <p class="style12">2. Find the equation of the ellipse with foci at (4, -2) and (10, -2) and a vertex at (12, -2)</p>
    <blockquote>
      <p class="style12">Solution: </p>
      <blockquote>
        <p class="style12">Foci = (7, -2)</p>
        <p class="style12">Distance between the foci = 6</p>
        <p class="style12">Vertex: 5 units from the center</p>
        <p class="style12">c = 3 ; a = 5 </p>
        <p class="style12">b<sup>2</sup> = a<sup>2</sup> - c<sup>2</sup> = 16  </p>
        <p class="style12"><img src="<?php echo base_url();?>assets/geometry/images/ellipse 2.1.jpg" width="143" height="32" /> </p>
        <p class="style12">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p>&nbsp;  </p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p class="style10">Figures Citation:</p>
    <p class="style10">Ellipse (pp. 535 - 541). Retrieved from Online Learning Center: www.mhhe.com/math/precalc/barnettpc5/graphics/.../bpc5_ch11-01.pdf</p>
  </div>