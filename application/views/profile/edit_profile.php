<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {
	font-size: x-large;
	font-weight: bold;
}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "profile";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form" style="max-width:600px;">
        <?php if($new):?>
          <p class="form-signin-heading style1">Add My Profile</p>
        <?php else:?>
          <p class="form-signin-heading style1">My Profile</p>
        <?php endif;?>
        <div id="success" class="alert alert-success"></div>
        <div id="error" class="alert alert-danger"></div>
        <input id="fname" autocomplete="off" value="<?php echo isset($result['fname']) ? $result['fname'] : '';?>" type="text" class="form-control" placeholder="First Name" required autofocus>
        <input id="mname" autocomplete="off" value="<?php echo isset($result['mname']) ? $result['mname'] : '';?>" type="text" class="form-control" placeholder="Middle Name" required>
        <input id="lname" autocomplete="off" value="<?php echo isset($result['lname']) ? $result['lname'] : '';?>" type="text" class="form-control" placeholder="Last Name" required>
        <select id="gender" class="form-control" required>
          <option value="M" <?php if(isset($result['gender']) && $result['gender'] == "M"):?>selected<?php endif;?>>Male</option>
          <option value="F" <?php if(isset($result['gender']) && $result['gender'] == "F"):?>selected<?php endif;?>>Female</option>
        </select>
        <input id="bday" autocomplete="off" value="<?php echo isset($result['bday']) ? $result['bday'] : '';?>" type="text" class="form-control" placeholder="Birthday" required>
        <hr />
        <p class="form-signin-heading style1">Education Information</p>        
        <table align="center" class="table">
          <tr>
            <td>School</td>
            <td>
              <input id="school" autocomplete="off" value="<?php echo isset($result['school']) ? $result['school'] : '';?>" type="text" class="form-control" placeholder="School" required>
            </td>
          </tr>
          <tr>
            <td>Course</td>
            <td>
              <select id="course" class="form-control" required>
                <option value="ECE" <?php if(isset($result['course']) && $result['course'] == "ECE"):?>selected<?php endif;?>>Electronics and Communications Engineering</option>
                <option value="EE" <?php if(isset($result['course']) && $result['course'] == "EE"):?>selected<?php endif;?>>Electrical Engineering</option>
                <option value="ME" <?php if(isset($result['course']) && $result['course'] == "ME"):?>selected<?php endif;?>>Mechanical Engineering</option>
                <option value="CE" <?php if(isset($result['course']) && $result['course'] == "CE"):?>selected<?php endif;?>>Civil Engineering</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Year Level</td>
            <td>
              <select id="yearlevel" class="form-control" required>
                <option value="5" <?php if(isset($result['yearlevel']) && $result['yearlevel'] == "5"):?>selected<?php endif;?>>5th year</option>
                <option value="4" <?php if(isset($result['yearlevel']) && $result['yearlevel'] == "4"):?>selected<?php endif;?>>4th year</option>
                <option value="3" <?php if(isset($result['yearlevel']) && $result['yearlevel'] == "3"):?>selected<?php endif;?>>3rd year</option>
                <option value="2" <?php if(isset($result['yearlevel']) && $result['yearlevel'] == "2"):?>selected<?php endif;?>>2nd year</option>
                <option value="1" <?php if(isset($result['yearlevel']) && $result['yearlevel'] == "1"):?>selected<?php endif;?>>1st year</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Favorite Subject</td>
            <td>
              <select id="subject" class="form-control" required>
                <option value="Algebra" <?php if(isset($result['subject']) && $result['subject'] == "ALGEBRA"):?>selected<?php endif;?>>Algebra</option>
                <option value="Geometry" <?php if(isset($result['subject']) && $result['subject'] == "GEOMETRY"):?>selected<?php endif;?>>Geometry</option>
                <option value="Trigonometry" <?php if(isset($result['subject']) && $result['subject'] == "TRIGONOMETRY"):?>selected<?php endif;?>>Trigonometry</option>
                <option value="Differential" <?php if(isset($result['subject']) && $result['subject'] == "DIFFERENTIAL"):?>selected<?php endif;?>>Differential</option>
                <option value="Integral" <?php if(isset($result['subject']) && $result['subject'] == "INTEGRAL"):?>selected<?php endif;?>>Integral</option>
                <option value="Advanced Math" <?php if(isset($result['subject']) && $result['subject'] == "ADVANCED MATH"):?>selected<?php endif;?>>Advanced Math</option>
                <option value="Differential Equation" <?php if(isset($result['subject']) && $result['subject'] == "DIFFERENTIAL EQUATION"):?>selected<?php endif;?>>Differential Equation</option>
              </select>
            </td>
          </tr>
        </table>
        <hr />
        <p class="form-signin-heading style1">My Grades</p>
        <table align="center" class="table">
          <tr>
            <th>Subject</th>
            <th>Grade</th>
          </tr>
          <tr>
            <td>Algebra</td>
            <td>
              <select id="algebra" required>
                <option value="N/A" <?php if(isset($result['algebra']) && $result['algebra'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['algebra']) && $result['algebra'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['algebra']) && $result['algebra'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['algebra']) && $result['algebra'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['algebra']) && $result['algebra'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['algebra']) && $result['algebra'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['algebra']) && $result['algebra'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['algebra']) && $result['algebra'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['algebra']) && $result['algebra'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['algebra']) && $result['algebra'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['algebra']) && $result['algebra'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['algebra']) && $result['algebra'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Trigonometry</td>
            <td>
              <select id="trigo" required>
                <option value="N/A" <?php if(isset($result['trigo']) && $result['trigo'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['trigo']) && $result['trigo'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['trigo']) && $result['trigo'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['trigo']) && $result['trigo'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['trigo']) && $result['trigo'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['trigo']) && $result['trigo'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['trigo']) && $result['trigo'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['trigo']) && $result['trigo'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['trigo']) && $result['trigo'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['trigo']) && $result['trigo'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['trigo']) && $result['trigo'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['trigo']) && $result['trigo'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Geometry</td>
            <td>
              <select id="geometry" required>
                <option value="N/A" <?php if(isset($result['geometry']) && $result['geometry'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['geometry']) && $result['geometry'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['geometry']) && $result['geometry'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['geometry']) && $result['geometry'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['geometry']) && $result['geometry'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['geometry']) && $result['geometry'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['geometry']) && $result['geometry'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['geometry']) && $result['geometry'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['geometry']) && $result['geometry'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['geometry']) && $result['geometry'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['geometry']) && $result['geometry'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['geometry']) && $result['geometry'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Differential</td>
            <td>
              <select id="differential" required>
                <option value="N/A" <?php if(isset($result['diff']) && $result['diff'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['diff']) && $result['diff'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['diff']) && $result['diff'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['diff']) && $result['diff'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['diff']) && $result['diff'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['diff']) && $result['diff'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['diff']) && $result['diff'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['diff']) && $result['diff'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['diff']) && $result['diff'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['diff']) && $result['diff'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['diff']) && $result['diff'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['diff']) && $result['diff'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Integral</td>
            <td>
              <select id="integral" required>
                <option value="N/A" <?php if(isset($result['integral']) && $result['integral'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['integral']) && $result['integral'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['integral']) && $result['integral'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['integral']) && $result['integral'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['integral']) && $result['integral'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['integral']) && $result['integral'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['integral']) && $result['integral'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['integral']) && $result['integral'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['integral']) && $result['integral'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['integral']) && $result['integral'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['integral']) && $result['integral'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['integral']) && $result['integral'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Advanced Math</td>
            <td>
              <select id="advanced" required>
                <option value="N/A" <?php if(isset($result['advmath']) && $result['advmath'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['advmath']) && $result['advmath'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['advmath']) && $result['advmath'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['advmath']) && $result['advmath'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['advmath']) && $result['advmath'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['advmath']) && $result['advmath'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['advmath']) && $result['advmath'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['advmath']) && $result['advmath'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['advmath']) && $result['advmath'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['advmath']) && $result['advmath'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['advmath']) && $result['advmath'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['advmath']) && $result['advmath'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>Differential Equation</td>
            <td>
              <select id="de" required>
                <option value="N/A" <?php if(isset($result['diffeq']) && $result['diffeq'] == "N/A"):?>selected<?php endif;?>>N/A</option>
                <option value="1.00" <?php if(isset($result['diffeq']) && $result['diffeq'] == "1.00"):?>selected<?php endif;?>>1.00</option>
                <option value="1.25" <?php if(isset($result['diffeq']) && $result['diffeq'] == "1.25"):?>selected<?php endif;?>>1.25</option>
                <option value="1.50" <?php if(isset($result['diffeq']) && $result['diffeq'] == "1.50"):?>selected<?php endif;?>>1.50</option>
                <option value="1.75" <?php if(isset($result['diffeq']) && $result['diffeq'] == "1.75"):?>selected<?php endif;?>>1.75</option>
                <option value="2.00" <?php if(isset($result['diffeq']) && $result['diffeq'] == "2.00"):?>selected<?php endif;?>>2.00</option>
                <option value="2.25" <?php if(isset($result['diffeq']) && $result['diffeq'] == "2.25"):?>selected<?php endif;?>>2.25</option>
                <option value="2.50" <?php if(isset($result['diffeq']) && $result['diffeq'] == "2.50"):?>selected<?php endif;?>>2.50</option>
                <option value="2.75" <?php if(isset($result['diffeq']) && $result['diffeq'] == "2.75"):?>selected<?php endif;?>>2.75</option>
                <option value="3.00" <?php if(isset($result['diffeq']) && $result['diffeq'] == "3.00"):?>selected<?php endif;?>>3.00</option>
                <option value="4.00" <?php if(isset($result['diffeq']) && $result['diffeq'] == "4.00"):?>selected<?php endif;?>>4.00</option>
                <option value="5.00" <?php if(isset($result['diffeq']) && $result['diffeq'] == "5.00"):?>selected<?php endif;?>>5.00</option>
              </select>
            </td>
          </tr>
        </table>
        <hr />
        <p class="form-signin-heading style1">Technology Information</p>
        <p>Where do you use computer most?</p>
        <select id="q1" class="form-control" required>
          <option value="Home" <?php if(isset($result['q1']) && $result['q1'] == "Home"):?>selected<?php endif;?>>Home</option>
          <option value="School" <?php if(isset($result['q1']) && $result['q1'] == "School"):?>selected<?php endif;?>>School</option>
          <option value="Computer Shop" <?php if(isset($result['q1']) && $result['q1'] == "Computer Shop"):?>selected<?php endif;?>>Computer Shop</option>
        </select>
        <p>How often do you use a computer?</p>
        <select id="q2" class="form-control" required>
          <option value="Everyday" <?php if(isset($result['q2']) && $result['q2'] == "Everyday"):?>selected<?php endif;?>>Everyday</option>
          <option value="Once a week" <?php if(isset($result['q2']) && $result['q2'] == "Once a week"):?>selected<?php endif;?>>Once a week</option>
          <option value="Once a month" <?php if(isset($result['q2']) && $result['q2'] == "Once a month"):?>selected<?php endif;?>>Once a month</option>
          <option value="Almost Never" <?php if(isset($result['q2']) && $result['q2'] == "Almost Never"):?>selected<?php endif;?>>Almost Never</option>
        </select>
        <p>How many computers, if any, do you have at home?</p>
        <select id="q3" class="form-control" required>
          <option value="0" <?php if(isset($result['q3']) && $result['q3'] == "0"):?>selected<?php endif;?>>0</option>
          <option value="1" <?php if(isset($result['q3']) && $result['q3'] == "1"):?>selected<?php endif;?>>1</option>
          <option value="2" <?php if(isset($result['q3']) && $result['q3'] == "2"):?>selected<?php endif;?>>2</option>
          <option value="3" <?php if(isset($result['q3']) && $result['q3'] == "3"):?>selected<?php endif;?>>3</option>
          <option value="more" <?php if(isset($result['q3']) && $result['q3'] == "more"):?>selected<?php endif;?>>more</option>
        </select>
        <p>How many people share your home computer?</p>
        <select id="q4" class="form-control" required>
          <option value="1 person" <?php if(isset($result['q4']) && $result['q4'] == "1 person"):?>selected<?php endif;?>>1 person</option>
          <option value="2-3 persons" <?php if(isset($result['q4']) && $result['q4'] == "2-3 persons"):?>selected<?php endif;?>>2-3 persons</option>
          <option value="4-5 persons" <?php if(isset($result['q4']) && $result['q4'] == "4-5 persons"):?>selected<?php endif;?>>4-5 persons</option>
          <option value="6 or more" <?php if(isset($result['q4']) && $result['q4'] == "6 or more"):?>selected<?php endif;?>>6 or more</option>
          <option value="We dont have a computer" <?php if(isset($result['q4']) && $result['q4'] == "We dont have a computer"):?>selected<?php endif;?>>We dont have a computer</option>
        </select>
        <button id="save" class="btn btn-lg btn-primary" type="submit">Save</button>
        <a id="cancel" class="btn btn-lg btn-default" href="<?php echo base_url();?>index.php/home">Cancel</a>
        <br />
        <br />
        <p align="center"><i>"Thank you for completing our survey"</i></p>
      </form>


 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
    <script>

      $(function(){

        var err_msg = new Array(),
        ctr_false = 0;

        err_msg[0] = false,
        err_msg[1] = false,
        err_msg[2] = false;

        $("#error, #success").hide();

        $("#bday").datepicker({ 
                dateFormat: "yy-mm-dd",
                yearRange: "-65:-15",
                changeMonth: true,
                changeYear: true,
                defaultDate: "<?php echo date('Y') - 18;?>" + "-01-01"
            });

        $("#save").click(function(e){
          if( $("#fname").val() == '' || $("#mname").val() == '' || $("#lname").val() == '' ||
            $("#bday").val() == '' || $("#gender").val() == '' || $("#school").val() == ''){
            err_msg[2] = "All fields are required!";
          }else{
            e.preventDefault();
            err_msg[2] = false;
            if( $("#error").text() == ""){
              $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>index.php/profile/save_profile',
                data: 
                {
                  fname     :   $("#fname").val(),
                  mname     :   $("#mname").val(),
                  lname     :   $("#lname").val(),
                  bday      :   $("#bday").val(),
                  gender    :   $("#gender").val(),
                  school    :   $("#school").val(),
                  course    :   $("#course").val(),
                  ylevel    :   $("#yearlevel").val(),
                  subject   :   $("#subject").val(),
                  algebra   :   $("#algebra").val(),
                  trigo     :   $("#trigo").val(),
                  geometry  :   $("#geometry").val(),
                  diff      :   $("#differential").val(),
                  integral  :   $("#integral").val(),
                  advmath   :   $("#advanced").val(),
                  diffeq    :   $("#de").val(),
                  q1        :   $("#q1").val(),
                  q2        :   $("#q2").val(),
                  q3        :   $("#q3").val(),
                  q4        :   $("#q4").val(),
                },
                success: function(msg){
                  if(msg == "success"){
                    $("#success").html("Profile successfully saved!").show();
                    // setTimeout(function(){$("#success").empty().hide()},3000);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>index.php/profile/edit_profile',100});
                  }
                }
              });
            }
          }
          generate_error();
        });

        function generate_error()
        {
          var msg = "";
          ctr_false = 0;

          for (var i = 0; i < err_msg.length; i++) {
            if(err_msg[i] != false){
              msg += err_msg[i] + "<br />";
            }else{
              ctr_false++;
            }
          }
          if(err_msg.length == ctr_false){
            $("#error").empty().hide();
          }else{
            $("#error").html(msg).show();
          }
          err_msg[2] = false;
        }

      });

    </script>
  </body>
</html>
