<div class="style1" id="content">
    <h2 align="center" class="style4">algebra</h2>
    <h2 align="center">INVERSE, EXPONENTIAL, AND LOGARITHMIC FUNCTIONS </h2>
    <p class="style17"><u>EXPONENTIAL FUNCTIONS </u></p>
    <ul>
      <li>The exponential function f with base a is f(x) = ax, where a &gt; 0, a is not equal to 1, x <img src="<?php echo base_url();?>assets/algebra/images/belong.jpg" width="16" height="21" />R. The domain of f is R, and the range is <img src="<?php echo base_url();?>assets/algebra/images/undefined.jpg" width="38" height="21" /> . (0 itself is not in the range f) </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="24" height="14" /><strong>Example:</strong> Consider f(x) = 2x. Create a table of values for x and f(x), and then sketch a graph of f.</p>
    <blockquote>
      <div id="Layer2"><img src="<?php echo base_url();?>assets/algebra/images/exponential function graph.jpg" width="271" height="162" /></div>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/exponential function.jpg" width="126" height="266" /></p>
      </blockquote>
    </blockquote>
    <ul>
      <li><strong>Theorem:</strong></li>
    </ul>
    <p>1. If a &gt; 1, the graph of f(x) = a<sup>x</sup> will rise to the right.</p>
    <p>2. If a &lt; 1, then the graph of f(x) = a<sup>x</sup> will fall to the right.</p>
    <p>3. The graph of g(x) = (1/a)<sup>x</sup> is the reflection of the graph of y = ax across the y-axis.</p>
    <p>4. If f(x) = a<sup>x</sup>, then f(0) = 1 and f(1) = a. </p>
    <blockquote>
      <p class="style3">Naturally, a larger value of a will cause the graph to rise more rapidly. </p>
    </blockquote>
    <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="23" height="16" /><strong>Example: </strong>Interest. Suppose you invest P dollars at an annual interest rate of r (expressed as a decimal), and interest is compounded n times per year. Let A(t) denote your account balance after t years have elapsed. Find a close formula for A(t) = in terms of P, r, n, and t.</p>
    <blockquote>
      <p align="justify" class="style3">Solution:</p>
      <p align="justify" class="style3">First since r is an annual rate, the rate of a single compounding period is r/n.</p>
      <p align="justify" class="style3">Initially, your account holds P dollars. After one compounding period, you add to this interest in the amount of r/n(P), so that your balance is:</p>
      <p align="justify" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/exponential 1.1.jpg" width="244" height="32" /> </p>
      <p align="justify" class="style3">This is the beginning balance for the next period.</p>
      <p align="justify" class="style3">The amount of money at the beginning of the period is irrelevant to the above computation: if you begin the period with x dollars, you need the period with (1+r/n)x dollars. Therefore, since you begin the second period with P(1+r/n) dollars, you end it with </p>
      <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/exponential 1.3.jpg" width="304" height="32" /> </p>
      <p class="style3">Notice that in each case, the exponent on <img src="<?php echo base_url();?>assets/algebra/images/exponential 1.4.jpg" width="47" height="32" /> is n times the argument of A. For example, the exponent on <img src="<?php echo base_url();?>assets/algebra/images/exponential 1.4.jpg" alt="1" width="47" height="32" />in A(3/n) is 3. In general, then, we have:</p>
      <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/exponential 1.5.jpg" width="135" height="32" /> </p>
    </blockquote>
    <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="23" height="16" /><strong>Example</strong>: In sample originally having 1 g of radium 226, the amount of radium 226 present after t years is given by A(t) = (1/2)<sup>t/1620</sup>.</p>
    <p class="style3">Where A is the amount of radium in grams and t is the time in years</p>
    <p class="style3">a. How much radium 226 will be present after 1620 years?</p>
    <p class="style3">b. How much radium 226 will be present after 3240 years?</p>
    <blockquote>
      <p class="style3">Solution:</p>
      <blockquote>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.3.jpg" width="91" height="38" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.4.jpg" width="124" height="28" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.5.jpg" width="34" height="28" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.6.jpg" width="30" height="16" /></p>
      </blockquote>
    </blockquote>
    <div class="style1" id="content2">
      <p class="style20"><u>NATURAL EXPONENTIAL FUNCTIONS</u></p>
      <ul>
        <li>
          <div align="justify">The irrational number <em>e</em> is approximately 2.71828... It is the base of the <span class="style2">natural exponential</span> and the natural <span class="style2">logarithm function</span>. </div>
        </li>
        <li>The function f(x) = e<sup>x</sup> is the<span class="style2"> natural exponential function</span>.</li>
        <li>The<span class="style2"> natural logarithm</span> function is defined by f(x) = log<sub>e</sub>x for x &gt; 0. It is usually written lnx. Its graph is given below: </li>
      </ul>
      <p align="center" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/natural logarithm.jpg" alt="1" width="267" height="223" /></p>
      <p align="left" class="style20">&nbsp;</p>
    </div>
    <p class="style17"><u>LOGARITHMIC FUNCTIONS</u></p>
    <ul>
      <li>Logarithmic functions g with base a is the inverse of the function f(x) = a<sup>x</sup> for a &gt; 0, a is not equal to 1. Write g(x) = log<sub>a</sub>(x). That is <em><strong>y = log<sub>a</sub>(x) if and only if a<sup>y</sup> = x.</strong></em>
          <ul>
            <li>The domain of log<sub>a</sub> is <img src="<?php echo base_url();?>assets/algebra/images/log 1.1.jpg" width="38" height="16" />, and the range <img src="<?php echo base_url();?>assets/algebra/images/log 1.2.jpg" width="42" height="16" /></li>
          </ul>
      </li>
    </ul>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="17" />Examples:</strong></p>
    <p>1. log<sub>2</sub>(8) = 3, since 2<sup>3</sup> = 8 </p>
    <p>2. log<sub>10</sub> (1/100) = -2, since 10<sup>-2</sup> = 1/100 </p>
    <ul>
      <li>Since the functions a<sup>x</sup> and log<sub>a</sub>(x) are inverse functions, if we compose them, we get identity function. Thus <strong>log<sub>a</sub>(a<sup>x</sup>) = x and a<sup>log<sub>a</sub>(x)</sup> = x </strong></li>
    </ul>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" />Examples:</strong></p>
    <p>1. log<sub>2</sub>(2<sup>x</sup>) = x </p>
    <p>2. log<sub>a</sub>(a<sup>3</sup>) = 3</p>
    <p>3. 4<sup>log<sub>4</sub>x</sup> = x</p>
    <ul>
      <li>Laws of exponents and logarithms:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/log.jpg" width="494" height="192" /></p>
    <p align="left">&nbsp;</p>
    <p align="left" class="style1"><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><span class="style3">Example:</span></strong> log<sub>3</sub>(4x - 7) =2 </p>
    <blockquote>
      <p align="left" class="style1 style18 style3">Solution:</p>
      <blockquote>
        <p align="left" class="style1 style18 style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.jpg" width="125" height="14" /></p>
        <p align="left" class="style1 style18 style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.1.jpg" width="55" height="15" /></p>
        <p align="left" class="style1 style18 style3"><img src="<?php echo base_url();?>assets/algebra/images/log2.2.jpg" width="38" height="15" /></p>
      </blockquote>
    </blockquote>
    <p align="left">&nbsp;</p>
    <p align="left"><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" />Example:</strong> Express 2log<sub>3</sub>(x + 3) + log<sub>3</sub>(x) - log<sub>3</sub>7 as a single logarithm. </p>
    <blockquote>
      <p align="left" class="style3">Solution:</p>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log 1.3.jpg" width="415" height="32" /></p>
        <blockquote>
          <blockquote>
            <blockquote>
              <blockquote>
                <blockquote>
                  <blockquote>
                    <blockquote>
                      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log 1.4.jpg" width="135" height="32" /></p>
                    </blockquote>
                  </blockquote>
                </blockquote>
              </blockquote>
            </blockquote>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left"><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /></strong><strong>Example:</strong> Given log<sub>3</sub> (x<sup>2</sup> - 8x) =2. Find x</p>
    <blockquote>
      <p align="left"><span class="style3">Solution:</span></p>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log 1.5.jpg" width="135" height="20" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log 1.6.jpg" width="274" height="20" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log1.7.jpg" width="155" height="22" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log1.8.jpg" width="75" height="15" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/log1.9.jpg" width="125" height="16" /></p>
      </blockquote>
    </blockquote>
    <p align="left">&nbsp;</p>
    <p align="left"><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /></strong><span class="style19">Exercises:</span></p>
    <p align="left">1. log<sub>3</sub>(x) + log<sub>3</sub>(2)</p>
    <p align="left">2. -4log<sub>3</sub>(2x) </p>
    <p class="style20">&nbsp;</p>
  </div>