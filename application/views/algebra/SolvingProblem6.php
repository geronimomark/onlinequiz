<div class="style1" id="content">
    <h2 align="center" class="style13">algebra</h2>
    <h2 align="center">Practice problem (6) </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">A   piece of paper is 0.05 thick. Each time the paper is folded into half,      the thickness is doubled. If the paper was folded 12 times, how thick       the feet the folded paper be? </p>
    <p align="justify">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style12">a<sub>1</sub>&nbsp; = 0.05; a2 = 0.10 .... an</p>
      <p class="style12">a<sub>n</sub> = a<sub>1</sub>r<sup>n-1</sup></p>
      <p class="style12"> a<sub>12</sub> = (0.10)(2)<sup>12-1</sup> </p>
      <p class="style12">a<sub>12</sub> = 204.8 inches </p>
      <p class="style12">a<sub>12</sub> = 204.8 in x 1ft/12in </p>
      <p class="style12"><em><strong> answer:&nbsp; a<sub>12</sub> = 17.07 feet </strong></em></p>
      <p class="style12 style12">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
  </div>