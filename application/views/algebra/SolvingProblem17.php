<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(17)</h2>
    <p align="center">&nbsp;</p>
    <p align="justify">Find the product of x + y - 2 and x + y + 2</p>
    <blockquote>
      <p align="justify" class="style11">Solution:</p>
      <blockquote class="style11">
        <p>&nbsp; (x + y - 2) (x + y + 2) = [(x + y) - 2][(x + y) + 2]</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = (x + y)<sup>2</sup> - 2<sup>2</sup> </p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = x<sup>2</sup> + 2xy + y<sup>2</sup> - 4 </p>
        <p>&nbsp;</p>
        <p align="justify">&nbsp;</p>
        <blockquote>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
  </div>