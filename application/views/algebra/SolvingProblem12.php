<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(12)</h2>
    <p align="justify">Find the real solution of the following equations (x<sup>2</sup> + 3x - 12)<sup>3/2</sup> = 8 </p>
    <p align="left">&nbsp;</p>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <div align="justify">
      <blockquote class="style11">&nbsp;&nbsp;Since the left hand side of the equation is raised to the 3/2 power, extend the power property of equality and raise each side of the equation to the 2/3 power </blockquote>
    </div>
    <blockquote>
      <p align="justify">&nbsp;<span class="style11">(x<sup>2</sup> + 3x - 14)<sup>3/2</sup> = 8 </span></p>
      <p align="justify" class="style11">(&nbsp;(x<sup>2</sup> + 3x - 14)<sup>3/2</sup> )<sup>2/3</sup> = 8<sup>2/3</sup></p>
      <p align="justify" class="style11">x<sup>2</sup> + 3x - 14 = 4</p>
      <p align="justify" class="style11">x<sup>2</sup> + 3x - 18 = 0</p>
      <p align="justify" class="style11">(x + 6) (x - 3)</p>
      <p align="justify" class="style11">x = -6 ; x = 3   </p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p class="style11">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
  </div>