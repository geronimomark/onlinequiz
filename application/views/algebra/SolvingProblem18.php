<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(18)</h2>
    <p align="center">&nbsp;</p>
    <p align="justify">A football quarterback throws a pass from the 28 - yard line, 40   yards from the sideline. The pass is caught by a wide receiver on the   5-yard line, 20 yards from the same sideline. How long is the line</p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="justify" class="style11">Solution:</p>
      <blockquote class="style11">
        <p>&nbsp;<img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/18.1.jpg" width="193" height="17" /></p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/18.2.jpg" width="193" height="16" /></p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/18.3.jpg" width="87" height="16" /></p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/18.4.jpg" width="35" height="16" /></p>
        <blockquote>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
  </div>