<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (25)</h2>
    <p align="center">&nbsp;</p>
    <p>Find the 5th term of the expression of (x<sup>2</sup> + 1/x)<sup>10</sup></p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <blockquote>
        <p align="justify">	<img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/25.1.jpg" width="77" height="49" /></p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/25.2.jpg" width="200" height="38" /></p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/25.3.jpg" width="130" height="17" /></p>
        <p class="style13"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/25.4.jpg" width="370" height="31" /></p>
        <p class="style13"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/25.5.jpg" width="125" height="16" /></p>
        <p class="style13">&nbsp;</p>
        <p class="style13">&nbsp;</p>
      </blockquote>
      <p>&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
  </div>