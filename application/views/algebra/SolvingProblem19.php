<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(19)</h2>
    <p align="center">&nbsp;</p>
    <p align="justify">Show that the given sequence is geometric: {t<sub>n</sub>} = {3 * 4<sub>n</sub>} </p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="justify" class="style11">Solution:</p>
      <blockquote class="style11">
        <p>First Term: T<sub>1</sub> = 3 * 4<sup>1</sup> </p>
        <p>Nth Term: T<sub>n</sub> = 3 * 4<sup>n-1</sup> and T<sub>n</sub> = 3 * 4<sup>n-1</sup> </p>
        <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/19.1.jpg" width="186" height="33" /></p>
        <p>The sequence, {tn}, is geometric sequence with common ratio 4</p>
        <p>&nbsp; </p>
        <blockquote>
          <p>&nbsp;</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style10">&nbsp;</p>
  </div>