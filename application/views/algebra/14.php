<div class="style1" id="content">
    <h2 align="center" class="style17">algebra</h2>
    <h2 align="center" class="style20">FOrmulas</h2>
    <ul>
      <li class="style20"><span class="style24">Imaginary number and it's equivalent:
        
        </span>
        <blockquote class="style24">
            <p><img src="<?php echo base_url();?>assets/algebra/images/f2.3.jpg" width="65" height="20" /></p>
            <p><img src="<?php echo base_url();?>assets/algebra/images/f2.4.jpg" width="60" height="20" /></p>
            <p><img src="<?php echo base_url();?>assets/algebra/images/f2.5.jpg" width="120" height="22" /></p>
            <p><img src="<?php echo base_url();?>assets/algebra/images/f2.6.jpg" width="60" height="20" /></p>
        </blockquote>
      </li>
      <li class="style20">Revolution and it's equivalent in units of angle:
        <blockquote>
          <p class="style24"><img src="<?php echo base_url();?>assets/algebra/images/f2.7.jpg" width="175" height="22" /></p>
          <blockquote>
            <blockquote>
              <p class="style24"><img src="<?php echo base_url();?>assets/algebra/images/f2.8.jpg" width="95" height="22" /></p>
            </blockquote>
          </blockquote>
        </blockquote>
      </li>
      <li class="style20">Slope - Intercept Form:
        
        <blockquote class="style24">
            <p>y = mx + b </p>
        </blockquote>
      </li>
      <li class="style20">Slope:
        <blockquote>
            <p class="style24">m = y<sub>2</sub> - y<sub>1</sub>/ x<sub>2</sub> - x<sub>1</sub></p>
        </blockquote>
      </li>
      <li class="style20">Distance Formula:
        <blockquote>
            <p class="style24">d<sup>2</sup> = (x<sub>2</sub> - x<sub>1</sub>)<sup>2</sup> + (y<sub>2</sub> - y<sub>1</sub>)<sup>2</sup></p>
        </blockquote>
      </li>
      <li class="style20">General Form Quadratic:
        <blockquote>
            <p class="style24">ax<sup>2</sup> + bx + c = 0 or f(x) = ax<sup>2</sup> + bx + c; f(x) = y </p>
        </blockquote>
      </li>
      <li class="style20">Quadratic Formula:
        <blockquote>
            <p><img src="<?php echo base_url();?>assets/algebra/images/f2.2.jpg" width="150" height="32" /></p>
        </blockquote>
      </li>
      <li class="style20">Difference of Squares:
        <blockquote>
          <p class="style24">(x<sup>2</sup> - y<sup>2</sup>) = (x - y)(x + y) </p>
        </blockquote>
      </li>
      <li class="style20">Sum of Squares:
        <blockquote>
            <p class="style24">x<sup>2</sup> + y<sup>2</sup> ; prime </p>
        </blockquote>
      </li>
      <li class="style20">Perfect Square Trinomial:
        <blockquote>
            <p class="style24">x<sup>2</sup> + 2xy + y<sup>2</sup> = (x + y)<sup>2</sup></p>
          <p class="style24">x<sup>2</sup> - 2xy + y<sup>2</sup> = (x - y)<sup>2</sup></p>
        </blockquote>
      </li>
      <li class="style20">Difference of Cubes:
        <blockquote>
            <p class="style24">x<sup>3</sup> - y<sup>3</sup> = (x - y) (x<sup>2</sup> + xy + y<sup>2</sup>) </p>
        </blockquote>
      </li>
    </ul>
    <p class="style23">&nbsp;</p>
    <ul>
      <li><span class="style20">Exponent Properties</span>      
        <blockquote>
          <p><span class="style31">x<sup>0</sup> = 1 </span></p>
          <p class="style24"><span class="style20">0<sup>0</sup> = 1</span></p>
          <p class="style24"><span class="style20">x<sup>-n</sup> = 1/x<sup>n</sup></span></p>
        </blockquote>
        </li>
      <li><span class="style23">Logarithmic Properties</span>
        <blockquote>
          <p><span class="style23"><strong>y = log<sub>b</sub>x = b<sup>y</sup> = x </strong></span></p>
          <p class="style24"><span class="style20"><strong>log<sub>b</sub>1 = 0 </strong></span></p>
          <p class="style24"><span class="style20"><strong>log<sub>b</sub>0 = undefined </strong></span></p>
          <p><span class="style23">log<sub>b</sub>b<sup>x</sup> = x </span></p>
        </blockquote>
      </li>
    
      <blockquote>.</blockquote>
    </ul>
    <!-- <blockquote>
      <div align="right"><img src="<?php echo base_url();?>assets/algebra/images/next.jpg" width="84" height="47" /></div>
    </blockquote> -->
  </div>