<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (26)</h2>
    <p align="center">&nbsp;</p>
    <p>The population of the Bahamas in 2000 was estimated at 300,000 with an annual rate of increase of 2%.</p>
    <p>a. Find the mathematical model that relates the population of the Bahamas as a function of the number of years after 2000.</p>
    <p>b. If the annual rate of increase remains the same, use this model to predict the population of the Bahamas in the year 2010. </p>
    <p>&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify"><span class="style13">a. The initial population is Po = 300,000 and the rates of increase is r = 2%</span></p>
      <blockquote>
        <p align="justify" class="style13">P(t) = Po (1 + r)<sup>t</sup></p>
        <p align="justify" class="style13">= 300,000 (1 + 0.02)<sup>t</sup></p>
        <p align="justify" class="style13">= 300,000 (1.02)<sup>t</sup></p>
        <p class="style13">Hence t = 0 corresponds to the year 2000</p>
      </blockquote>
      <p class="style13">&nbsp;</p>
      <p class="style13">b. Because the initial population (t = 0) corresponds to the year 2000, we use t = 10 to find the population in the year 2010.</p>
      <blockquote>
        <p class="style13">P(10) = 300,000(1.02)<sup>10</sup></p>
        <p class="style13">= 366,000</p>
      </blockquote>
      <p>&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
  </div>