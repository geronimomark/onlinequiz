<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(2)</h2>
    <p align="justify">The world wide carbon dioxide (CO<sub>2</sub>) emissions have increased from 14 billion tons in 1970 to 24 billion tons in 1995. </p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/Given 2.jpg" width="206" height="190" /></p>
    <p>a. Find and interpret the slope of the lline in the accompanying figure.</p>
    <p>b. Predict the amount of worldwide CO<sub>2</sub> emission in 2005. </p>
    <p align="left">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <p>&nbsp;</p>
    <blockquote>
      <p class="style8">a. Find the slope of the line through (1970, 14) and (1995, 24):</p>
      <blockquote>
        <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 2.1.jpg" width="168" height="30" /> </p>
        <p class="style8">&nbsp;</p>
      </blockquote>
      <p class="style9">The slope of the line is 0.4 billion tons per year </p>
      <p class="style9">&nbsp;</p>
      <p align="justify" class="style9">b. If the (CO<sub>2</sub> ) emissions keep increasing at 0.4 billion tons per year, then in 10 years the level will go up to 10(0.4) or 4 billion tons. So in 2005 CO<sub>2</sub> emissions will be 28 billion tons. </p>
      <p align="justify" class="style9">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
  </div>