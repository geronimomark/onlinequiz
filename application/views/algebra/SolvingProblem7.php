<div class="style1" id="content">
    <h2 align="center" class="style13">algebra</h2>
    <h2 align="center">Practice problem (7) </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">In a club of 40 executives,  33 like to smoke marlboro and 20 like to       smoke philip morris. How many like to smoke both cigarettes? </p>
    <p align="justify">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style12">Let x = number of executives who smoke both brands. </p>
      <p align="center" class="style12"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 7.1.jpg" width="215" height="154" /></p>
    </blockquote>
    <blockquote>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;</p>
      <p>&nbsp;&nbsp;<span class="style12">&nbsp; (33 - x) + x + (20-x) = 40</span></p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;Answer:&nbsp;  &nbsp;      &nbsp;  &nbsp;                &nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; x = 13 &nbsp;&nbsp;&nbsp;&nbsp;</p>
      <p class="style12">&nbsp;</p>
    </blockquote>
    </div>