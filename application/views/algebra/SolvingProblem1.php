<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (1) </h2>
    <p align="justify">Volume of Rectangular Solid. Mille has just completed pouring 14 cubic yards of concrete to construct a rectangular driveway. If the concrete is 4 inches thick and the driveway is 18 feet wide, then how long is her driveway.</p>
    <p align="left">&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p class="style8">Draw the Diagram </p>
      <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/Solution 1.1.jpg" width="219" height="147" /></strong></p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 1.2.jpg" width="177" height="30" /></p>
        <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 1.3.jpg" width="203" height="30" /></p>
        <p class="style9">Replace W, H, and V by the appropriate values: </p>
        <p class="style9"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 1.4.jpg" width="77" height="15" /></p>
        <p class="style9"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 1.5.jpg" width="92" height="30" /></p>
        <p class="style9"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 1.6.jpg" width="45" height="15" /></p>
        <p class="style9">The length of the driveway is 21 yards</p>
      </blockquote>
    </blockquote>
    <p class="style9">&nbsp;</p>
  </div>