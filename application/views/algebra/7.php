<div class="style1" id="content">
    <h2 align="center" class="style18">algebra</h2>
    <h2 align="center">Quadratic EquationS</h2>
    <p class="style19">QUADRATIC FORMULA </p>
    <ul>
      <li><span class="style20">If ax<sup>2</sup> + bx + c = 0, where a, b, and c are real numbers with a is not equal to zero, then the solution of the equation are: </span>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/algebra/images/quadratic formula.jpg" width="140" height="32" /></p>
        </blockquote>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="28" height="15" /><strong>Example:</strong> Determine the value of m so that the following quadratic equation will have one repeated rational solution: mx<sup>2</sup> + 2x - 6 = 0 </p>
    <blockquote>
      <p class="style20">Solution:</p>
      <p class="style20">For the equation to have one repeated rational solution the discriminant must be zero.</p>
      <p class="style20">Thus: b<sup>2</sup> - 4ac = 0</p>
      <blockquote>
        <p class="style20">2<sup>2</sup> - 4(m)(-6) = 0</p>
        <p class="style20">4 - (-24m) = 0</p>
        <p class="style20">4 + 24m = 0</p>
        <p class="style20">24m = -4</p>
        <p class="style20">m = -4/24</p>
        <p class="style20">m = -1/6     </p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
        <p class="style20">&nbsp;</p>
      </blockquote>
    </blockquote>
  </div>