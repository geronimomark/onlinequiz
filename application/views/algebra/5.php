<div class="style1" id="content">
    <h2 align="center" class="style4">algebra</h2>
    <h2 align="center">Functions and their graphs </h2>
    <ul>
      <li>A<span class="style2"> function</span> associate every number in some set of real numbers, called the<span class="style2"> domain </span>of the function, with exactly one real number.
        <ul>
          <li>The function ussually denote by letters such as f, g, and h. If f is a function and x is a number of the domain of f, then the number that f associates with x denoted by f(x) and is called the value of<span class="style2"> f at x</span>. </li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="28" height="14" /><strong>Examples:</strong></p>
    <p>Suppose the function f is define by the formula f(x) = x<sup>2</sup> for every real number x. Evaluate each of the following:</p>
    <p>a. f(3) </p>
    <p>b. f(-1/2)</p>
    <p>c. f(1 + t)</p>
    <blockquote>
      <p class="style3">Solution:  </p>
      <p class="style3">a. f(3) = 3<sup>2</sup> = 9</p>
      <p class="style3">b. f(-1/2) = (-1/2)<sup>2</sup> = 1/4</p>
      <p class="style3">c. f(1+ t) = (1+t)<sup>2</sup> = 1 + 2t + t<sup>2</sup></p>
    </blockquote>
    <ul>
      <li>A function need not be defined by a single algebraic expression, as shown by the following example. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="28" height="14" /><strong> Example:</strong> </p>
    <p>The U.S. 2010 federal income tax for a single person with taxable income x dollars (this is the net income after allowable deduction and exemptions) is g(x) dollars, where g is the function defined by the federal law as follows:</p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/function 1.1.jpg" width="320" height="128" /></p>
    <p align="left"> What is the 2010 federal income tax for single person whose taxable income that year was $20,000?</p>
    <blockquote>
      <p align="left" class="style3">Solution:</p>
      <p align="left" class="style3">Because 20,000 is between 8,375 and 3,4000, use the second line of the definition of g:</p>
      <p align="left" class="style3">g(20,000) = 0.15 x 20,000 - 418.75 = 2,581. 25</p>
      <p align="left" class="style3">&nbsp;</p>
    </blockquote>
    <ul>
      <li>A function f can be visualized as a machine that takes an input x and produces the output f(x).  </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/function 1.2.jpg" width="219" height="85" /></p>
    <ul>
      <li>The graph of the function is the set of points of the form (x, f(x)) as x varies over the domain f. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="28" height="14" /><strong>Example:</strong></p>
    <p>Suppose f is the function with domain [0, 3] defined by f(x) = x<sup>2</sup> - 2x + 3. Sketch the graph of f.</p>
    <blockquote>
      <p class="style3">Solution: </p>
      <p class="style3">The graph of f is the set of points (x, y) in the plane such that x is in the interval [0, 3] and </p>
      <p class="style3">y = x<sup>2</sup> - 2x + 3</p>
      <p class="style3">To sketch the parabola defined by the equation above, we complete the square:</p>
      <p class="style3">y = x<sup>2</sup> - 2x + 3</p>
      <p class="style3">= (x - 1)<sup>2</sup> -1 + 3</p>
      <p align="justify" class="style3">= (x - 1)<sup>2</sup> + 2</p>
      <p align="justify" class="style3">Thus this parabola has a vertex at point (1, 2). We also see that if x = 0, then y = 3, producing the point (0, 3), and if x = 3 then y = 6, producing the point (3, 6) This information lead the graph shown below:   </p>
      <p align="center" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/function 1.3.jpg" width="201" height="165" /></p>
    </blockquote>
  </div>