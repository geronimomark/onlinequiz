<div class="style1" id="content">
    <h2 align="center" class="style13">algebra</h2>
    <h2 align="center">Practice problem (4) </h2>
    <p align="justify">Simplify the expression i1997 + i1999, where i is an imaginary number</p>
    <p align="left">&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p class="style12"><em><strong>i<sup>1997</sup>&nbsp;&nbsp; -----&gt; 1996 is multiple of 4 (i<sup>4</sup>) whose value is 1</strong></em></p>
      <p class="style12"><em><strong>i<sup>1997</sup>&nbsp; = &nbsp; i<sup>1996</sup> - i</strong></em></p>
      <p class="style12"><em><strong> i<sup>1997</sup> = i </strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp; Analyze i<sup>1999</sup></strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i<sup>1999</sup> = i<sup>1996</sup>-i<sup>3</sup></strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i<sup>1997</sup>= 1 - i<sup>3</sup></strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp; i<sup>1997</sup> = i<sup>3</sup> </strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;i<sup>1999</sup> = i<sup>2</sup> - i &nbsp;&nbsp; </strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;i<sup>1999</sup> = (-1)i</strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i<sup>1999</sup> = i<sup>2</sup> - i</strong></em></p>
      <p class="style12"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp; Adding the two:</strong></em></p>
      <p>&nbsp;  &nbsp;  &nbsp;<span class="style12"> &nbsp;  &nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; i<sup>1997</sup>+ i<sup>1999</sup> = i - 1</span></p>
      <p class="style12">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i<sup>1997</sup>+ i<sup>1999</sup> = 0</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Answer: 0 </p>
      <p class="style12">&nbsp;</p>
    </blockquote>
    </div>