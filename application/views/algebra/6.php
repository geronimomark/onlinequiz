<div class="style1" id="content">
    <h2 align="center" class="style8">algebra</h2>
    <h2 align="center">LINear equations</h2>
    <ul>
      <li>
        <div align="justify"><strong>Types of Equations:</strong>
            <ul>
              <li>An<span class="style4"> identity</span> is an equation that is satisfied by every number for which both sides are defined. </li>
              <li>A <span class="style4">conditional equation</span> is an equation that is satisfied by at least one number but is not an identity.</li>
              <li>An<span class="style4"> inconsistent equation</span> is an equation whose solution set is the empty set. </li>
            </ul>
        </div>
        <ul>
        </ul>
        <img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="24" height="16" /><strong>Example:</strong></li>
    </ul>
    <blockquote>
      <p class="style3">1. Solve the equation <img src="<?php echo base_url();?>assets/algebra/images/linear equations.jpg" width="113" height="30" /></p>
      <p class="style3">Solution:</p>
      <p class="style3">First multiply each side of the equation by 10, the LCD for 2, 5, and 10. </p>
      <blockquote>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear equations.jpg" alt="1" width="113" height="30" /> </p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear 2.jpg" width="117" height="18" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear 3.jpg" width="117" height="18" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear 4.jpg" width="58" height="18" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear 5.jpg" width="40" height="18" /></p>
      </blockquote>
    </blockquote>
    <p class="style6">&nbsp;</p>
    <p class="style6"><u>APPLICATIONS</u>.</p>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="2" width="24" height="16" /> Example: Completing High School - </strong>In 1940 only 24% of persons 25 years and over had completed 4 years of high school or more, but that percentage has been growing steadily shown in figure below. The expression 1.05n + 24 gives the percentage of persons 25 and over who jave completed 4 years of high school in the year 1940 + n.</p>
    <p>a. What was the percentage in 1998?</p>
    <p>b. When will the percentage reach 94%? </p>
    <p align="justify">&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/graph linear applications.jpg" width="308" height="213" /></p>
    <blockquote>
      <p class="style3">Solution:</p>
      <p class="style3">a. Since 1998 is 58 years after 1940, n = 58 </p>
      <blockquote>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear example 2.jpg" width="160" height="16" />	</p>
        <p class="style3">In 1998 approximately 85% of persons 25 and over had completed 4 years of high school or more</p>
      </blockquote>
      <p class="style3">b. To find when the percentage will reach 94% solve the following equation:</p>
      <blockquote>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear ex. 2.jpg" width="107" height="18" />  </p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear ex 2.4.jpg" width="80" height="18" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/linear ex 2.5.jpg" width="110" height="35" /></p>
        <p class="style3">In 2007 (1940 + 67) the percentage will reach 94%</p>
      </blockquote>
    </blockquote>
    <p class="style3">&nbsp; </p>
    <blockquote>
      <p class="style7"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" /> Exercise:</p>
      <p class="style3">Teacher's average salary. The expression 553.7x + 27,966 can be used to approximate the average annual salary in dollars of public school teachers in the year 1985 + x</p>
      <blockquote>
        <p class="style3">a. What was the average teacher's salary in 1993</p>
        <p class="style3">b. In which year will the average salary reach $40,000?  </p>
      </blockquote>
    </blockquote>
    <p class="style6"><u>FORMULAS</u></p>
    <ul>
      <li>A real-life problem may involve many variable quantities that are related to each other. The relationship between these variables may be expressed by a formula.</li>
    </ul>
    <ul>
      <li>A <span class="style4">formula</span> or <span class="style4">literal equation </span>is an equation involving two or more variables  </li>
    </ul>
    <ul>
      <li><span class="style4">Equation </span>is a mathematical statement stating that two quantities are equal.</li>
    </ul>
    <blockquote>
      <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" /> Example: </p>
      <blockquote class="style3">
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Arec = LW,&nbsp; C = 2&nbsp;&#1055;r, &nbsp; 3 + x = 5 </p>
      </blockquote>
    </blockquote>
    <ul>
      <li><span class="style4">Members of the equation</span> - mark as equals or equal to
        equations are composed of constants and unknowns or variables </li>
    </ul>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" /> Example:</strong> Solve the formula C = 5/9 (F - 32) for F.</p>
    <blockquote>
      <p class="style3">Solution:</p>
      <p align="justify" class="style3">To solve he formula for F, isolate F on one side of the equation. Eliminate both 9 and the 5 from the right hamd side of the equation by multiplying by 9/5. the reciprocal of 5/9. </p>
      <blockquote>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/formula.jpg" width="109" height="30" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/formula ex 1.jpg" width="158" height="30" /></p>
        <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/formula ex 1.1.jpg" width="106" height="30" /></p>
      </blockquote>
    </blockquote>
    <p class="style6"><u>LINEAR EQUATIONS USING SUBSTITUTION</u></p>
    <p>Method In Using Substitution for Linear Equations </p>
    <ol>
      <li>Isolate one variables in one of the equations.</li>
      <li>Substitute the equation in step 1 to the other variable</li>
      <li>Solve the new equation.</li>
      <li>Using one equations containing both variables, substitute   the value found in step 3 for that variable and solve for the value of   other variable.</li>
      <li>Check the solution in the original equations.</li>
    </ol>
    <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" />&nbsp; <strong>Example:</strong></p>
    <p>Solve the equation using substitution</p>
    <p>x + y = 8&nbsp;&nbsp;&nbsp; -------------------------&gt; Eq. (1) </p>
    <p>2x - 4y = 10&nbsp;------------------------&gt; Eq. (2)</p>
    <blockquote>
      <p class="style3">Solution</p>
      <p class="style3">Step 1: Isolate the variable in one equation then solved for x.</p>
      <p class="style3">Step 2: Substitute x = 8 - y in eq. (2)</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 (8-y)&nbsp; - 4y = 10</p>
      <p class="style3">Step 3: Solve for y.</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 16 - 2y - 4y = 10</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 16 - 6y = 10</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -6y = 10 -16</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = -6/-6 = 1</p>
      <p class="style3">Step 4: Solve for x by    substituting the value of y in x = 8 - y.</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; x = 8 - 1 = 7 </p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;The answer is (7, 1)</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Check the answer, by substituting the value of x and y to the eq. (1) and (2). </p>
      <blockquote class="style3">
        <blockquote>
          <p class="style3">x + y = 8&nbsp;&nbsp;&nbsp; ; 7 +1 = 8 </p>
          <p class="style3">2x - 4y = 10&nbsp;&nbsp;&nbsp; ; 2(7) - 4(1) = 10 </p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p class="style3">&nbsp;</p>
    <p class="style6"><u>SOLVING WORD PROBLEM USING LINEAR EQUATION</u> </p>
    <p>Steps in Solving Word Problem Using Linear Equations </p>
    <ol>
      <li>Select a variable for each of the unknown.</li>
      <li>Translate each relationship to an equation.</li>
      <li>Solve the system.</li>
    </ol>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" />&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;Example:</strong></p>
    <p>Terry is designing a table so that the length is twice the   width. The perimeter is to be 216 inches. Find the length and width of   the table. </p>
    <blockquote>
      <p class="style3">Solution: </p>
      <p class="style3">&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp; (1.1) <em>You need to understand the problem and plan for the solution</em> </p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (1.2) Let l represent the length and w the width. </p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (1.3) The length is twice the width l = 2w</p>
      <p class="style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (1.4) Perimeter is 216 inches ; 2l + 2w = 216</p>
      <blockquote>
        <blockquote>
          <blockquote>
            <p class="style3">l = 2w ; 2l + 2w =216</p>
            <p class="style3">Substitute l</p>
            <p class="style3">2(2w) + 2w = 216</p>
            <p class="style3">4w + 2w = 216</p>
            <p class="style3">6w = 216</p>
            <p class="style3">w = 36</p>
            <p class="style3">l = 2(36) = 72 </p>
            <p class="style3">Answer: w = 36 inches ; l = 72 inches </p>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p class="style6">&nbsp;</p>
    <p class="style6"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="16" /> Exercise:</p>
    <p align="justify" class="style3">At football game, Lina buys one hamburger, two popcorns and two   softdrinks, all for $12.00. Dina buys two hamburgers, three popcorns,   and one softdrink for $ 17.00. Andrew buys one popcorn, one hamburger,   and three softdrink $11.00. Find the individual cost of one popcorn, one   drink and one hamburger.</p>
    <h2>&nbsp;</h2>
  </div>