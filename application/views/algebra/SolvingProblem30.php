<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (30)</h2>
    <p align="center">&nbsp;</p>
    <p>Given that &quot;w&quot; varies directly as the product of &quot;x&quot; and &quot;y&quot; and inversely as the square of &quot;z&quot; and that w = 4 when x = 2, y = 6 and z = 3. Find the value of &quot;w&quot; when x = 1, y = 4, and z = 2.</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.1.jpg" width="65" height="36" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.2.jpg" width="250" height="18" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.3.jpg" width="100" height="40" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.4.jpg" width="35" height="20" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.5.jpg" width="217" height="18" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/30.6.jpg" width="140" height="40" /></p>
      <blockquote>
      <p align="left">&nbsp;</p>
        <p align="left">&nbsp;</p>
      </blockquote>
    </blockquote>
  </div>