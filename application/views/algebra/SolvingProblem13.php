<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(13)</h2>
    <p align="justify">Charcoal from an ancient tree that was burned during a volcanic eruption has only 45% of the standard amount of carbon 14. When did the volcano erupt? </p>
    <p align="left">&nbsp;</p>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <div align="justify">
      <blockquote class="style11"> A(t) = A<sub>0</sub> (1/2)<sup>t/5700</sup> and A(t) = 0.45A<sub>0</sub> </blockquote>
    </div>
    <blockquote>
      <p align="justify"><span class="style11">A(t) = </span><span class="style11">A<sub>0</sub> (1/2)<sup>t/5700</sup> </span></p>
      <p align="justify"><span class="style11">A(t) = </span><span class="style11">0.45A<sub>0</sub></span></p>
      <p align="justify"><span class="style11">A<sub>0</sub> (1/2)<sup>t/5700</sup> = 0.45A<sub>0</sub></span></p>
      <p align="justify"><span class="style11"> (1/2)<sup>t/5700</sup></span> <span class="style11">= 0.45</span> </p>
      <p align="justify"><span class="style11">ln((1/2)<sup>t/5700</sup>)</span> <span class="style11">= ln(0.45)</span></p>
      <p align="justify" class="style11">t/5700ln(1/2) = ln0.45</p>
      <p align="justify" class="style11">t = 5700 (ln 0.45/ln 0.5)</p>
      <p align="justify" class="style11">t = 6566   </p>
      <p align="justify">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
  </div>