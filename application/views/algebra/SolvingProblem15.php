<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(15)</h2>
    <p align="center">&nbsp;</p>
    <p align="justify">Find the smallest number such that the total number of grains of rice on the first n squares of the chessboard is more than 30,000,000    </p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <blockquote>
      <p align="justify" class="style11">Solution:</p>
      <blockquote class="style11">
        <p align="justify">The formula of geometric series shows that the total number of grains of rice on the first n squares of the chessboard is 2<sup>n</sup> - 1. Thus we want the smallest integer n such that: </p>
        <blockquote>
          <p align="justify" class="style10 style11">2<sup>n</sup> &gt; 30000001</p>
        </blockquote>
        <p align="justify" class="style10 style11">Taking logs of both sides, we see that we need</p>
        <blockquote>
          <p align="justify" class="style10"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/15.1.jpg" width="165" height="32" /> </p>
          <p align="justify" class="style10">&nbsp;</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
    <p align="left" class="style10">&nbsp;</p>
  </div>