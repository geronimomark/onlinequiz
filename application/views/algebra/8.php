<div class="style1" id="content">
    <h2 align="center" class="style6">algebra</h2>
    <h2 align="center">Permutations and combinations</h2>
    <ul>
      <li>An arrangement where order is important is called <span class="style3">permutation</span>. </li>
      <li>An arrangement where order is not important is called <span class="style3">combination</span>.
        <ul>
          <li> A seating arrangement is an example of <span class="style3">permutation</span> because the arrangement of the &quot;n&quot; object is a specific order. The order is important in permutation. </li>
          <li>When the order does not matter, it is a<span class="style3"> combination</span>, because you are only interested in the group. </li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="14" /><strong>Examples:</strong></p>
    <p><strong>Permutation</strong></p>
    <p>1. Twelve people need to be photographed, but there are only five chairs. How many ways can you sit the twelve people on the five chair? </p>
    <blockquote>
      <p class="style4">Solution:</p>
      <p class="style4">The rest of the people will be standing behind and their order does not matter</p>
      <p class="style4">12 x 11 x 10 x 9 x 8 = 95040 ways</p>
      <p class="style4">&nbsp;</p>
    </blockquote>
    <p class="style4">2. Mario, Sandy, Fred, and Shanna are running for the offices of president, secretary, and treasurer. In how many ways can these offices be filled?</p>
    <blockquote>
      <p class="style4">Solution:</p>
      <p class="style4">4 x 3 x 2 = 24</p>
      <p class="style4">The offices can be filled in 24 ways</p>
      <p class="style4">&nbsp;</p>
    </blockquote>
    <p class="style5">Combination</p>
    <p>3.Charles has four coins in his pocket and pulls out three at one time. How many different can he get?</p>
    <blockquote>
      <p class="style4">Solution:   </p>
      <p class="style4"><img src="<?php echo base_url();?>assets/algebra/images/combination.jpg" width="238" height="32" /></p>
    </blockquote>
    <p class="style5">Permutation and Combination</p>
    <p>Determine if the situation represents a permutation or combination </p>
    <p class="style4">4.   In how many ways can five books be arranged on a book of shelf in the library?</p>
    <blockquote>
      <p class="style4">Answer: Permutation</p>
    </blockquote>
    <p class="style4">5. In how many ways can three student-council members be elected from five candidates?</p>
    <blockquote>
      <p class="style4">Answer: Combination</p>
    </blockquote>
    <p class="style4">6. Seven students line up to sharpen their pencils.</p>
    <blockquote>
      <p class="style4">Answer: Permutation</p>
    </blockquote>
    <p class="style4">7. A DJ will play three CD choices from the 5 requests.</p>
    <blockquote>
      <p class="style4">Answer: Combination     </p>
    </blockquote>
  </div>