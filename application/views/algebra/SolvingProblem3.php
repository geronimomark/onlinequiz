<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">Practice problem (3) </h2>
    <p align="justify">The root of quadratic equation are 1/3 and 1/4. What is the equation? </p>
    <p align="left">&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/Solution 3.1.jpg" width="126" height="33" /></p>
      <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 3.2.jpg" width="140" height="33" /></p>
      <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 3.3.jpg" width="161" height="33" /></p>
      <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 3.4.jpg" width="181" height="16" /></p>
      <p class="style8">&nbsp;</p>
      <p class="style8"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/solution 3.5.jpg" width="117" height="16" /></p>
      <p class="style8">&nbsp;</p>
      <p class="style8">&nbsp;</p>
      <p class="style8">&nbsp;</p>
      <blockquote>
        <p class="style9">&nbsp;</p>
      </blockquote>
    </blockquote>
    </div>