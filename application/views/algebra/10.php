<div class="style1" id="content">
    <h2 align="center" class="style20">algebra</h2>
    <h2 align="center">matrices</h2>
    <ul>
      <li>An n x m matrix is a <span class="style18">rectangular array</span> of numbers with m rows and n columns. </li>
      <li>An n x n matrix is a <span class="style18">square matrix</span>.</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="17" /><strong>Example:</strong></p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/matrix.jpg" width="498" height="90" /></p>
    <ul>
      <ul>
        <li>The matrix A is a 2x2 matrix; It is a square. Write as A<sub>2x2</sub></li>
        <li>The matrix B is a 2x3 matrix: which could denote  B <sub>2x3</sub></li>
        <li>The matrix C <sub>mxn</sub> is a generix matrix: it is m x n, and the symbol is C <sub>ij</sub> represent the entry in row i and column j.</li>
      </ul>
      <p>&nbsp;</p>
      <li>Matrix Operations:</li>
    </ul>
    <ol>
      <li><span class="style18">Matrix Addition: </span>If A<sub>m x n</sub> = [aij] and B<sub>m x n</sub> = [b<sub>ij</sub>], then </li>
    </ol>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /> Example: A= [4, 2, 6] ; B = [1, 7, 2]</p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p class="style19">A + B = [4+1 2+7 6+2]</p>
      <p class="style19">=[5 9 8]   </p>
    </blockquote>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><strong>Example: </strong>Let  <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.1.jpg" width="102" height="49" /> and <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.2.jpg" width="102" height="49" /></p>
    <blockquote>
      <p class="style19">Solution:</p>
      <blockquote>
        <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/matrix 1.3.jpg" width="268" height="49" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/matrix 1.4.jpg" width="188" height="49" /></p>
      </blockquote>
    </blockquote>
    <p class="style19">2. <span class="style18">Matrix Subtraction:</span>  If A<sub>m x n</sub> = [a<sub>ij</sub>] and B<sub>m x n</sub> = [b<sub>ij</sub>]        </p>
    <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><strong>Example:</strong> A = [4 2 6] B = [1 7 2]</p>
    <blockquote>
      <p class="style19">Solution:</p>
    </blockquote>
    <ul>
      <blockquote>
        <p class="style19">A-B = [4-1 2-7 6-2]</p>
        <p class="style19">= [3 -5 4]</p>
      </blockquote>
    </ul>
    <p class="style19">3.<span class="style18"> Inverse Matrix</span>: If A is a square matrix and if there exists a matrix A-1 such that A-1 A = AA-1  = 1, </p>
    <p align="left">then A-1&nbsp; is called the inverse of A for multiplication.</p>
    <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><strong> Example:</strong> </p>
    <ul>
      <blockquote>
        <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/matrix 1.13.jpg" width="186" height="39" /></p>
      </blockquote>
    </ul>
    <blockquote>
      <p class="style19">Solution:</p>
    </blockquote>
    <ul>
      <blockquote>
        <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/matrix 1.14.jpg" width="302" height="39" /></p>
      </blockquote>
      <li>Let A be an m matrix, and let r <img src="<?php echo base_url();?>assets/algebra/images/belong.jpg" width="21" height="20" />R. The matrix rA is the matrix obtained by multiplying each entry of A by r.</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><strong>Example:</strong> <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.6.jpg" width="80" height="49" />and r = -2, we get</p>
    <blockquote>
      <p class="style19">Solution:</p>
      <blockquote>
        <p> <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.7.jpg" width="229" height="49" /></p>
      </blockquote>
    </blockquote>
    <ul>
      <li>Let A = [a<sub>ij</sub>] be an m x n matrix and let B = [b<sub>ij</sub>] be an n x r matrix. The product AB of A and B is the m x r matrix C = [C<sub>ij</sub>], where C<sub>ij</sub> = a<sub>i1</sub>b1j + a<sub>i2</sub>b<sub>2j</sub> + a<sub>i3</sub>b<sub>3j</sub> + ... + a<sub>in</sub>b<sub>nj</sub> </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="17" /><strong>Example:</strong> Let <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.8.jpg" width="80" height="49" />and <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.10.jpg" width="102" height="49" /></p>
    <blockquote>
      <p class="style19">Solution:</p>
      <blockquote>
        <p class="style19">row 1 of A and column 1 of B: 1(2) + 2(-1) = 0</p>
        <p class="style19">row 1 of A and column 2 of B: 1(-3) + 2(2) = 1</p>
        <p class="style19">row 1 of A and column 3 of B: 1(1) + 2(4) = 9</p>
        <p class="style19">row 2 of A and column 1 of B: 5(2) + (-2)(-1)=12</p>
        <p class="style19">row 2 of A and column 2 of B: 5(-3) + (-2)(2) = -19</p>
        <p class="style19">row 2 of A and column 3 of B: 5(1) + (-2)(4) = -3</p>
        <p class="style19">Therefore the product <img src="<?php echo base_url();?>assets/algebra/images/matrix 1.11.jpg" width="155" height="39" />   </p>
      </blockquote>
    </blockquote>
  </div>