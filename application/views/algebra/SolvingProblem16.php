<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (16) </h2>
    <p align="center">&nbsp; </p>
    <p align="justify">The current i amperes flowing in a capacitor at time t seconds is given by i = 8.0 (1 - e<sup>-t/CR</sup>), where the circuit resistance R is 25 x 10<sup>3</sup> ohm and capacitance C is 16 x 10<sup>-6</sup> farads. Determine a.) the current i after 0.5 seconds and b.) the time to the nearest millisecond. </p>
    <blockquote>
      <blockquote><blockquote>&nbsp;</blockquote>
      </blockquote>
    </blockquote>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style11">a.) Current i = 8 (1 - e <sup>-t/CR</sup>)</p>
      <blockquote>
        <p class="style11">= 8 (1 - e<sup>0.5/(16x10<sup>-6</sup>)(25x10<sup>3</sup>)</sup>)</p>
        <p class="style11">= 8 (1 - e<sup>-1.25</sup>) </p>
        <p class="style11">= 5. 71 Amperes</p>
      </blockquote>
      <p class="style11">b.) Transposing i =  8 (1 - e <sup>-t/CR</sup>)</p>
      <blockquote>
        <p class="style11">i/8 = 1 - e <sup>-t/CR</sup></p>
        <p class="style11">e <sup>-t/CR</sup> = 8/8 - i</p>
        <p class="style11">t/CR = ln (8/8 - i)</p>
        <p class="style11">t = CR ln    (8/8 - i)</p>
        <p class="style11">t = (16x10-6)(25x103) ln (8/8 - 6)</p>
        <p class="style11">when i = 6 amperes </p>
        <p class="style11">t = 555 ms </p>
      </blockquote>
    </blockquote>
    </div>