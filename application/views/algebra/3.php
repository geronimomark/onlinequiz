<div class="style1" id="content">
    <h2 align="center" class="style17">algebra</h2>
    <h2 align="center">exponents and radicals </h2>
    <h2 align="center"></h2>
    <ul>
      <li><span class="style5">Like Radicals </span>- radicals w/ the same index and the same radicands.      </li>
      <li><span class="style5">Rationalizing the Denominators </span>- the process of rationalizing the denominators of radical expression without a radical. </li>
      <li><span class="style5">Rationalizing the Numerators</span> - the process of rationalizing the numerators of radical expression without a radical. </li>
    </ul>
    <p><strong>To solve radical equation : </strong></p>
    <ol>
      <li>Write the equations so that radical by itself on one side of the equation.</li>
      <li>Raise each side of the equation to a power equal to the index of the radical.</li>
      <li>Simplify Each side of the equation.</li>
      <li>Repeat step 1 through 3, if the equation still have radical.</li>
      <li>Solve the equation </li>
    </ol>
    <ul>
      <li><span class="style5">Complex Number</span> - the number that can be written in the form of&nbsp;a + bi where a and b are real numbers.
        <blockquote>
            <p class="style14">Example:</p>
          <p class="style14">i^2 = -1 and i = &radic;-1 </p>
        </blockquote>
      </li>
      <li><span class="style5">Conjugate</span> - the conjugate of a complex number a + bi is the complex number of a - bi.</li>
    </ul>
    <blockquote>&nbsp;</blockquote>
    <p><strong>Examples :</strong></p>
    <p class="style14">a. (-3 + 5i) + (5 - 10i) = 0 </p>
    <blockquote>
      <p class="style14">Solution:</p>
    </blockquote>
    <ul class="style14">
      <blockquote>
        <p class="style14">-3 + 5 + 5i - 10i = 0</p>
        <p class="style14">Answer: 2 - 5i </p>
      </blockquote>
    </ul>
    <p>b&nbsp; 6i (-5 + 2i) </p>
    <blockquote class="style14">
      <p>Solution:</p>
    </blockquote>
    <ul>
      <blockquote>
        <p class="style14">30i + 12i<sup>2</sup></p>
        <p class="style14">but i<sup>2</sup> = -1</p>
        <p class="style14">30i + 12(-1)</p>
        <p class="style14">Answer: 30i -12</p>
      </blockquote>
    </ul>
    <p><strong>Example:</strong> Solve the equation given below and write your answer in standard form:</p>
    <p>a. (5- i )/ (2 + 7i) = 0 </p>
    <p><em><strong>Solution: </strong></em></p>
    <blockquote>
      <p class="style14">(5-i)(2+7i)/(2+7i)(2+7i)&nbsp; = 0</p>
      <p class="style14">10-33i-7i<sup>2</sup> / 4+14i+49i<sup>2</sup> = 0</p>
      <p class="style14">10- 33i-7(-1) / 4+14i+49(-1) = 0</p>
      <p class="style14">17 - 33i / 45+14i = 0</p>
      <p class="style14">17 - 33i = 45 + 14i</p>
      <p class="style14">Answer: 28 + 47i = 0 </p>
    </blockquote>
    <ul>
      <li><strong>Various power of i: </strong></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/radicals.jpg" alt="1" width="317" height="183" /></p>
    <p align="left">&nbsp;</p>
    <ul>
      <li>The quantity of an is called the<span class="style5"> nth power of the base a</span> and n is called the<span class="style5"> exponent</span>.</li>
    </ul>
    <blockquote>
      <p class="style14">Law I. a<sup>m</sup>a<sup>n</sup> = a<sup>m+n</sup> </p>
      <p class="style14">Law II. a<sup>m</sup>/a<sup>n</sup> = a<sup>m-n</sup> </p>
      <p class="style14">Law III. (a<sup>m</sup>)<sup>n</sup> = a<sup>mn</sup></p>
      <p class="style14">Law IV. (ab)<sup>m</sup> = a<sup>m</sup>b<sup>m</sup></p>
    </blockquote>
    <blockquote>
      <p class="style14">Law V. (ab)<sup>m</sup> = a<sup>m</sup>b<sup>m</sup> </p>
    </blockquote>
    <p class="style15"><u>FRACTIONAL EXPONENTS</u> </p>
    <blockquote>
      <p class="style16">1. <img src="<?php echo base_url();?>assets/algebra/images/fraction exp 1.2.jpg" width="74" height="28" /></p>
      <p class="style16">2. <img src="<?php echo base_url();?>assets/algebra/images/fraction exp 1.1.jpg" width="147" height="28" /></p>
      <p class="style16">3. <img src="<?php echo base_url();?>assets/algebra/images/fraction exp 1.3.jpg" width="84" height="41" /></p>
    </blockquote>
    <p class="style15"><u>LAWS OF RADICALS</u></p>
    <blockquote>
      <p class="style16">1. <img src="<?php echo base_url();?>assets/algebra/images/radical 1.1.jpg" width="89" height="22" /></p>
      <p class="style16">2. <img src="<?php echo base_url();?>assets/algebra/images/radical 1.2.jpg" width="90" height="22" /></p>
      <p class="style16">3. <img src="<?php echo base_url();?>assets/algebra/images/radical 1.3.jpg" width="83" height="32" /></p>
      <p class="style16">4. <img src="<?php echo base_url();?>assets/algebra/images/radical 1.4.jpg" width="87" height="22" /></p>
    </blockquote>
    <p><span class="style16"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" /> Example: </span><span class="style14">Rationalize the denominator</span> <img src="<?php echo base_url();?>assets/algebra/images/radical ex 1.1.jpg" width="48" height="32" /></p>
    <blockquote>
      <p class="style14">Solution:</p>
      <p class="style14">Multiply the numerator and denominator by 4x and then apply law 3. </p>
      <p class="style14"><img src="<?php echo base_url();?>assets/algebra/images/radical ex 1.2.jpg" width="294" height="38" /></p>
    </blockquote>
    <p class="style14"><span class="style16"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /></span><strong>Example: </strong>Simplify the radical <img src="<?php echo base_url();?>assets/algebra/images/radical ex 2.1.jpg" width="41" height="34" /></p>
    <blockquote>
      <p class="style14"><img src="<?php echo base_url();?>assets/algebra/images/radical ex 2.2.jpg" width="342" height="38" /></p>
    </blockquote>
  </div>