<div class="style1" id="content">
    <h2 align="center" class="style16">algebra</h2>
    <h2 align="center">Sets and operations  </h2>
    <ul>
      <li><strong>Example</strong>:
        <p>Sets of books, set of chairs, set of foods</p>
      </li>
      <li>Each object of a set is called a <span class="style5">member </span>or <span class="style5">element </span>of the set.      </li>
    </ul>
    <ul>
      <li>Specifying sets with following notations:
        <ul>
          <li>
            <div align="justify">A = {a, e, i, o, u}</div>
          </li>
          <li>B = {1, 3, 5, .... , 49} -- <span class="style5"><em>the dot indicate that the missing odd integers are members of the set</em></span> </li>
          <li>C {x|x is a state of the United States}  --<span class="style5"><em> it is the set of states x such that x is a state of the United States </em></span> </li>
        </ul>
      </li>
      <li>If the elements of a set can be listed from the first element through the last element the set is said to be <span class="style13">finite</span>. A set which is not finite is said to be<span class="style13"> infinite</span>      
        <p><strong>Example:  </strong> Express the set of months in the year having 31 days by (a) the roster method, (b) the description method</p>
      </li>
      
      <blockquote>
        <p class="style14">Solution:</p>
        <p class="style14">{ Jan, Mar, May, Jul, Aug, Oct, Dec}</p>
        <p class="style14">{x|x is a month of the year having 31 days}           </p>
      </blockquote>
      <li>The symbol <img src="<?php echo base_url();?>assets/algebra/images/belong.jpg" width="21" height="25" />may read as &quot;a member of&quot; or &quot;belongs to&quot;. The symbol <img src="<?php echo base_url();?>assets/algebra/images/not belong.jpg" width="20" height="27" /> is read &quot;not a member of &quot;. </li>
    </ul>
    <p>&nbsp;</p>
    <p class="style15"><u>OPERATIONS OF SETS </u></p>
    <ul>
      <li>
        <div align="justify"><span class="style5">Universal Set</span> is the set consisting of the totality of elements under consideration of particular discussion. Commonly denoted as <span class="style5">U</span>. </div>
      </li>
      <li>
        <div align="justify">The set of all elements which  belong to a given universal set and do not belong to a given subset A of U is called <span class="style5">complement of A</span>. Denoted the complement of a set A by A' </div>
      </li>
      <li>
        <div align="justify">The<span class="style5"> union</span> of two sets A and B is defined to be composed of all elements which belong to A and B is defined to be composed of all elements which belong to A or to B or to both A and B. Denoted as A <img src="<?php echo base_url();?>assets/algebra/images/Union.jpg" width="24" height="23" />B.</div>
      </li>
      <li>
        <div align="justify">The<span class="style5"> intersection</span> of two sets A and B is the set of all elements that belongs to both A and to B. Denoted as A <img src="<?php echo base_url();?>assets/algebra/images/intersection.jpg" width="22" height="21" />B.</div>
      </li>
    </ul>
    <p class="style15"><u>VENN DIAGRAMS </u></p>
    <ul>
      <li>Certains operations on sets can be portrayed by means of geometric figures</li>
      <li>Created by English logician namely &quot;John Venn (1834 - 1883)</li>
    </ul>
  </div>