<div class="style1" id="content">
    <h2 align="center" class="style21">algebra</h2>
    <h2 align="center">OPERATION WITH ALGEBRAIC EXPRESSIONS </h2>
    <p class="style20"><u>ADDING AND SUBTRACTIONG ALGEBRAIC EXPRESSIONS </u></p>
    <ul>
      <li>Two or more terms that contain the same variable or variables with corresponding variables having the same exponents, are called<span class="style5"> like terms</span> or <span class="style5">similar terms</span>.</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="22" height="16" /><span class="style14"><strong>Example:</strong> 6k and 5k, 5x<sup>2</sup> and -7x<sup>2</sup>, 9ab and 0.4ab</span></p>
    </blockquote>
    <ul>
      <li>Two terms are unlike terms when they contain different variables, or the same variables with different exponents. </li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="16" /><span class="style14">Example: 3x and 4y, 5x<sup>2</sup> and 5x<sup>3</sup>, 9ab and 0.4a </span></p>
    </blockquote>
    <p>Recall that when like terms are added:</p>
    <ol>
      <li>The sum or difference has the same variable as the original terms.</li>
      <li>The numerical coefficients of the sum or difference is the sum or difference of the numerical coefficients of the terms that were added.
        <ul>
          <li>The sum of unlike terms cannot be expressed as a single term.   </li>
        </ul>
      </li>
    </ol>
    <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="16" /><strong>Example:</strong> </p>
    <p align="justify">An isosceles triangle has two sides that are equal in length. The length of each of the two equal sides of an isosceles triangle is twice the length of the third side of the trianlge. If the length of the third side is represented by n, represent in simplest form the perimeter of the triangle. </p>
    <blockquote>
      <p align="justify" class="style14">Solution:</p>
      <p align="justify" class="style14">n = length of the base</p>
      <p align="justify" class="style14">2n = length of one of the equal sides</p>
      <p align="justify" class="style14">2n = length of the other equal sides</p>
      <p align="justify" class="style14">Perimeter = n + 2n + 2n = (1 + 2 + 2)n = 5n   </p>
      <p align="justify" class="style14">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style20"><u>MONOMIALS AND POLYNOMIAL</u></p>
    <ul>
      <li>A term that has no variable in the denominator is called a <span class="style5">monomial</span>. Ex. 5, and -5w</li>
      <li>A monomial or the sum of monomials is called a <span class="style5">polynomial</span>.</li>
      <li>A polynomial of two unlike terms, such as 10a + 12b, is called a <span class="style5">binomial</span>.   </li>
    </ul>
    <p class="style20"><u>MULTIPLYING THE POWER THAT HAVE THE SAME BASE</u></p>
    <ul>
      <li>x<sup>a</sup> * x<sup>b</sup> = x<sup>a+b</sup></li>
      <li>(x<sup>a</sup>y<sup>b</sup>)<sup>c</sup> = x<sup>ac</sup>y<sup>bc</sup></li>
    </ul>
  </div>