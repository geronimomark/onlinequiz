<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (21) </h2>
    <p align="justify">A piece of timber 273 cm long is cut into three pieces in the ratio of 3 to 7 to 11. Determine the lengths of the three pieces. </p>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p><span class="style11">The total number of parts is 3 + 7 + 11, that is 21. Hence 21 parts correspond to 273 cm.</span></p>
      <p><span class="style11">1 part = 273/21 = 13 cm</span></p>
      <p class="style11">3 parts = 3 x 13 = 39 cm</p>
      <p class="style11">7 parts = 7 x 13 = 91 cm</p>
      <p class="style11">11 parts = 11 x 13 = 143 cm</p>
      <p class="style11">The lengths of the three pieces are 39 cm, 91 cm, and 143 cm.</p>
      <p class="style11">Check 39 + 91 + 143 = 273     </p>
      <p>&nbsp;</p>
      <p class="style11">&nbsp;</p>
    </blockquote>
    </div>