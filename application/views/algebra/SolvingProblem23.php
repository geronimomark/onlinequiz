<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (23) </h2>
    <p align="justify">Calculate the diameter of a solid cylinder which has a height of 82.0 cm and a total surface area of 2.0 m<sup>2</sup>. </p>
    <blockquote>
      <blockquote><blockquote>&nbsp;</blockquote>
      </blockquote>
    </blockquote>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style11">Total surface area of the cylinder = curve surface area + 2 circular ends</p>
      <blockquote>
        <p class="style11"> = 2&pi;rh + 2&pi;r<sup>2</sup>  </p>
        <p class="style11">2.0 = 2&pi;r(0.82) + 2&pi;r<sup>2</sup> </p>
        <p class="style11">Dividing throughout by 2&pi;</p>
        <p class="style11">r<sup>2</sup> + 0.82r - 1/<span class="style11">&pi;</span> = 0  </p>
        <p class="style11">Using quadratic formula:</p>
        <p class="style11">r = 0.2874 or -1.1074</p>
        <p class="style11">hence the diameter of the cylinder</p>
        <p class="style11">d = 2 x 0.2874 = 0.5748 cm   </p>
        <p class="style11">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&lrm;</p>
  </div>