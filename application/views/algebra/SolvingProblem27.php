<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (27)</h2>
    <p align="center">&nbsp;</p>
    <p>A survey of 100 persons revealed that 72 of them had eaten at restaurants P and that 52 of them had eaten at restaurant Q. Which of the following could not be the number of persons in the surveyed group who had eaten at both P and Q?</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/27.1.jpg" width="174" height="109" /></p>
    <blockquote>
      <p align="justify" class="style13">Let x = number of persons who have eaten in both restaurants</p>
      <p align="justify" class="style13">(72 - x) + x + (52 - x) = 100</p>
      <p align="justify" class="style13">72 + 52 - x = 100</p>
      <p align="justify" class="style13">x = 24 persons</p>
      <p align="justify" class="style13">&nbsp;</p>
      <p align="justify" class="style13">&nbsp;</p>
      <p align="justify" class="style13">&nbsp;</p>
      <p align="justify" class="style13">&nbsp;</p>
      <p align="justify"><img src="../img/arrow.jpg" alt="1" width="144" height="19" />    </p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
  </div>