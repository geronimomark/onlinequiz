<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(9)</h2>
    <p align="justify">The denominator of a certain  fraction is three more than twice the     numerator. If three more than twice the denominator. If 7 added to both     terms of the fraction, the resulting fraction i 3/5. Find the original     fraction.</p>
    <p align="left">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;<em><strong>&nbsp;&nbsp;&nbsp;x = numerator of the fraction</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = denominator of the fraction</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The denominator is three more than twice the numerator</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = 2x + 3&nbsp;&nbsp; ---&gt; (1) </strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If 7 is added to both terms of the fraction the resulting fraction is 3/5</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; x+7/y+7 = 3/5</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5x + 35 = 3y + 21 ---&gt; (2)</strong></em></p>
    <blockquote>
      <p class="style11">Substitute (1) in (2)</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="style11">&nbsp;&nbsp; 5x + 35 = 3(2x+3) + 21</span></p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5x + 35 = 6x + 9 + 21</p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 35 - 30 = 6x - 5x</p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5 = x</p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Solving for y by substituting the value of x in (1)</p>
      <p class="style11">&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = 2(5) + 3</p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = 13</p>
      <p class="style11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; answer: 5/13 </p>
      <p class="style11">&nbsp;</p>
    </blockquote>
    </div>