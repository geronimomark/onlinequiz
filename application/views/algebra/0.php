  <div class="style1" id="content">
    <h2 align="center" class="style13">algebra</h2>
    <h2 align="center">Variable, equations and inequalities </h2>
    <ul>
      <li>In mathematics, a sentence that contains an equal sign, =, is called an equation. Some equation contain only numbers.</li>
    </ul>
    <blockquote>
      <p class="style3">25 - 15 = 10 this equation is true</p>
      <p><span class="style3">8 + 15 = 22 this equation is false </span></p>
    </blockquote>
    <ul>
      <li>An equation that contains at least one variable is called an <span class="style5">open sentence</span>. </li>
    </ul>
    <blockquote>
      <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="22" height="18" />Example: x + 5 = 20 --&gt;<span class="style5"> it is neither true nor false until x is replaced with some number. </span></p>
    </blockquote>
    <p class="style7"><span class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="22" height="18" /></span> Exercises:</p>
    <blockquote>
      <p class="style3">Solve each equation: </p>
      <p class="style3">a. r + 11 = 17</p>
      <p class="style3">b. 17 = k - 3  </p>
      <p class="style3">&nbsp;</p>
    </blockquote>
    <ul>
      <li>
        <div align="justify">An<span class="style5"> algebraic equation</span> in the variable x is a statement that two algebraic expressions in x are equal. A variable in an equation is sometimes called an<span class="style5"> unknown</span>. The <span class="style5">domain</span> of a variable in an equation is the set of numbers for which the algebraic expressions in the equation are defined. </div>
      </li>
    </ul>
    <ul>
      <li>
        <div align="justify">When the variable in an equation is replaced by a specific number, the resulting statement may be either true or false. If it is true, then that number is called <span class="style5">solution (or root)</span> of the equation. The set of all solutions is called the solution set of the equation. A number that is a solution is sait <span class="style5">satisfy the equation</span>.</div>
      </li>
    </ul>
    <p><strong><span class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="18" /></span> Examples of polynomial equations in one variable:</strong>    </p>
    <blockquote>
      <p class="style11">7x - 21 = 0 (first degree)</p>
      <p class="style3"><strong><em>2y<sup>2</sup> - 3y - 5 = 0 (second degree) </em></strong></p>
    
      <p> <span class="style10">4z<sup>3</sup> - 8z<sup>2</sup> -z + 2 = 0 (third degree) </span></p>
      <p class="style3">9w4 - 13w2 + 4 = 0 (fourth degree) </p>
      <p class="style3">&nbsp;</p>
    </blockquote>
    <p><strong><span class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="22" height="18" /></span> Example</strong>: Find the solution set of the equation </p>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/algebra/images/Example Given 1.jpg" width="224" height="41" /></p>
      <p class="style3">Solution:</p>
      <blockquote>
        <p class="style3">Factor the denominator on the right side </p>
        <p align="justify" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Example 1.jpg" alt="1" width="232" height="40" /></p>
        <p align="justify" class="style3">When x is -5/2 or 1/2 we obtain 0 in one of the denominators, these numbers are not in the domain of x. The LCD is (2x + 5) (2x - 1); Multiply each side of the equation by the LCD and obtain </p>
        <p align="justify" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/solution 1.jpg" width="196" height="15" /></p>
      </blockquote>
    </blockquote>
    <h2>&nbsp;</h2>
  </div>