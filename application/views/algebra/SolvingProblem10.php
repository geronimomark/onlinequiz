<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(10)</h2>
    <p align="justify">A piece of paper is 0.05 thick. Each time the paper is folded into half,     the thickness is doubled. If the paper was folded 12 times, how thick     in feet the folded paper be? </p>
    <p align="left">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style11"> a<sub>1</sub>&nbsp; = 0.05; a<sub>2</sub> = 0.10 .... an </p>
    </blockquote>
    <p>&nbsp;          &nbsp;&nbsp;&nbsp;&nbsp;          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em><strong>a<sub>n</sub> = a<sub>1</sub>r<sup>n-1</sup> </strong></em></p>
    <p><em><strong>&nbsp;                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a<sub>12</sub> = (0.10)(2)12-1 </strong></em></p>
    <p><em><strong>&nbsp;            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a<sub>12</sub> = 204.8 inches </strong></em></p>
    <p><em><strong>&nbsp;              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a<sub>12</sub> = 204.8 in x 1ft/12in </strong></em></p>
    <blockquote>
      <p class="style11">&nbsp;                    answer:&nbsp; a12 = 17.07 feet </p>
    </blockquote>
    <blockquote><p class="style11">&nbsp;</p>
      <p class="style11">&nbsp;</p>
      <p class="style11">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
  </div>