<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (22) </h2>
    <p align="justify">Resolve into partial fraction:</p>
    <blockquote>
      <blockquote>
        <blockquote>
          <p align="justify"> <img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/22.1.jpg" width="65" height="32" /></p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p align="justify"><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p class="style11">The denominator factorises as (x - 1) (x + 3) and the numerator is of less degree than the denominator. </p>
      <p class="style11">&nbsp;</p>
      <p class="style11"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/22.2.jpg" width="300" height="32" /></p>
      <p class="style11">where A and B are constants and determined. </p>
      <p class="style11"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/22.3.jpg" width="280" height="32" /></p>
      <p class="style11">Since the denominator are the same on each side of the identity then the numerators are equal to each other.</p>
      <p class="style11"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/22.4.jpg" width="200" height="17" /></p>
      <p class="style11"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/22.6.jpg" width="95" height="17" /></p>
      <p class="style11">&nbsp;</p>
      <p class="style11">&nbsp;</p>
      <p class="style11">&nbsp;</p>
      <p class="style11">&nbsp;</p>
    </blockquote>
    </div>