<div class="style1" id="content">
    <h2 align="center" class="style13">algebra</h2>
    <h2 align="center">Practice problem (8) </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">It takes an airplane one hour and forty five minutes to travel 500 miles     against the wind and covers the same distance in one hour and fifteen     minutes with the wind. What is the speed of the airplane? </p>
    <p align="justify">&nbsp;</p>
    <p><em><strong>Solution:</strong></em></p>
    <blockquote>
      <p>&nbsp;<span class="style12">&nbsp;&nbsp;&nbsp; VA = velocity of airplane</span></p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp; VW = velocity of wind </p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp; Distance = Velocity x time</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; S = (VA + VW)t</p>
      <p class="style12">&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500 =  (VA + VW) (1 + 45/60) </p>
      <p class="style12">&nbsp;&nbsp;      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;500 =  (VA + VW) (1.75)</p>
      <p class="style12">&nbsp;&nbsp; (VA + VW) = 285.714&nbsp;&nbsp; ---&gt;(1)</p>
      <p class="style12">Against the wind:</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; S = (VA + VW)t </p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; 500 = (VA + VW) (1+15/60)</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500 = (VA + VW) (1.25)</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (VA + VW) = 400</p>
      <p class="style12">&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    &nbsp; VW = VA - 400 ---&gt; (2) </p>
      <p class="style12">Substituting (1) in (2):</p>
      <p class="style12">&nbsp;&nbsp;&nbsp;&nbsp;VA + VA - 400 = 285.714</p>
      <p class="style12">&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2VA = 685.714</p>
      <p class="style12">&nbsp;  &nbsp;&nbsp;&nbsp;Answer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; VA = 342.857 &nbsp;&nbsp;</p>
      <p class="style12">&nbsp;</p>
      <p class="style12">&nbsp;</p>
    </blockquote>
    <blockquote>
      <p class="style12">&nbsp;</p>
    </blockquote>
    </div>