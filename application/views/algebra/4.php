<div class="style1" id="content">
    <h2 align="center" class="style21">algebra</h2>
    <h2 align="center">relations and functions</h2>
    <p class="style17"><u>RELATIONS</u></p>
    <ul>
      <li>A<span class="style18"> relation</span> is a set of ordered pair</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="24" height="14" /><strong>Example:</strong> A = { (-1, 3), (2, 0), (2, 5), (-3, 2)}</p>
    <ul>
      <li>Domain is a set of all first coordinates  </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="14" />{-1, 2, 2, -3}</p>
    <ul>
      <li>Range is the set of all second coordinates</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="14" />{3, 0, 5, 2} </p>
    <p class="style19"><u>FUNCTIONS</u> </p>
    <ul>
      <li>A<span class="style18"> function</span> is a relation that satisfies the following: each x-value is allowed only one y- value. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="24" height="14" /><strong>Example:</strong></p>
    <p>1. The above is not a function, because 2 has y-values 0 and 5 </p>
    <p>&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/not function.jpg" width="98" height="81" /></p>
    <p align="left">2. Check to see if the following relation are functions</p>
    <blockquote>
      <p align="left" class="style20">B = {(3, 4), (2, 4), (1, 4), (-3, 2)}</p>
      <p align="left" class="style20">C = {(1, 2), (-2, 3), (5, 1), (1, 4)}</p>
      <p align="left" class="style20">Solution:</p>
      <p align="left" class="style20">Make a mapping table for B</p>
      <p align="left" class="style20">&nbsp;</p>
      <blockquote>
        <p align="left" class="style20"><img src="<?php echo base_url();?>assets/algebra/images/function and relation 1.1.jpg" width="160" height="72" />    </p>
        <p align="left" class="style20">Thus we see that B is a function</p>
        <p align="left" class="style20">Make a mapping table for C</p>
        <p align="left" class="style20"><img src="<?php echo base_url();?>assets/algebra/images/function and relation 1.2.jpg" width="160" height="72" />  </p>
        <p align="left" class="style20">Thus we see that C is not a function </p>
      </blockquote>
    </blockquote>
  </div>