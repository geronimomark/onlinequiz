<div class="style1" id="content">
    <h2 align="center" class="style6">algebra</h2>
    <h2 align="center">Complex numbers  </h2>
    <ul>
      <li>A <span class="style2">complex number</span> is a number of the form a + bi, where a and b are real numbers and i is the imaginary unit. </li>
      <li>it is an extension of real numbers</li>
      <li>Imaginary unit of i: <img src="<?php echo base_url();?>assets/algebra/images/complex number.jpg" width="40" height="21" /></li>
    </ul>
    <ul>
      <li>i<sup>2</sup> = -1 ; i<sup>3</sup> = -i ; i<sup>4</sup> = i ; i<sup>5</sup> = -1</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="26" height="14" /><strong>Example:</strong></p>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/algebra/images/complex 1.1.jpg" width="156" height="16" /></p>
    </blockquote>
    <ul>
      <li>A<span class="style2"> complex number</span> is the sum of real number and an imaginary number. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/complex 1.4.jpg" alt="1" width="260" height="187" /></p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="26" height="14" /><strong>Example:</strong></p>
    <p>1. Product of Complex Numbers: (-1 + 3i) (2 - 5i)</p>
    <blockquote>
      <p class="style3">Solution: </p>
      <p class="style3">(-1 + 3i) (2 - 5i) = (-1)(2 - 5i) + 3i (2 - 5i)</p>
      <blockquote>
        <blockquote>
          <p class="style3">= -2 + 5i + 6i -15(-1) = 13 + 11i   </p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p>2. Division of Complex Numbers: Express the number <img src="<?php echo base_url();?>assets/algebra/images/complex 1.2.jpg" width="53" height="32" />in the form a + bi.</p>
    <blockquote>
      <p class="style3">Solution: </p>
      <p class="style3"><img src="<?php echo base_url();?>assets/algebra/images/complex 1.3.jpg" width="411" height="32" /></p>
      <p class="style3">&nbsp;</p>
    </blockquote>
    <ul>
      <li><span class="style2">Roots of the Quadratic Equation</span>: ax<sup>2</sup> + bx + c = 0 </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/complex 1.5.jpg" width="139" height="35" /></p>
    <p align="left">&nbsp;</p>
    <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="26" height="14" /><strong>Example:</strong> Find the roots of the equation x<sup>2</sup> + x + 1 = 0 </p>
    <blockquote>
      <p align="left" class="style3">Solution: Using the quadratic formula</p>
      <blockquote>
        <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/complex 1.6.jpg" width="231" height="35" /> </p>
      </blockquote>
    </blockquote>
    <p class="style4"><u>POLAR FORM </u></p>
    <ul>
      <li>Complex number z in the polar form:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.1.jpg" width="165" height="16" /></p>
    <blockquote>
      <blockquote>
        <p align="left" class="style3">where: <img src="<?php echo base_url();?>assets/algebra/images/polar 1.2.jpg" width="114" height="20" />and <img src="<?php echo base_url();?>assets/algebra/images/polar 1.3.jpg" width="64" height="38" /></p>
        <p align="left" class="style3">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="26" height="14" /><strong>Example:</strong> Write the given number in polar form: z = 1 + i </p>
    <blockquote>
      <p align="left" class="style3">Solution:</p>
      <blockquote>
        <p align="left" class="style3">	<img src="<?php echo base_url();?>assets/algebra/images/polar 1.4.jpg" width="190" height="17" /></p>
        <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.5.jpg" width="50" height="16" />; <img src="<?php echo base_url();?>assets/algebra/images/polar 1.6.jpg" width="59" height="32" /></p>
        <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.7.jpg" width="196" height="32" /></p>
        <p align="left" class="style3">&nbsp;</p>
      </blockquote>
    </blockquote>
    <ul>
      <li class="style2">De Moivre's Theorem: <span class="style3">If <img src="<?php echo base_url();?>assets/algebra/images/polar 1.9.jpg" width="103" height="17" />and n is a positive integer, then <img src="<?php echo base_url();?>assets/algebra/images/polar 1.8.jpg" width="207" height="17" /></span> </li>
    </ul>
    <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="26" height="14" /><strong>Example: </strong>Find <img src="<?php echo base_url();?>assets/algebra/images/polar 1.10.jpg" width="66" height="32" /> </p>
    <blockquote>
      <p align="left" class="style3">Solution:</p>
      <p align="left" class="style3">Since 1/2 + 1/2i = 1/2(1 + i) ; 1/2 + 1/2i has the polar form</p>
      <blockquote>
        <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.11.jpg" width="239" height="32" /> </p>
      </blockquote>
      <p align="left" class="style3">Using De Moivre's Theorem:</p>
      <blockquote>
        <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.12.jpg" width="341" height="32" /> </p>
        <blockquote>
          <blockquote>
            <p align="left" class="style3"><img src="<?php echo base_url();?>assets/algebra/images/polar 1.13.jpg" width="270" height="32" /></p>
          </blockquote>
        </blockquote>
      </blockquote>
    </blockquote>
    <p> <span class="style3"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="26" height="14" /></span> <span class="style5">Exercises:</span></p>
    <p>Compute the following (express in the form x + iy)</p>
    <p>a. (2 + 3i) (5 + 7i)</p>
    <p>b. (2 + 3i)<sup>2</sup>  </p>
    <p>c. (2i)<sup>3</sup> - (2i)<sup>2</sup> + 2i -1 </p>
  </div>