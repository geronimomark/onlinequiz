<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">practice problem (29)</h2>
    <p align="center">&nbsp;</p>
    <p>A boat man rows to a place 4.8 miles with the stream and back in 14 hours, but find that he can row 14 miles with the stream in the same time as 3 miles against the stream. Find the rate of the stream.</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.1.jpg" width="405" height="172" /></p>
    <blockquote>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.2.jpg" width="165" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.3.jpg" width="200" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.4.jpg" width="220" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.6.jpg" width="186" height="18" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.7.jpg" width="253" height="89" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.8.jpg" width="145" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.9.jpg" width="135" height="32" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.10.jpg" width="155" height="16" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.11.jpg" width="86" height="18" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.13.jpg" width="235" height="115" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.12.jpg" width="135" height="16" /></p>
        <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/29.14.jpg" width="105" height="18" /></p>
        <p align="left">&nbsp;</p>
        <p align="left">&nbsp;</p>
        <p align="left">&nbsp;</p>
      </blockquote>
      </blockquote>
  </div>