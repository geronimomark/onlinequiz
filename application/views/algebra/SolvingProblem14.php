<div class="style1" id="content">
    <h2 align="center" class="style12">algebra</h2>
    <h2 align="center">SAMPLE PROBLEM(14)</h2>
    <p align="justify">Suppose </p>
    <blockquote>
      <blockquote>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/matrix 1.1.jpg" width="75" height="68" />	</p>
        <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/matrix 1.2.jpg" width="129" height="45" /></p>
      </blockquote>
    </blockquote>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="justify" class="style11">Solution:</p>
      <blockquote class="style11">
        <p>Row 1 of A is [5, 4]. Column of 4 of B is <img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/matrix 1.3.jpg" width="23" height="53" />. </p>
        <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/matrix 1.4.jpg" width="177" height="47" /></p>
      </blockquote>
    </blockquote>
    <blockquote>
      <blockquote>
        <p><span class="style11">= 22 </span></p>
      </blockquote>
    </blockquote>
    <p align="left">&nbsp;</p>
    <blockquote>&nbsp;</blockquote>
    <p align="justify" class="style10">&nbsp;</p>
  </div>