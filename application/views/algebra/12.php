<div class="style1" id="content">
    <h2 align="center" class="style8">algebra</h2>
    <h2 align="center">sequence, series, and limits </h2>
    <ul>
      <li><span class="style2">Sequence</span> is an ordered list on numbers. </li>
    </ul>
    <ul>
      <li>A sequence that ends is called a<span class="style2"> finite sequence</span>.</li>
    </ul>
    <ul>
      <li>A sequence that does not end is called<span class="style2"> infinite sequence</span>. </li>
    </ul>
    <ul>
      <li><span class="style2">Arithmetic Sequence </span> is a sequence such that the difference between two consecutive terms is constant thoughout the sequence.
        <ul>
          <li><span class="style5">Formula for Arithmetic Sequence</span>: <span class="style4">The n<sup>th</sup> term of an arithmetic sequence with first term b and with difference d between consecutive terms is<strong> b + (n - 1)/d</strong>. </span></li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="22" height="14" /><strong>Example:</strong></p>
    <p>Suppose at the beginning of the year your iPod contains 53 songs and that you purchase four new songs each week to place on your iPod. Consider the sequence whose n<sup>th</sup> term is the number of songs on your iPod at the beginning of the nth week of the year.</p>
    <p>a. What are the first fourth term of this sequence?</p>
    <p>b. What is the 30<sup>th</sup> term of this sequence? In other words, how many songs will be on your iPod at the beginning of the 30<sup>th</sup> week?  </p>
    <blockquote>
      <p><span class="style4">Solution:</span></p>
      <p><span class="style4">a. The first four terms of this sequence are 53, 57, 61, 65</span></p>
      <p><span class="style4">b. Solving for the 30<sup>th</sup> term, applying arithmetic sequence formula: b = 53, n = 30, and d = 4.</span></p>
      <blockquote>
        <p class="style4">53 + (30 - 1) (4) = 169 </p>
      </blockquote>
    </blockquote>
    <ul>
      <li>A <span class="style2">geometric sequence </span>is a sequence such that the ratio of two consecutive terms is constant throughout the sequence. 
        <ul>
          <li><span class="style5">Formula for Geometric Sequence: </span>The nth term of geometric sequence with first term b and with ratio r of consecutive terms is<strong> br<sup>n-1</sup></strong>. </li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Example:</strong></p>
    <p>Suppose at the beginning of the year $ 10,000 is deposited in the bank account that pays 5% interest per year, compounded once per year at the end of the year. Consider the sequence whose n<sup>th</sup> term is the amount in the bank account at the beginning of the n<sup>th</sup> year. </p>
    <p>a. What are the first four of this sequence?</p>
    <p>b. What is the 20<sup>th</sup> term of this sequence? In other words, how much will be in the bank account at the beginning of the 20<sup>th</sup> year </p>
    <blockquote>
      <p class="style4">Solution:</p>
      <p align="justify"><span class="style4">a. Each term of this sequence is obtained by multiplying the previous term by 1.05. Thus we have geometric sequence whose first four terms are:</span></p>
      <blockquote><p align="justify"><span class="style4">	$1000, $ 1000*1.05, $1000(1.05)<sup>2</sup>, $1000(1.05)<sup>3</sup></span> </p>
        <p align="justify"><span class="style4">These four terms can be written as $1000, $1050, $1102.5, $1157.63</span></p>
      </blockquote>
      <p align="justify" class="style4">b. Finding the 20<sup>th</sup> term of this sequence, use geometric formula.</p>
      <blockquote>
        <p align="justify" class="style4">Where b = $1000, r = 1.05, and n = 20. </p>
        <p align="justify" class="style4">$1000(1.05)<sup>19</sup></p>
      </blockquote>
    </blockquote>
    <p align="justify" class="style7"><u>RECURSIVELY DEFINED SEQUENCES</u> </p>
    <ul>
      <li><span class="style4">A<span class="style2"> recursive defined sequence</span> is a sequence in which each term from some point on is defined by using previous terms. </span></li>
    </ul>
    <p align="justify" class="style7">&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Examples:</strong></p>
    <p>1. Write the geometric sequence 6, 12, 24, 48,... Whose nth terms is defined by:</p>
    <p>a<sub>n</sub> = 3*2<sup>n</sup></p>
    <blockquote>
      <p><span class="style4">Solution:</span></p>
      <p class="style4">Each term of this sequence is obtained by multiplying the previous term by 2. Thus the recursive definition of this sequence is given by the equation.</p>
      <p class="style4">a<sub>1</sub> = 6 and a<sub>n+1</sub> = 2a<sub>n</sub> for n is greater than or equal to 1  </p>
    </blockquote>
    <p class="style4">&nbsp;</p>
    <p class="style4">2. Find the first ten terms of the Fibonacci Sequence</p>
    <blockquote>
      <p class="style4">Solution:</p>
      <p class="style4">The first two terms of Fibonacci sequence are 1, 1. The third term of Fibonacci sequence is the sum of the first two terms; thus the third term is 2. The fourth term of Fibonacci sequence is the sum of the second and third tems; thus the fourth term is 3. Continuing this, we get:</p>
      <p class="style4">1, 1, 2, 3, 5, 8, 13, 21, 34, 55  </p>
    </blockquote>
    <p align="left" class="style7"><u>SERIES</u></p>
    <ul>
      <li>A<span class="style2"> series </span>is the sum of the terms of a sequence.
        <ul>
          <li>For example, corresponding to the finite sequence 1, 4, 9, 16 is the series 1 + 4 + 9 + 16 which equals 30</li>
        </ul>
      </li>
    </ul>
    <ul>
      <li><span class="style2">Arithmetic Series</span> is the sum obtained by adding up the terms of an arithmetic sequence.
        <ul>
          <li><span class="style5">Formula for Arithmetic Series: </span></li>
        </ul>
      </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/arithmetic series.jpg" width="123" height="46" /></p>
    <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Example:</strong></p>
    <p align="justify">You rode your bicycle everyday, starting with 5 miles on the first day of summer. You increased the distance your rode by one-half mile each day. How many miles total did your ride over the 90 days of summer? </p>
    <blockquote>
      <p align="justify" class="style4">Solution:</p>
      <p align="justify" class="style4">Using arithmetic series n = 90 , the initial term b = 5, and the difference between consecutive terms d = 0.5.</p>
      <blockquote>
        <p align="justify" class="style4"><img src="<?php echo base_url();?>assets/algebra/images/arithmetic series 1.1.jpg" width="137" height="42" /></p>
        <p align="justify" class="style4">= 2,452.5 miles </p>
      </blockquote>
    </blockquote>
    <ul>
      <li><span class="style2">Geometric Series</span>: is the sum obtained by adding up the terms of a geometric sequence. 
        <ul>
          <li><span class="style5">Geometric Series Formula</span>: The sum of a geometric sequence with initial term b, ratio r is not equal to 1 of consecutive terms, and n term is 
            <div align="center"><img src="<?php echo base_url();?>assets/algebra/images/geometric series.jpg" width="74" height="44" /></div>
          </li>
        </ul>
      </li>
    </ul>
    <p class="style7"><u>PASCAL'S TRIANGLE</u> </p>
    <ul>
      <li><span class="style2">Pascal's triangle</span> is a triangular array with n numbers in the n<sup>th</sup> row. The first and last entries in each row are 1.</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/algebra/images/pascal triangle.jpg" width="148" height="125" /></p>
    <p align="center">&nbsp;</p>
    <ul>
      <li class="style2">Coefficient of (x + y)<sup>n</sup><span class="style4">, suppose n is nonnegative integer. Then row n + 1 of Pascal's triangle gives the coefficient in the expansion of (x + y)<sup>n</sup>.</span></li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Example:</strong> Use the Pascal's triangle to find the expansion (x + y)<sup>7</sup>. </p>
    <blockquote>
      <p class="style4">Solution: </p>
      <blockquote>
        <p class="style4"><img src="<?php echo base_url();?>assets/algebra/images/pascal ex 1.1.jpg" width="177" height="56" /></p>
        <p class="style4">(x + y)7 = x<sup>7</sup> + 7x<sup>6</sup>y + 21x<sup>5</sup>y<sup>2</sup> + 35x<sup>4</sup>y<sup>3</sup> + 35x<sup>3</sup>y<sup>4</sup> + 21x<sup>2</sup>y<sup>5</sup> + 7xy<sup>6</sup> + y<sup>7</sup></p>
        <p class="style4">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style7"><u>BINOMIAL COEFFICIENT</u>  </p>
    <ul>
      <li>Suppose n and k are nonnegative integers with k is less than or equal n. </li>
    </ul>
    <p align="center">	<img src="<?php echo base_url();?>assets/algebra/images/binomial coefficient.jpg" width="139" height="46" /></p>
    <p align="center">&nbsp;</p>
    <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Example:</strong> Find the coefficient of x<sup>97</sup>y<sup>3</sup> in the expansion of (x + y)<sup>100</sup> </p>
    <blockquote>
      <p align="left" class="style4">Solution:</p>
      <blockquote>
        <p align="left" class="style4"><img src="<?php echo base_url();?>assets/algebra/images/binomial coef 1.1.jpg" width="99" height="32" /> </p>
        <p align="left" class="style4"><img src="<?php echo base_url();?>assets/algebra/images/binomial coef 1.2.jpg" width="105" height="32" /></p>
        <p align="left" class="style4">= 161700 </p>
      </blockquote>
    </blockquote>
    <p align="left" class="style7"><u>LIMITS</u></p>
    <ul>
      <li>A sequence has a limit from some point on, all the terms of the sequence are very close to limit.
        <ul>
          <li class="style5">Limit notation:  
            <blockquote>
              <p><img src="<?php echo base_url();?>assets/algebra/images/limit.jpg" width="77" height="20" /></p>
            </blockquote>
          </li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="22" height="14" /><strong>Example:</strong> In the decimal expanssion of 0.99<sup>100000</sup>, how many zeros follow the decimal point before the first non-zero digit?</p>
    <blockquote>
      <p class="style4">Solution:</p>
      <blockquote>
        <p class="style4">Calculators cannot evaluate 0.99<sup>100000</sup>, so take it's common logarithm</p>
        <p class="style4">log0.99<sup>100000</sup> = 100000 log 0.99 = 100000 (-0.004365) = -436.5 </p>
        <p class="style4">&nbsp;</p>
      </blockquote>
    </blockquote>
    <ul>
      <li><strong>Infinite Geometric Series: </strong> If r &lt; 1, then
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/algebra/images/infinite geometric series.jpg" width="180" height="44" /></p>
        </blockquote>
      </li>
    </ul>
    <p>&nbsp;</p>
  </div>