<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/starter.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-size: xx-large;
	font-weight: bold;
}
.style2 {
	font-family: "Times New Roman";
	font-size: x-large;
}
.style3 {font-size: x-large}
-->
    </style>
</head>

  <script>
    function HandleOnClose(e){
      // var r = confirm('Are you sure you want to exit this page?');
      // if (r == true){

      // }else{
      //   return;
      // }
    }

    function ConfirmClose(e){
      e.preventDefault();
      save_t();
      var r = confirm('Are you sure you want to exit this page?');
      if (r == false){
        return;
      }else{
        window.location.replace('<?php echo base_url();?>index.php/home/logout');
      }
    }

    var time = '<?php echo $time[0]["time"];?>';

    var counter=setInterval(timer, 1000);

    var arrx = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];

    function timer()
    { 
      if (time <= 0)
      {
        save_t();
        clearInterval(counter);
        document.getElementById("timer").innerHTML = '00:00:00';
        document.getElementById("qq").innerHTML = "";
        document.getElementById("sq").innerHTML = "";
        return;
      }
      time = time - 1;
      // console.log(time);
      var hour = Math.floor(Math.floor(time / 60) / 60);
      var mins = Math.floor(time / 60);
      var secs = time % 60;
      if (mins > 59)
        mins = mins % 60;
      if ( arrx.indexOf(hour) != -1) hour = '0' + hour;
      if ( arrx.indexOf(mins) != -1) mins = '0' + mins;
      if ( arrx.indexOf(secs) != -1) secs = '0' + secs;
      document.getElementById("timer").innerHTML = hour + ':' + mins + ':' + secs;
    }

    function save_t()
    {
      var xmlhttp;
      if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      }
      else
      {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
          {

          }
      }
      xmlhttp.open("POST","<?php echo base_url() . 'index.php/exam/save_time';?>",false);
      xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      xmlhttp.send("time=" + time + "&tid=<?php echo $time[0]['tm_tid'];?>");
    }
  </script>
  <body onbeforeunload="ConfirmClose(event)">

    <div class="container">
      <div class="starter-template">
        <p><span class="style1">Exam Description:</span> <?php echo $quiz_d[0]["qz_qname"] . ' - ' . $quiz_d[0]["qz_qdesc"];?></p>
        <br />
        <p class="lead">Examinee Full Name: <?php echo $this->session->userdata("oq_name") . ' ' . $this->session->userdata("oq_lname");?></p>
        <span id="ss"><button id="submit" class="btn btn-lg btn-primary" type="submit">Submit Answer Sheet</button></span>
        <br /><br />
        <p style="font-size:30px; font-weight:bold;"><span class="style2">Time Remaining:</span> <span class="style3" id="timer" style="color:red;">00:00:00</span></p>
        <p id="mins"></p>
        <?php $count = 1;?>
        <p class="lead" id="sq">Select Question Number
        <select id="question">
          <option value="0"> ---- </option>
          <?php foreach ($questions as $key => $value):?>
          <option value="<?php echo $value['qs_qsid']?>"><?php echo $count++;?></option>
          <?php endforeach;?>
        </select>
        </p>
        <hr />
        <div id="qq"></div>
      </div>

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>

      $(function(){

        $( "#question" ).change(function() {
          save_time();
          if ( $(this).val() > 0 ){
            $.ajax({
              type: "POST",
              async: false,
              url: '<?php echo base_url();?>index.php/exam/getquestion',
              data:{qid : $(this).val(), qzid : '<?php echo $quiz_d[0]["qz_qid"]; ?>'},
              success: function(msg){
                  $("#qq").html(msg);
              }
            });
          }else{
            $( "#qq" ).empty();
          }
        });

        $("#submit").click(function(){
          var r = confirm("Are you sure you want to submit your answers? This will automatically compute for your test results.");
          if (r == true){
            $("#submit").remove();
            time = 0;
            $.ajax({
              type: "POST",
              async: false,
              url: '<?php echo base_url();?>index.php/submit/update_link',
              data:{ linkid : "<?php echo $linkid;?>", qzid : '<?php echo $quiz_d[0]["qz_qid"]; ?>'}
            });
            window.location.replace('<?php echo base_url()."index.php/submit/generate_result/".$quiz_d[0]["qz_qid"]."/".$this->session->userdata("oq_uid");?>'); 
          }else{
            return;
          }
        });

        function save_time()
        {
          $.ajax({
              type: "POST",
              async: false,
              url: '<?php echo base_url();?>index.php/exam/save_time',
              data:{'time' : time, 'tid' : "<?php echo $time[0]['tm_tid'];?>"}
            });
        }

      });

    </script>
  </body>
</html>
