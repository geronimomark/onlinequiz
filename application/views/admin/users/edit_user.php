<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">
    <style type="text/css">
      .style1 {
      	font-size: x-large;
      	font-weight: bold;
      }
      .accesstext{
        font-size: 16px;
      }
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">Edit User</p>
        <div id="success" class="alert alert-success"></div>
        <div id="error" class="alert alert-danger"></div>
        <input id="fname" autocomplete="off" type="text" class="form-control" placeholder="First Name" required autofocus value="<?php echo $edit_data[0]['oq_fname']?>">
        <input id="lname" autocomplete="off" type="text" class="form-control" placeholder="Surname" required value="<?php echo $edit_data[0]['oq_lname']?>">
        <input id="email" autocomplete="off" type="text" class="form-control" placeholder="Email address" required value="<?php echo $edit_data[0]['oq_email']?>">
        <select id="utype" class="form-control" required>
          <option value="2">Student</option>
          <option value="1">Admin</option>
        </select>
        <!-- <input type="text" class="form-control" placeholder="Username" required> -->
        <input class="studentoptions" id="exam" type="checkbox" <?php if($edit_data[0]['oq_exam'] == "1") echo "checked";?>/> <span class='accesstext'>Access Exam Module</span><br />
        <input class="studentoptions" id="lecture" type="checkbox" <?php if($edit_data[0]['oq_lecture'] == "1") echo "checked";?>/> <span class='accesstext'>Access Lectures Module</span><br />
        <input class="studentoptions" id="activities" type="checkbox" <?php if($edit_data[0]['oq_activity'] == "1") echo "checked";?>/> <span class='accesstext'>Access Activities Module</span><br />
        <input id="password" autocomplete="off" maxlength="16" type="password" class="form-control" placeholder="Password" required value="<?php echo $edit_data[0]['oq_password']?>">
        <input id="cpassword" autocomplete="off" maxlength="16" type="password" class="form-control" placeholder="Confirm Password" required value="<?php echo $edit_data[0]['oq_password']?>">
        <button id="save" class="btn btn-lg btn-primary" type="submit">Update</button>
        <a id="cancel" class="btn btn-lg btn-default" href="<?php echo base_url();?>index.php/admin/users">Cancel</a>

      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>

      $(function(){

        var err_msg = new Array(),
        ctr_false = 0;

        err_msg[0] = false,
        err_msg[1] = false;

        var exam = "0";
        var activity = "0";
        var lecture = "0";

        var admin = "<?php echo $edit_data[0]['oq_utype']?>";

        if(admin == "1"){
          $(".studentoptions").prop('checked', true).attr("disabled", "disabled");
        }

        $("#error, #success").hide();

        $("#utype").change(function(){
            if($(this).val() == "1"){
              $(".studentoptions").prop('checked', true).attr("disabled", "disabled");
              exam = "1";
              activity = "1";
              lecture = "1";
            }else{
              $(".studentoptions").prop('checked', false).removeAttr("disabled");
            }
        });

        $("#utype").val("<?php echo $edit_data[0]['oq_utype']?>");

        $(".studentoptions").click(function(){
            exam = "0";
            activity = "0";
            lecture = "0";
            if($("#exam").is(":checked")){
              exam = "1";
            }
            if($("#lecture").is(":checked")){
              lecture = "1";
            }
            if($("#activities").is(":checked")){
              activity = "1";
            }
        });

        $("#password, #cpassword").focusout(function() {
          if($("#password").val().length <= 7){
            err_msg[0] = "Minimum Password length is 8 characters.";
          }else if ($("#password").val() != $("#cpassword").val()){
            err_msg[0] = "Password doesn't match.";
          }else{
            err_msg[0] = false;
          }
          generate_error();
        });

        $("#save").click(function(e){
          if( $("#fname").val() == '' || $("#lname").val() == '' || $("#email").val() == '' || 
            $("#password").val() == '' || $("#cpassword").val() == ''){
            err_msg[1] = "All fields are required!";
          }else if( $("#password").val() != $("#cpassword").val() ){
            e.preventDefault();
          }else{
            e.preventDefault();
            err_msg[1] = false;
            if( $("#error").text() == ""){
              $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url();?>index.php/admin/save_user',
                data: 
                {
                  fname     :   $("#fname").val(),
                  lname     :   $("#lname").val(),
                  email     :   $("#email").val(),
                  utype     :   $("#utype").val(),
                  user      :   '<?php echo $edit_data[0]["oq_username"]?>',
                  pass      :   $("#password").val(),
                  uid       :   '<?php echo $edit_data[0]["oq_userid"]?>',
                  exam      :   exam,
                  lecture   :   lecture,
                  activity  :   activity
                },
                success: function(msg){
                  if(msg == "success"){
                    $("#success").html("User successfully updated!").show();
                    setTimeout(function(){$("#success").empty().hide()},3000);
                  }
                }
              });
            }
          }
          generate_error();
        });

        function generate_error()
        {
          var msg = "";
          ctr_false = 0;

          for (var i = 0; i < err_msg.length; i++) {
            if(err_msg[i] != false){
              msg += err_msg[i] + "<br />";
            }else{
              ctr_false++;
            }
          }
          if(err_msg.length == ctr_false){
            $("#error").empty().hide();
          }else{
            $("#error").html(msg).show();
          }
          err_msg[1] = false;
        }

      });

    </script>
  </body>
</html>
