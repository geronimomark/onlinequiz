<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-size: x-large;
	font-weight: bold;
}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">
      <style>.form-signin{max-width: 800px;}</style>
      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">View Users</p>
        <div id="error" class="alert alert-danger"></div>
        <input id="searchtxt" autocomplete="off" type="text" class="form-control" placeholder="E.g. Name, User Type" required autofocus>
        <button id="search" class="btn btn-lg btn-primary" type="submit">Search</button>
        <a href='<?php echo base_url();?>index.php/admin/users/add_users' class="btn btn-danger btn-lg" role="button">Add New</a>
        <hr />
        <div id="dynamictable">
        <table border='0' width='100%'>
          <tr>
            <th>Name</th>
            <th>Usertype</th>
            <th colspan="2">Action</th>
          </tr>
          <?php foreach ($new_users as $key => $value):?>
            <?php if( $this->session->userdata("oq_uid") != $value['oq_userid'] ):?>
            <tr>
            <td><?php echo $value["oq_fname"] . " " . $value["oq_lname"]; ?></td>
            <td><?php echo $x = ($value["oq_utype"] == 1) ? "Admin" : "Student"; ?></td>
            <td><a href="<?php echo base_url();?>index.php/admin/users/edit_user/<?php echo $value['oq_userid'];?>">Edit</a></td>
            <td><a id="<?php echo $value['oq_userid'];?>" href="" value="<?php echo $value['oq_userid'];?>">Delete</a></td> 
            <script>
              $(function(){
                $("#<?php echo $value['oq_userid'];?>").click(function(e){
                  e.preventDefault();
                  var r = confirm("Are you sure you want to Delete <?php echo $value['oq_fname'] . ' ' . $value['oq_lname']; ?>?");
                  if (r == true){
                    $.ajax({
                      type: "POST",
                      async: false,
                      url: '<?php echo base_url();?>index.php/admin/delete_user',
                      data: {uid :   "<?php echo $value['oq_userid'];?>"},
                      success: function(msg){
                        if(msg == "success"){
                          alert("User successfully deleted!");
                          window.location.replace('<?php echo base_url();?>index.php/admin/users'); 
                        }else{
                          alert("Database error occurred, please try again..");
                        }
                      }
                    });
                  }
                });
              });
            </script>          
            </tr>
            <?php endif;?>
          <?php endforeach;?>
        </table>
        </div>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
    <script>
      $(function(){

        var err_msg = new Array(),
        ctr_false = 0;

        err_msg[0] = false;

        $("#error, #success").hide();

        $("#add").click(function(){
          window.location.replace(); 
        });

        $("#search").click(function(e){
          if( $("#searchtxt").val() == '' ){
            err_msg[0] = "Search Text field is required!";
          }else{
            e.preventDefault();
            err_msg[0] = false;
            // if( $("#error").text() == ""){
              $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url();?>index.php/admin/search_user',
                data: {search :   $("#searchtxt").val()},
                success: function(msg){
                  var msg = msg.split("|");
                  if( msg[1] > 0 ){
                    $("#dynamictable").empty().html(msg[0]);
                    $("#error").empty().hide();
                  }else{
                    $("#dynamictable").empty();
                    $("#error").html("No Records Found!").show();
                  }
                }
              });
            // }
          }
        });

        function generate_error()
        {
          var msg = "";
          ctr_false = 0;

          for (var i = 0; i < err_msg.length; i++) {
            if(err_msg[i] != false){
              msg += err_msg[i] + "<br />";
            }else{
              ctr_false++;
            }
          }
          if(err_msg.length == ctr_false){
            $("#error").empty().hide();
          }else{
            $("#error").html(msg).show();
          }
          err_msg[0] = false;
        }

      });
    </script>
  </body>
</html>
