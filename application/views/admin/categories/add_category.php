<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {font-family: "Times New Roman"}
.style2 {
	font-size: x-large;
	font-weight: bold;
}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper style1">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>
      </div>
    </div>

  <p class="style1"><br />
      <br />
      <br />
      <span class="style2"><br />
      <br />
  </span></p>
    <div class="container marketing style1">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <p class="style2">Add Category </p>
        <div id="success" class="alert alert-success"></div>
        <div id="error" class="alert alert-danger"></div>
        <input id="cname" autocomplete="off" type="text" class="form-control" placeholder="Category Name" required autofocus>
        <button id="save" class="btn btn-lg btn-primary" type="submit">Save</button>
        <a id="cancel" href="<?php echo base_url() . 'index.php/admin/categories/';?>" class="btn btn-lg btn-default" >Cancel</a>
      </form>
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 
  </div>

    <span class="style1">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>

      $(function(){

        $("#error, #success").hide();

        var _error = false;

        $("#save").click(function(e){
          if ( $("#cname").val() != '' ){
            e.preventDefault();
            check_category();
            if( _error == false ){
              $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url();?>index.php/admin/save_category',
                data: {cat:$("#cname").val()},
                success: function(msg){
                  if(msg == "success"){
                      $("#cname").val('');
                      $("#success").html("Category successfully saved!").show();
                      setTimeout(function(){$("#success").empty().hide()},3000);
                  }
                }
              });
            }
          }
          
        });

        function check_category(){
          $.ajax({
            type: "POST",
            async: false,
            url: '<?php echo base_url();?>index.php/admin/check_duplicate_cat',
            data: {cat:$("#cname").val()},
            success: function(msg){
              if (msg > 0){
                $("#error").html("Category already exist").show();
                _error = true;
              }else{
                $("#error").empty().hide();
                _error = false;
              }
            }
          });
        }

      });

    </script>
    </span>
  </body>
</html>
