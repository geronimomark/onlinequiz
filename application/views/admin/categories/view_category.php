<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script>
        function check_link(_cid)
        {
          var res;
          $.ajax({
              type: "POST",
              async: false,
              url: '<?php echo base_url();?>index.php/admin/check_link',
              data: {cid :  _cid},
              success: function(msg){
                if(msg > 0){
                  res = false;
                }else{
                  res = true;
                }
              }
            });
          return res;
        }
    </script>
    <style type="text/css">
<!--
.style1 {
	font-size: x-large;
	font-weight: bold;
}
.style2 {font-family: "Times New Roman"}
.style4 {font-family: "Times New Roman"; font-size: large; }
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">
      <style>.form-signin{max-width: 800px;}</style>
      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">View Categories</p>
        <a href="<?php echo base_url() . 'index.php/admin/categories/add_category/';?>"   class="btn btn-lg btn-danger style2" id="save">Add New</a>
        <hr />
        <table border='0' width='100%'>
          <tr>
            <th><span class="style4">Category Name</span></th>
            <th colspan="2"><p class="style4">Action</p></th>
          </tr>
          <?php foreach ($new_category as $key => $value) :?>
          <tr>
            <td><?php echo $value["sc_sname"];?></td>
            <td><a href="<?php echo base_url() . 'index.php/admin/categories/edit_category/' . $value['sc_sid'];?>" class="style2">Edit</a></td>
            <td><a href="" class="style2" id="<?php echo $value['sc_sid'];?>" value="<?php echo $value['sc_sid'];?>">Delete</a></td> 
            <script>
              $(function(){
                $("#<?php echo $value['sc_sid'];?>").click(function(e){
                  e.preventDefault();
                  if ( check_link("<?php echo $value['sc_sid'];?>") == true ){
                    var r = confirm("Are you sure you want to Delete <?php echo $value['sc_sname']; ?>?");
                    if (r == true){
                      $.ajax({
                        type: "POST",
                        async: false,
                        url: '<?php echo base_url();?>index.php/admin/delete_category',
                        data: {cid :   "<?php echo $value['sc_sid'];?>"},
                        success: function(msg){
                          if(msg == "success"){
                            alert("Category successfully deleted!");
                            window.location.replace('<?php echo base_url();?>index.php/admin/categories'); 
                          }else{
                            alert("Database error occurred, please try again..");
                          }
                        }
                      });
                    }
                  }else{
                    alert("This category is linked to a question. Cannot delete this category.");
                  }
                    
                });
              });
            </script>         
          </tr>  
          <?php endforeach; ?>     
        </table>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>


  </body>
</html>
