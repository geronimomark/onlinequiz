<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">
      <style>.form-signin{max-width: 1000px;}</style>
      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">View Exam</h2>
        <!-- <input type="text" class="form-control" placeholder="E.g. Quiz Name, Description" required autofocus> -->
        <!-- <button id="save" class="btn btn-lg btn-primary" type="submit">Search</button> -->
        <a id="cancel" href='<?php echo base_url();?>index.php/admin/quizzez/add_quiz' class="btn btn-lg btn-danger" >Add New</a>
        <hr />
        <table border='0' width='100%'>
          <tr>
            <th>Exam Name</th>
            <th>Description</th>
            <th>Date Created</th>
            <th colspan="4">Action</th>
          </tr>
          <?php foreach ($new_quiz as $key => $value):?>
          <tr>
            <td><?php echo $value["qz_qname"];?></td>
            <td><?php echo $value["qz_qdesc"];?></td>
            <td><?php echo $value["qz_qdate"];?></td>
            <td><a href="<?php echo base_url() . 'index.php/admin/assign/index/' . $value['qz_qid'];?>">Assign</a></td>
            <td><a href="<?php echo base_url() . 'index.php/admin/questions/view_question/' . $value['qz_qid'];?>">Questions</a></td>
            <td><a href="<?php echo base_url();?>index.php/admin/quizzez/edit_quiz/<?php echo $value['qz_qid'];?>">Edit</a></td>
            <td><a id="<?php echo $value['qz_qid'];?>" href="" value="<?php echo $value['qz_qid'];?>">Delete</a></td>    
          <script>
              $(function(){
                $("#<?php echo $value['qz_qid'];?>").click(function(e){
                  e.preventDefault();
                  var r = confirm("Are you sure you want to Delete <?php echo $value['qz_qname']; ?>?");
                  if (r == true){
                    $.ajax({
                      type: "POST",
                      async: false,
                      url: '<?php echo base_url();?>index.php/admin/delete_quiz',
                      data: {qid :   "<?php echo $value['qz_qid'];?>"},
                      success: function(msg){
                        if(msg == "success"){
                          alert("Exam successfully deleted!");
                          window.location.replace('<?php echo base_url();?>index.php/admin/quizzez'); 
                        }else{
                          alert("Database error occurred, please try again..");
                        }
                      }
                    });
                  }
                });
              });
            </script> 
          </tr>
          <?php endforeach;?>
        </table>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
  </body>
</html>
