<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Add Exam</h2>
        <div id="success" class="alert alert-success" style="display:none;"></div>
        <input id="qname" autocomplete="off" type="text" class="form-control" placeholder="Exam Name" required autofocus>
        <textarea id="qdesc" autocomplete="off" class="form-control" placeholder="Exam Description" required autofocus></textarea>
        <input id="qtime" autocomplete="off" maxlength="3" onkeypress="return isNumber(event)" type="text" class="form-control" placeholder="Exam Time in minutes" required autofocus>
        <input id="qrate" autocomplete="off" maxlength="2" onkeypress="return isNumber(event)" type="text" class="form-control" placeholder="Exam Passing Rate in percentage" required autofocus>
        <button id="save" class="btn btn-lg btn-primary" type="submit">Save</button>
        <a id="cancel" class="btn btn-lg btn-default" href="<?php echo base_url();?>index.php/admin/quizzez">Cancel</a>

      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>
      function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
      }

      $(function(){

        $("#save").click(function(e){
          if ( $("#qname").val() == "" || $("#qdesc").val() == "" ||
              $("#qtime").val() == "" || $("#qrate").val() == "" ){
              //
          }else{
            e.preventDefault();
            $.ajax({
              type: "POST",
              async: false,
              url: '<?php echo base_url();?>index.php/admin/save_quiz',
              data: 
              {
                qname :   $("#qname").val(),
                qdesc :   $("#qdesc").val(),
                qtime :   $("#qtime").val(),
                qrate :   $("#qrate").val()
              },
              success: function(msg){
                if(msg == "success"){
                  cleartext();
                  $("#success").html("Exam successfully saved!").show();
                  setTimeout(function(){$("#success").empty().hide()},3000);
                }
              }
            });
          }

        });

        function cleartext()
        {
          $("#qname").val("");
          $("#qdesc").val("");
          $("#qtime").val("");
          $("#qrate").val("");
        }

      });
      
    </script>
  </body>
</html>
