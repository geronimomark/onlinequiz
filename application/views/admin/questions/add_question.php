<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/question.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-size: x-large;
	font-weight: bold;
}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "maintenance";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">Add Question</p>
        <div id="success" class="alert alert-success"></div>
        <div id="error" class="alert alert-danger"></div>

        <form method="post" action="somepage">
  		    <textarea id="question" name="content" style="width:100%"></textarea>
		</form>

			<form class="form-signin" role="form">
		        <select id="category" class="form-control" required>
              <option value="0"> -- SELECT CATEGORY -- </option>
              <?php foreach ($categories as $key => $value): ?>
                <option value='<?php echo $value["sc_sid"];?>'><?php echo $value["sc_sname"];?></option>
              <?php endforeach; ?>
              </select>
		        <input id ="ans_a" type="text" class="form-control" placeholder="A.) Answer" required>
		        <input id ="ans_b" type="text" class="form-control" placeholder="B.) Answer" required>
		        <input id ="ans_c" type="text" class="form-control" placeholder="C.) Answer" required>
		        <input id ="ans_d" type="text" class="form-control" placeholder="D.) Answer" required>
		        <select id="answer" class="form-control" required>
              <option value="0"> -- SELECT CORRECT ANSWER -- </option>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
            </select>
		        <button id="save" class="btn btn-lg btn-primary" type="submit">Save</button>
		        <a id="cancel" href="<?php echo base_url() . 'index.php/admin/questions/view_question/' . $quizid;?>" class="btn btn-lg btn-default">Cancel</a>

        </form>

      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

<div id="dialog_a" title="Answer A">
    <textarea id="edit_a" name="content" style="width:100%"></textarea>
</div>
<div id="dialog_b" title="Answer B">
    <textarea id="edit_b" name="content" style="width:100%"></textarea>
</div>
<div id="dialog_c" title="Answer C">
    <textarea id="edit_c" name="content" style="width:100%"></textarea>
</div>
<div id="dialog_d" title="Answer D">
    <textarea id="edit_d" name="content" style="width:100%"></textarea>
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/tinymce/tinymce.min.js"></script>
  	<script type="text/javascript">

  		$(function() {

        $("#error, #success").hide();

  			tinymce.init({
  			    selector: "#question",
  			    theme: "modern",
  			    plugins: [
  			        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
  			        "searchreplace wordcount visualblocks visualchars code fullscreen",
  			        "insertdatetime media nonbreaking save table contextmenu directionality",
  			        "emoticons template paste textcolor"
  			    ],
  			    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
  			    toolbar2: "print preview media | forecolor backcolor emoticons",
  			    image_advtab: true,
  			    templates: [
  			        {title: 'Test template 1', content: 'Test 1'},
  			        {title: 'Test template 2', content: 'Test 2'}
  			    ]

  			});

        $("#dialog_a").hide();
        $("#dialog_b").hide();
        $("#dialog_c").hide();
        $("#dialog_d").hide();

        $( "#save" ).click(function(e){

          if( tinyMCE.get('question').getContent() == '' || 
              $("#category").val() == '0' || 
              $("#answer").val() == '0' ){
            e.preventDefault();
            $("#error").html("All fields are required!").show();
          }else{
            e.preventDefault();
            $("#error").empty().hide();
            $.ajax({
                  type: "POST",
                  url: '<?php echo base_url();?>index.php/admin/save_question',
                  data: 
                  {
                    qz_qid      : '<?php echo $quizid;?>',
                    qs_question : tinyMCE.get('question').getContent(),
                    sc_sid      : $("#category").val(),
                    qs_choice1  : $("#ans_a").val(),
                    qs_choice2  : $("#ans_b").val(),
                    qs_choice3  : $("#ans_c").val(),
                    qs_choice4  : $("#ans_d").val(),
                    qs_ans      : $("#answer").val()
                  },
                  success: function(msg){
                    if(msg == "success"){
                      cleartext();
                      $("#success").html("Question successfully saved!").show();
                      setTimeout(function(){$("#success").empty().hide()},3000);
                    }
                  }
                });
          }

        });

        function cleartext()
        {
          tinymce.get('question').setContent('');
          $("#category").val('');
          $("#answer").val('');
          $("#ans_a").val('');
          $("#ans_b").val('');
          $("#ans_c").val('');
          $("#ans_d").val('');
        }

        $( "#ans_a" ).click(function(){

          $( "#dialog_a" ).dialog({
            modal: true,
            open: function ()
            {
              tinymce.init({
                selector: "#edit_a",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]

              });
              tinymce.execCommand('mceAddControl',false,'edit_a' );
              },
              close: function ()
              {
                tinyMCE.execCommand('mceRemoveControl', false, '#edit_a');
                $(this).dialog('destroy');
              },
              buttons: 
              {
                'Ok': function ()
                {
                  $("#ans_a").val(tinyMCE.get('edit_a').getContent());
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_a');
                  $(this).dialog('destroy');
                },
                'Cancel': function ()
                {
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_a');
                  $(this).dialog('destroy');
                }
              }
            }).show();

            if ( $( "#ans_a" ).val() != '')
              tinymce.get('edit_a').setContent($( "#ans_a" ).val());

          });

        $( "#ans_b" ).click(function(){

          $( "#dialog_b" ).dialog({
            modal: true,
            open: function ()
            {
              tinymce.init({
                selector: "#edit_b",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]

                });
                tinymce.execCommand('mceAddControl',false,'edit_b' );
              },
              close: function ()
              {
                tinyMCE.execCommand('mceRemoveControl', false, '#edit_b');
                $(this).dialog('destroy');
              },
              buttons: 
              {
                'Ok': function ()
                {
                  $("#ans_b").val(tinyMCE.get('edit_b').getContent());
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_b');
                  $(this).dialog('destroy');
                },
                'Cancel': function ()
                {
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_b');
                  $(this).dialog('destroy');
                }
              }
            }).show();

            if ( $( "#ans_b" ).val() != '')
              tinymce.get('edit_b').setContent($( "#ans_b" ).val());

          });

        $( "#ans_c" ).click(function(){

          $( "#dialog_c" ).dialog({
            modal: true,
            open: function ()
            {
              tinymce.init({
                selector: "#edit_c",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]

              });
              tinymce.execCommand('mceAddControl',false,'edit_c' );
              },
              close: function ()
              {
                tinyMCE.execCommand('mceRemoveControl', false, '#edit_c');
                $(this).dialog('destroy');
              },
              buttons: 
              {
                'Ok': function ()
                {
                  $("#ans_c").val(tinyMCE.get('edit_c').getContent());
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_c');
                  $(this).dialog('destroy');
                },
                'Cancel': function ()
                {
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_c');
                  $(this).dialog('destroy');
                }
              }
            }).show();
            
            if ( $( "#ans_c" ).val() != '')
              tinymce.get('edit_c').setContent($( "#ans_c" ).val());

          });

        $( "#ans_d" ).click(function(){

          $( "#dialog_d" ).dialog({
            modal: true,
            open: function ()
            {
              tinymce.init({
                selector: "#edit_d",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]

              });
              tinymce.execCommand('mceAddControl',false,'edit_d' );
              },
              close: function ()
              {
                tinyMCE.execCommand('mceRemoveControl', false, '#edit_d');
                $(this).dialog('destroy');
              },
              buttons: 
              {
                'Ok': function ()
                {
                  $("#ans_d").val(tinyMCE.get('edit_d').getContent());
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_d');
                  $(this).dialog('destroy');
                },
                'Cancel': function ()
                {
                  tinyMCE.execCommand('mceRemoveControl', false, '#edit_d');
                  $(this).dialog('destroy');
                }
              }
            }).show();

            if ( $( "#ans_d" ).val() != '')
              tinymce.get('edit_d').setContent($( "#ans_d" ).val());

          });


      });
  		
  	</script>
  </body>
</html>