<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Logs</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/question.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/stripe.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "logs";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Logs</h2>

        <table>
          <tr>
            <td width="300px">Date: <input type="text" id="datepicker"></td>
            <td width="150px"><button id="save" class="btn btn-sm btn-primary" style="margin-bottom:10px;" type="submit">Search</button></td>
        </table>

 
      <div id="logsss">
        <table class="table table-striped" align="center">
          <tr>
            <th>Name</th>
            <th>Timestamp</th>
            <th>Action</th>
          </tr>
          <?php foreach ($logs as $key => $value) :?>
          <tr>
            <td><?php echo $value["Name"];?></td>
            <td><?php echo $value["lg_date"];?></td>
            <td><?php echo $value["lg_action"];?></td>
          </tr>
          <?php endforeach;?>
        </table>
      </div>

      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
  	<script type="text/javascript">

  		$(function() {

        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });

        $( "#save" ).click(function(e){
          e.preventDefault();
          if( $( "#datepicker" ).val() != ''){
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>index.php/admin/get_logs',
                data: { date: $( "#datepicker" ).val() },
                success: function(msg){
                  $("#logsss").html(msg.split("|")[0]);
                  if(msg.split("|")[1] == "0")
                    alert("No records found!");
                }
              });
          }else{
            alert("Date Error");
          }
          
        });

      });
  		
  	</script>
  </body>
</html>