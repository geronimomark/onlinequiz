  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (17)  </h2>
    <p>&nbsp;</p>
    <p>A ship travels 60 miles due east, then adjust its course northward. After traveling 80 miles in the direction, the ship is 139 miles from its point of departure. Describe the change in bearing from point B and C. </p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <blockquote>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/17.1.jpg" width="321" height="165" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/17.2.jpg" width="200" height="16" /></p>
        <p class="style17">&nbsp;</p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/17.3.jpg" width="230" height="32" /></p>
        <p class="style17">&nbsp;</p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/17.4.jpg" width="200" height="16" /></p>
        <p class="style17">&nbsp;</p>
        <p class="style17">The bearing measured from due north from point B to point C is given by 166.15<sup>0</sup> - 90<sup>0</sup> = 76.150</p>
        <p class="style17">&nbsp;</p>
        <p class="style17">N 76.15<sup>0</sup> E or 76.15<sup>0</sup> east of north  </p>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    </div>