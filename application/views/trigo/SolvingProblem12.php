<div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (12)  </h2>
    <p>If sin x = -4/5, and cos x &lt; 0, find sin (2x), cos (2x), and tan (2x). </p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <p class="style17">&nbsp;</p>
      <blockquote>
        <p class="style17">sin x = - 4/5 ; cos x = -3/5</p>
        <p class="style17">sin<sup>2</sup>x + cos<sup>2</sup>x = 1</p>
        <p class="style17">(-4/5)<sup>2</sup> + cos<sup>2</sup>x = 1</p>
        <p class="style17">cos<sup>2</sup>x = 9/25</p>
        <p class="style17">cosx = -3/5    </p>
        <p class="style17">sin(2x) = 2sinxcosx</p>
        <p class="style17">sin(2x) = 2(-4/5)(-3/5)</p>
        <p class="style17">sin(2x) = 24/25  </p>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    </div>