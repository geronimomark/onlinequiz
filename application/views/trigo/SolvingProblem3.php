<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (3)  </h2>
    <p align="justify"> Left overshoe is 25.0 miles due south of North Waving Grass, and   Greater Metrapolitan Corn Husk is 90.0 miles due east. What is the   bearing of Corn Husk from Left Over-shoe?</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/3.1.jpg" width="290" height="194" /></p>
      <blockquote>
        <p align="left" class="style17">tan&theta; = opposite/hypotenuse</p>
        <p align="left" class="style17">tan&theta; = 90/25</p>
        <p align="left" class="style17">= 3.6 </p>
        <p align="left" class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p align="left" class="style17">&nbsp;</p>
  </div>