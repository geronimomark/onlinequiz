<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (5)  </h2>
    <p align="justify"> Solve 2sinx - 1 = 0 for x. </p>
    <p>&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="left" class="style17">2sinx - 1 = 0</p>
      <p align="left" class="style17">2 sinx = 1</p>
      <p align="left" class="style17">sin x = 1/2</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/5.1.jpg" width="188" height="131" />  </p>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p class="style17">&nbsp;</p>
  </div>