  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (18)  </h2>
    <p>&nbsp;</p>
    <p align="justify">A person standing 400 ft from the base of a mountain measures the angle elevation from the ground to the top of the mountain to be 250. The person then walks 500 ft straight back and measures the angle of elevation to now be 200. How tall is the mountain? </p>
    <p>&nbsp;</p>
    <blockquote>
      <p align="justify" class="style17"><strong>Solution:</strong></p>
      <blockquote><p align="justify" class="style17">Let assume that the ground is flat and not inclined relative to the base of the mountain. Let h be the height of the mountain, and let x be the distance from the base of the mountain to the point directly beneath the top of the mountain, as the figure shown below. </p>
        <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.1.jpg" width="193" height="111" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.2.jpg" width="100" height="32" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.3.jpg" width="130" height="15" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.4.jpg" width="139" height="32" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.5.jpg" width="120" height="16" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.6.jpg" width="220" height="15" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.7.jpg" width="300" height="16" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.9.jpg" width="250" height="32" /></p>
        <p align="left" class="style17">&nbsp;</p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/18.jpg" width="284" height="16" /></p>
      </blockquote>
    </blockquote>
    <p align="justify" class="style19">&nbsp;</p>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p align="justify" class="style19">&nbsp;</p>
    <p class="style17">&nbsp;</p>
  </div>