<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (7)  </h2>
    <p align="justify"> A hot-air balloon is flying over a dry lake when the win stops blowing. The balloon comes to a stop 450 feet above the ground at a point D as shown in figure below. A jeep following the balloon runs out of gas at a point A. The nearest service station is due north of the jeep at point B. The bearing of the bolloon from the jeep at A is N 13<sup>0</sup> E, while the bearing of the balloon from the service station at B is S 19<sup>0</sup> E. If the angle of elevation of the balloon from A is 12<sup>0</sup>, how far will the people in the jeep have to walk to reach the service station at point B? </p>
    <p align="justify">&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.1.jpg" width="345" height="127" /></p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.2.jpg" width="100" height="32" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.3.jpg" width="199" height="32" /></p>
      <p align="left" class="style17">&nbsp;</p>
      <p align="left" class="style17">Find angle ACB</p>
      <p align="left" class="style17">&nbsp;</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.4.jpg" width="231" height="16" /> </p>
      <p class="style17">&nbsp;</p>
      <p class="style17">Find AB using the law of sines </p>
      <p class="style17">&nbsp;</p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.5.jpg" width="117" height="32" /></p>
      <p class="style17">&nbsp;</p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/7.6.jpg" width="122" height="32" /></p>
      <p class="style17">&nbsp;</p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/12.jpg" width="85" height="16" /></p>
      <p class="style17">&nbsp;</p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/12.1.jpg" width="52" height="32" /></p>
      <p class="style17">&nbsp;</p>
      <p class="style17">Answer: 0.64 mile to get to the to the service station at B.</p>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    
    
    <p class="style17">&nbsp;</p>
  </div>