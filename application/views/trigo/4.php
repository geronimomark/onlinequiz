<div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">Inverse trigonometric functions </h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li>Inverse Sine, Inverse Cosine, and Inverse Tangent Functions: </li>
    </ul>
    <div align="center">
      <table width="306" border="1" bordercolor="#000000">
        <tr>
          <td width="87" bgcolor="#669966"><div align="center">Function</div></td>
          <td width="83" bgcolor="#669966"><div align="center">Domain</div></td>
          <td width="132" bgcolor="#669966"><div align="center">Range</div></td>
        </tr>
        <tr>
          <td bgcolor="#66FFCC"><div align="center">sin<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">[-1, 1] </div></td>
          <td bgcolor="#66FFCC"><p align="center">[-&pi;/2, &pi;/2]</p></td>
        </tr>
        <tr>
          <td height="30" bgcolor="#66FFCC"><div align="center">cos<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">[-1, 1] </div></td>
          <td bgcolor="#66FFCC"><div align="center">[0, &pi;]</div></td>
        </tr>
        <tr>
          <td height="31" bgcolor="#66FFCC"><div align="center">tan<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">R</div></td>
          <td bgcolor="#66FFCC"><div align="center">[-&pi;/2, &pi;/2]</div></td>
        </tr>
      </table>
      <p align="left"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="21" height="16" /><strong>Example:</strong></p>
      <p align="left">&nbsp; Evaluate sin-1 2x when x = 0.15 </p>
      <p align="left">&nbsp;  &nbsp;   &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;<em><strong>&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp; Solution:</strong></em></p>
      <p align="left"><em><strong>&nbsp; &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp; For x&nbsp;= 0.15 ; y =  sin<sup>-1</sup> (0.30), therefore sin y = 0.30</strong></em></p>
      <p align="left"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = 17.46o = 0.3047 radians</strong></em></p>
      <p align="left"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; therefore  sin-1 0.30 = 0.3047 </strong></em></p>
      <p align="left">&nbsp;</p>
      <p align="left"><u><strong>INVERSE TRIGONOMETRIC FUNCTIONS AND THEIR GRAPHS</strong></u></p>
      <ul>
        <li>
          <div align="left">The figure below, show that sine is one to one on this restricted domain and so has inverse.</div>
        </li>
      </ul>
      <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/inverse sine.jpg" width="418" height="130" /></p>
      <ul>
        <li>
          <div align="left">Inverse cosine function: </div>
        </li>
      </ul>
      <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/inverse cosine.jpg" alt="1" width="406" height="109" /></p>
      <p>1 (0.30), therefore sin y = 0.30</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = 17.46o = 0.3047 radians</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; therefore  sin-1 0.30 = 0.3047 </p>
      <p align="left">&nbsp;</p>
      <p align="left" class="style18">Figures Citation:</p>
      <ul>
        <li>
          <div align="left" class="style18">Gutafson (1982). Inverse trigonometric functions and their graphs (p. 504). Algebra and Trigonometry. USA: Prindle Weber &amp; Schmidt.</div>
        </li>
      </ul>
      <p align="left">&nbsp;</p>
      <blockquote><blockquote>&nbsp;</blockquote>
      </blockquote>
    </div>
    <p align="left">&nbsp;</p>
    <blockquote><blockquote>&nbsp;</blockquote>
    </blockquote>
  </div>