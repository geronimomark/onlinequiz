  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (20)  </h2>
    <p>&nbsp;</p>
    <p>In the circle of radius r = 2 cm, what is the length s of the arc intercepted by a central angle of measure &theta; = 1/2 rad? </p>
    <blockquote>
      <p align="justify" class="style17"><strong>Solution:</strong></p>
      <p align="justify" class="style17">&nbsp;</p>
      <blockquote>
        <p align="justify" class="style17">s = r&theta; = (2)(1.2) = 2.4 cm </p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p align="justify" class="style17">&nbsp; </p>
    <p class="style10">&nbsp;</p>
    <p class="style17">&nbsp;</p>
  </div>