<div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (16)  </h2>
    <p>&nbsp;</p>
    <p align="justify">Assuming that the surface of the earth is a sphere, any point on the earth can be thought of as an object traveling on a circle which completes one revolution in (approximately) 24 hours. The path traced out by the point during this 24 hour period is the latitude of that point. Lakeland Community College is at 41.628<sup>0</sup> north altitude, and it can shown that the radius of the earth at this latitude is approximately 2960 miles. Find the linear velocity, in miles per hour, of Lakeland Community College as the world turns. </p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <blockquote>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/16.1.jpg" width="45" height="16" /></p>
        <p class="style17">The earth makes one revolution in 24 hours, and one revolution is 2&pi;</p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/16.2.jpg" width="193" height="32" /> </p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/16.3.jpg" width="250" height="32" /></p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    <blockquote>
      <blockquote>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
  </div>