<div class="style1" id="content">
    <h2 align="center" class="style6">TRIGONOMETRY</h2>
    <h2 align="center">referenceS</h2>
    <p align="justify" class="style4">TRIGONOMETRIC FUNCTIONS </p>
    <div align="justify">
      <ul>
        <li>Definition of Trigonometry. Retrieved from: Wikipedia: en.wikipedia.org/wiki/Trigonometry</li>
        <li>Lial &amp; Hornsby (2000). Trigonometric Functions (p. 433). College algebra and trigonometry. USA: Addison - Wesley. </li>
        <li>Cameron (1971). Trigonometric Functions (p. 235). Algebra and trigonometry, with analytic geometry. New York : Holt, Rinehart and Winston.</li>
      </ul>
    </div>
    <p align="justify"><span class="style4">TRIGONOMETRIC IDENTITIES AND CONDITIONAL FUNCTIONS</span></p>
    <ul>
      <li>Barnett (1984). Trigonometric Identities (pp. 401-409). College Algebra, Trigonometry, and Analytic Geometry. USA: Mc Graw Hill.</li>
      <li>Cameron (1971). Trigonometric Identities (p. 222). Algebra and trigonometry, with analytic geometry. New York : Holt, Rinehart and Winston. </li>
    </ul>
    <p align="justify"><span class="style4">TRIGONOMETRIC FUNCTIONS OF ANY ANGLE </span></p>
    <div align="justify">
      <ul>
        <li>Gutafson (1993). Trigonometric functions (p. 209, 215). College Algebra and Trigonometry (4th Ed.). USA: Brooks/Cole Pub. Co. </li>
      </ul>
    </div>
    <p align="justify"><span class="style4">INVERSE TRIGONOMETRIC FUNCTION</span></p>
    <div align="justify">
      <ul>
        <li>Gutafson (1982). Right triangle trigonometry (pp. 417, 516). Concept of Algebra and Trigonometry. USA: Prindle Weber &amp; Schmidt.</li>
      </ul>
    </div>
    <p align="justify"><span class="style4">APPLICATIONS OF TRIGONOMETRY </span></p>
    <div align="justify">
      <ul>
        <li>Zeagar, J., Stitz, C. (2013). Applications of trigonometry (Chapter 8). College Trigonometry. Retrieved from www.stitz-zeager.com: www.stitz-zeager.com/szct07042013.pdf.</li>
      </ul>
    </div>
    <p align="justify"><span class="style4">OTHER TOPICS</span></p>
    <div align="justify">
      <ul>
        <li>Young (2013). Additional topics in Trigonometry (Chapter 8). Algebra &amp; Trigonometry. USA: John Wiley &amp; Sons Inc.</li>
      </ul>
    </div>
    <p align="justify" class="style4">&nbsp;</p>
  </div>