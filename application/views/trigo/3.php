<div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">right triangle </h2>
    <ul>
      <li><span class="style5">Vector</span>: is a directed line segment. The direction of the   vector is indicated by the angle it makes with some convenient reference   line</li>
      <li><span class="style5">Magnitude</span>: is the length of the line.</li>
    </ul>
    <p align="left"><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" />Examples:</strong></p>
    <p align="left">1. A 3000-lb. car sits on a 23o incline. What force would be   required to prevent the car from rolling down the hill? With what force   is it held to the roadway? </p>
    <blockquote>
      <p align="left" class="style19">Solution:</p>
      <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/right triangle 1.1.jpg" width="249" height="152" /></p>
      <blockquote>
        <blockquote>
          <p class="style19">sin 23<sup>o</sup> = |t|/3000</p>
          <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |t|&nbsp; = 3000 sin 23<sup>o</sup> </p>
          <p class="style19">Also,</p>
          <p class="style19">cos 23o&nbsp; = |n|/3000</p>
          <p class="style19">|n| = 3000 cos  23<sup>o</sup></p>
          <p class="style19">|n| = 2760</p>
        </blockquote>
      </blockquote>
    </blockquote>
    <p class="style19">2. A person in a boat on a lake measures the angle of elevation of a mountain peak and above the boat and finds it to be 18.7o. After Sailing 1950 ft. directly toward the peak, the person finds that the angle of elevation of the peak is then 24.3o. How high above the boat is the peak? </p>
    <blockquote>
      <p align="left" class="style19">Solution:</p>
      <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/right triangle 1.2.jpg" alt="1" width="203" height="224" /></p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp; cot 24.3<sup>o</sup> = d/h</p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d&nbsp; = h cot 24.3o </p>
      <p class="style19">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; cot 24.3<sup>o</sup> = (1950 + d)/h </p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d = h cot 24.3<sup>o</sup>&nbsp;- 1,950</p>
      <p class="style19">&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Solving for h:</p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h cot 18.7<sup>o</sup> - 1,950 = h cot 24.3<sup>o</sup></p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h&nbsp; cot 18.7<sup>o</sup>- h cot 24.3<sup>o</sup> = 1,950</p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;h = 1,950 / (cot 18.7<sup>o</sup> - cot 24.3<sup>o</sup>)</p>
      <p class="style19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h = 2,640</p>
      <p align="left" class="style19 style19">&nbsp;</p>
    </blockquote>
    <blockquote>
      <blockquote></blockquote>
    </blockquote>
    <div align="center"><blockquote>&nbsp;</blockquote>
    </div>
    <p align="left">&nbsp;</p>
    <blockquote><blockquote>&nbsp;</blockquote>
    </blockquote>
  </div>