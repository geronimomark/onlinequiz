  <div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">trigonometric functions </h2>
    <h2 align="center"></h2>
    <ul>
      <li><span class="style5">Trigonometry</span> is derived from the greek words trigon (three angles) and metron (measure).</li>
      <li><span class="style5">Trigonometry</span> is the branch of mathematics which deals with triangles,   particularly triangles in a plane where one angle of the triangle is 90   degrees </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/Intro 1.1.jpg" width="195" height="147" /></p>
    <ul>
      <li><span class="style5">Trigonometric functions </span>(sine, cosine, tangent, cotangent, secant, and cosecant) can be defined as right triangle rations. Ratio of sides of right triangle. </li>
    </ul>
    <p>&nbsp;</p>
    <p class="style18"><u>ANGLES AND DEGREE MEASURE</u></p>
    <p class="style18">&nbsp;</p>
    <ul>
      <li>A <span class="style5">line</span> is a straight path connecting two points . </li>
      <li>The portion of a line between two points is called<span class="style5"> line segment</span>.</li>
      <li>A<span class="style5"> ray</span> is part of the line that starts at one point and extends to infinity.</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/angle degree.jpg" width="255" height="117" /></p>
    <ul>
      <li>In geometry,<span class="style5"> angle</span> is formed when two rays share the same end point. The common end point is called the <span class="style5">vertex</span>. </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/angle.jpg" width="175" height="135" /></p>
    <ul>
      <li><span class="style5">Right angle</span>, angle measuring exactly 90<sup>0</sup>.</li>
      <li><span class="style5">Straight angle</span>, angele measuring exactly 180<sup>0</sup></li>
      <li><span class="style5">Acute angle</span>, angle measuring greater than 0<sup>0</sup> but less than 90<sup>0</sup>.</li>
      <li><span class="style5">Obtuse angle</span>, angle measuring greater than 90<sup>0</sup> but less than 180<sup>0</sup></li>
    </ul>
    <p>&nbsp;</p>
    <p class="style18"><u>TRIGONOMETRIC FUNCTIONS </u></p>
    <p class="style18">&nbsp;</p>
    <ul>
      <li>Triangle having vertices at O, P, and Q.</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/trigo func 1.1.jpg" width="231" height="183" /></p>
    <ul>
      <li>Distance Formula:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/trigo/images/distance formula.jpg" width="107" height="20" /></p>
    </blockquote>
    <ul>
      <li>Trigonometric Functions:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/trigo/images/trigo func 1.2.jpg" width="216" height="120" /></p>
    </blockquote>
    <ul>
      <li>Trigonometric Ratios using Quadrant I and II</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/trigo ratio.jpg" width="352" height="205" /></p>
    <ul>
      <li>Unit circle: the circle which has a radius 1. </li>
    </ul>
    <ul>
      <li>Trigonometric Function for Quadrant Angles </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/quadrant angles.jpg" width="345" height="146" /></p>
    <ul>
      <ul>
        <li>The values given in this table can also be found with a calculator that has trigonometric function keys.</li>
      </ul>
    
      <p>&nbsp;</p>
      <li>Signs of Functions Values </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/sign of func.jpg" width="400" height="148" /></p>
    <p align="center">&nbsp;</p>
    <ul>
      <li>Ranges of Trigonometric Functions: </li>
    </ul>
    <p>&nbsp;&nbsp;&nbsp;&nbsp; 1. -1 is &lt; or equal to sin &theta; &lt; or equal to 1 and -1 is &lt; or equal to cos &theta;  is &lt; or equal to 1.</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp; 2. tan &theta; and cot &theta; may be equal to any real numbers.</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp; 3. sec &theta; is &lt; or equal to -1 or sec &theta; is &gt; or equal to 1 and csc &theta; is &lt; or equal to -1&nbsp;or csc &theta; is </p>
    <p>&nbsp;&nbsp;  &nbsp;  &nbsp;  &nbsp;&nbsp; is &gt; or equal to 1 (sec &theta; and csc &theta; are never between -1 and 1). </p>
    <p>&nbsp;</p>
    <ul>
      <li>Pythagorean Identities      </li>
    </ul>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. sin<sup>2</sup>&theta; + cos<sup>2</sup>&theta; = 1&nbsp;&nbsp;</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. tan<sup>2</sup>&theta; + 1 = sec<sup>2</sup>&theta;</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. 1+ cot<sup>2</sup>&theta; = csc<sup>2</sup>&theta;</p>
    <p>&nbsp;</p>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" />Examples</strong>:</p>
    <p>1. Find sin&theta; and cos&theta;, if tan&theta; = 4/3 and &theta; is in quadrant III. </p>
    <blockquote>
      <p><span class="style19">Solution:</span></p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex1.1.jpg" width="110" height="15" /></p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex1.2.jpg" width="178" height="31" /></p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex1.3.jpg" width="82" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex1.5.jpg" width="74" height="32" /></p>
        <p class="style19">but <img src="<?php echo base_url();?>assets/trigo/images/ex1.6.jpg" width="87" height="16" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex1.7.jpg" width="115" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex1.8.jpg" width="76" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex1.9.jpg" width="56" height="32" /></p>
        <p class="style19">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style19">2. A tower casts a shadow 208.5 ft long when the angle of elevation&nbsp;of the   tower as seen from the end of the shadow is 250 40' (This angle is also   the angle of elevation of the sun. Find the height of the tower.</p>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex2.1.jpg" width="250" height="161" /></p>
    <blockquote>
      <p class="style19">Solution: </p>
      <blockquote>
        <p class="style19">a = 208.5 tan25o401</p>
        <p class="style19">a = 208.2 (0.4806) = 100.2 ft. </p>
      </blockquote>
      <p align="left" class="style19">&nbsp;</p>
    </blockquote>
    <p class="style19">&nbsp;</p>
    <p class="style19">&nbsp;</p>
  </div>