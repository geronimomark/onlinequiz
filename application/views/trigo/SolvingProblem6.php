<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (6)  </h2>
    <p align="justify"> If sin A = 3/5 with A in QI and cos B = -5/13 with B in QIII, find tan (A + B) by using the formula</p>
    <p align="justify">&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.1.jpg" width="164" height="32" /></p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.2.jpg" width="154" height="60" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.3.jpg" width="74" height="30" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.4.jpg" width="158" height="60" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.5.jpg" width="72" height="32" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.6.jpg" width="324" height="60" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.7.jpg" width="177" height="60" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.7.jpg" width="178" height="60" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/6.8.jpg" width="142" height="32" /></p>
    </blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p class="style17">&nbsp;</p>
  </div>