<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (9)  </h2>
    <p align="justify">&nbsp; </p>
    <p align="justify">Triangle ABC is right triangle with right angle at C. CD is perpendicular to AB. BC = 4 and CD = 1. Find the area of the triangle ABC. </p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.1.jpg" width="225" height="144" /></p>
      <p align="left" class="style17">Solving for angle B: </p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.2.jpg" width="77" height="32" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.3.jpg" width="93" height="15" /></p>
      <p align="left" class="style17">Solving for side AC</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.5.jpg" width="77" height="32" /> </p>
      <p align="left" class="style17">AC = 1.0328</p>
      <p align="left" class="style17">Solving for the area of triangle:</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.8.jpg" width="169" height="32" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/9.9.jpg" width="125" height="32" /></p>
      <p align="left" class="style17">A = 2.0656 sq. units </p>
      <p align="left" class="style17">&nbsp;</p>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p class="style17">&nbsp;</p>
  </div>