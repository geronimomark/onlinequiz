  <div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (2)  </h2>
    <p align="justify">Assume that &theta; is in QII and that sin&#415; = 4/5. Use the eight fundamental   relationships of trigonometric functions to find the values of the other   five trigonometric functions </p>
    <p>&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.1.jpg" width="136" height="45" /></p>
      <p align="left" class="style17">but sin<sup>2</sup>&theta; + sin<sup>2</sup>&theta; = 1 </p>
      <p><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.2.jpg" width="116" height="18" /></p>
      <p class="style17">but &theta; is in QII </p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.3.jpg" width="118" height="32" /></p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.4.jpg" width="124" height="32" /></p>
      <p class="style17">but sec&theta; is reciprocal of cos&theta;</p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.5.jpg" width="64" height="32" />  </p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.6.jpg" width="146" height="55" /></p>
      <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/2.7.jpg" width="58" height="32" /></p>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p class="style17">&nbsp;</p>
  </div>