<div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">TRIgonometric function of any angle </h2>
    <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="14" /><strong>Example:</strong></p>
    <p class="style19">Use a protractor, compass, and ruler to find, approximately, the sine, cosine and tangent angle of 107<sup>o</sup></p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex5.1.jpg" width="224" height="178" /></p>
      <blockquote>
        <p class="style19">Cos 107<sup>o</sup>&nbsp;&nbsp;&nbsp; =&nbsp; OA/OP = OA/1 = OA</p>
        <p class="style19">Tan 107<sup>o</sup>&nbsp;&nbsp;&nbsp;&nbsp; =&nbsp; BQ/OB = BQ/-1 = - BQ </p>
        <p class="style19">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style19">&nbsp;</p>
    <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="14" /><strong>Example:</strong></p>
    <p class="style19">Left overshoe is 25.0 miles due south of North Waving Grass, and Greater   Metropolitan Corn Husk is 90.0 miles due east. What is the bearing of   Corn Husk from Left Over- Shoe? </p>
    <blockquote>
      <p><span class="style19">Solution:</span></p>
      <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/ex6.1.jpg" width="279" height="212" /></p>
      <blockquote>
        <p align="left" class="style19">Tan &#415; = opposite side / adjacent side = 90/25 = 3.6 </p>
      </blockquote>
    </blockquote>
  </div>