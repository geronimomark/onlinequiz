  <div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (1)  </h2>
    <p align="justify">A satellite orbiting the earth passes directly overhead at observation stations in Phoenix and Los Angeles, 340 mi. apart. At an instant when the satellite is between these two stations, its angle of elevation is simultaneously observed to 600 at Phoenix and 750 at Los Angeles. How far is the satellite from Los Angeles? </p>
    <p>&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/1.1.jpg" width="264" height="185" /></p>
      <p align="left" class="style17">&nbsp;</p>
      <p align="left" class="style17">Find the distance b</p>
      <blockquote>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/1.2.jpg" width="100" height="32" /> </p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/1.3.jpg" width="113" height="33" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/1.4.jpg" width="107" height="32" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/1.5.jpg" width="45" height="16" /></p>
      </blockquote>
    </blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p class="style17">&nbsp;</p>
  </div>