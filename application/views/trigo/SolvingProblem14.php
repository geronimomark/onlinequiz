<div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (14)  </h2>
    <p>&nbsp;</p>
    <p>At what distance may a mountain 1 mile high be seen at sea, if the earth's radius is 3960 mile? </p>
    <p>&nbsp;</p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <blockquote>
        <p class="style17">Let s = distance in miles, earth between the radius to the mountain and the radius to the point at sea be &theta; </p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.1.jpg" width="215" height="30" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.2.jpg" width="175" height="30" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.3.jpg" width="150" height="30" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.4.jpg" width="150" height="15" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.6.jpg" width="130" height="30" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.7.jpg" width="75" height="15" /></p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p class="style17">&nbsp;</p>
    <p class="style17">&nbsp;</p>
  </div>