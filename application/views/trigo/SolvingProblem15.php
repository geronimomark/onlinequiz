  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (15)  </h2>
    <p>&nbsp;</p>
    <p>Given sin &alpha; = 3/5 and cos &beta; = 5/13; find sin (&alpha; + &beta;) </p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <blockquote>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.8.jpg" width="250" height="15" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/14.9.jpg" width="250" height="15" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/15.1.jpg" width="250" height="32" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/15.2.jpg" width="250" height="32" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/15.3.jpg" width="242" height="25" /></p>
        <p class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/15.4.jpg" width="241" height="27" /></p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p class="style17">&nbsp;</p>
    <p class="style17">&nbsp;</p>
    <p class="style17">&nbsp;</p>
    <p class="style17">&nbsp;</p>
  </div>