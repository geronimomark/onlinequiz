  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (22)  </h2>
    <p>&nbsp;</p>
    <p>Represent the complex number -2 - i in trigonometric form. </p>
    <p>&nbsp;</p>
    <blockquote>
      <p align="justify" class="style17"><strong>Solution:</strong></p>
      <p align="justify" class="style17">&nbsp;</p>
      <blockquote>
        <p align="justify" class="style17">Let z = -2 - i = x + yi, so that x = -2 and y = -1. Then &theta; is in QIII, as shown in the figure. So since tan&theta; = y/x = 1/2, &theta; = 206.6<sup>0</sup>.  </p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/22.3.jpg" width="250" height="17" /></p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/22.1.jpg" width="240" height="16" /> </p>
        <p align="justify" class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    <p align="justify" class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p align="justify" class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
  </div>