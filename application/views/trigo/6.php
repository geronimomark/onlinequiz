<div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">FORMULAS</h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li class="style19">Pythagorean Identities:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.jpg" width="525" height="195" /></p>
    <ul>
      <li class="style19">Beyond the unit circle:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.2.jpg" width="391" height="187" /></p>
    <ul>
      <li class="style19">Inverse Sine, Inverse Cosine, and Inverse Tangent Functions: </li>
    </ul>
    <div align="center">
      <table width="306" border="1" bordercolor="#000000">
        <tr>
          <td width="87" bgcolor="#669966"><div align="center">Function</div></td>
          <td width="83" bgcolor="#669966"><div align="center">Domain</div></td>
          <td width="132" bgcolor="#669966"><div align="center">Range</div></td>
        </tr>
        <tr>
          <td bgcolor="#66FFCC"><div align="center">sin<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">[-1, 1] </div></td>
          <td bgcolor="#66FFCC"><p align="center">[-&pi;/2, &pi;/2]</p></td>
        </tr>
        <tr>
          <td height="30" bgcolor="#66FFCC"><div align="center">cos<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">[-1, 1] </div></td>
          <td bgcolor="#66FFCC"><div align="center">[0, &pi;]</div></td>
        </tr>
        <tr>
          <td height="31" bgcolor="#66FFCC"><div align="center">tan<sup>-1</sup></div></td>
          <td bgcolor="#66FFCC"><div align="center">R</div></td>
          <td bgcolor="#66FFCC"><div align="center">[-&pi;/2, &pi;/2]</div></td>
        </tr>
      </table>
    </div>
    <div align="center"></div>
    <ul>
      <li class="style19">Sum and Difference Identities for Cosine: </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.3.jpg" width="410" height="98" /></p>
    <ul>
      <li><span class="style19">Cofunction Identities:</span></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.4.jpg" width="469" height="127" /></p>
    <ul>
      <li class="style19">Sum and Difference for Sine: </li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.5.jpg" width="410" height="97" /></p>
    <ul>
      <li><span class="style19">Double Angle Identities</span><span class="style19">:</span></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.6.jpg" width="214" height="185" /></p>
    <ul>
      <li><span class="style19">Power Reduction </span><span class="style19">:</span></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.7.jpg" width="424" height="72" /></p>
    <ul>
      <li><span class="style19">Half Angle Formulas</span><span class="style19">:</span></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f1.9.jpg" width="303" height="204" /></p>
    <ul>
      <li><span class="style19">Product of Sum Formulas (for all angle &alpha; and &beta; </span>):</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f2.jpg" width="442" height="168" /></p>
    <ul>
      <li><span class="style19">Law of Sines</span>:</li>
    </ul>
    <div align="center"><img src="<?php echo base_url();?>assets/trigo/images/a1.6.jpg" alt="1" width="312" height="139" /></div>
    <ul>
      <li><span class="style19">Area of Triangle</span>:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/a2.7.jpg" width="466" height="68" /></p>
    <ul>
      
      <li><span class="style19">Heron's Formula</span>:</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/b1.5.jpg" width="374" height="67" /></p>
    <blockquote>
      <p align="right" class="style21">&nbsp;</p>
      <p align="right" class="style21"><a href="<?php echo base_url();?>index.php/lecture/trigo/7">Next>></a></p>
    </blockquote>
    <p align="left" class="style22">Reference: Zeagar, J., Stitz, C. (2013). Foundation of trigonometry (Chapter 10), Application of trigonometry (Chapter 11). College Trigonometry. Retrieved from www.stitz-zeager.com: www.stitz-zeager.com/szct07042013.pdf.</p>
    <p align="left" class="style20">&nbsp;</p>
    <blockquote><blockquote>&nbsp;</blockquote>
    </blockquote>
  </div>