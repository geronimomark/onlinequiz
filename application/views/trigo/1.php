  <div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">Trigonometric identities and conditional functions </h2>
    <ul>
      <li>Basic Trigonometric Identities:</li>
    </ul>
    <p>1. Reciprocal Identities</p>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/reciprocal identities.jpg" width="325" height="28" /></p>
    <blockquote>&nbsp;</blockquote>
    <p class="style19">2. Quotient Identities</p>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/quotient identities.jpg" width="210" height="30" /> </p>
    <p align="center" class="style19">&nbsp;</p>
    <p align="left" class="style19">3. Identities for Negatives </p>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/identities for negatives.jpg" width="280" height="30" /></p>
    <p align="center" class="style19">&nbsp;</p>
    <p align="left" class="style19">4. Phytagorean Identites</p>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/phytagorean identites.jpg" width="300" height="29" /> </p>
    <p class="style19">&nbsp;</p>
    <p class="style19"><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="14" /><strong>Examples:</strong></p>
    <p class="style19">&nbsp;</p>
    <p class="style19">1. Establish the identity: cotxcosx + sinx = cscx</p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p class="style19">Proof: <img src="<?php echo base_url();?>assets/trigo/images/ex3.1.jpg" width="241" height="32" /> (Quotient Identity) </p>
      <blockquote>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex3.2.jpg" width="112" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex3.3.jpg" width="118" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex3.4.jpg" width="45" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex3.5.jpg" width="45" height="16" /></p>
        <p class="style19">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style19">2. Establish the identity <img src="<?php echo base_url();?>assets/trigo/images/ex4.1.jpg" width="183" height="16" /></p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p class="style19">Proof</p>
      <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex4.2.jpg" width="236" height="32" /></p>
      <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex4.3.jpg" width="234" height="32" /></p>
      <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex4.4.jpg" width="203" height="32" /></p>
    </blockquote>
  </div>