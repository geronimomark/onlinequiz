  <div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (10)  </h2>
    <p align="justify">&nbsp; </p>
    <p align="justify">Points A and B 1000 m apart are plotted on a straight highway running   east and west. From A, the bearing of tower C is 32 degrees W of N and   from B, the bearing of C is 26 degrees N of E. Approximate the shortest   distance of the tower from the highway.</p>
    <p>&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.1.jpg" width="260" height="205" /></p>
      <p align="left" class="style17">d = shortest distance of the tower from the highway</p>
      <p align="left" class="style17">Solving for triangle BCD  </p>
      <blockquote>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.2.jpg" width="91" height="32" /></p>
      </blockquote>
      <p align="left" class="style17">Solving triangle ACD:</p>
      <blockquote>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.3.jpg" width="83" height="15" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.4.jpg" width="45" height="15" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.5.jpg" width="125" height="32" /></p>
        <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/10.6.jpg" width="145" height="32" /></p>
        <p align="left" class="style17">Substitute eq. (1) in eq. (2)</p>
        <p align="left" class="style17">d = 373.83 m  </p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    </div>