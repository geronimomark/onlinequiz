  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (19)  </h2>
    <p>&nbsp;</p>
    <p>An observer at the top of the mountain 3 miles above sea level measures an angle of depression of 2.23<sup>0</sup>to the ocean horizon. Use this to estimate the radius of the earth. </p>
    <blockquote>
      <p align="justify" class="style17"><strong>Solution:</strong></p>
      <blockquote>
        <p align="justify" class="style17">Assume that the earth is a sphere. Let r be the radius of the earth. Let the point A represent the top of the mountain, and let H be the ocean horizon in the line of sight from A, as shown in figure below. Let O be the center of the earth, and let B be a point on the horizontal line of sight from A. (i.e. on the line perpendicular to OA) Let &theta; be the angle OAH.</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/19.jpg" width="160" height="168" /> </p>
        <p align="justify" class="style17">Since A is 3 miles above sea level, OA = r + 3. Also, OH = r. Since AB is perpendicular to OA we have, OAB = 90<sup>0</sup>, so we have OAH = 90<sup>0</sup> - 2.23<sup>0</sup> = 87.77<sup>0</sup> </p>
        <p align="justify" class="style17">We see that the line through A and H is a tangent line to the surface of the earth (considering the surface as the circle of radius r through H). The angle OHA = 90<sup>0</sup>.</p>
        <p align="justify" class="style17">Triangle OAH add up to 180<sup>0</sup>, we have &theta; = 180<sup>0</sup> - 90<sup>0</sup> - 87.77<sup>0</sup> = 2.23<sup>0</sup></p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/19.1.jpg" width="110" height="32" /> </p>
        <p align="justify" class="style17">&nbsp;</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/19.2.jpg" width="110" height="32" /></p>
        <p align="justify" class="style17">Solving for r:</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/19.3.jpg" width="240" height="16" /> </p>
        <p align="justify" class="style17">This answer is very close to the earth's actual (mean) radius of 3956.6 miles</p>
        <p align="justify" class="style17">&nbsp; </p>
      </blockquote>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p align="justify" class="style10"></p>
    <p align="justify" class="style10">&nbsp;</p>
    <p></p>
    <p class="style17">&nbsp;</p>
    <p class="style17">&nbsp;</p>
  </div>