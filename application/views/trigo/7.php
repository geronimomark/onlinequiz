  <div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">FORMULAS</h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li class="style19">Degree to radian/ radian to degree: </li>
    </ul>
    <p align="left"><img src="<?php echo base_url();?>assets/trigo/images/f2.2.jpg" width="521" height="95" /></p>
    <ul>
      <li><span class="style19">Commonly used angles in radian:</span></li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f2.4.jpg" width="89" height="83" /></p>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/f2.3.jpg" width="472" height="184" /></p>
    <blockquote>
      <p align="right" class="style21"><a href="<?php echo base_url();?>index.php/lecture/trigo/6">&lt; Previous </a></p>
    </blockquote>
    <p align="justify" class="style20">Reference: Zeagar, J., Stitz, C. (2013). Foundation of trigonometry (Chapter 10), Application of trigonometry (Chapter 11). College Trigonometry. Retrieved from www.stitz-zeager.com: www.stitz-zeager.com/szct07042013.pdf.</p>
    <p align="justify" class="style20">&nbsp;</p>
    <blockquote><blockquote>&nbsp;</blockquote>
    </blockquote>
  </div>