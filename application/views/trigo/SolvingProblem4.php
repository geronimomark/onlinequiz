<div class="style1" id="content">
    <div class="style1" id="div">
      <h2 align="center">PRACTICE PROBLEM (4) </h2>
      <p align="justify"> Scott knows that the moon is about 237,000 miles from the earth, but   he has forgotten its diameter. If the angle between his lines of sight   to either side of the moon is 0.52<sup>o</sup>&nbsp;how can Scott estimate its diameter? </p>
      <p><strong>Solution:</strong></p>
      <blockquote>
        <p align="center" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/4.1.jpg" width="195" height="104" /></p>
        <blockquote>
          <p align="left" class="style17">&pi; = 180<sup>0</sup></p>
          <p align="left" class="style17">&pi;/ 180<sup>0</sup> = 1<sup>0</sup></p>
          <p align="left" class="style17">0.52(&pi;/ 180<sup>0</sup> ) = 0.52(1<sup>0</sup> )</p>
          <p align="left" class="style17">0.0091 = 0.52 <sup>0</sup></p>
          <p align="left" class="style17">s = r&theta; = 237,000 (0.0091) = 2160 miles </p>
          <p align="left" class="style17">&nbsp;</p>
        </blockquote>
      </blockquote>
      <blockquote>
        <blockquote>&nbsp;</blockquote>
      </blockquote>
      <?php $this->load->view("template/navlinks_trigo");?>
      
      <p align="left" class="style17">&nbsp;</p>
    </div>
    <h2 align="center" class="style16">&nbsp;</h2>
  </div>