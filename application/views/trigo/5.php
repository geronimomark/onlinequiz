<div class="style1" id="content">
    <h2 align="center" class="style17">TRIGONOMETRY</h2>
    <h2 align="center">ADDITIONAL TOPICS </h2>
    <span class="style20"><u>SOLVING OBLIQUE TRIANGLES</u></span>
    <ul>
      <li>An oblique triangle is any triangle that does not have a right angle.</li>
    </ul>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/oblique triange.jpg" width="308" height="221" /></p>
    <ul>
      <li>Angle alpha &alpha;, opposite side a.</li>
      <li>Angle bheta &beta;, opposite side b.</li>
      <li>Angle gamma &gamma;, opposite side c.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
      <li>The law of sines:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/trigo/images/sinlaw.jpg" width="148" height="32" /></p>
    </blockquote>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="18" />Example:</strong></p>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex 6.1.jpg" width="200" height="167" /></p>
    <blockquote>
      <p align="left" class="style19">Solution:</p>
      <blockquote>
        <p align="left" class="style19">Find &beta;</p>
        <p align="left" class="style19">The sum of measure of the angles in triangle is 180<sup>0</sup></p>
        <p align="left" class="style19">&alpha; =  110<sup>0</sup> and &gamma; = 33<sup>0</sup></p>
        <p class="style19">&alpha; + &beta; + &gamma; =  <span class="style19">180<sup>0</sup></span></p>
        <p class="style19">&beta; = 37 <sup>0</sup></p>
        <p align="left" class="style19">Find b</p>
        <p align="left" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex6.2.jpg" width="83" height="32" /> </p>
        <p align="left" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex6.3.jpg" width="68" height="32" /></p>
        <p align="left" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex6.4.jpg" width="70" height="32" /> </p>
        <p align="left" class="style19">b = 4.5m</p>
        <p align="left" class="style19">Find c  </p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex6.5.jpg" width="74" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex6.6.jpg" width="93" height="32" /></p>
        <p class="style19">c = 4.1m </p>
      </blockquote>
    </blockquote>
    <p class="style21"><u>APPLICATIONS</u></p>
    <p><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="18" />Example:</strong></p>
    <p>The Tower of Pisa was originally  built 56 meters tall. Because of poor soil in the foundation, it started to lean. At a distance of 44 meters from the base of the tower, the angle of elevation is 55<sup>0</sup>. How much is the Tower of Pisa leaning away from the vertical position?</p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p align="center" class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex 7.1.jpg" width="178" height="148" /></p>
      <blockquote>
        <p align="left" class="style19">&beta; = 55<sup>0</sup>, c = 44 meters, and b = 56 meters</p>
        <p align="left" class="style19">Solving sin&gamma;</p>
        <p align="left"><img src="<?php echo base_url();?>assets/trigo/images/ex7.3.jpg" width="95" height="32" />  </p>
        <p><img src="<?php echo base_url();?>assets/trigo/images/ex7.3.jpg" width="95" height="32" /> </p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex7.4.jpg" width="95" height="32" /></p>
        <p class="style19"><img src="<?php echo base_url();?>assets/trigo/images/ex7.5.jpg" width="115" height="32" /></p>
        <p class="style19">sin&gamma; = 0.643619463</p>
        <p class="style19">&gamma; = 40<sup>0</sup>  </p>
        <p class="style19">Find &alpha;</p>
        <p class="style19">&alpha; + &gamma; + &beta; = 180<sup>0</sup></p>
        <p class="style19">&alpha; = 85<sup>0</sup>  </p>
      </blockquote>
    </blockquote>
    <p class="style19">&nbsp;</p>
    <p class="style21"><u>LAW OF COSINES</u></p>
    <ul>
      <li>a<sup>2</sup> = b<sup>2</sup> + c<sup>2</sup> - 2bccos&alpha;</li>
      <li>b<sup>2</sup> = a<sup>2</sup> + c<sup>2</sup> - 2accos&beta;</li>
      <li>c2 = a<sup>2</sup> + b<sup>2</sup> - 2abcos&gamma; </li>
    </ul>
    <p><span class="style21"><u>AREA OF TRIANGLE</u></span></p>
    <blockquote>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/trigo/images/area triangle.jpg" width="71" height="32" /></p>
      </blockquote>
    </blockquote>
    <ul>
      <li>Using Heron's Formula: 
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/trigo/images/herons.jpg" width="165" height="18" /></p>
        </blockquote>
        <ul>
          <li>S = semiperimeter</li>
        </ul>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/trigo/images/herons2.jpg" width="86" height="32" /></p>
        </blockquote>
      </li>
    </ul>
  </div>