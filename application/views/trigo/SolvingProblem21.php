  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (21)  </h2>
    <p>&nbsp;</p>
    <p align="justify">A rope is fastened to a wall in two places 8 ft apart at the same height. A cylindrical container with a radius of 2 ft is pushed away from the wall as far as it can go while being held in by the rope, as in figure below which shows the top view. If the center of the container is 3 feet away from the point on the wall midway between the ends of the rope, what is the length L of the rope? </p>
    <p>&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.1.jpg" width="124" height="153" /></p>
    <p>&nbsp;</p>
    <blockquote>
      <p align="justify" class="style17"><strong>Solution:</strong></p>
      <p align="justify" class="style17">&nbsp;</p>
      <blockquote>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.2.jpg" width="100" height="15" /></p>
        <p align="justify" class="style17">Hyphothenuse:</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.3.jpg" width="250" height="16" /></p>
        <p align="justify" class="style17">Using pythagorean theorem: </p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.4.jpg" width="250" height="16" /></p>
        <p align="justify" class="style17">The arc BC has length BE * &theta; , where &theta; = BEC is the supplement of AED + AEB. </p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.5.jpg" width="210" height="30" /></p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.6.jpg" width="260" height="30" /></p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.7.jpg" width="430" height="16" /></p>
        <p align="justify" class="style17">Converting to radians:</p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.8.jpg" width="180" height="32" /> </p>
        <p align="justify" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/21.9.jpg" width="450" height="20" /></p>
        <p align="justify" class="style17">&nbsp; </p>
      </blockquote>
    </blockquote>
    <p class="style10">&nbsp;</p>
    <p class="style17">&nbsp;</p>
    <?php $this->load->view("template/navlinks_trigo");?>
    </div>