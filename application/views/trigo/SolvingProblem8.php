<div class="style1" id="content">
    <h2 align="center" class="style16">trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (8)  </h2>
    <p align="justify">&nbsp; </p>
    <p align="justify">Three times the sine of certain angle is twice the square of the cosine of the same angle. Find the angle. </p>
    <p align="justify">&nbsp;</p>
    <p><strong>Solution:</strong></p>
    <blockquote>
      <p align="left" class="style17">From:</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.1.jpg" width="130" height="15" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.2.jpg" width="125" height="16" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.3.jpg" width="165" height="16" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.4.jpg" width="166" height="16" /></p>
      <p align="left" class="style17">Using quadratic formula:</p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.5.jpg" width="183" height="32" /> </p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.6.jpg" width="84" height="15" /></p>
      <p align="left" class="style17"><img src="<?php echo base_url();?>assets/trigo/images/Problem Solving/8.7.jpg" width="66" height="16" /></p>
    </blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    
    <p align="left" class="style17">&nbsp;</p>
  </div>