  <div class="style1" id="content">
    <h2 align="center" class="style16">Trigonometry</h2>
    <h2 align="center">PRACTICE PROBLEM (13)  </h2>
    <p>&nbsp;</p>
    <p>A car is moving up an incline, making an angle of 35 degees with the horizontal, at the rate of 26 ft. per second. What is the horizontal velocity? </p>
    <blockquote>
      <p class="style17"><strong>Solution:</strong></p>
      <p class="style17">&nbsp;</p>
      <blockquote>
        <p class="style17">Horizontal velocity = 26 cos 35<sup>0</sup> = 21.3 ft. per second</p>
        <p class="style17">Vertical velocity = 26 sin 35<sup>0</sup> = 14.9 ft. per second </p>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style17">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p class="style17">&nbsp;</p>
      </blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_trigo");?>
    </div>