<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Change Password</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <style type="text/css">
<!--
.style1 {font-family: "Times New Roman", Times, serif}
.style2 {font-family: "Times New Roman"}
.style3 {font-size: 14px}
.style4 {
	font-size: x-large;
	font-weight: bold;
}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header style1">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>              </button>
              <a class="navbar-brand" href="#"><?php echo quiz_title;?></a>            </div>
            <?php 
              $link["active"] = "profile";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="container marketing">

      <div class="jumbotron">

      <form class="form-signin" role="form">
        <p class="form-signin-heading style2 style4">Change Password</p>
        <div class="alert alert-success style2" id="success"></div>
        <div id="error" class="alert alert-danger"></div>
        <input id="o_pass" type="password" class="form-control" placeholder="Old Password" required>
        <input id="n_pass1" type="password" class="form-control" placeholder="New Password" required>
        <input id="n_pass2" type="password" class="form-control" placeholder="Confirm Password" required>
        <button type="submit" class="btn btn-lg btn-primary style2 style3" id="save">Update</button>
        <a href="<?php echo base_url();?>index.php" class="btn btn-lg btn-default style2" id="cancel">Cancel</a>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; <span class="style2">2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></span></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>

    <script>

      $(function(){

        var err_msg = new Array();
        err_msg[0] = false,
        err_msg[1] = false;

        $("#error, #success").hide();

        $("#o_pass").focusout(function() {
          if( $("#o_pass").val() != '<?php echo $this->session->userdata("oq_password");?>' ){
            err_msg[0] = "Old Password doesn't match current password!";
          }else{
            err_msg[0] = false;
          }
          generate_error();
        });

        $("#n_pass1, #n_pass2").focusout(function() {
          if($("#n_pass1").val().length <= 7){
            err_msg[1] = "Minimum Password length is 8 characters.";
          }else if ($("#n_pass1").val() != $("#n_pass2").val()){
            err_msg[1] = "Password doesn't match.";
          }else{
            err_msg[1] = false;
          }
          generate_error();
        });

        function generate_error()
        {
          var msg = "",
          ctr_false = 0;

          for (var i = 0; i < err_msg.length; i++) {
            if(err_msg[i] != false){
              msg += err_msg[i] + "<br />";
            }else{
              ctr_false++;
            }
          }
          if(err_msg.length == ctr_false){
            $("#error").empty().hide();
          }else{
            $("#error").html(msg).show();
          }
        }

        $("#save").click(function(e){
          if( $("#n_pass1").val() != $("#n_pass2").val() ){
            e.preventDefault();
          }else {
            if ($("#n_pass1").val() != "" && $("#n_pass2").val() != "" && $("#o_pass").val() != ""){
                $.ajax({
                  type: "POST",
                  async: false,
                  url: '<?php echo base_url();?>index.php/home/update_password',
                  data: 
                  {
                    password :   $("#n_pass1").val()
                  },
                  success: function(msg){
                    if(msg == "success"){
                      $("#n_pass1").val("");
                      $("#n_pass2").val("");
                      $("#o_pass").val("");
                      $("#success").html("User successfully saved!").show();
                      setTimeout(function(){$("#success").empty().hide()},3000);
                    }
                  }
                });
            }
               
          } 
        });

      }); 

    </script>
  </body>
</html>
