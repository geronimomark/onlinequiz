<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">FUNCTION, GRAPH, AND LIMITS </h2>
    <ul>
      <li>A <span class="style23">function </span>is a rule that assigns to each object in a set A, one and only one object in a set B.</li>
      <li>Variables: the two types of variables are <span class="style23">dependent</span> and<span class="style23"> independent variable.</span></li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" /><strong>Example: </strong></p>
    <blockquote>
      <p class="style20">y = x2 + 7 ; y is dependent variable and x is independent variable.</p>
    </blockquote>
    <ul>
      <li><span class="style23">Functional Notation</span>: it used the symbol f(x) that read &quot;f of x&quot;. </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="15" /><strong>Example:</strong> find f(3) if f(x) = x2 + 4 </p>
    <blockquote class="style20">
      <blockquote>
        <p class="style20">Solution: f(3) = 32 + 4 = 13 </p>
      </blockquote>
    </blockquote>
    <ul>
      <li><span class="style23">Domain of a function</span>: the set of values of the inndependent variable for which a function can be evaluated.</li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="15" /> <strong>Example</strong>: f(x) = 1/(x-3) </p>
    <ul>
      <blockquote>
        <p class="style20">Solution:</p>
        <p align="justify" class="style20"> Since the division by any real number except   zero is possible, the only value of x for which f(x) = 1/(x-3) cannot be   evaluated is x = 3, the value that makes the denominator of f equal to   zero. Hence the domain of f consists of all real numbers except 3. </p>
      </blockquote>
    </ul>
    <p align="justify" class="style24"><u>FUNCTIONS AND GRAPHS</u> </p>
    <blockquote>
      <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/1.1.jpg" width="208" height="179" /></p>
      <p class="style19">&nbsp;</p>
    </blockquote>
    <p align="center" class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/1.3.jpg" width="350" height="188" /></p>
    <div id="Layer5">
      <ul>
        <li>In linear function:
          <ul>
            <li>The line rises as  x increases (m&gt;0), and falls if m &lt; 0.</li>
            <li>With b = 0; y = mx,&nbsp;then y is proportional to x.</li>
            <li>Slope m is called &quot;constant of proportionality. </li>
          </ul>
        </li>
      </ul>
    </div>
    <ul>
      <li>Operationns on Functions:</li>
    </ul>
    <p>&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;    &nbsp;&nbsp;  &nbsp;  &nbsp;&nbsp; 1. (f + g)(x) =&nbsp; f (x) + g (x)</p>
    <p>&nbsp;&nbsp;     &nbsp;    &nbsp;&nbsp;     &nbsp;   &nbsp;&nbsp;     &nbsp;    &nbsp;  &nbsp;&nbsp;  &nbsp; 2. (f - g)(x) = &nbsp;f (x) - g (x) </p>
    <p>&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;   &nbsp;&nbsp; 3. (f * g)(x) = f(x) * f(g)</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; 4. (f/g)(x) = f(x)/f(g) </p>
    <p align="left" class="style24"><u>LIMITS</u></p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="15" /> Example:  Investigate the instantaneous rate of change of y with respect to x at x = 1 if </p>
    <ul>
      <li><div id="Layer6"><img src="<?php echo base_url();?>assets/diffcalc/images/1.4.jpg" alt="1" width="60" height="29" /></div>
      </li>
    </ul>
    <blockquote>
      <p class="style20">Solution:</p>
      <p class="style20">If x changes from 1 to 1 + change of x, then the corresponding change in y is</p>
    
      <blockquote>
        <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/1.5.jpg" width="211" height="16" /></p>
      </blockquote>
      <p class="style20"> The the value of x near 1 produce small values of both change in x and change&nbsp; in y</p>
      <blockquote>
        <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/1.6.jpg" width="195" height="32" /></p>
      </blockquote>
      <p class="style20">&nbsp;&nbsp; Change in x gets closer and close to 0 by computing: </p>
      <blockquote>
        <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/1.7.jpg" width="95" height="32" /></p>
      </blockquote>
      <p class="style20">&nbsp;</p>
    </blockquote>
    <ul>
      <li>Properties of Limits:</li>
    </ul>
    <ol>
      <li>Limits of sum, difference, multiple, or product:</li>
    </ol>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.9.jpg" width="290" height="22" /></p>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.10.jpg" width="257" height="21" /></p>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.11.jpg" width="264" height="22" /></p>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.11.jpg" width="264" height="22" /></p>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.12.jpg" width="185" height="22" /></p>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.13.jpg" width="250" height="22" /></p>
    </blockquote>
    <p>2. Limit of quotient</p>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/1.14.jpg" width="235" height="22" /> <img src="<?php echo base_url();?>assets/diffcalc/images/1.15.jpg" width="146" height="22" /></p>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/2.1.jpg" width="124" height="45" /></p>
    </blockquote>
    <p class="style20">3. Limit of power</p>
    <blockquote>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/2.2.jpg" width="267" height="22" /> </p>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/2.3.jpg" width="173" height="22" /></p>
    </blockquote>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="15" /><strong>Example:</strong></p>
    <p>Find <img src="<?php echo base_url();?>assets/diffcalc/images/2.4.jpg" width="146" height="22" /></p>
    <div id="Layer2">Domain of a function <img src="<?php echo base_url();?>assets/diffcalc/images/1.2.jpg" width="102" height="17" /></div>
    <blockquote>
      <p class="style20">Solution:</p>
      <p align="justify" class="style20">The given limit cannot be found by direct substitution because the   denominator is zero when x = 2. Instead, the numerator and denominator   are factored; assuming x is not equal to 2.</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/2.5.jpg" width="261" height="32" /></p>
      <p align="justify" class="style20">therefore x is not equal to 2 </p>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/2.6.jpg" width="242" height="32" /></p>
      <blockquote>
        <p class="style20">&nbsp;</p>
      </blockquote>
    </blockquote>
  </div>