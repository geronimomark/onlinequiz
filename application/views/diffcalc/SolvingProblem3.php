<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 3 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">If f(x) = e1/x, then f' (x) = </p>
    <p align="justify">&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.3.jpg" width="138" height="64" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.4.jpg" width="143" height="40" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.5.jpg" width="95" height="40" /></p>
    </blockquote>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
  </div>