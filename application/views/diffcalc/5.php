<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">derivative of inverse trigonometric functions </h2>
    <ul>
      <li>Derivative of the Inverse Trigonometric Functions:
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/i1.jpg" width="210" height="310" /></p>
          <p>&nbsp;</p>
        </blockquote>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" /><strong>Examples:      </strong></p>
    <p>1. Differentiate y&nbsp; tan<sup>-1</sup> 3x</p>
    <blockquote>
      <p>&nbsp;&nbsp;&nbsp;<span class="style21"> Solution:</span></p>
      
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/i3.jpg" width="37" height="16" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/i2.jpg" width="200" height="32" /></p>
        <p>&nbsp;</p>
      </blockquote>
    </blockquote>
    <p>2. Differentiate <img src="<?php echo base_url();?>assets/diffcalc/images/i4.jpg" width="145" height="20" /></p>
    <blockquote>
      <p class="style21">Solution:</p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/i6.jpg" width="300" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/i7.jpg" width="300" height="32" /></p>
      </blockquote>
      <p>&nbsp;</p>
    </blockquote>
    <p>&nbsp;</p>
  </div>