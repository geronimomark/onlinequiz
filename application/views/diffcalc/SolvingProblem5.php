<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 5 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">At each point (x, y) on a certain curve, the slope of the curve is 3x<sup>2</sup>y. If the curve contains the point (0, 8), find the equation: </p>
    <p align="justify">_______________________________________________________________________________________</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.1.jpg" width="85" height="32" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.2.jpg" width="95" height="32" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.3.jpg" width="96" height="18" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.4.jpg" width="66" height="20" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.5.jpg" width="65" height="18" /></p>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.6.jpg" width="67" height="22" /></p>
    </blockquote>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p class="style20"><span class="style10">Reference: Calculus, College Board Exam Questions (p. 56, # 44). Retrieved from: www.collegeboard.com:</span></p>
  </div>