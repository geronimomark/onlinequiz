<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">DERIVATIVE</h2>
    <ul>
      <li><span class="style20">Derivative </span>are found all over science and math, and are measure of how one variable changes with respect to another variable.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
      <li>How to Compute the Derivative of f(x):
        <p>1. Form the difference quotient.</p>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/6.1.jpg" width="108" height="35" /></p>
        </blockquote>
        <p>2. Simplify the difference quotient algebraically.</p>
        <p>3. Let  change of x approach zero in the simplified difference quotient</p>
      </li>
      <li>The Power Rule:
        <ul>
          <li>Power Function: fx = x<sup>n</sup> </li>
        </ul>
        <ul>
          <li>Derivative Formula: </li>
        </ul>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/6.2.jpg" width="105" height="33" /></p>
        </blockquote>
        <ul>
          <li>To compute the derivative find the following limit.</li>
        </ul>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/6.3.jpg" width="162" height="33" /></p>
        </blockquote>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" />Example:</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Find the derivative of the function f(x) = x<sup>3</sup> </p>
    <blockquote>
      <p class="style21">Solution:</p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.1.jpg" width="190" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.2.jpg" width="300" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.3.jpg" width="277" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.4.jpg" width="190" height="32" /></p>
        <p>&nbsp;</p>
      </blockquote>
    </blockquote>
    <ul>
      <li>General Rule:</li>
    </ul>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.5.jpg" width="400" height="20" /></p>
    </blockquote>
    <p>&nbsp;</p>
    <p class="style22"><u>LINEARITY OF THE DERIVATIVE</u> </p>
    <ul>
      <li><img src="<?php echo base_url();?>assets/diffcalc/images/7.5.jpg" width="390" height="18" /></li>
    </ul>
    <p>&nbsp;</p>
    <ul>
      <li><img src="<?php echo base_url();?>assets/diffcalc/images/7.7.jpg" width="510" height="32" /></li>
    </ul>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /><strong>Example:</strong></p>
    <p>Find the derivative of f(x) = x5 + 5x2 </p>
    <blockquote>
      <p class="style21">Solution</p>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.8.jpg" width="136" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/7.9.jpg" width="155" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/8.1.jpg" width="142" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/9.2.jpg" width="118" height="16" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/9.3.jpg" width="104" height="16" /></p>
        <p>&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style22"><u>DERIVATIVE USING PRODUCT RULE</u> </p>
    <p align="justify" class="style22">&nbsp;</p>
    <div align="justify">
      <ul>
        <li>The product rule says that the derivative of a product of   two function is the first function time the derivative of the second   function&nbsp; plus the second function times the derivative of the first   function.</li>
      </ul>
      <p>&nbsp;</p>
      <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" />Examples:      </p>
      <p>1. Differentiate y = (x3 + 7x - 1) (5x + 1)</p>
      <blockquote>
        <p class="style21">Solution:        </p>
        <blockquote class="style21">
          <p>y' = (x<sup>3</sup> + 7x -1) </p>
        </blockquote>
        <p class="style21"><em><strong>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = d/dx(5x + 2) + d/dx(x<sup>3</sup> + 7x -1) (5x+2) </strong></em></p>
      </blockquote>
    </div>
    <p class="style21"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = (x<sup>3</sup> + 7x - 1)(5) + (3x<sup>2</sup> + 7)(5x + 2) </strong></em></p>
    <p class="style21"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= 5x<sup>3</sup> + 35x - 5 + 15x<sup>3</sup> + 6x<sup>2</sup> + 35x + 14</strong></em></p>
    <p class="style21"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= 20x<sup>3</sup> + 6x<sup>2</sup> + 70x + 9 </strong></em></p>
    <ul>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;
    </ul>
    <p>2. If f(x) = xe<sup>x</sup> , find f'(x)</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;<em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solution:</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      &nbsp;&nbsp;f''(x) = d/dx (xe<sup>x</sup>) = x d/dx (e<sup>x</sup>) + e<sup>x</sup> d/dx (x)</strong></em></p>
    <p><em><strong>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   = xe<sup>x</sup> + e<sup>x</sup> (1) = (x+1) e<sup>x</sup> </strong></em></p>
    <p>&nbsp;</p>
    <p>&nbsp; 3. f(x) = sin(x)cos(x)</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em><strong>Solution:</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;f'(x) = d/dx sin(x)cos(x) + sin(x) d/dx cos(x)</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;   &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;= cos(x) cos(x) + [sin(x)][-sin(x)]</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= cos2(x) - sin2(x) </strong></em></p>
    <p>&nbsp;</p>
    <p align="justify" class="style22"><u>DERIVATIVE USING QUOTIENT RULE </u></p>
    <div align="justify">
      <ul>
        <li>The quotient rule says that the derivative of quoatient is the   denominator times the derivative of numerator minus the numerator times   the derivative of the denominator, all divided by the square of the   denominator.
          <blockquote>
            <p><img src="<?php echo base_url();?>assets/diffcalc/images/9.4.jpg" width="290" height="61" /></p>
            <blockquote>
              <p><img src="<?php echo base_url();?>assets/diffcalc/images/9.5.jpg" width="222" height="34" /></p>
            </blockquote>
          </blockquote>
        </li>
      </ul>
      <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /> <strong>Example:</strong></p>
      <blockquote>
        <blockquote>
          <p><strong><img src="<?php echo base_url();?>assets/diffcalc/images/9.6.jpg" width="95" height="33" /></strong></p>
        </blockquote>
        <p class="style21">Solution:</p>
        <blockquote>
          <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/9.7.jpg" width="388" height="46" /></p>
          <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/9.9.jpg" width="251" height="32" /></p>
          <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/10.1.jpg" width="330" height="32" /></p>
          <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/10.2.jpg" width="206" height="32" /></p>
        </blockquote>
      </blockquote>
    </div>
    <ul>
    </ul>
  </div>