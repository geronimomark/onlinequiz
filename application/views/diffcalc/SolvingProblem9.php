<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 9 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">If d/dx (f(x)) = g(x) and d/dx (g(x))=f(x<sup>2</sup>), then d<sup>2</sup>/dx<sup>2</sup> (f(x<sup>3</sup>)) = </p>
    <p align="justify">&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3,3.jpg" width="85" height="19" /></p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.3.jpg" width="100" height="19" /></p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.4.jpg" width="240" height="20" /></p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.5.jpg" width="350" height="19" /></p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.6.jpg" width="138" height="19" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <blockquote>&nbsp;</blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>