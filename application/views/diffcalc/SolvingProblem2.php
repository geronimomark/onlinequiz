<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 2 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">Travel during the time interval when its velocity increases from 4 meters per second to 10 meters per second? </p>
    <p align="justify">&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify" class="style20">v(t) = 3t + C</p>
      <p align="justify" class="style20">v(2) = 10 ; C = 4</p>
      <p align="justify" class="style20">v(t) = 3t + 4</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.2.jpg" width="235" height="36" />  </p>
      <p align="justify" class="style20">&nbsp;</p>
    </blockquote>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>