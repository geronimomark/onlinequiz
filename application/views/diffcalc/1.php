<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">Chain rule </h2>
    <ul>
      <li><span class="style23">Chain rule</span>: deals with composite functions and adds to the other rules.
        <ul>
          <li>Without chain rule:</li>
        </ul>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/3.1.jpg" width="75" height="72" /></p>
        </blockquote>
        <ul>
          <li>With chain rule:</li>
        </ul>
        <blockquote>
          <p><img src="<?php echo base_url();?>assets/diffcalc/images/3.2.jpg" width="75" height="72" /></p>
        </blockquote>
        </li>
    </ul>
    <p><em><strong><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="24" height="17" />Example:</strong></em></p>
    <p> 1. Find dy/dx for y = (x2 + 1)3 </p>
    <blockquote>
      <p class="style20"> Solution:</p>
      <blockquote>
        <p class="style20">dy/dx = 3(x<sup>2</sup> + 1)<sup>2</sup> (2x) = 6x(x<sup>2</sup> + 1)<sup>2</sup> </p>
        <p class="style20">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p align="justify" class="style20"> 	&nbsp; 2. The cost of producing x units of a particular commodity   is <img src="<?php echo base_url();?>assets/diffcalc/images/3.3.jpg" width="128" height="32" />dollars, and the production level t hours into a particular   production run is x(t) = 0.2t2 + 0.03r units. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; what rate is   cost changing with respect to time after 4 hours? </p>
    <blockquote>
      <p align="justify" class="style20">Solution:</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/4.2.jpg" width="98" height="32" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/4.3.jpg" width="114" height="32" /></p>
      <p align="justify" class="style20">Using chain rule:</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/4.1.jpg" alt="1" width="254" height="32" /> </p>
      <p class="style20">When t = 4 </p>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/4.4.jpg" width="210" height="17" /></p>
      <p class="style20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By substituting t = 4 and x = 3.32 unit</p>
      <p class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/4.5.jpg" width="307" height="32" /></p>
      <p class="style20">Answer: After 4 hours, cost is increasing at the rate of approximately $10.13/ hour </p>
    </blockquote>
  </div>