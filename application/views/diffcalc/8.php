<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">maxima and minima</h2>
    <p align="center">&nbsp;</p>
    <p align="center"><img src="<?php echo base_url();?>assets/diffcalc/images/j1.jpg" width="257" height="190" /> </p>
    <ul>
      <li><span class="style22">Absolute maximum</span>:<img src="<?php echo base_url();?>assets/diffcalc/images/j2.jpg" width="55" height="16" /></li>
      <li><span class="style22">Absolute minimum</span>: x = a</li>
    </ul>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" /><strong>Examples:</strong></p>
    <p>1. Find the absolute maximunm and absolute minimum of the function 2x3 + 3x2 - 12x -7 on the interval <img src="<?php echo base_url();?>assets/diffcalc/images/j3.jpg" width="78" height="17" /></p>
    <blockquote>
      <p class="style23">Solution:</p>
      <blockquote>
        <p class="style23"><span class="style23">From the der</span>ivative: f'(x) = 6x2 + 6x - 12 = 6(x+2)(x-1) </p>
        <p class="style23">At the first order critical points occur when x = -2 and x = 1 , only x = -2 lies in the interval <img src="<?php echo base_url();?>assets/diffcalc/images/j4.jpg" width="97" height="15" /></p>
        <p class="style23">Compute f(x) at x = -2 and at the endpoints x = -3 and x = 0</p>
        <p class="style23">f(-2) = 13, f(-3) = 2, f(0) = -7</p>
        <p class="style23">Compare these values to conclude that the absolute   maximum of f on the interval&nbsp;<img src="<?php echo base_url();?>assets/diffcalc/images/j4.jpg" alt="1" width="97" height="15" /> is f(-2) = 13 and   the absolute minimum is f(0) = -7 </p>
        <p align="center" class="style23"><img src="<?php echo base_url();?>assets/diffcalc/images/j5.jpg" width="193" height="244" /></p>
        <p class="style23">&nbsp;</p>
      </blockquote>
    </blockquote>
    <p class="style23">2. y = x<sup>2</sup> - 4x + 7 </p>
    <blockquote>
      <p class="style23">Solution:</p>
      <blockquote>
        <p align="justify" class="style23">By assigning a number of successive values to x, and finding the corresponding values to y, the equation representing a curve with a minimum: </p>
        <p align="justify" class="style23"><img src="<?php echo base_url();?>assets/diffcalc/images/b1.jpg" width="433" height="57" /></p>
        <p align="justify" class="style23">The figure below show that y has minimum value of 3.</p>
        <p align="justify" class="style23"><img src="<?php echo base_url();?>assets/diffcalc/images/b2.jpg" width="240" height="180" /> </p>
      </blockquote>
    </blockquote>
    <p class="style23">&nbsp;</p>
  </div>