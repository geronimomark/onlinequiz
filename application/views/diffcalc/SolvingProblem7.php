<div class="style1" id="content">
  <h2 align="center" class="style12">integral calculus </h2>
  <h2 align="center">practice problem (1) </h2>
  <p align="justify">Determine the function f(x):</p>
  <blockquote>
    <blockquote>
      <p align="justify"> <img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.1 (2).jpg" width="200" height="20" /></p>
    </blockquote>
  </blockquote>
  <p align="left">_________________________________________________________________________________________ </p>
  <p><strong>Solution:</strong></p>
  <blockquote>
    <p><span class="style9">f(x) = </span><span class="style9">4x<sup>3</sup> - 9 + 2sinx + 7e<sup>x</sup> dx </span></p>
    <p><span class="style9">= x<sup>4</sup> - 9x - 2cosx + 7e<sup>x</sup> + c</span></p>
    <p><span class="style9">When x = 0</span></p>
    <p class="style9">15 = f(0)<sup>4</sup> - 9(0) - 2cos(0) + 7e<sup>0</sup> + c </p>
    <p class="style9">= -2 + 7 + c</p>
    <p class="style9">= 5 + c</p>
    <p class="style9">When c = 10; the function is </p>
    <p class="style9">= x<sup>4</sup> - 9x - 2cosx + 7e<sup>x</sup> + 10  </p>
    <p class="style8">&nbsp;</p>
    <blockquote>&nbsp;</blockquote>
  </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>