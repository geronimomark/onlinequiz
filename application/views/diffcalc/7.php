<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">explicit and implicit functions</h2>
    <ul>
      <li><span class="style22">Explicit form</span>: expressing two variables; y = f(x)</li>
      <li><span class="style22">Implicit form</span>: expressing one variable</li>
    </ul>
    <p>Example:    </p>
    <p>1. Calculate dy/dx directly from the equation for the unit circle x<sup>2</sup> + y<sup>2</sup> = 1 </p>
    <blockquote>
      <p class="style21">Solution:</p>
      <blockquote>
        <p class="style21">x is independent variable, replace y with y(x)</p>
      </blockquote>
      <p class="style21">&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;       &nbsp;  &nbsp;&nbsp; x2 + (y(x))2 = 1 </p>
      <blockquote>
        <p class="style21">take the derivative of each term: </p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o3.png" width="200" height="32" /></p>
        <p class="style21"> by chain rule:</p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o4.png" width="190" height="32" /> </p>
        <p class="style21">or </p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o7.jpg" width="130" height="32" /></p>
        <p class="style21">the result: </p>
        <p class="style21">&nbsp;&nbsp; <img src="<?php echo base_url();?>assets/diffcalc/images/o6.jpg" width="100" height="32" /></p>
        <p class="style21">solve for dy/dx:</p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o8.jpg" width="85" height="32" /> </p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o9.jpg" width="75" height="32" /></p>
      </blockquote>
      <p class="style21">the result provided that y is not equal to 0.&nbsp; At the points (1,0) and (-1,0), the circle has vertical tangent lines </p>
    </blockquote>
  </div>