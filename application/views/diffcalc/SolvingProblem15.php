<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 15</h2>
    <p align="center">&nbsp; </p>
    <p align="justify">Find the maximum or minimum of:</p>
    <p align="justify">a. y = 4x<sup>2</sup> - 9x - 6</p>
    <p align="justify">b. y = 6 + 9x - 4x<sup>2</sup></p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.8.jpg" width="100" height="18" /></p>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/b3.jpg" width="285" height="32" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/b4.jpg" width="300" height="32" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.9.jpg" width="120" height="16" /></p>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify" class="style20"> <img src="<?php echo base_url();?>assets/diffcalc/images/b5.jpg" width="280" height="32" /></p>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/b6.jpg" width="340" height="32" /></p>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/algebra/images/arrow.jpg" alt=" " width="144" height="19" />    </p>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>