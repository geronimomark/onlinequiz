<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 12 </h2>
    <p align="left">&nbsp;</p>
    <blockquote>
      <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.6.jpg" width="400" height="45" /></p>
    </blockquote>
    <p>&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.7.jpg" width="95" height="20" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.8.jpg" width="95" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.9.jpg" width="150" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.jpg" width="85" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.1.jpg" width="110" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.2.jpg" width="265" height="19" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.3.jpg" width="500" height="20" /></p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <blockquote>&nbsp;</blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>