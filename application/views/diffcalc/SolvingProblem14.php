<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 14</h2>
    <p align="center">&nbsp; </p>
    <p align="justify">A body moves so that the distance x (in feet), in which it travels from a certain point O, is given by the relation x = 0.2t<sup>2</sup> + 10.4, where t is the time in seconds elapsed since a certain instant. Find the velocity and acceleration 5 seconds after the body began to move, and also find the corresponding values when the distance covered is 100 feet. Find also the average velocity during the first 10 seconds of its motion. (Suppose distances and motion to the right to be positive.) </p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.8.jpg" width="110" height="18" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.9.jpg" width="120" height="33" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.jpg" width="135" height="33" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.1.jpg" width="330" height="16" /></p>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.2.jpg" width="210" height="15" /> <img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.3.jpg" width="150" height="16" /></p>
      <p align="justify">&nbsp;</p>
      <blockquote>
        <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.4.jpg" width="200" height="16" /></p>
      </blockquote>
      <p align="justify">&nbsp;</p>
      <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.5.jpg" width="85" height="16" /></p>
      <p align="justify">&nbsp;</p>
      <blockquote>
        <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.6.jpg" width="320" height="16" /></p>
        <p align="justify">&nbsp;</p>
        <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/6.7.jpg" width="200" height="30" /></p>
      </blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
</div>