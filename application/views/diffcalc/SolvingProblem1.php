<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 1 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">At t = 0 particle starts at rest and moves along a line in such a way that at time t its acceleration is 24t<sup>2</sup> feet per second per second. Through how many feet does the particle move during the first 2 seconds? </p>
    <p align="justify">&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify" class="style20">a(t) = 24t<sup>2</sup> </p>
      <p align="justify" class="style20">&nbsp;</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.1.jpg" width="142" height="39" /></p>
    </blockquote>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>