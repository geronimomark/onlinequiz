<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">Higher order derivatives  </h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li>Higher-Order Derivatives </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/diffcalc/images/x1.jpg" width="521" height="168" /></p>
    <blockquote>&nbsp;</blockquote>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="15" />&nbsp;&nbsp; Example: </p>
    <p>1. Find the first derivative of f(x) = 2x4 - 3x2 </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp; Solution:</strong></p>
    <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;<em> f(x) = 2x4 - 3x2</em></strong></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f'(x) = 8x3 - 6x</strong></em></p>
    <p><em><strong>&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f&quot;(x) = 24x<sup>2</sup> = 6</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f&quot;'(x) = 48x</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f&quot;&quot;=48</strong></em></p>
    <p><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f<sup>5</sup> = 0 </strong></em></p>
    <p align="justify"> &nbsp;&nbsp;&nbsp;&nbsp; 2. A ball is thrown upward from the top of a 160 foot cliff, as   shown in figure below, The initial velocity of the ball is 48 ft/sec.,   which implies that the position function is s = -16t2 + 48t + 160 where the time i is measured in seconds. Find the height, velocity, and acceleration of the ball at t = 3</p>
    <blockquote>
      <p class="style21">Solution: </p>
      <p align="center" class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/o2.jpg" width="256" height="170" /></p>
      <blockquote>
        <p class="style21"> s = -16t<sup>2</sup> + 48t + 160 </p>
        <p class="style21">ds/dt = -32t + 48 </p>
        <p class="style21">d2s/dt2 = -32</p>
        <p class="style21">Finding height, velocity, and acceleration if t = 3</p>
        <p class="style21">Height = -16(3)<sup>2</sup> + 48(3) + 160 = 160 feet </p>
        <p class="style21">Velocity = -32(3) + 48 = -48 feet per second</p>
        <p class="style21">Acceleration = -32 feet per second squared &nbsp;&nbsp;&nbsp; </p>
        <p class="style21">&nbsp;</p>
      </blockquote>
    </blockquote>
  </div>