<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 4 </h2>
    <p align="center">&nbsp;</p>
    <p align="justify">The region enclosed by the graph of y = x<sup>2</sup> , the line x = 2, and the x - axis is revolved about the y-axis. The volume of the solid generated is </p>
    <p align="justify">&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="center"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/2.jpg" width="137" height="189" /></p>
    <blockquote>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.6.jpg" width="168" height="20" /></p>
      <p align="justify" class="style20">where: R = 2 and r = x</p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.7.jpg" width="184" height="32" /> </p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.8.jpg" width="145" height="45" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/1.9.jpg" width="180" height="32" /></p>
    </blockquote>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center">&nbsp;</p>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>