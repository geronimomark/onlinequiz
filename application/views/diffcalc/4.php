<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">APPROXIMATION BY DIFFERENTIATION </h2>
    <p align="center">&nbsp;</p>
    <ul>
      <li>Approximation Formula:
        <ul>
          <li>If y = f(x) and
            <img src="<?php echo base_url();?>assets/diffcalc/images/5.1.jpg" width="22" height="15" />is a small change in x, then the corresponding change in y is: 
            <blockquote>
              <p><img src="<?php echo base_url();?>assets/diffcalc/images/5.2.jpg" width="81" height="32" /></p>
            </blockquote>
          </li>
          <li>Corresponding change in f:
            <blockquote>
              <p><img src="<?php echo base_url();?>assets/diffcalc/images/5.3.jpg" width="210" height="16" /></p>
              <p>&nbsp;</p>
            </blockquote>
          </li>
        </ul>
      </li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="25" height="14" /> <strong>Example:</strong></p>
    <p>&nbsp;</p>
    <p>Suppose the total cost in dollars of manufacturing q units of a certain commodity is C(q) = 3q2 + 5q + 10. If the current level of production is 40 units, estimate how the total cost will change if 40.5 units are produced. </p>
    <blockquote>
      <p class="style19">Solution:</p>
      <p class="style19">&nbsp;</p>
      <blockquote>
        <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/5.4.jpg" width="42" height="16" /></p>
        <p class="style19">&nbsp;</p>
        <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/5.5.jpg" width="59" height="15" /></p>
        <p class="style19">&nbsp;</p>
        <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/5.6.jpg" width="260" height="16" /></p>
        <p class="style19">&nbsp;</p>
        <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/5.7.jpg" width="104" height="15" /></p>
        <p class="style19">&nbsp;</p>
        <p class="style19"><img src="<?php echo base_url();?>assets/diffcalc/images/5.8.jpg" width="142" height="16" /></p>
        <p>&nbsp;</p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/5.9.jpg" width="299" height="15" /></p>
      </blockquote>
    </blockquote>
  </div>