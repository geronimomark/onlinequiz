<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 10 </h2>
    <p align="left">&nbsp;</p>
    <p align="left">The region in the first quadrant bounded by the graph of y = sec x, x = &pi;/4, and the axes is rotated about the x-axis. What is the volume of the solid generated </p>
    <p>&nbsp;</p>
    <p align="justify">_______________________________________________________________________________________</p>
    <p align="justify"><strong>Solution:</strong></p>
    <blockquote>
      <p align="justify" class="style20">Washers: <img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.8.jpg" width="65" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.9.jpg" width="100" height="18" /></p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.jpg" width="185" height="22" /> </p>
      <p align="justify" class="style20"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/4.1.jpg" width="223" height="32" /></p>
    </blockquote>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <div align="center"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/3.7.jpg" width="154" height="201" />
      </div>
      <p align="justify">&nbsp;</p>
      <p align="justify">&nbsp;</p>
    </blockquote>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <p align="justify" class="style10">&nbsp;</p>
    <blockquote>&nbsp;</blockquote>
    <blockquote>
      <blockquote>&nbsp;</blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>