<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL CALCULUS </h2>
    <h2 align="center">rules of differentiation </h2>
    <ul>
      <li><span class="style22">Constant rule</span>:&nbsp; If c is real number, then:
        <img src="<?php echo base_url();?>assets/diffcalc/images/y1.jpg" alt="1" width="61" height="32" /></li>
    </ul>
    <ul>
      <li><span class="style22">Power rule</span>: If n is positive integer, then <img src="<?php echo base_url();?>assets/diffcalc/images/y2.jpg" width="88" height="32" /></li>
    </ul>
    <p>&nbsp;</p>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" width="31" height="16" />Examples    </p>
    <p>1.  Power function: <img src="<?php echo base_url();?>assets/diffcalc/images/y3.jpg" width="45" height="32" /></p>
    <blockquote>
      <p class="style21">Solution:</p>
    </blockquote>
    <ul>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/y4.jpg" width="156" height="32" class="style21" /></p>
      </blockquote>
    </ul>
    <p>2. Constant Function: <img src="<?php echo base_url();?>assets/diffcalc/images/y5.jpg" width="47" height="32" /></p>
    <blockquote>
      <p class="style21">Solution:</p>
    </blockquote>
    <ul>
      <blockquote><p class="style21">28 = 256 is a constant</p>
        <p class="style21">by constant rule:&nbsp; <img src="<?php echo base_url();?>assets/diffcalc/images/y6.jpg" width="65" height="32" /> </p>
        <p class="style21">&nbsp;</p>
      </blockquote>
      <li><span class="style22">Constant Multiple Rule</span>: <img src="<?php echo base_url();?>assets/diffcalc/images/y7.jpg" width="75" height="32" /></li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /> <strong>Example:</strong></p>
    <p>1. Constant Function: <img src="<?php echo base_url();?>assets/diffcalc/images/y9.jpg" width="76" height="32" /></p>
    <blockquote>
      <p class="style21">Solution:</p>
    </blockquote>
    <ul>
      <blockquote>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/y8.jpg" width="188" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/u1.jpg" width="162" height="32" /></p>
        <p><img src="<?php echo base_url();?>assets/diffcalc/images/u2.jpg" width="154" height="32" /></p>
        <p>&nbsp;</p>
      </blockquote>
      <li><span class="style22">Sum Rule</span>: <img src="<?php echo base_url();?>assets/diffcalc/images/u3.jpg" width="180" height="32" /></li>
    </ul>
    <p><img src="<?php echo base_url();?>assets/algebra/images/Solving Problems/example.jpg" alt="1" width="31" height="16" /> <strong>Example:</strong></p>
    <blockquote>
      <p><img src="<?php echo base_url();?>assets/diffcalc/images/u4.jpg" width="130" height="32" /></p>
      <p class="style21">Solution:</p>
      <blockquote>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/u5.jpg" width="298" height="32" /></p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/u6.jpg" width="310" height="32" /></p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/u7.jpg" width="210" height="16" /></p>
        <p class="style21"><img src="<?php echo base_url();?>assets/diffcalc/images/u8.jpg" width="100" height="16" /></p>
      </blockquote>
    </blockquote>
  </div>