<div class="style1" id="content">
    <h2 align="center" class="style18">DIFFERENTIAL/integral CALCULUS </h2>
    <h2 align="center">Practice problem 13 </h2>
    <p align="left">&nbsp;</p>
    <p align="left">Differentiate:</p>
    <blockquote>
      <blockquote>
        <p align="left"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.4.jpg" width="85" height="32" /></p>
      </blockquote>
    </blockquote>
    <p>&nbsp;</p>
    <p align="justify"><strong>Solution:</strong></p>
    <p align="justify">&nbsp;</p>
    <blockquote>
      <blockquote>
        <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.6.jpg" width="110" height="32" /></p>
        <p align="justify">&nbsp;</p>
        <p align="justify"><img src="<?php echo base_url();?>assets/diffcalc/images/Practice Problem/5.7.jpg" width="65" height="32" /></p>
        <p align="justify">&nbsp;</p>
      </blockquote>
    </blockquote>
    <?php $this->load->view("template/navlinks_diffcalc");?>
    <p align="left">&nbsp;</p>
    <p align="center">&nbsp;</p>
  </div>