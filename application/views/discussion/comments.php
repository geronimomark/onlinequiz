<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title><?php echo quiz_title;?> - Discussion</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/form.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/stripe.css" rel="stylesheet">
    <style type="text/css">
<!--
.style1 {
	font-family: "Times New Roman";
	font-size: xx-large;
	font-weight: bold;
}
.style2 {font-size: x-large}
-->
    </style>
</head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><?php echo quiz_title;?></a>
            </div>
            <?php 
              $link["active"] = "discussion";
              $this->load->view("template/navlinks", $link);
            ?>
          </div>
        </div>

      </div>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="container marketing">

      <div class="jumbotron">
      <style>.form-signin{max-width: 1000px;}</style>
      <form class="form-signin" role="form">
        <p class="form-signin-heading style1">Exam Discussions</p>
        <p class="lead"><?php echo $x[0]["qz_qname"]. " - Creator: " . $x[0]["Name"] . " (" . $x[0]["dt"] .")";?></p>
<!--         <input type="text" class="form-control" placeholder="E.g. Quiz Name, Quiz Description, 2014-01-01" required autofocus>
        <button id="save" class="btn btn-lg btn-primary" type="submit">Search</button> -->
        <div id="dynamic_comment">
          <?php 
            $table = "<table align='center' class='table table-striped' border='0' width='100%' style='table-layout:fixed;'>";
            foreach ($y as $key => $value) 
            {
              $table .= "<tr>
              <td style='word-wrap:break-word;' width='25%'>".$value['Name']."</td>
              <td style='word-wrap:break-word;' width='15%'>".$value['dt']."</td>
              <td style='word-wrap:break-word;'>".$value['cm_comment']."</td>";
              if($this->session->userdata("oq_utype") == 1)
              {
                $table .= "<td style='word-wrap:break-word;'><a class='delete' href='".base_url()."index.php/discussion/delete_comment/".$value['cm_cid']."/".$value['fr_fid']."'>Delete</a></td>";
              }
              
              $table .= "</tr>";
            }
            echo $table .= "</table>";
            ?>
          </table>
        </div>
          
      </form>

      <form class="form-signin" role="form">
        <p class="form-signin-heading style1 style2">Add Comment</p>
        <div id="success" class="alert alert-success"></div>
        <div id="error" class="alert alert-danger"></div>
        <textarea id="comment" type="text" class="form-control" placeholder="Comment Here" required autofocus></textarea>
        <button id="save" class="btn btn-lg btn-primary" >Save</button>
      </form>
 
      </div>
      
      <!-- FOOTER -->
      <footer>
        <!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Online Quiz &middot; Like us on <a href="#">Facebook</a> &middot; or Follow us on <a href="#">Twitter</a></p>
      </footer> 

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/holder.js"></script>
    <script>

      $(function(){

        $("#error, #success").hide();

        var _error = false;

        $("#save").click(function(e){
          if ( $("#comment").val() != '' ){
            e.preventDefault();
            if( _error == false ){
              $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url();?>index.php/discussion/add_comment',
                data: {comment:$("#comment").val(), fid: "<?php echo $x[0]['fr_fid']?>"},
                success: function(msg){
                  if(msg.split("|")[0] == "success"){
                      $("#comment").val('');
                      $("#success").html("Comment saved!").show();
                      setTimeout(function(){$("#success").empty().hide()},3000);
                      $("#dynamic_comment").html(msg.split("|")[1]);
                  }
                }
              });
            }
          }
          
        });

      });

    </script>
  </body>
</html>
