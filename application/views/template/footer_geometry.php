<div id="right-col">
    <h2>GEOMETRY</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/0">Cartesian Coordinate System and Straight Line</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/1">Circle</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/2">Parabola</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/3">Ellipse</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/4">Hyperbola</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/5">Equation of Locus and Polar Coordinates </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/6">Mensuration</a></li>
    </ul>
    <h2>others</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/7">Formulas</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/SolvingProblem1">Practice Problems </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/geometry/9">References</a></li>
    </ul>