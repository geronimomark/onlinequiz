<link href="<?php echo base_url();?>assets/css/ece.css" rel="stylesheet">
          <style type="text/css">
<!--
.style1 {font-family: "Times New Roman"}
.style2 {font-size: 16px}
-->
          </style>
          <div class="navbar-collapse collapse style1">
              <ul class="nav navbar-nav style2">
                <li <?php if($active == "home"):?>class="active"<?php endif;?>><a href="<?php echo base_url();?>index.php/home">Home</a></li>
                <?php if($this->session->userdata("oq_utype") == 1):?>
                <li li <?php if($active == "maintenance"):?>class=" dropdown active"<?php else:?>class="dropdown"<?php endif;?>>
                  <a href="" class="dropdown-toggle" data-toggle="dropdown">Admin Panel </a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>index.php/admin/users">Users</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>index.php/admin/quizzez">Exams</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>index.php/admin/categories">Categories</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>index.php/admin/reports">Reports</a></li>
                  </ul>
                </li>
              <?php endif;?>
                <?php if($this->session->userdata("oq_utype") != 1 && $this->session->userdata("oq_exam") == "1"):?>
                <li <?php if($active == "history"):?>class="active"<?php endif;?>><a href="<?php echo base_url();?>index.php/history">My Exams</a></li>
                <?php endif;?>
                <?php if($this->session->userdata("oq_utype") == 1):?>
                <li <?php if($active == "adx"):?>class="active"<?php endif;?>><a href="<?php echo base_url();?>index.php/admin/exams">My Exams</a></li>
                <li <?php if($active == "logs"):?>class="active"<?php endif;?>><a href="<?php echo base_url();?>index.php/admin/logs">Logs</a></li>
                <?php endif;?>
                <li <?php if($active == "discussion"):?>class="active"<?php endif;?>><a href="<?php echo base_url();?>index.php/discussion">Forum</a></li>
                <?php if($this->session->userdata("oq_lecture") == "1"):?>
                <!-- <li><a href="../../../../MakeitEcE/greener/Algebra/Variables, Equations, and Inequalities.html" target="_self">Lectures</a></li> -->
                <li><a href="<?php echo base_url();?>index.php/lecture/algebra">Lectures</a></li>
                <?php endif;?>
                <?php if($this->session->userdata("oq_activity") == "1"):?>
                <!-- <li><a href="../../../../MakeitEcE/greener/Activities/web/Activities.html" target="_self">Activities</a></li> -->
                  <li <?php if($active == "activity"):?>class="active"<?php endif;?>><a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities</a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url();?>index.php/activity/game">My Activities</a></li>
                      <li class="divider"></li>
                      <li><a href="<?php echo base_url();?>index.php/activity/scores">My Scores</a></li>
                    </ul>
                  </li>
                <?php endif;?>
                <li <?php if($active == "profile"):?>class="active"<?php endif;?>><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <?php echo $this->session->userdata("oq_name");?> </a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>index.php/profile/edit_profile">My Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>index.php/account">Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url();?>index.php/home/logout">Logout</a></li>
                  </ul>
                </li>
            </ul>
          </div>
          