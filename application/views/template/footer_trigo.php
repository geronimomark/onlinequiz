  <div id="right-col">
    <h2>TRIGO </h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/0">Trigonometric Functions </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/1">Trigonometric Identities and Conditional Functions</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/2">Trigonometric Function of any Angle</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/3">Right Triangle </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/4">Inverse Trigonometric Function</a>  </li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/5">Additional Topics</a></li>
    </ul>
    <h2>others</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/6"> Formula</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/SolvingProblem1">Practice Problems </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/trigo/9">References</a></li>
    </ul>