  <div id="right-col">
    <h2>Algebra </h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/0" >Variable, Equations, and Inequalities </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/1">Sets and Operations </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/2" >Operation on Algebraic Expressions</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/3" >Exponents and Radicals </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/4" >Relations and Functions</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/5">Functions and Their Graphs </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/6" >Systems of Linear Equations </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/7" > Quadratic Equation</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/8" >Permutations and Combinations</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/9" >Complex Number</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/10" >Matrices and Determinants</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/11" >Inverse, Exponential, and Logarithmic Functions</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/12" >Sequence, Series, and Limits </a></li>
    </ul>
    <h2>others</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/13" > Formula</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/algebra/15" >Practice Problems</a></li>
    </ul>