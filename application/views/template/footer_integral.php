<div id="right-col">
    <h2>integral</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/0">Indefinite Integral</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/1">Integration Using Partial Fractions </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/2">Area Problem</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/3">Application of Integration </a></li>
    </ul>
    <h2>otherS</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/formula"> Formula</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/SolvingProblem1">Practice Problems</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/integral/4">References</a></li>
    </ul>