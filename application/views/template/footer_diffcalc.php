<div id="right-col">
    <h2>calculus</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/0">Function, Graph, and Limits</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/1">Chain Rule</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/2">Derivative</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/3">Rules of Differentiation</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/4">Approximation by differentiation</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/5">Derivative of Inverse Trigonometric Functions</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/6">Higher order derivative</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/7">Explicit and implicit functions </a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/8">Maxima and minima</a></li>
    </ul>
    <p>&nbsp;</p>
    <h2>others</h2>
    <ul class="side">
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/formula"> Formula</a></li>
      <li><a href="<?php echo base_url();?>index.php/lecture/diffcalc/SolvingProblem1">Practice Problems</a></li>
    </ul>